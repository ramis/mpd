<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\catalog\tag\Group;

/* @var yii\web\View $this */
/* @var $group Group */

$this->title = 'Catalog Tag Group';
?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1><?= ($group->getId() ? $group->getTitle() : 'Новый') ?></h1>
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($group->getId() ? Url::to(['edit', 'id'=>$group->getId()]) : Url::to(['create'])),
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($group, 'title');
        echo '</fieldset>';
        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
