<?php

namespace common\models;

use common\tools\Tools;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\models\behavior\Account as AccountBehavior;
use common\models\catalog\Product;
use common\models\cart\Product as CartProduct;
use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\Id;

/**
 * This is the model class for table "{{%cart}}".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $created_uts
 * @property integer $updated_uts
 *
 * @property Account $account
 * @property CartProduct[] $cartProducts
 */
class Cart extends ActiveRecord
{
    use Id;
    use CreatedUpdatedUts;
    use AccountBehavior;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cart}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id'], 'required'],
            [['id', 'account_id', 'created_uts', 'updated_uts'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account Id'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'updated_uts' => Yii::t('app', 'Updated Uts'),
        ];
    }

    /**
     * @return CartProduct[]
     */
    public function getCartProducts()
    {
        return CartProduct::find()->where(['cart_id' => $this->getId()])->all();
    }

    /**
     * @return int $price
     */
    public function getTotalPrice()
    {
        $price = 0;
        $products = $this->getCartProducts();
        foreach ($products as $product) {
            $price += $product->getCatalogProduct()->getPrice() * $product->getQuantity();
        }

        return $price;
    }

    /**
     * @return int $quantity
     */
    public function getCartProductsQuantity()
    {
        $quantity = 0;
        $products = $this->getCartProducts();
        foreach ($products as $product) {
            $quantity += $product->getQuantity();
        }

        return $quantity;
    }

    /**
     * Количество товара в корзине
     *
     * @param Product $product
     * @return int
     */
    public function getQuantityToProduct(Product $product)
    {
        /* @var CartProduct $cartProduct */
        $cartProduct = CartProduct::find()->where(['cart_id' => $this->getId(), 'catalog_product_id' => $product->getId()])->one();
        return ($cartProduct ? $cartProduct->getQuantity() : 0);
    }

    /**
     * Добавление товара в корзину
     *
     * @param Product $product
     * @param $quantity
     * @return boolean|string
     */
    public function addCartProduct(Product $product, $quantity)
    {
        try {
            if ($product->getQuantity() < $quantity) {
                throw new Exception('Не достаточно товара');
            }

            /**
             * @var CartProduct $cartProduct
             */
            $cartProduct = CartProduct::find()->where(['catalog_product_id' => $product->getId(), 'cart_id' => $this->getId()])->one();

            if ($cartProduct) {
                $quantity += $cartProduct->getQuantity();
                if ($product->getQuantity() < $quantity) {
                    throw new Exception('Не достаточно товара');
                }
                $cartProduct->setQuantity($quantity);
            } else {
                $cartProduct = new CartProduct();
                $cartProduct->setCart($this);
                $cartProduct->setCatalogProduct($product);
                $cartProduct->setQuantity($quantity);
            }

            if($cartProduct->save() === false){
                Tools::getError($cartProduct);
            }
            return true;
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        return $error;
    }

    /**
     * Удаление товара из корзины
     *
     * @param Product $product
     * @param $quantity
     * @return boolean|string
     */
    public function delCartProduct(Product $product, $quantity)
    {
        $error = '';

        try {
            /* @var CartProduct $cartProduct */
            $cartProduct = CartProduct::find()->where(['catalog_product_id' => $product->getId(), 'cart_id' => $this->getId()])->one();

            if ($cartProduct) {
                $quantity = $cartProduct->getQuantity() - $quantity;

                if ($quantity > 0) {
                    $cartProduct->setQuantity($quantity);
                    if($cartProduct->save() === false){
                        Tools::getError($cartProduct);
                    }
                } else {
                    $cartProduct->delete();
                }
                return true;
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        return $error;
    }

    /**
     * Очищение корзины
     */
    public function clear()
    {
        CartProduct::deleteAll(['cart_id' => $this->getId()]);
    }

}
