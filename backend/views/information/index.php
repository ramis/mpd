<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\Information;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">
    <div class="right"><?=Html::a('Добавить', \yii\helpers\Url::to(['create'])); ?></div>

    <h1>Информационные страницы</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'url',
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Information $information) {
                    return Html::a('Редактировать', ['edit', 'id' => $information->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
