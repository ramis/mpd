<?php

namespace common\models\behavior;

trait Direct
{
    /**
     * @return int
     */
    public function getDirectId()
    {
        return $this->direct_id;
    }

    /**
     * @param int $direct_id
     */
    public function setDirectId($direct_id)
    {
        $this->direct_id = $direct_id;
    }

}