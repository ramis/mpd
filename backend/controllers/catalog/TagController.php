<?php
namespace backend\controllers\catalog;

use common\models\catalog\Product;
use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use backend\controllers\AbstractController;
use common\models\catalog\Tag;

/**
 * Catalog Tag controller
 */
class TagController extends AbstractController
{
    public function beforeAction($action)
    {
        $this->appendBreadCrumbs([
            'title' => 'Каталог',
            'url' => '',
        ]);
        $this->appendBreadCrumbs([
            'title' => 'Теги',
            'url' => ['catalog/tag'],
        ]);

        return parent::beforeAction($action);
    }

    /**
     * Tag list
     *
     * @return string
     */
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Tag::find()->orderBy('id asc'),
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Tag create
     *
     * @return string
     */
    public function actionCreate()
    {
        $tag = new Tag();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $tag->load($_POST);
            if ($tag->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/tag']) . '#' . $tag->getId());
            }
            $errors = $tag->getErrors();
        }

        $this->appendBreadCrumbs([
            'title' => 'Новый тег',
            'url' => '',
        ]);

        return $this->render('form', ['tag' => $tag, 'errors' => $errors]);
    }

    /**
     * Tag create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Tag $tag */
        $tag = Tag::findOne((int)$id);

        if ($tag === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['catalog/tag']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $tag->load($_POST);

            if ($tagProductsIds = (array)Yii::$app->request->post('products_ids')) {
                $products = Product::find()->where(['id' => $tagProductsIds])->all();
                $tag->setCatalogProducts($products);
            }

            if ($tag->modelSave()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/tag']) . '#' . $tag->getId());
            }
            $errors = $tag->getErrors();
        }

        $this->appendBreadCrumbs([
            'title' => $tag->getTitle(),
            'url' => '',
        ]);

        return $this->render('form', ['tag' => $tag, 'errors' => $errors]);
    }

    /**
     * Product upload excel
     *
     * @return string
     */
    public function actionUpload()
    {
        $countUpload = -1;

        if (Yii::$app->request->isPost) {
            $tempFile = $_FILES['upload']['tmp_name'];
            $excel_reader = new \PHPExcel_Reader_Excel5();

            $excel = $excel_reader->load($tempFile);
            $excel->setActiveSheetIndex(0);
            $sheet = $excel->getActiveSheet();
            $highest_row_num = $sheet->getHighestRow();
            $countUpload = 0;
            for ($row_num = 1; $row_num <= $highest_row_num; $row_num++) {
                $value = (string)trim($sheet->getCellByColumnAndRow(\PHPExcel_Cell::columnIndexFromString('A') - 1, $row_num)->getValue());

                if ($value === '') {
                    continue;
                }
                $tag = new Tag();
                $tag->setTitle($value);
                $tag->setTag($value);

                $url = self::getInTranslateToEn($value);

                $tag->setUrl($url);
                $tag->setGroupId(9);

                try {
                    if ($tag->save()) {
                        $countUpload++;
                    } else {
                        var_dump($value);
                        var_dump($tag->getFirstErrors());
                    }
                } catch (\Exception $e) {
                    var_dump($value . ' ' . $e->getMessage());
                }
            }
        }

        return $this->render('upload', ['countUpload' => $countUpload]);
    }

    public static function getInTranslateToEn($string)
    {
        $arStrES = array("ае", "уе", "ое", "ые", "ие", "эе", "яе", "юе", "ёе", "ее", "ье", "ъе", "ый", "ий");
        $arStrOS = array("аё", "уё", "оё", "ыё", "иё", "эё", "яё", "юё", "ёё", "её", "ьё", "ъё", "ый", "ий");
        $arStrRS = array("а$", "у$", "о$", "ы$", "и$", "э$", "я$", "ю$", "ё$", "е$", "ь$", "ъ$", "@", "@");

        $replace = array("А" => "A", "а" => "a", "Б" => "B", "б" => "b", "В" => "V", "в" => "v", "Г" => "G", "г" => "g", "Д" => "D", "д" => "d",
            "Е" => "Ye", "е" => "e", "Ё" => "Ye", "ё" => "e", "Ж" => "Zh", "ж" => "zh", "З" => "Z", "з" => "z", "И" => "I", "и" => "i",
            "Й" => "Y", "й" => "y", "К" => "K", "к" => "k", "Л" => "L", "л" => "l", "М" => "M", "м" => "m", "Н" => "N", "н" => "n",
            "О" => "O", "о" => "o", "П" => "P", "п" => "p", "Р" => "R", "р" => "r", "С" => "S", "с" => "s", "Т" => "T", "т" => "t",
            "У" => "U", "у" => "u", "Ф" => "F", "ф" => "f", "Х" => "Kh", "х" => "kh", "Ц" => "Ts", "ц" => "ts", "Ч" => "Ch", "ч" => "ch",
            "Ш" => "Sh", "ш" => "sh", "Щ" => "Shch", "щ" => "shch", "Ъ" => "", "ъ" => "", "Ы" => "Y", "ы" => "y", "Ь" => "", "ь" => "",
            "Э" => "E", "э" => "e", "Ю" => "Yu", "ю" => "yu", "Я" => "Ya", "я" => "ya", "@" => "y", "$" => "ye");

        $string = str_replace($arStrES, $arStrRS, $string);
        $string = str_replace($arStrOS, $arStrRS, $string);

        return iconv("UTF-8", "UTF-8//IGNORE", strtr($string, $replace));
    }
}
