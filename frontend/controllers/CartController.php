<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Order;
use common\models\Cart;
use common\models\catalog\Product;
use common\models\order\Shipping;
use frontend\models\AccountSingleton;

/**
 * Site controller
 */
class CartController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @TODO Переделать Разнести на разные методы.
     *
     * Показ корзины и формы оформление заказа
     * Оформление заказа
     * Показ данных оформленного заказа
     *
     * @return string
     */
    public function actionIndex()
    {
        $error = '';
        $order = new Order();

        if (Yii::$app->request->getIsPost()) {

            $value = $_POST;
            $params['geo_id'] = (int)Yii::$app->request->cookies->getValue('geo_id');
            $account = AccountSingleton::getInstance()->getAccount();
            $cart = $account->getCart();
            $products = $cart->getCartProducts();
            if(count($products) > 0){
                foreach ($products as $product) {
                    $catalogProduct = $product->getCatalogProduct();
                    $value['Order']['products'][] = [
                        'id' => $catalogProduct->getId(),
                        'quantity' => $product->getQuantity(),
                        'price' => $catalogProduct->getPrice(),
                    ];
                }

                $result = $order->saveOrder($value, $params);
                if ($result !== true) {
                    unset($order);
                    $order = new Order();
                    $error = !empty($result['error']) ? $result['error'] : '';
                } else {
                    $cart->clear();
                }
            }
        }

        return $this->render('index', ['order' => $order, 'error' => $error]);
    }

}
