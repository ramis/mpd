<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=playdoh',
            'username' => 'postgres',
            'password' => 'postgres',
            'charset' => 'utf8',
            'tablePrefix' => 'pd_',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'sale@mrplaydoh.ru',
                'password' => '294683e0d173',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
    ],
];