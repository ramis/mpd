<?php
namespace backend\controllers\catalog;

use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use backend\controllers\AbstractController;
use common\models\catalog\Attribute;

/**
 * Catalog Attribute controller
 */
class AttributeController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Attribute::find()->orderBy('sort asc'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    /**
     * Attribute list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Attribute create
     *
     * @return string
     */
    public function actionCreate()
    {
        $attribute = new Attribute();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $attribute->load($_POST);

            if ($attribute->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/attribute']));
            }
            $errors = $attribute->getErrors();
        }
        return $this->render('form', ['attribute' => $attribute, 'errors' => $errors]);
    }

    /**
     * Attribute create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Attribute $attribute */
        $attribute = Attribute::findOne((int)$id);

        if ($attribute === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['catalog/attribute']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $attribute->load($_POST);

            if ($attribute->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/attribute']));
            }
            $errors = $attribute->getErrors();
        }

        return $this->render('form', ['attribute' => $attribute, 'errors' => $errors]);
    }

}
