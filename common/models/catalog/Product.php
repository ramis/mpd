<?php
namespace common\models\catalog;

use common\models\ints\BreadCrumbsInts;
use Yii;
use common\models\AbstractModel;
use common\models\Message;
use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\cart\Product as CartProduct;
use common\models\order\Product as OrderProduct;
use common\models\catalog\Tag;
use common\models\catalog\product\Tag as ProductTag;
use common\models\catalog\product\Review;
use common\models\catalog\product\Subscription;
use common\models\catalog\product\AttributeValue;
use common\models\catalog\product\Category as ProductCategory;
use common\models\behavior\Description;
use common\models\behavior\Published;
use common\models\behavior\Title2;
use common\models\behavior\seo\Description as SeoDescription;
use common\models\behavior\seo\Title as SeoTitle;
use common\models\behavior\seo\Keywords as SeoKeywords;
use common\models\behavior\seo\Text as SeoText;
use common\models\behavior\Media;
use common\models\media\ints\MediaInts;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%catalog_product}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $title2
 * @property string $article
 * @property integer $barcode
 * @property integer $code
 * @property integer $quantity
 * @property string $description
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property boolean $is_published
 * @property integer $created_uts
 * @property integer $updated_uts
 * @property integer $dt_review_rating
 * @property integer $dt_review_count
 * @property integer $price
 * @property integer sale_price
 * @property integer old_price
 * @property integer v3t_price
 * @property integer buy_price
 * @property integer base_price
 * @property integer mrc_price
 * @property integer persent_price
 * @property integer manual_price
 * @property string $base_keywords
 * @property boolean $d_is_stock
 * @property integer $catalog_category_id
 * @property string $title_direct
 *
 * @property CartProduct[] $cartProducts
 * @property AttributeValue[] $catalogProductAttributeValues
 * @property Attribute[] $catalogProductAttributes
 * @property ProductCategory[] $catalogProductCategories
 * @property Category[] $catalogCategories
 * @property Review[] $catalogProductReviews
 * @property Subscription[] $catalogProductSubscriptions
 * @property Message[] $messages
 * @property OrderProduct[] $orderProducts
 */
class Product extends AbstractModel implements MediaInts, BreadCrumbsInts
{
    use Id;
    use Title;
    use Title2;
    use Description;
    use CreatedUpdatedUts;
    use SeoDescription;
    use SeoTitle;
    use SeoText;
    use SeoKeywords;
    use Published;
    use Media;

    /**
     * @var Category[] $categories
     */
    private $categories;

    /**
     * @var Tag[] $tags
     */
    private $tags;

    /**
     * @var AttributeValue[] $values
     */
    private $values;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article', 'code', 'is_published', 'catalog_category_id'], 'required'],
            [
                [
                    'id',
                    'barcode',
                    'code',
                    'quantity',
                    'created_uts',
                    'updated_uts',
                    'dt_review_rating',
                    'dt_review_count',
                    'catalog_category_id',
                    'price'

                ],
                'integer'
            ],
            [['description', 'base_keywords'], 'string'],
            [['is_published', 'd_is_stock'], 'boolean'],
            [['title', 'title2', 'article', 'seo_title', 'seo_keywords', 'seo_description', 'title_direct'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title2' => Yii::t('app', 'Title2'),
            'article' => Yii::t('app', 'Article'),
            'barcode' => Yii::t('app', 'Barcode'),
            'code' => Yii::t('app', 'Code'),
            'quantity' => Yii::t('app', 'Quantity'),
            'description' => Yii::t('app', 'Description'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'is_published' => Yii::t('app', 'Published'),
            'created_uts' => Yii::t('app', 'Created'),
            'updated_uts' => Yii::t('app', 'Updated'),
            'dt_review_rating' => Yii::t('app', 'Review Rating'),
            'dt_review_count' => Yii::t('app', 'Review Count'),
            'product_categories' => Yii::t('app', 'Product Categories'),
            'product_tags' => Yii::t('app', 'Product Tags'),
            'catalog_category_id' => Yii::t('app', 'Product Category'),
            'title_direct' => Yii::t('app', 'Title Direct'),
        ];
    }

    /**
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param string $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getBasekeywords()
    {
        return $this->base_keywords;
    }

    /**
     * @param string $base_keywords
     */
    public function setBasekeywords($base_keywords)
    {
        $this->base_keywords = $base_keywords;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return ($this->title ? $this->title : $this->title2);
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return array
     */
    public function getBreadCrumbs()
    {
        $category = $this->getMainCategory();
        $breadCrumbs = [];
        if ($category) {
            $breadCrumbs[] = Html::a($category->getTitle(), ['route/index', 'url' => $category->getUrl()]);
        }
        $breadCrumbs[] = $this->getTitle();

        return $breadCrumbs;
    }

    /**
     * @return Product[]
     */
    public function getSimilarProducts()
    {
        /** @TODO Переделать на поиск по катекориям */
        return Product::find()
            ->where(['is_published' => true])
            ->where(['catalog_category_id' => $this->getMainCategory()->getId()])
            ->andWhere(['>', 'quantity', 0])
            ->andWhere(['>', 'price', 0])
            ->andWhere(['<>', 'id', $this->id])
            ->limit(5)
            ->all();
    }


    /**
     * @return Category[]
     */
    public function getCatalogProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['catalog_product_id' => 'id'])->orderBy('sort asc')->all();
    }

    /**
     * @return Category
     */
    public function getMainCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'catalog_category_id'])->one();
    }

    /**
     * @param $catalogCategoryId
     * @return $this
     */
    public function setMainCategory($catalogCategoryId)
    {
        $this->catalog_category_id = $catalogCategoryId;
        return $this;
    }

    /**
     * @return Category[] $categories
     */
    public function getCategories()
    {
        if ($this->categories === null) {
            $this->categories = $this->hasMany(Category::className(),
                ['id' => 'catalog_category_id'])->viaTable('{{%catalog_product_category}}',
                ['catalog_product_id' => 'id'])->all();
        }

        return $this->categories;
    }

    /**
     * @return Tag[] $tags
     */
    public function getTags()
    {
        if ($this->tags === null) {
            $this->tags = $this->hasMany(Tag::className(),
                ['id' => 'catalog_tag_id'])->viaTable('{{%catalog_product_tag}}',
                ['catalog_product_id' => 'id'])->all();
        }

        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return AttributeValue[] $values
     */
    public function getAttributeValues()
    {
        if ($this->values === null) {
            $this->values = $this->hasMany(AttributeValue::className(), ['catalog_product_id' => 'id'])->all();
        }

        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setAttributeValues(array $values)
    {
        $this->values = $values;
    }


    /**
     * @param bool|true $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function modelSave($runValidation = true, $attributeNames = null)
    {

        if (parent::save()) {

            //Save category
            ProductCategory::deleteAll('catalog_product_id = :catalog_product_id', [':catalog_product_id' => $this->getId()]);
            $values = [];
            foreach ($this->getCategories() as $category) {
                $values[] = [$this->getId(), $category->getId()];
            }

            if (!empty($values)) {
                self::getDb()->createCommand()->batchInsert(ProductCategory::tableName(), ['catalog_product_id', 'catalog_category_id'], $values)->execute();
            }

            //Save tag
            ProductTag::deleteAll('catalog_product_id = :catalog_product_id', [':catalog_product_id' => $this->getId()]);
            $values = [];
            foreach ($this->getTags() as $tag) {
                $values[] = [$this->getId(), $tag->getId()];
            }

            if (!empty($values)) {
                self::getDb()->createCommand()->batchInsert(ProductTag::tableName(), ['catalog_product_id', 'catalog_tag_id'], $values)->execute();
            }

            //Save attribute
            AttributeValue::deleteAll('catalog_product_id = :catalog_product_id', [':catalog_product_id' => $this->getId()]);
            foreach ($this->getAttributeValues() as $value) {
                $value->setCatalogProduct($this);
                $value->save();
            }

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        if ($this->getSeoTitle()) {
            $title = $this->getSeoTitle();
        } else {
            $title = $this->getTitle() . ' купить ' . $this->getTitle2() . ' в интернет магазине ' . Yii::$app->params['DOMAIN'];
        }

        return $title;
    }


    /************ Цены ************
     *
     * price         - обычная цена на сайте
     * sale price    - скидка
     * old price     - перечеркнутая цена на сайте
     * v3t price     - цена v3toys
     * buy price     - закупочная цена
     * base price    - базовая цена
     * mrc price     - мрц цена
     * persent_price - % авто скидки от цены v3toys
     * manual price  - цена проставленная в ручную
     */

    public function calcPrice()
    {
        if ($this->getManualPrice() > 0) {
            $price = (int)$this->getManualPrice();
        } elseif ($this->getMrcPrice() > 0) {
            $price = (int)$this->getMrcPrice();
        } else {
            if ($this->getPersentPrice() > 0) {
                $price = (int)round((((100 - $this->getPersentPrice()) / 100) * $this->getV3tPrice()), -1);
            } else {
                $price = (int)round($this->getV3tPrice(), -1);
            }
        }

        if ($this->getSalePrice() > 0 && $price > 0) {
            $this->setSalePrice($price);
        }

        $this->setPrice($price);
        $this->save();
    }

    /**
     * @return bool
     */
    public function isSale()
    {
        return ($this->getSalePrice() > 0);
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getSalePrice()
    {
        return $this->sale_price;
    }

    /**
     * @param int $salePrice
     */
    public function setSalePrice($salePrice)
    {
        $this->sale_price = $salePrice;
    }

    /**
     * @return int
     */
    public function getOldPrice()
    {
        return $this->old_price;
    }

    /**
     * @param int $oldPrice
     */
    public function setOldPrice($oldPrice)
    {
        $this->old_price = $oldPrice;
    }

    /**
     * @return int
     */
    public function getV3tPrice()
    {
        return $this->v3t_price;
    }

    /**
     * @param int $v3tPrice
     */
    public function setV3tPrice($v3tPrice)
    {
        $this->v3t_price = $v3tPrice;
    }

    /**
     * @return int
     */
    public function getBuyPrice()
    {
        return $this->buy_price;
    }

    /**
     * @param int $buyPrice
     */
    public function setBuyPrice($buyPrice)
    {
        $this->buy_price = $buyPrice;
    }

    /**
     * @return int
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * @param int $basePrice
     */
    public function setBasePrice($basePrice)
    {
        $this->base_price = $basePrice;
    }

    /**
     * @return int
     */
    public function getMrcPrice()
    {
        return $this->mrc_price;
    }

    /**
     * @param int $mrcPrice
     */
    public function setMrcPrice($mrcPrice)
    {
        $this->mrc_price = $mrcPrice;
    }

    /**
     * @return int
     */
    public function getPersentPrice()
    {
        return $this->persent_price;
    }

    /**
     * @param int $persentPrice
     */
    public function setPersentPrice($persentPrice)
    {
        $this->persent_price = $persentPrice;
    }

    /**
     * @return int
     */
    public function getManualPrice()
    {
        return $this->manual_price;
    }

    /**
     * @param int $manualPrice
     */
    public function setManualPrice($manualPrice)
    {
        $this->manual_price = $manualPrice;
    }

    /**
     * @return bool
     */
    public function isStock()
    {
        return $this->d_is_stock;
    }

    /**
     * @param bool $isStock
     */
    public function setStock($isStock)
    {
        $this->d_is_stock = $isStock;
    }

    /******************/

    /**
     * @param mixed $condition
     * @return null|Product
     */
    public static function findOne($condition)
    {
        return parent::findOne($condition);
    }

    /**
     * @return string
     */
    public function getTitleDirect()
    {
        return $this->title_direct;
    }

    /**
     * @param string $titleDirect
     */
    public function setTitleDirect($titleDirect)
    {
        $this->title_direct = $titleDirect;
    }

    public static function getProductBuy()
    {
        return Product::find()
            ->select('p.*, count(p.id) as cnt')
            ->from(Product::tableName() . ' p')
            ->innerJoin(OrderProduct::tableName() . ' op', "op.catalog_product_id = p.id")
            ->where(['is_published' => true])
            ->andWhere(['>', 'p.price', 0])
            ->andWhere(['>', 'p.quantity', 0])
            ->groupBy('p.id')
            ->orderBy('cnt desc')
            ->limit(6)
            ->all();
    }

    /**
     * @param string $search
     * @return array
     */
    public static function searchSphinx($search)
    {
        $ids = [];
        $words = explode(' ', $search);
        if (count($words) == 1) {
            if ((mb_stripos($search, 'k', 0, 'UTF-8') === 0) OR (mb_stripos($search, 'к', 0, 'UTF-8') === 0)) {
                $v3toysId = intval(preg_replace('/\D/', '', $search));
                $product = Product::find()->where(['code' => $v3toysId])->one();
                if ($product) {
                    return [$words, $ids, $product];
                }
            }
        }

        $search = [];
        foreach ($words as $w) {
            $search[] = '*' . trim($w, '*') . '*';
        }
        $search = implode(' ', $search);

        $cl = new \SphinxClient();

        $ranker = SPH_RANK_BM25;
        $mode = SPH_MATCH_ALL;
        $host = "localhost";
        $port = 9312;

        $cl->SetServer($host, $port);
        $cl->SetConnectTimeout(1);
        $cl->SetArrayResult(true);
        $cl->SetFieldWeights(['barcode' => 10, 'article' => 5, 'title' => 3, 'title2' => 2, 'id' => 1, 'description' => 1]);
        $cl->SetMatchMode($mode);
        $cl->SetLimits(0, 100);

        $cl->SetRankingMode($ranker);
        $res = $cl->Query($search, 'catalogProductIndex');

        if ($res !== false && !$cl->GetLastWarning()) {
            $ids = [];
            if (!empty($res['matches'])) {
                foreach ($res['matches'] as $matches) {
                    $ids[] = (int)$matches['id'];
                }
            }
        }

        return [$words, $ids, null];

    }


    /**
     * @return Review[]
     */
    public function getReviews()
    {
        return Review::find()->where(['catalog_product_id' => $this->getId(), 'state' => Review::STATE_PUBLISHED])->orderBy(['created_uts' => SORT_DESC])->all();
    }

    /**
     * @return int
     */
    public function getReviewCount()
    {
        return $this->dt_review_count;
    }

    /**
     * @param $review_count
     * @return $this
     */
    public function setReviewCount($review_count)
    {
        $this->dt_review_count = $review_count;
        return $this;
    }

    /**
     * @return int
     */
    public function getReviewRating()
    {
        return $this->dt_review_rating;
    }

    /**
     * @param $review_rating
     * @return $this
     */
    public function setReviewRating($review_rating)
    {
        $this->dt_review_rating = $review_rating;
        return $this;
    }
}
