<?php

use yii\db\Schema;
use yii\db\Migration;

class m150827_212433_catalog_product_add_d_is_stock extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%catalog_product}}', 'd_is_stock', Schema::TYPE_BOOLEAN);

        $this->execute('update {{%catalog_product}} set d_is_stock = (quantity>0)');

//        $this->alterColumn('{{%catalog_product}}', 'd_is_stock', Schema::TYPE_BOOLEAN.' NOT NULL');
    }

    public function safeDown()
    {
    }

}
