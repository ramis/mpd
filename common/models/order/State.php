<?php

namespace common\models\order;

use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\Order;

/**
 * This is the model class for table "{{%order_state}}".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Order[] $orders
 */
class State extends ActiveRecord
{
    use Id;
    use Title;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_state}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return Order[]
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['state_id' => 'id']);
    }

}
