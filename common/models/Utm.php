<?php

namespace common\models;

use common\models\behavior\Id;
use common\models\behavior\Order as BehaviorOrder;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%utm}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_campaign
 * @property string $utm_content
 * @property string $utm_term
 * @property integer $created_uts
 * @property string $utm_order_id
 */
class Utm extends ActiveRecord
{
    Use Id;
    Use BehaviorOrder;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%utm}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'created_uts', 'utm_order_id'], 'integer'],
            [['utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order Id'),
            'utm_source' => Yii::t('app', 'Utm Source'),
            'utm_medium' => Yii::t('app', 'Utm Medium'),
            'utm_campaign' => Yii::t('app', 'Utm Campaign'),
            'utm_content' => Yii::t('app', 'Utm Content'),
            'utm_term' => Yii::t('app', 'Utm Term'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'utm_order_id' => Yii::t('app', 'Utm Order Id'),
        ];
    }

    /**
     * @return int
     */
    public function getCreatedUts()
    {
        return $this->created_uts;
    }

    /**
     * @param int $created_uts
     */
    public function setCreatedUts($created_uts)
    {
        $this->created_uts = $created_uts;
    }

    /**
     * @return string
     */
    public function getUtmSource()
    {
        return $this->utm_source;
    }

    /**
     * @return string $utm_source
     */
    public function setUtmSource($utmSource)
    {
        $this->utm_source = $utmSource;
    }

    /**
     * @return string
     */
    public function getUtmMedium()
    {
        return $this->utm_medium;
    }

    /**
     * @return string $utm_medium
     */
    public function setUtmMedium($utmMedium)
    {
        $this->utm_medium = $utmMedium;
    }

    /**
     * @return string
     */
    public function getUtmCampaign()
    {
        return $this->utm_campaign;
    }

    /**
     * @return string $utm_campaign
     */
    public function setUtmCampaign($utmCampaign)
    {
        $this->utm_campaign = $utmCampaign;
    }

    /**
     * @return string
     */
    public function getUtmContent()
    {
        return $this->utm_content;
    }

    /**
     * @return string $utm_content
     */
    public function setUtmContent($utmContent)
    {
        $this->utm_content = $utmContent;
    }

    /**
     * @return string
     */
    public function getUtmTerm()
    {
        return $this->utm_term;
    }

    /**
     * @return string $utm_term
     */
    public function setUtmTerm($utmTerm)
    {
        $this->utm_term = $utmTerm;
    }

    /**
     * @return string
     */
    public function getUtmOrderId()
    {
        return $this->utm_order_id;
    }

    /**
     * @return string $utm_OrderId
     */
    public function setUtmOrderId($utmOrderId)
    {
        $this->utm_order_id = $utmOrderId;
    }


}
