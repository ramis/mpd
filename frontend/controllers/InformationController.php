<?php
namespace frontend\controllers;

use common\models\Information;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Site controller
 */
class InformationController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @param $id
     * @return string
     */
    public function actionIndex($id)
    {
        /**
         * @var Information $information
         */
        $information = Information::findOne((int)$id);
        if ($information === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['error']));
        }

        if($information->getUrl() === 'shipping'){
            return $this->render('shipping', ['information' => $information]);
        }

        return $this->render('index', ['information' => $information]);
    }

}
