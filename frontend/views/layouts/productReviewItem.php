<?php
use common\models\catalog\product\Review;

/**
 * @var Review $review
 */
if (!($review instanceof Review)) {
    return;
}

$reviewHtml = '<div class="review" itemtype="http://schema.org/Review" itemscope="" itemprop="review" data-review-id="'.$review->getId().'">
    <div class="stars" itemtype="http://schema.org/Rating" itemscope="" itemprop="reviewRating" date-rate="'.$review->getRating().'">';

$selected = $review->getRating() === 5 ? 'checked' : '';
$reviewHtml .= '<label for="star5" class="' . $selected . '"></label>';
$selected = $review->getRating() === 4 ? 'checked' : $selected;
$reviewHtml .= '<label for="star4" class="' . $selected . '"></label>';
$selected = $review->getRating() === 3 ? 'checked' : $selected;
$reviewHtml .= '<label for="star3" class="' . $selected . '"></label>';
$selected = $review->getRating() === 2 ? 'checked' : $selected;
$reviewHtml .= '<label for="star2" class="' . $selected . '"></label>';
$selected = $review->getRating() === 1 ? 'checked' : $selected;
$reviewHtml .= '<label for="star1" class="' . $selected . '"></label>';
$name = $review->getName() === '' ? 'Анонимно' : $review->getName();
$reviewDescription = preg_replace("/\n/", "</p><p>", $review->getReview());

$reviewHtml .= '</div>
    <div class="review_content" itemprop="description">
        <p> ' . $reviewDescription . '</p>
    </div>
    <div class="review_author">
        <meta content="' . Yii::$app->getFormatter()->asDate($review->getCreatedUts(), 'yyyy-MM-dd') . 'T'.Yii::$app->getFormatter()->asDate($review->getCreatedUts(), 'HH:mm:ss').'" itemprop="datePublished">
        <span class="review_fio" itemprop="author">' . $name . '</span><span class="review_date">, ' . Yii::$app->getFormatter()->asDate($review->getCreatedUts(), 'dd.MM.yyyy г.') . '</span>
    </div>
</div>';

echo $reviewHtml;