<?php

namespace common\models\behavior;

trait Published
{
    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->is_published;
    }

    /**
     * @param boolean $is_published
     */
    public function setPublished($is_published)
    {
        $this->is_published = $is_published;
    }

}