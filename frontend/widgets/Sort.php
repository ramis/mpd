<?php

namespace frontend\widgets;

use yii\bootstrap\Widget;

class Sort extends Widget
{

    /**
     * Тип сортировки (по возрастанию или по убыванию)
     * @var string
     */
    public $order;

    /**
     * Сортировка
     * @var string
     */
    public $sort;

    /**
     * Кол страниц
     * @var int
     */
    public $limit;

    /**
     * Url
     * @var string
     */
    public $url;

    /**
     * Renders the widget.
     */
    public function run()
    {
        return $this->render('sort', ['order' => $this->order, 'sort' => $this->sort, 'limit' => $this->limit, 'url' => $this->url]);
    }

}
