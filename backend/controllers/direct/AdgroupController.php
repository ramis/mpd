<?php
namespace backend\controllers\direct;

use backend\models\direct\Adgroup;
use Yii;
use backend\controllers\AbstractController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use common\models\catalog\Product;
use common\models\catalog\Tag;

class AdgroupController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Adgroup::find(),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);
    }

    /**
     * Product list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Product price list
     *
     * @return string
     */
    public function actionPrice()
    {
        return $this->render('price', ['dataProvider' => $this->getDataProvider()]);
    }

    public function actionSave($id)
    {
        /**
         * @var Product $product
         */
        $product = Product::findOne((int)$id);

        if ($product === null) {
            return json_encode([]);
        }

        $product->setSalePrice((int)Yii::$app->request->get('sale_price'));
        $product->setOldPrice((int)Yii::$app->request->get('old_price'));
        $product->setPersentPrice((int)Yii::$app->request->get('persent_price'));
        $product->setManualPrice((int)Yii::$app->request->get('manual_price'));

        $product->save();
        $product->calcPrice();

        return json_encode(['price' => $product->getPrice(), 'sale_price' => $product->getSalePrice()]);
    }

    /**
     * Product create
     *
     * @return string
     */
    public function actionCreate()
    {
        $product = new Product();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            if ($this->fillModel($product)) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/product']) . '#' . $product->getId());
            }
            $errors = $product->getErrors();
        }

        return $this->render('form', ['product' => $product, 'errors' => $errors]);
    }

    /**
     * Product create
     *
     * @var int $id
     * @return string
     */
    public function actionEdit($id)
    {
        /**
         * @var Product $product
         */
        $product = Product::findOne((int)$id);

        if ($product === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['catalog/product']));
        }
        $errors = [];

        if (Yii::$app->request->getIsPost()) {
            if ($this->fillModel($product)) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/product']) . '#' . $product->getId());
            }
            $errors = $product->getErrors();
        }

        return $this->render('form', ['product' => $product, 'errors' => $errors]);
    }

    private function fillModel(Product $product)
    {
        $product->setQuantity(0);
        $product->load($_POST);

        //Category
        if ($productCategoriesId = (array)Yii::$app->request->post('product_categories_id')) {
            $categories = Category::find()->where(['id' => $productCategoriesId])->all();
            $product->setCategories($categories);
        }
        //Tag
        if ($productTagsId = (array)Yii::$app->request->post('product_tags_id')) {
            $tags = Tag::find()->where(['id' => $productTagsId])->all();
            $product->setTags($tags);

        }
        //Attribute
        if ($values = (array)Yii::$app->request->post('values')) {

            $productAttributeValue = [];

            foreach ($values as $key => $val) {
                if ($val !== '') {
                    $attribute = Attribute::findOne($key);

                    if ($attribute) {
                        $value = new AttributeValue();
                        $value->setCatalogAttribute($attribute);
                        $value->setValue($val);
                        $productAttributeValue[] = $value;
                    }

                }
            }
            $product->setAttributeValues($productAttributeValue);
        }

        if ($product->modelSave()) {
            if ($product instanceof MediaInts) {
                $media = $product->getMedia();
                $media->saveContainer($product);
                return true;
            }
        }
        return false;
    }

    /**
     * Product upload excel
     *
     * @return string
     */
    public function actionUpload()
    {
        $countUpload = -1;

        if (Yii::$app->request->isPost) {
            $tempFile = $_FILES['upload']['tmp_name'];
            $excel_reader = new \PHPExcel_Reader_Excel5();

            $excel = $excel_reader->load($tempFile);
            $excel->setActiveSheetIndex(0);
            $sheet = $excel->getActiveSheet();
            $highest_row_num = $sheet->getHighestRow();
            $countUpload = 0;
            for ($row_num = 2; $row_num <= $highest_row_num; $row_num++) {
                $id = (int)trim($sheet->getCellByColumnAndRow(\PHPExcel_Cell::columnIndexFromString('A') - 1, $row_num)->getValue());

                $product = Product::find()->where(['code' => $id])->one();
                if($product === null){
                    $title = trim($sheet->getCellByColumnAndRow(\PHPExcel_Cell::columnIndexFromString('B') - 1, $row_num)->getValue());
                    $article = trim($sheet->getCellByColumnAndRow(\PHPExcel_Cell::columnIndexFromString('G') - 1, $row_num)->getValue());

                    $product = new Product();
                    $product->setCode($id);
                    $product->setTitle('');
                    $product->setTitle2($title);
                    $product->setArticle($article);
                    $product->setPublished(false);
                    $product->setQuantity(0);
                    $product->setPrice(0);
                    try{
                        if($product->save()){
                            $countUpload++;
                        }else{
                            var_dump($product->getFirstErrors());die;
                        }
                    }catch (\Exception $e){
                        var_dump($e->getMessage());
                    }
                }
            }
        }

        return $this->render('upload', ['countUpload' => $countUpload]);
    }

}
