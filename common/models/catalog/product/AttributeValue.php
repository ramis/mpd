<?php

namespace common\models\catalog\product;

use Yii;
use yii\db\ActiveRecord;
use common\models\catalog\Product as CatalogProduct;
use common\models\catalog\Attribute as CatalogAttribute;

/**
 * This is the model class for table "{{%catalog_product_attribute_value}}".
 *
 * @property integer $catalog_product_id
 * @property integer $catalog_attribute_id
 * @property string $value
 *
 * @property CatalogProduct $product
 * @property CatalogAttribute $attributes
 */
class AttributeValue extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_attribute_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_product_id', 'catalog_attribute_id', 'value'], 'required'],
            [['catalog_product_id', 'catalog_attribute_id'], 'integer'],
            [['value'], 'string'],
            [
                ['catalog_product_id', 'catalog_attribute_id'],
                'unique',
                'targetAttribute' => ['catalog_product_id', 'catalog_attribute_id'],
                'message' => 'The combination of Catalog Product ID and Catalog Product Attribute ID has already been taken.'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'catalog_product_id' => Yii::t('app', 'Catalog Product ID'),
            'catalog_attribute_id' => Yii::t('app', 'Catalog Product Attribute ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return CatalogAttribute
     */
    public function getCatalogAttribute()
    {
        return $this->hasOne(CatalogAttribute::className(), ['id' => 'catalog_attribute_id'])->one();
    }

    /**
     * @param CatalogAttribute $attribute
     */
    public function setCatalogAttribute(CatalogAttribute $attribute)
    {
        $this->catalog_attribute_id = $attribute->getId();
    }

    /**
     * @param CatalogProduct $product
     */
    public function setCatalogProduct(CatalogProduct $product)
    {
        $this->catalog_product_id = $product->getId();
    }

    /**
     * @return int
     */
    public function getCatalogProductId()
    {
        return $this->catalog_product_id;
    }

    /**
     * @return int
     */
    public function getCatalogProduct()
    {
        return CatalogProduct::findOne($this->catalog_product_id);
    }

    /**
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
