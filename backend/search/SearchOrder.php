<?php

namespace backend\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * SearchOrder represents the model behind the search form about `common\models\Order`.
 */
class SearchOrder extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['created_uts', 'updated_uts'], 'string'],
//            [['title', 'url', 'description', 'annotation', 'seo_title', 'seo_description', 'seo_keywords'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
//            'created_uts' => $this->created_uts,
//            'updated_uts' => $this->updated_uts,
        ]);


        if(!empty($this->created_uts)) {
            $createdDate = explode(' - ', $this->created_uts);
            if(!empty($createdDate[0])) {
                $query->andFilterWhere(['>', 'created_uts', Yii::$app->formatter->asTimestamp($createdDate[0] . ' 00:00:00')]);
            }
            if(!empty($createdDate[1])) {
                $query->andFilterWhere(['<', 'created_uts', Yii::$app->formatter->asTimestamp($createdDate[1] .  ' 23:59:59')]);
            }

        }

//        $query->andFilterWhere(['like', 'title', $this->title])
//            ->andFilterWhere(['like', 'url', $this->url])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'annotation', $this->annotation])
//            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
//            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
//            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords]);

        $query->orderBy('id desc');
        return $dataProvider;
    }
}
