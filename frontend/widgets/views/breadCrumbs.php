<?php
    use yii\helpers\Html;
?>

<div class="breadcrumbs">
    <ul>
        <li>
            <?= Html::a('Главная', Yii::$app->homeUrl);?> /
        </li>
        <?php
            foreach($breadCrumbs->getBreadCrumbs() as $bread) {
                echo '<li>' . $bread . '</li> / ';
            }
        ?>
    </ul>
</div>