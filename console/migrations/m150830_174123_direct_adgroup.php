<?php

use yii\db\Schema;
use yii\db\Migration;

class m150830_174123_direct_adgroup extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%direct_adgroup}}', [
            'id' => Schema::TYPE_PK,
            'direct_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'campaign_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'region_ids' => Schema::TYPE_TEXT . ' NOT NULL',
            'negative_keywords' => Schema::TYPE_TEXT . ' NOT NULL',

            'is_active' => Schema::TYPE_BOOLEAN . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_direct_adgroup_campaign_id', '{{%direct_adgroup}}', 'campaign_id', '{{%direct_campaign}}', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function safeDown()
    {

    }
}
