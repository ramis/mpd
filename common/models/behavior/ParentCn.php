<?php

namespace common\models\behavior;

trait ParentCn
{

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return int
     */
    public function getParentCn()
    {
        return $this->parent_cn;
    }

    /**
     * @param string $parent_cn
     */
    public function setParentCn($parent_cn)
    {
        $this->parent_cn = $parent_cn;
    }
}