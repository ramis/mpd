<?php

$domain = 'mrplaydoh.ru';

return [
    'DOMAIN' => $domain,
    'HTTP_SITE' => 'http://' . $domain . '/',
    'HTTP_ADMIN' => 'http://a.' . $domain . '/',

    'HTTP_IMAGE' => 'http://'.$domain.'/images/',

    'HTTP_IMAGE_C_210_190' => 'http://i.'.$domain.'/c/210x190/',

    'HTTP_IMAGE_P_210_190' => 'http://i.'.$domain.'/p/210x190/',
    'HTTP_IMAGE_P_100_100' => 'http://i.'.$domain.'/p/100x100/',

    'HTTP_IMAGE_P_200_200' => 'http://i.'.$domain.'/p/200x200/',
    'HTTP_IMAGE_P_500_500' => 'http://i.'.$domain.'/p/500x500/',
    'HTTP_IMAGE_P_430_430' => 'http://i.'.$domain.'/p/430x430/',
    'HTTP_IMAGE_P_120_120' => 'http://i.'.$domain.'/p/120x120/',
    'HTTP_IMAGE_P_50_50'   => 'http://i.'.$domain.'/p/50x50/',
    'HTTP_IMAGE_P_150_150' => 'http://i.'.$domain.'/p/150x150/',
    'HTTP_IMAGE_P_420_360' => 'http://i.'.$domain.'/p/420x360/',
];