<?php
namespace backend\controllers\direct;

use backend\models\direct\Sitelinksset;
use Yii;
use backend\controllers\AbstractController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class SitelinkssetController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Sitelinksset::find(),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);
    }

    /**
     * Sitelinksset list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

}
