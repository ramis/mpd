<?php
namespace console\controllers;

use common\models\catalog\Tag;
use common\models\Yml;
use Yii;
use common\models\catalog\Product;
use common\models\catalog\Category;
use common\models\Information;

class XmlController extends AbstractController
{

    /**
     * @param \DOMElement $urlset
     * @param $loc
     * @param $lastmod
     * @param $changefreq
     * @param $priority
     * @return \DOMNode
     */
    private static function appChild(\DOMElement $urlset, $loc, $lastmod, $changefreq, $priority)
    {
        $element = $urlset->appendChild(new \DOMElement('url'));
        $element->appendChild(new \DOMElement('loc', $loc));
        $element->appendChild(new \DOMElement('lastmod', $lastmod));
        $element->appendChild(new \DOMElement('changefreq', $changefreq));
        $element->appendChild(new \DOMElement('priority', $priority));

        return $element;
    }

    /**
     * Генерация Sitemaps
     */
    public function actionSitemap()
    {
        $file = Yii::$app->basePath . '/../frontend/web/sitemap.xml';

        if (file_exists($file)) {
            unlink($file);
        }

        $dom = new \DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        $urlSet = $dom->appendChild(new \DOMElement('urlset'));
        $urlSet->setAttributeNode(new \DOMAttr('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9'));


        $main = self::appChild($urlSet, Yii::$app->params['HTTP_SITE'], date("Y-m-d"), 'daily', '0.9');
//        $search = self::appChild($urlSet, Yii::$app->params['HTTP_SITE'] . 'search/', date("Y-m-d"), 'daily', '0.5');

        self::appChild($urlSet, Yii::$app->params['HTTP_SITE'] . 'map/', date("Y-m-d"), 'daily', '0.8');
        self::appChild($urlSet, Yii::$app->params['HTTP_SITE'] . 'tags/', date("Y-m-d"), 'daily', '0.8');

        //category
        $categories = Category::find()->where(['is_published' => true])->orderBy('sort asc')->all();
        foreach ($categories as $category) {
            self::appChild($urlSet, Yii::$app->params['HTTP_SITE'] . $category->getUrl() . '/', date("Y-m-d"), 'daily', '0.6');
        }

        //infomation
        $informations = Information::find()->all();
        foreach ($informations as $information) {
            self::appChild($urlSet, Yii::$app->params['HTTP_SITE'] . $information->getUrl() . '/', date("Y-m-d"), 'daily', '0.6');
        }

        //product
        $products = Product::find()->where(['is_published' => true])->all();
        foreach ($products as $product) {
            self::appChild($urlSet, Yii::$app->params['HTTP_SITE'] . 'product/' . $product->getId() . '/', date("Y-m-d"), 'daily', '0.7');
        }

        //tags
        $tags = Tag::find()->all();
        foreach ($tags as $tag) {
            self::appChild($urlSet, Yii::$app->params['HTTP_SITE'] . 'tags/' . $tag->getUrl() . '/', date("Y-m-d"), 'daily', '0.8');
        }

        file_put_contents($file, $dom->saveXML());

        return self::EXIT_CODE_NORMAL;
    }

    /**
     * Генерация Yml
     */
    public function actionYml()
    {

        $ymls = Yml::find()->all();
        foreach ($ymls as $yml) {

            /** @var Yml $yml */
            $file = Yii::$app->basePath . '/../frontend/web/' . $yml->getFile() . '.yml';
            if (file_exists($file)) {
                unlink($file);
            }
            $handle = fopen($file, 'w');

            $xml = '<?xml version="1.0" encoding="UTF-8"?>' .
                '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">' .
                '<yml_catalog date="' . date('Y-m-d H:i:s') . '">' .
                '<shop>' .
                '<name>' . $yml->getName() . '</name>' .
                '<company>' . $yml->getCompany() . '</company>' .
                '<url>' . Yii::$app->params['HTTP_SITE'] . '</url>' .
                '<currencies>' .
                '<currency id="RUR" rate="1" plus="0"/>' .
                '</currencies>' .
                '<categories>';
            $categories = Category::find()->where(['is_published' => true, 'is_special' => false])->all();
            foreach ($categories as $category) {
                $xml .= '<category id="' . $category->getId() . '">' . $category->getTitle() . '</category>';
            }
            $xml .= '</categories>' .
                '<local_delivery_cost>' . $yml->getShippingCost() . '</local_delivery_cost><offers>';
            fwrite($handle, $xml);

            $where = ['is_published' => true];
            if ($yml->getExportProductInStock()) {
                $where['d_is_stock'] = true;
            }

//            if($yml->getId() === 1 ){
//                $where['code'] = [123451, 76374, 109470, 119324, 119825, 76542, 68057, 120122, 41483, 25329, 95523, 119823, 114376, 55001, 96347, 76122, 108171, 119819, 96360, 119834, 119319, 79110, 78583, 110169, 31865, 177943, 4795, 78423, 79109, 189228, 76118];
//            }

            $products = Product::find()->where($where)->all();

            foreach ($products as $product) {

                /** @var Product $product */
                if($product->getPrice() === 0) {
                    continue;
                }

                $dom = new \DOMDocument('1.0', 'UTF-8');
                $dom->formatOutput = true;
                $offer = $dom->appendChild(new \DOMElement ('offer'));
                $offer->setAttribute('id', $product->getId());
                $offer->setAttribute('available', ($product->isStock() ? 'true' : 'false'));

                $url = $offer->appendChild(new \DOMElement ('url'));
                $utm = $yml->getUrlParams();
                if (!empty($utm)) {
                    $utm = '?' . str_replace('[product_id]', $product->getId(), $utm);
                }
                $url->appendChild(new \DOMText (Yii::$app->params['HTTP_SITE'] . 'product/' . $product->getId() . '/' . $utm));

                $price = $offer->appendChild(new \DOMElement ('price'));
                $price->appendChild(new \DOMText ($product->getPrice()));

                $currencyId = $offer->appendChild(new \DOMElement ('currencyId'));
                $currencyId->appendChild(new \DOMText ('RUR'));

                $categoryId = $offer->appendChild(new \DOMElement ('categoryId'));
                $categoryId->appendChild(new \DOMText ($product->getMainCategory()->getId()));

                $market_category = $offer->appendChild(new \DOMElement ('market_category'));
                $market_category->appendChild(new \DOMText ('Детские товары/Игрушки и игровые комплексы/Игровые наборы'));

                $image = $product->getMainImage();
                if ($image) {
                    $picture = $offer->appendChild(new \DOMElement ('picture'));
                    $picture->appendChild(new \DOMText ($image->getHref(['width' => 500, 'height' => 500])));
                }

                $store = $offer->appendChild(new \DOMElement ('store'));
                $store->appendChild(new \DOMText ('false'));

                $pickup = $offer->appendChild(new \DOMElement ('pickup'));
                $pickup->appendChild(new \DOMText ('true'));

                $delivery = $offer->appendChild(new \DOMElement ('delivery'));
                $delivery->appendChild(new \DOMText ('true'));

                $name = $offer->appendChild(new \DOMElement ('name'));
                $name->appendChild(new \DOMCdataSection (self::_xmlReplace($product->getTitle())));

                $vendor = $offer->appendChild(new \DOMElement ('vendor'));
                $vendor->appendChild(new \DOMText ('Hasbro'));

                $vendorCode = $offer->appendChild(new \DOMElement ('vendorCode'));
                $vendorCode->appendChild(new \DOMText ($product->getArticle()));

                if ($product->getBarcode()) {
                    $barcode = $offer->appendChild(new \DOMElement ('barcode'));
                    $barcode->appendChild(new \DOMText ($product->getBarcode()));
                }

                $description = $offer->appendChild(new \DOMElement ('description'));
                $description->appendChild(new \DOMCdataSection ($yml->isShort() ? '' : self::_xmlReplace($product->getDescription())));

                $param = $offer->appendChild(new \DOMElement ('param'));
                $param->setAttribute('name', 'Тип, Творчество');
                $param->appendChild(new \DOMText('Пластилин'));

                fwrite($handle, str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $dom->saveXml()));
                unset($dom);
            }

            $xml = '</offers></shop></yml_catalog>';

            fwrite($handle, $xml);
        }

        return self::EXIT_CODE_NORMAL;

    }

    private static function _xmlReplace($txt)
    {
        $txt = html_entity_decode($txt, ENT_QUOTES, 'UTF-8');
        $txt = strip_tags($txt);
        $txt = preg_replace('/[^\r\n\t\x20-\xFF]/', '', $txt);
        $txt = htmlspecialchars($txt, ENT_QUOTES, 'UTF-8');
        return $txt;
    }

}
