<?php

use yii\helpers\Html;
use common\models\catalog\Category;
use common\models\catalog\product\Review;

/**
 * @var Category[] $categories
 */
$category = Yii::$app->get('category', false);

$ref = null;

?>
<div class="categories">
    <div class="categories_title h4">
        Каталог товаров
    </div>
    <?php
    foreach ($categories as $item) {
        echo '<div class="category">';

        $image = $item->getMainImage();
        if ($image) {
            echo '
                    <div class="category_img">
                        ' . Html::img(Yii::$app->params['HTTP_IMAGE'] . $image->getHash(), ['width' => '45']) . '
                    </div>
            ';
        }
        echo '<div class="category_title '.($category && $item->getId() === $category->getId() ? ' selected ' : '').'">' .
                    Html::a(mb_strtoupper($item->getTitle(), 'UTF-8'), ['route/index', 'url' => $item->getUrl()],
                        ($category && $item->getId() === $category->getId() ? [] : $ref)) .
                '</div>
                 <div class="clear"></div>
            </div>';

        echo '<div class="category_border">&nbsp;</div>';
    }
    ?>
</div>
<?php
{
    if(Yii::$app->controller instanceof \frontend\controllers\SiteController){

        $productReviews = Review::find()->where(['state' => Review::STATE_PUBLISHED])->orderBy(['created_uts' => SORT_DESC])->limit(3)->all();
        $reviews = '';

        foreach ($productReviews as $review) {
            $reviews .= $this->render('/layouts/productReviewItem', ['review' => $review]);
        }

        echo '<div class="reviews"><div class="h4">Отзывы покупателей</div>'.$reviews.'</div>';
    }
}
?>

<!--
<div class="news">
    <div class="h4">
        Новости и статьи
    </div>
    <div class="news_item">
        <div class="news_date">
            19 августа
        </div>
        <div class="news_link">
            <a href="">Рады сообщить Вам, что мы готовы принимать Ваши звонки</a>
        </div>
    </div>
    <div class="news_item">
        <div class="news_date">
            19 августа
        </div>
        <div class="news_link">
            <a href="">Рады сообщить Вам, что мы готовы принимать Ваши звонки</a>
        </div>
    </div>
    <div class="news_all">
        <a href="">Все новости</a>
    </div>
</div>
-->