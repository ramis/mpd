<?php
use yii\grid\GridView;
use yii\helpers\Html;
use backend\models\direct\Sitelinksset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">

    <h1>Быстрые ссылки</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'Link 1',
                'format' => 'raw',
                'value' => function (Sitelinksset $set) {
                    return $set->getObject1()->getTitleDirect();
                }
            ],
            [
                'attribute' => 'Link 2',
                'format' => 'raw',
                'value' => function (Sitelinksset $set) {
                    return $set->getObject2()->getTitleDirect();
                }
            ],
            [
                'attribute' => 'Link 3',
                'format' => 'raw',
                'value' => function (Sitelinksset $set) {
                    return $set->getObject3()->getTitleDirect();
                }
            ],
            [
                'attribute' => 'Link 4',
                'format' => 'raw',
                'value' => function (Sitelinksset $set) {
                    return $set->getObject4()->getTitleDirect();
                }
            ],
            [
                'attribute' => 'Count Ads',
                'format' => 'raw',
                'value' => function (Sitelinksset $set) {
                    return $set->getAdsCount();
                }
            ],
        ],
    ]);
    ?>
</div>
