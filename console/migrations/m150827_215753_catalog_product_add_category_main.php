<?php

use yii\db\Schema;
use yii\db\Migration;

class m150827_215753_catalog_product_add_category_main extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%catalog_product}}', 'catalog_category_id', Schema::TYPE_INTEGER);
        $this->addForeignKey('FK_catalog_product_catalog_category_id', '{{%catalog_product}}', 'catalog_category_id', '{{%catalog_category}}', 'id', 'NO ACTION', 'NO ACTION');

        $this->execute('
            update
                {{%catalog_product}}
            set
                catalog_category_id = category.catalog_category_id
            from
                (select catalog_category_id, catalog_product_id from {{%catalog_product_category}}) as category
            where
              category.catalog_product_id = id
        ');

//        $this->alterColumn('{{%catalog_product}}', 'catalog_category_id', Schema::TYPE_BIGINT.'NOT NULL');
    }

    public function safeDown()
    {
    }

}
