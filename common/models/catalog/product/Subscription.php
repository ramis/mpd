<?php

namespace common\models\catalog\product;

use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\id;
use common\models\behavior\state;
use Yii;
use yii\db\ActiveRecord;
use common\models\Account;
use common\models\catalog\Product as CatalogProduct;

/**
 * This is the model class for table "{{%catalog_product_subscription}}".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $catalog_product_id
 * @property integer $state
 * @property integer $count_send
 * @property integer $created_uts
 *
 * @property Account $account
 * @property CatalogProduct $catalogProduct
 */
class Subscription extends ActiveRecord
{
    use Id;
    use State;
    use CreatedUpdatedUts;

    /**
     * @var Account
     */
    private $account;

    /**
     * @var CatalogProduct
     */
    private $catalogProduct;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_subscription}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'catalog_product_id', 'state', 'count_send', 'created_uts', 'updated_uts'], 'required'],
            [['id', 'account_id', 'catalog_product_id', 'state', 'count_send', 'created_uts', 'updated_uts'], 'integer'],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'catalog_product_id' => 'Catalog Product ID',
            'state' => 'State',
            'count_send' => 'Count Send',
            'created_uts' => 'Created Uts',
            'updated_uts' => 'Updated Uts',
        ];
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        if($this->account === null && $this->account_id !== null) {
            $this->account = Account::findOne($this->account_id);
        }
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account)
    {
        $this->account_id = $account->getId();
        $this->account = $account;
    }

    /**
     * @return CatalogProduct
     */
    public function getCatalogProduct()
    {
        if($this->catalogProduct === null && $this->catalog_product_id !== null) {
            $this->catalogProduct = CatalogProduct::findOne($this->catalog_product_id);
        }

        return $this->catalogProduct;

    }

    /**
     * @param CatalogProduct $product
     */
    public function setCatalogProduct(CatalogProduct $product)
    {
        $this->catalogProduct = $product;
        $this->catalog_product_id = $product->getId();
    }

    /**
     * @param $count_send
     */
    public function setCountSend($count_send)
    {
        $this->count_send = $count_send;
    }

    /**
     * @return int
     */
    public function getCountSend()
    {
        return $this->count_send;
    }

}
