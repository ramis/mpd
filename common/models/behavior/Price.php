<?php

namespace common\models\behavior;

trait Price
{
    /**
     * @return int
     */
    public function getPrice()
    {
        return (int)$this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

}