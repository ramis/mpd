<?php

use common\models\catalog\Product;
use frontend\widgets\LeftColumn;
use frontend\widgets\BreadCrumbs;
use frontend\widgets\Paginator;
use frontend\widgets\Sort;

/* @var $this yii\web\View */
$title = 'Результаты поиска';
$this->title = $title;

$countPage = ceil(count($ids) / $limit);

$products = [];
if(!empty($ids) && $sort) {
    $products = Product::find()->where(['id' => $ids])->orderBy( ['d_is_stock' => SORT_DESC, $sort => SORT_ASC])->offset(($page - 1) * $limit)->limit($limit)->all();
}
$searchObject = new \frontend\models\Search();

?>

<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col">
        <?= BreadCrumbs::widget(['breadCrumbs' => $searchObject]); ?>

        <div id="site_index" class="search_index">
            <?php

            echo '<h1>' . $title . '</h1>';
            /*       ?>

                   <div id="block_filters">
                       <div id="clear_all">x Сбросить все</div>
                       <ul>
                           <li>
                               Фильтр
                           </li>
                           <li class="filter">
                               Цена
                           </li>
                           <li class="filter active" style="width: 110px">
                               Категория
                               <div class="selected_filter">2 категории</div>
                           </li>
                           <li class="filter">
                               Возраст
                               <div class="select_filters">
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">до 1 года</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 1 года до 2 лет</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 2 лет до 4 лет</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 4 лет</label>
                                   </div>
                                   <div class="filter_btn"><div class="btn">Применить</div></div>
                               </div>
                           </li>
                           <li class="filter">
                               Пол
                           </li>
                       </ul>
                   </div>
            */
            if(count($ids) > 0) {
                echo Sort::widget(['url' => $_SERVER['REQUEST_URI'], 'limit'=> $limit, 'sort' => $sort, 'order' => $order]);
            }
            ?>
            <div class="product_item_list">
                <?php

                if(count($ids) === 0) {
                    echo '<div class="product_not_search">К сожалению, по вашему запросу "<span class="search_word">'.$search.'</span>" ничего не найдено</div>';
                    echo '<div class="product_not_search_help">Поробуйте уточнить запрос или поискать нужный товар в каталоге.</div>';
                    echo '<div class="product_list_small"><h3>Вас могут заинтерисовать</h3>';

                    $category = \common\models\catalog\Category::findOne(10);
                    $products = $category->getProductsInStock(4);
                    foreach ($products as $prod) {
                        echo $this->render('/layouts/productItemSmall', ['product' => $prod]);
                    }
                    echo '</div><div class="clear"></div>';
                } else {
                    if(!empty($products)){
                        foreach ($products as $product) {
                            echo $this->render('/layouts/productItem', ['product' => $product]);
                        }
                    } else {
                        $ids = array_slice($ids, (($page - 1) * $limit), $limit);

                        foreach ($ids as $id) {
                            $product = Product::findOne($id);
                            echo $this->render('/layouts/productItem', ['product' => $product]);
                        }
                    }
                }
                ?>

            </div>

            <div class="clear"></div>

            <?= Paginator::widget(['url' => $_SERVER['REQUEST_URI'], 'page'=> $page, 'countPage' => $countPage]); ?>

        </div>
    </div>
</div>