<?php

namespace common\models\order\shipping;

use common\models\behavior\City;
use common\models\behavior\Region;
use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Address;
use common\models\behavior\Fio;
use common\models\behavior\Id;
use common\models\behavior\OrderShipping;
use common\models\behavior\ShippingGeoValue;

/**
 * This is the model class for table "{{%order_shipping_post}}".
 *
 * @property integer $id
 * @property integer $order_shipping_id
 * @property integer $shipping_geo_value_id
 * @property integer $index
 * @property string $region
 * @property string $city
 * @property string $address
 * @property string $fio
 */
class Post extends ActiveRecord
{
    use Id;
    use Fio;
    use OrderShipping;
    use ShippingGeoValue;
    use Address;
    use City;
    use Region;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_shipping_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_shipping_id', 'shipping_geo_value_id', 'index', 'region', 'city', 'address', 'fio'], 'required'],
            [['order_shipping_id', 'shipping_geo_value_id', 'index'], 'integer'],
            [['region', 'city', 'address', 'fio'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_shipping_id' => Yii::t('app', 'Order Shipping ID'),
            'shipping_geo_value_id' => Yii::t('app', 'Shipping Geo Value ID'),
            'index' => Yii::t('app', 'Index'),
            'region' => Yii::t('app', 'Region'),
            'city' => Yii::t('app', 'City'),
            'address' => Yii::t('app', 'Address'),
            'fio' => Yii::t('app', 'Fio'),
        ];
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @return string
     */
    public function getText()
    {
        $geoTitle = $this->getShippingGeoValue()->getGeo()->getTitle();
        $typeTitle = $this->getShippingGeoValue()->getShipping()->getTitle();

//        return $geoTitle . ' ' . $typeTitle . ' (' . $this->getIndex() . ' ' . $this->getRegion() . ' ' . $this->getCity().')';
        return $geoTitle . ' ' . $typeTitle;
    }

}
