<?php
namespace common\models\behavior;

use common\models\Account as ModelAccount;

trait Account
{
    /**
     * @var ModelAccount
     */
    private $account;

    /**
     * @return ModelAccount
     */
    public function getAccount()
    {
        if ($this->account === null && $this->account_id !== null) {
            $this->account = ModelAccount::find()->where(['id' => $this->account_id])->one();
        }

        if ($this->account === null) {
            $this->account = new ModelAccount();
        }

        return $this->account;
    }

    /**
     * @param ModelAccount $account
     */
    public function setAccount(ModelAccount $account)
    {
        $this->account = $account;
        $this->account_id = $account->getId();
    }

}