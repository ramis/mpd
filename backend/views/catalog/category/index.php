<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\catalog\Category;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
$this->title = 'Catalog Category List';
?>

<div class="site-index">
    <h1>Категории</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Id',
                'value' => function (Category $category) {
                    return $category->getId();
                }
            ],
            [
                'attribute' => 'Foto',
                'format' => 'raw',
                'value' => function (Category $category) {
                    $image = $category->getMainImage();
                    return ($image) ? $image->getHtml() : '';
                }
            ],
            [
                'attribute' => 'Title',
                'value' => function (Category $category) {
                    return $category->getTitle();
                }
            ],
            [
                'attribute' => 'Url',
                'value' => function (Category $category) {
                    return $category->getUrl();
                }
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Category $category) {
                    return Html::a('Редактировать', ['edit', 'id' => $category->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
