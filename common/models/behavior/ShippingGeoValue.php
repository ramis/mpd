<?php

namespace common\models\behavior;

use common\models\shipping\GeoValue;

trait ShippingGeoValue
{
    /**
     * @var GeoValue
     */
    private $shipping_geo_value;

    /**
     * @return GeoValue
     */
    public function getShippingGeoValue()
    {
        if ($this->shipping_geo_value === null && $this->shipping_geo_value_id !== null) {
            $this->shipping_geo_value = GeoValue::find()->where(['id' => $this->shipping_geo_value_id])->one();
        }

        if ($this->shipping_geo_value === null) {
            $this->shipping_geo_value = new GeoValue();
        }

        return $this->shipping_geo_value;
    }

    /**
     * @param GeoValue $shippingGeoValue
     */
    public function setShippingGeoValue(GeoValue $shippingGeoValue)
    {
        $this->shipping_geo_value = $shippingGeoValue;
        $this->shipping_geo_value_id = $shippingGeoValue->getId();
    }

}