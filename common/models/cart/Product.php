<?php

namespace common\models\cart;

use common\models\behavior\CatalogProductId;
use Yii;
use yii\db\ActiveRecord;
use common\models\catalog\Product as CatalogProduct;
use common\models\Cart;
use common\models\behavior\Id;
use common\models\behavior\Quantity;

/**
 * This is the model class for table "{{%cart_product}}".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property integer $catalog_product_id
 * @property integer $catalog_product_set_id
 * @property integer $quantity
 * @property string $hash
 *
 * @property Cart $cart
 * @property CatalogProduct $product
 */
class Product extends ActiveRecord
{
    use Id;
    use Quantity;
    use CatalogProductId;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cart_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cart_id', 'catalog_product_id', 'quantity'], 'required'],
            [['id', 'cart_id', 'catalog_product_id', 'quantity', 'catalog_product_set_id'], 'integer'],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cart_id' => Yii::t('app', 'Cart Id'),
            'catalog_product_id' => Yii::t('app', 'Catalog Product ID'),
            'catalog_product_set_id' => Yii::t('app', 'Catalog Product Set ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'hash' => Yii::t('app', 'Hash'),
        ];
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        if ($this->cart === null && $this->cart_id !== null) {
            $this->cart = Cart::find()->where(['id' => $this->cart_id])->one();
        }

        return $this->cart;
    }

    /**
     * @param Cart $cart
     */
    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
        $this->cart_id = $cart->getId();
    }

}
