<?php

namespace common\models\geo;

use common\models\behavior\Id;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%geo_cidr}}".
 *
 * @property integer $id
 * @property integer $ip_start
 * @property integer $ip_end
 * @property string $ip_interval
 * @property integer $geo_cities_id
 *
 * @property Cities $geoCities
 */
class Cidr extends ActiveRecord
{
    use Id;

    /**
     * @var Cities
     */
    private $geo_cities;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_cidr}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip_start', 'ip_end', 'ip_interval', 'geo_cities_id'], 'required'],
            [['ip_start', 'ip_end', 'geo_cities_id'], 'integer'],
            [['ip_interval'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ip_start' => Yii::t('app', 'Ip Start'),
            'ip_end' => Yii::t('app', 'Ip End'),
            'ip_interval' => Yii::t('app', 'Ip Interval'),
            'geo_cities_id' => Yii::t('app', 'Geo Cities ID'),
        ];
    }

    /**
     * @return Cities
     */
    public function getGeoCities()
    {
        if ($this->geo_cities === null && $this->geo_cities_id !== null) {
            $this->geo_cities = Cities::findOne($this->geo_cities_id);
        }
        return $this->geo_cities;
    }

    /**
     * @param Cities $cities
     */
    public function setGeoCities(Cities $cities)
    {
        $this->geo_cities_id = $cities->getId();
        $this->geo_cities = $cities;
    }

    /**
     * @param int $geo_cities_id
     */
    public function setGeoCitiesId($geo_cities_id)
    {
        $this->geo_cities_id = $geo_cities_id;
        $this->geo_cities = null;
    }

    /**
     * @return int
     */
    public function getGeoCitiesId()
    {
        return ($this->geo_cities ? $this->geo_cities->getId() : $this->geo_cities_id);
    }

    /**
     * @return int
     */
    public function getIpStart()
    {
        return $this->ip_start;
    }

    /**
     * @param int $ipStart
     */
    public function setIpStart($ipStart)
    {
        $this->ip_start = $ipStart;
    }

    /**
     * @return int
     */
    public function getIpEnd()
    {
        return $this->ip_end;
    }

    /**
     * @param int $ipEnd
     */
    public function setIpEnd($ipEnd)
    {
        $this->ip_end = $ipEnd;
    }

    /**
     * @return string
     */
    public function getIpInterval()
    {
        return $this->ip_interval;
    }

    /**
     * @param string $ipInterval
     */
    public function setIpInterval($ipInterval)
    {
        $this->ip_interval = $ipInterval;
    }
}
