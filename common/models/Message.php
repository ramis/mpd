<?php

namespace common\models;

use Yii;
use common\models\catalog\Product;
use common\models\message\Comment;

/**
 * This is the model class for table "{{%message}}".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $state
 * @property integer $catalog_product_id
 * @property string $catalog_product_title
 * @property string $catalog_product_article
 * @property string $comment
 * @property integer $created_uts
 * @property integer $updated_uts
 *
 * @property Account $account
 * @property Product $catalogProduct
 * @property Comment[] $messageComments
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'account_id', 'state', 'comment', 'created_uts', 'updated_uts'], 'required'],
            [['id', 'account_id', 'state', 'catalog_product_id', 'created_uts', 'updated_uts'], 'integer'],
            [['comment'], 'string'],
            [['catalog_product_title', 'catalog_product_article'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'state' => Yii::t('app', 'State'),
            'catalog_product_id' => Yii::t('app', 'Catalog Product ID'),
            'catalog_product_title' => Yii::t('app', 'Catalog Product Title'),
            'catalog_product_article' => Yii::t('app', 'Catalog Product Article'),
            'comment' => Yii::t('app', 'Comment'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'updated_uts' => Yii::t('app', 'Updated Uts'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'catalog_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageComments()
    {
        return $this->hasMany(Comment::className(), ['message_id' => 'id']);
    }
}
