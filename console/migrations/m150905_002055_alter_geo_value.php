<?php

use yii\db\Schema;
use yii\db\Migration;

class m150905_002055_alter_geo_value extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shipping_geo_value}}', 'latitude', Schema::TYPE_DECIMAL.'(10,8)');
        $this->addColumn('{{%shipping_geo_value}}', 'longitude', Schema::TYPE_DECIMAL.'(10,8)');
    }

    public function safeDown()
    {
    }
}
