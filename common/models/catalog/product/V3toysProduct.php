<?php

namespace common\models\catalog\product;

use Yii;
use common\models\catalog\Product as CatalogProduct;
use common\models\v3toys\Product as V3toys;
/**
 * This is the model class for table "{{%catalog_product_v3toys_product}}".
 *
 * @property integer $id
 * @property integer $v3toys_product_id
 * @property integer $catalog_product_id
 *
 * @property CatalogProduct $catalogProduct
 * @property V3toys $v3toysProduct
 */
class V3toysProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_v3toys_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['v3toys_product_id', 'catalog_product_id'], 'required'],
            [['v3toys_product_id', 'catalog_product_id'], 'integer'],
            [['v3toys_product_id', 'catalog_product_id'], 'unique', 'targetAttribute' => ['v3toys_product_id', 'catalog_product_id'],
                'message' => 'The combination of V3toys Product ID and Catalog Product ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'v3toys_product_id' => Yii::t('app', 'V3toys Product ID'),
            'catalog_product_id' => Yii::t('app', 'Catalog Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProduct()
    {
        return $this->hasOne(CatalogProduct::className(), ['id' => 'catalog_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getV3toysProduct()
    {
        return $this->hasOne(V3toys::className(), ['v3toys_product_id' => 'v3toys_product_id']);
    }
}
