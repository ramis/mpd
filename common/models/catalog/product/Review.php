<?php

namespace common\models\catalog\product;

use common\models\Account;
use common\models\behavior\AccountId;
use common\models\behavior\CatalogProductId;
use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\Email;
use common\models\behavior\Id;
use common\models\behavior\Name;
use common\models\behavior\State;
use common\models\catalog\Product;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%catalog_product_review}}".
 *
 * @property integer $id
 * @property integer $catalog_product_id
 * @property integer $account_id
 * @property string $name
 * @property string $email
 * @property string $review
 * @property string $dignity
 * @property string $shortcomings
 * @property integer $rating
 * @property integer $state
 * @property integer $created_uts
 * @property integer $updated_uts
 *
 * @property Account $account
 * @property Product $catalogProduct
 */
class Review extends ActiveRecord
{
    use Id;
    use AccountId;
    use CatalogProductId;
    use CreatedUpdatedUts;
    use State;
    use Name;
    use Email;

    CONST STATE_NEW = 10;
    CONST STATE_PUBLISHED = 20;
    CONST STATE_REJECT = 30;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_product_id', 'account_id', 'review', 'rating', 'state'], 'required'],
            [['id', 'catalog_product_id', 'account_id', 'rating', 'state', 'created_uts', 'updated_uts'], 'integer'],
            [['review', 'dignity', 'shortcomings', 'name', 'email'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catalog_product_id' => 'Catalog Product ID',
            'account_id' => 'Account ID',
            'name' => 'Name',
            'email' => 'Email',
            'review' => 'Review',
            'dignity' => 'Dignity',
            'shortcomings' => 'Shortcomings',
            'rating' => 'Rating',
            'state' => 'State',
            'created_uts' => 'Created Uts',
            'updated_uts' => 'Updated Uts',
        ];
    }

    public function getStateLabel() {
        $stateLabel = [
            self::STATE_NEW => 'warning',
            self::STATE_PUBLISHED => 'success',
            self::STATE_REJECT => 'danger',
        ];
        return $stateLabel[$this->getAttribute('state')];
    }

    public function getStateTitle() {
        return self::getStatus($this->getAttribute('state'));
    }

    /**
     * @param null $stateId
     * @return array
     */
    public static function getStatus ($stateId = null) {
        $state = [
            self::STATE_NEW => 'Новый',
            self::STATE_PUBLISHED => 'Опубликован',
            self::STATE_REJECT => 'Отклонен',
        ];
        return ($stateId && !empty($state[$stateId]) ? $state[$stateId] : $state);
    }
    /**
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param string $review
     * @return $this
     */
    public function setReview($review)
    {
        $this->review = $review;
        return $this;
    }

    /**
     * @return string
     */
    public function getDignity()
    {
        return $this->dignity;
    }

    /**
     * @param string $dignity
     * @return $this
     */
    public function setDignity($dignity)
    {
        $this->dignity = $dignity;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortcomings()
    {
        return $this->shortcomings;
    }

    /**
     * @param string $shortcomings
     * @return $this
     */
    public function setShortcomings($shortcomings)
    {
        $this->shortcomings = $shortcomings;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            /** @var Account $account */
            $account = Account::findOne($this->getAccountId());
            if($account->isAuto()) {
                $account
                    ->setFio($this->getName())
                    ->setEmail($this->getEmail())
                    ->setAuto(false);
                $account->save();
            }
        } else {
            $product = $this->getCatalogProduct();

            $dtReviewRating = 0;
            $dtReviewCount = 0;
            $productReviews = $product->getReviews();
            foreach ($productReviews as $review) {
                if ($review->getState() === Review::STATE_PUBLISHED) {
                    $dtReviewCount++;
                    $dtReviewRating += $review->getRating();
                }
            }

            $dtReviewRating = $dtReviewCount > 0 ? round(($dtReviewRating / $dtReviewCount), 0) : 0;
            $product
                ->setReviewCount($dtReviewCount)
                ->setReviewRating($dtReviewRating);
            $product->save();
        }
    }

}
