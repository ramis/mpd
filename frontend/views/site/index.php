<?php

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\LeftColumn;
use common\models\catalog\Category;
use common\models\catalog\Product;

/* @var $this yii\web\View */
/* @var $categories Category[] */
/* @var $categoryMain Category */
$categoryMain = Category::findOne(['url' => 'main']);
$metaDescription = '';
if($categoryMain) {
    $this->title = $categoryMain->getSeoTitle();
    $metaDescription = $categoryMain->getSeoDescription();
}
Yii::$app->view->registerMetaTag(['name' => 'vk:url', 'content' => Url::to(['site/index'], true),]);
Yii::$app->view->registerMetaTag(['name' => 'vk:title', 'content' => $this->title]);
Yii::$app->view->registerMetaTag(['name' => 'vk:image', 'content' => 'http://mrplaydoh.ru/favicon.png']);
Yii::$app->view->registerMetaTag(['name' => 'vk:description', 'content' => $metaDescription]);
Yii::$app->view->registerMetaTag(['name' => 'og:url', 'content' => Url::to(['site/index'], true),]);
Yii::$app->view->registerMetaTag(['name' => 'og:title', 'content' => $this->title]);
Yii::$app->view->registerMetaTag(['name' => 'og:image', 'content' => 'http://mrplaydoh.ru/favicon.png']);
Yii::$app->view->registerMetaTag(['name' => 'og:description', 'content' => $metaDescription]);
Yii::$app->view->registerMetaTag(['name' => 'og:site_name', 'content' => Yii::$app->params['DOMAIN']]);
Yii::$app->view->registerMetaTag(['name' => 'og:type', 'content' => 'website']);

Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $metaDescription]);
Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => 'плей до play doh хасбро hasbro купить цена интернет магазин']);

$categoriesSpecial = Category::find()->where(['is_published' => true, 'is_special' => true])->orderBy('sort asc')->all();
$categories = Category::find()->where(['is_published' => true, 'is_special' => false])->limit(10)->orderBy('sort asc')->all();

?>

<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col">
        <div class="site_index main_index">
<!--            <img src="/img/banner.jpg"/>-->

            <div class="category_list">
                <?php
                    foreach ($categories as $key=>$category) {
                        echo '<div class="category_main">';
                                $image = $category->getMainImage();
                                if($image){
                                    echo Html::a($image->getHtml(['title' => $category->getTitle(), 'alt' => $category->getTitle()]), ['route/index', 'url' => $category->getUrl()]);
                                }
                        echo
                                '<div>'.Html::a($category->getTitle(), ['route/index', 'url' => $category->getUrl()]) . '</div>'.
                            '</div>';
//                        if($key === 5){
//                            echo '<div class="category_main">&nbsp;</div>';
//                        }
                    }
                ?>
                <div class="clear"></div>
            </div>
            <?php
//                foreach ($categoriesSpecial as $category) {
//                    /* @var Category $category */
                    echo  '<div><div class="h1">Новые поступления</div>';
                    echo  '<div class="product_item_list">';

                    $i = 0;
                    foreach ([1266, 1270, 1267, 1268, 1279, 1269, 1287, 1290, 1286, 1225, 1288] as $productId) {
                        $product = Product::findOne($productId);
                        if($product && $product->getQuantity() > 0) {
                            $i++;
                            echo $this->render('/layouts/productItem', ['product' => $product]);
                        }
                        if($i > 2) {
                            break;
                        }
                    }

                    echo '</div><div class="clear"></div></div>';
//                }
//                }
                echo  '<div><div class="h1">Лидеры продаж</div>';
                echo  '<div class="product_item_list">';

                $products = Product::getProductBuy();
                foreach ($products as $product) {
                    echo $this->render('/layouts/productItem', ['product' => $product]);
                }

                echo '</div><div class="clear"></div></div>';
            ?>

            <?php
                if($categoryMain) {
                    echo '<h1>'.$categoryMain->getTitle().'</h1>';

                    $description = preg_replace("/\n/", "</p><p>", $categoryMain->getDescription());
                    $description = preg_replace('/[\,\.\!\?]/', Html::a('$0', ['route/index', 'url' => $category->getUrl()]), $description, 1);

                    echo '<div class="description"><p>'.$description.'</p>';
                    echo '<a href="https://plus.google.com/u/1/108888585391425984422/posts" rel="author">Mr Play-Doh</a></div>';
                }
            ?>



            <?php
            //
            //
            //        echo '<a href="/">id</a><br/>';
            //        echo '<a href="/?sort=price">price</a><br/>';
            //
            //        echo '<select class="form-control">
            //                <option value="">----</option>
            //                <option value="?id=asc" '.(isset($_GET['id']) ? 'selected="selected"' : '').'>id</option>
            //                <option value="?sort=price" '.(isset($_GET['sort']) ? 'selected="selected"' : '').'>price</option>
            //            </select>';
            //
            //        \yii\widgets\Pjax::begin();
            //        foreach ($products as $product) {
            //            echo '<div class="product"><div>' .
            //                $product->getTitle() . '</div><div>' .
            //                $product->getPrice() . '</div>' .
            //                '</div>';
            //        }
            //
            //
            //        \yii\widgets\Pjax::end();
            ?>
        </div>
    </div>
</div>