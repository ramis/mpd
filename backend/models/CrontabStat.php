<?php

namespace backend\models;

use common\models\behavior\CreatedUpdatedTs;
use common\models\behavior\Title;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%crontab_stat}}".
 *
 * @property integer $id
 * @property string $controller
 * @property string $command
 * @property string $title
 * @property string $created_ts
 * @property string $updated_ts
 */
class CrontabStat extends ActiveRecord
{
    use Title;
    use CreatedUpdatedTs;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%crontab_stat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['command', 'title', 'controller'], 'required'],
            [['created_ts', 'updated_ts'], 'safe'],
            [['command', 'title'], 'string', 'max' => 255],
            [['command'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'command' => Yii::t('app', 'Command'),
            'title' => Yii::t('app', 'Title'),
            'created_ts' => Yii::t('app', 'Created Ts'),
            'updated_ts' => Yii::t('app', 'Updated Ts'),
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param string $command
     * @return $this
     */
    public function setCommand($command)
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     * @return $this
     */
    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

}
