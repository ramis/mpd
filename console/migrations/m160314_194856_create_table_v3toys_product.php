<?php

use yii\db\Migration;
use yii\db\Schema;

class m160314_194856_create_table_v3toys_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%v3toys_product}}', [
            'id' => Schema::TYPE_PK,
            'v3toys_product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'title_unit' => Schema::TYPE_STRING . ' NOT NULL',
            'sku' => Schema::TYPE_STRING . ' NOT NULL',
            'barcode' => Schema::TYPE_STRING . ' NOT NULL',
            'brand' => Schema::TYPE_STRING . ' NOT NULL',
            'price' => Schema::TYPE_STRING . ' NOT NULL',
            'buy_price' => Schema::TYPE_INTEGER . ' NOT NULL',
            'base_price' => Schema::TYPE_INTEGER . ' NOT NULL',
            'mr_price' => Schema::TYPE_INTEGER . ' NOT NULL',
            'quantity' => Schema::TYPE_INTEGER . ' NOT NULL',
            'is_assortiment' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'is_deleted' => Schema::TYPE_BOOLEAN . ' NOT NULL',

            'catalog_product_id' => Schema::TYPE_INTEGER,

            'title' => Schema::TYPE_STRING,
            'category' => Schema::TYPE_STRING,
            'img' => Schema::TYPE_STRING,
        ]);

        $this->addForeignKey('FK_v3toys_product_catalog_product_id', '{{%v3toys_product}}', 'catalog_product_id', '{{%catalog_product}}', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function safeDown()
    {
    }

}
