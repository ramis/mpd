<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\Yml;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">
    <div class="right"><?=Html::a('Добавить', \yii\helpers\Url::to(['create'])); ?></div>

    <h1>Yml files</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'file',
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Yml $yml) {
                    return Html::a('Редактировать', ['edit', 'id' => $yml->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
