<?php

namespace backend\models\direct;

use common\models\behavior\Active;
use common\models\behavior\Campaign;
use common\models\behavior\Direct;
use common\models\behavior\Id;
use common\models\behavior\Name;
use common\models\behavior\Status;
use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%direct_adgroup}}".
 *
 * @property integer $id
 * @property integer $direct_id
 * @property integer $campaign_id
 * @property integer $status
 * @property string $name
 * @property string $region_ids
 * @property string $negative_keywords
 * @property boolean $is_active
 *
 * @property Campaign $campaign
 * @property Ads[] $directAds
 * @property Bids[] $directBids
 * @property Keywords[] $directKeywords
 */
class Adgroup extends ActiveRecord
{
    use Id;
    use Direct;
    use Name;
    use Status;
    use Active;
    use Campaign;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%direct_adgroup}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direct_id', 'campaign_id', 'status', 'name',  'is_active'], 'required'],
            [['direct_id', 'campaign_id', 'status'], 'integer'],
            [['region_ids', 'negative_keywords'], 'string'],
            [['is_active'], 'boolean'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'direct_id' => Yii::t('app', ' ID'),
            'campaign_id' => Yii::t('app', 'Campaign ID'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Name'),
            'region_ids' => Yii::t('app', 'Region Ids'),
            'negative_keywords' => Yii::t('app', 'Negative Keywords'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return Ads
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['adgroup_id' => 'id']);
    }

    /**
     * @return Bids
     */
    public function getBids()
    {
        return $this->hasMany(Bids::className(), ['adgroup_id' => 'id']);
    }

    /**
     * @return Keywords
     */
    public function getKeywords()
    {
        return $this->hasMany(Keywords::className(), ['adgroup_id' => 'id']);
    }

    public function getRegionIds()
    {
        return [];
    }

    public function getNegativeKeywords()
    {
        return [];
    }

}
