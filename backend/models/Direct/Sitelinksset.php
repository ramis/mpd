<?php

namespace backend\models\direct;

use common\models\behavior\Active;
use common\models\behavior\Direct;
use common\models\behavior\Id;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%direct_sitelinksset}}".
 *
 * @property integer $id
 * @property integer $direct_id
 * @property integer $id1
 * @property string $class1
 * @property integer $id2
 * @property string $class2
 * @property integer $id3
 * @property string $class3
 * @property integer $id4
 * @property string $class4
 * @property boolean $is_active
 *
 * @property Ads[] $directAds
 */
class Sitelinksset extends ActiveRecord
{
    use Id;
    use Direct;
    use Active;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%direct_sitelinksset}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direct_id', 'id1', 'class1', 'id2', 'class2', 'id3', 'class3', 'id4', 'class4', 'is_active'], 'required'],
            [['direct_id', 'id1', 'id2', 'id3', 'id4'], 'integer'],
            [['is_active'], 'boolean'],
            [['class1', 'class2', 'class3', 'class4'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'direct_id' => Yii::t('app', 'Direct ID'),
            'id1' => Yii::t('app', 'Id1'),
            'class1' => Yii::t('app', 'Class1'),
            'id2' => Yii::t('app', 'Id2'),
            'class2' => Yii::t('app', 'Class2'),
            'id3' => Yii::t('app', 'Id3'),
            'class3' => Yii::t('app', 'Class3'),
            'id4' => Yii::t('app', 'Id4'),
            'class4' => Yii::t('app', 'Class4'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return Ads
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['sitelinkset_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getAdsCount()
    {
        return Ads::find()->where(['sitelinkset_id' => $this->getId()])->count();
    }

    public function getObject1()
    {
        $class = $this->getClass1();
        return $class::findOne($this->getId1());
    }
    public function getObject2()
    {
        $class = $this->getClass2();
        return $class::findOne($this->getId2());
    }
    public function getObject3()
    {
        $class = $this->getClass3();
        return $class::findOne($this->getId3());
    }
    public function getObject4()
    {
        $class = $this->getClass4();
        return $class::findOne($this->getId4());
    }

    /**
     * @return string
     */
    public function getClass1()
    {
        return $this->class1;
    }

    /**
     * @param string $class1
     */
    public function setClass1($class1)
    {
        $this->class1 = $class1;
    }

    /**
     * @return string
     */
    public function getClass2()
    {
        return $this->class2;
    }

    /**
     * @param string $class2
     */
    public function setClass2($class2)
    {
        $this->class2 = $class2;
    }

    /**
     * @return string
     */
    public function getClass3()
    {
        return $this->class3;
    }

    /**
     * @param string $class3
     */
    public function setClass3($class3)
    {
        $this->class3 = $class3;
    }

    /**
     * @return string
     */
    public function getClass4()
    {
        return $this->class4;
    }

    /**
     * @param string $class4
     */
    public function setClass4($class4)
    {
        $this->class4 = $class4;
    }

    /**
     * @return int
     */
    public function getId1()
    {
        return $this->id1;
    }

    /**
     * @param int $id1
     */
    public function setId1($id1)
    {
        $this->id1 = $id1;
    }

    /**
     * @return int
     */
    public function getId2()
    {
        return $this->id2;
    }

    /**
     * @param int $id2
     */
    public function setId2($id2)
    {
        $this->id2 = $id2;
    }

    /**
     * @return int
     */
    public function getId3()
    {
        return $this->id3;
    }

    /**
     * @param int $id3
     */
    public function setId3($id3)
    {
        $this->id3 = $id3;
    }

    /**
     * @return int
     */
    public function getId4()
    {
        return $this->id4;
    }

    /**
     * @param int $id4
     */
    public function setId4($id4)
    {
        $this->id4 = $id4;
    }
}
