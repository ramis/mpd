<?php
namespace frontend\controllers;

use common\models\catalog\Tag;
use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Site controller
 */
class TagsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param string $url
     * @return string
     */
    public function actionTag($url)
    {

        $tag = Tag::find()->where(['url' => $url])->one();

        if (!($tag instanceof Tag)) {
            return Yii::$app->getResponse()->redirect(Url::to(['error']));
        }

        $page = Yii::$app->request->get('page', 1);
        $limit = (int)Yii::$app->request->get('limit', 12);
        $sort = Yii::$app->request->get('sort', 'price');
        $order = Yii::$app->request->get('order', 'asc');

        return $this->render('tag', ['tag' => $tag, 'page' => $page, 'sort' => $sort, 'order' => $order, 'limit' => $limit]);

    }

}
