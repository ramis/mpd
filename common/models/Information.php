<?php

namespace common\models;

use common\models\ints\BreadCrumbsInts;
use Yii;
use common\models\behavior\Description;
use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\behavior\Url;
use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\Published;
use common\models\behavior\seo\Description as SeoDescription;
use common\models\behavior\seo\Title as SeoTitle;
use common\models\behavior\seo\Keywords as SeoKeywords;
use common\models\behavior\seo\Text as SeoText;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%information}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $description
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $created_uts
 * @property integer $updated_uts
 */
class Information extends ActiveRecord implements BreadCrumbsInts
{
    use Id;
    use Title;
    use Url;
    use Description;
    use CreatedUpdatedUts;
    use SeoDescription;
    use SeoTitle;
    use SeoText;
    use SeoKeywords;
    use Published;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%information}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['description'], 'string'],
            [['created_uts', 'updated_uts'], 'integer'],
            [['title', 'url', 'seo_title', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['url'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Url'),
            'description' => Yii::t('app', 'Description'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'updated_uts' => Yii::t('app', 'Updated Uts'),
        ];
    }


    /**
     * @return array
     */
    public function getBreadCrumbs()
    {
        return [$this->getTitle()];
    }
}
