<?php

use yii\db\Schema;
use yii\db\Migration;

class m150830_174042_direct_sitelinksset extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%direct_sitelinksset}}', [
            'id' => Schema::TYPE_PK,

            'direct_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'id1' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'class1' => Schema::TYPE_STRING  . ' NOT NULL',

            'id2' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'class2' => Schema::TYPE_STRING  . ' NOT NULL',

            'id3' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'class3' => Schema::TYPE_STRING . ' NOT NULL',

            'id4' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'class4' => Schema::TYPE_STRING  . ' NOT NULL',

            'is_active' => Schema::TYPE_BOOLEAN . ' NOT NULL',


        ]);
    }

    public function safeDown()
    {

    }
}
