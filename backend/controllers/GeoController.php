<?php
namespace backend\controllers;

use common\models\geo\Cidr;
use common\models\geo\Cities;
use Yii;

/**
 * Geo controller
 */
class GeoController extends AbstractController
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $message = '';
        if (Yii::$app->request->isPost) {
            try {
                if (empty($_FILES['cities'])) {
                    throw new \Exception('cities null');
                }

                $file = $_FILES['cities'];
                $file_path = $file['tmp_name'];

                $handle = fopen($file_path, "r");
                if ($handle === false) {
                    throw new \Exception('Error:  Can`t open file ' . $file_path);
                }

                $transaction = Yii::$app->db->beginTransaction();
                try {

                    $transaction->commit();
                    $cities = [];
                    while (($buffer = fgets($handle, 4096)) !== false) {
                        $buffer = explode("\t", $buffer);
                        if (mb_stripos($buffer[3], 'Украина', 0, 'UTF-8') > 0) {
                            continue;
                        }
                        $id = (int)$buffer[0];
                        $cities[$buffer[0]] = $buffer;

                        $geoCity = Cities::findOne($id);
                        if($geoCity === null) {
                            $geoCity = new Cities();
                        }
                        $geoCity->setId($id);
                        $geoCity->setRegion($buffer[3]);
                        $geoCity->setArea($buffer[2]);
                        $geoCity->setCity($buffer[1]);
                        $geoCity->setGeoObject([]);
                        if($geoCity->save() === false){
                            throw new \Exception('Error: save geoCity ' . json_encode($buffer));
                        }
                    }

                    if (!feof($handle)) {
                        throw new \Exception('Error: unexpected fgets() fail\n');
                    }
                    fclose($handle);

                    if (empty($_FILES['cidr'])) {
                        throw new \Exception('cidr null');
                    }

                    $file = $_FILES['cidr'];
                    $file_path = $file['tmp_name'];

                    $handle = fopen($file_path, "r");
                    if ($handle === false) {
                        throw new \Exception('Error:  Can`t open file ' . $file_path);
                    }

                    while (($buffer = fgets($handle, 4096)) !== false) {
                        $buffer = explode("\t", $buffer);
                        if ($buffer[3] !== 'RU') {
                            continue;
                        }

                        $buffer[0] = (int)$buffer[0];
                        $buffer[1] = (int)$buffer[1];
                        $buffer[4] = (int)$buffer[4];

                        if (empty($buffer[4]) && !isset($cities[$buffer[4]])) {
                            continue;
                        }

                        $cidr = Cidr::find()->where(['ip_start' => $buffer[0], 'ip_end' => $buffer[1]])->one();
                        if($cidr === null) {
                            $cidr = new Cidr();
                            $cidr->setIpStart($buffer[0]);
                            $cidr->setIpEnd($buffer[1]);
                        }

                        if(!($cidr->getIpInterval() === $buffer[2] && $cidr->getGeoCitiesId() === $buffer[4])) {
                            $cidr->setIpInterval($buffer[2]);
                            $cidr->setGeoCitiesId($buffer[4]);
                            if($cidr->save() === false){
                                throw new \Exception('Error: save Cidr ' . json_encode($buffer));
                            }
                        }
                    }

                    if (!feof($handle)) {
                        throw new \Exception('Error: unexpected fgets() fail\n');
                    }
                    fclose($handle);
                    $message = 'ok';

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw new \Exception($e->getMessage());
                }
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }

        return $this->render('index', ['message' => $message]);
    }

}
