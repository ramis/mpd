<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\catalog\product\Review;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">
    <h1>Отзывы товара</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function (Review $review) {
            if ($review->getState() === Review::STATE_NEW) {
                return ['class' => 'review_new'];
            } elseif ($review->getState() === Review::STATE_REJECT) {
                return ['class' => 'review_reject'];
            }
        },
        'columns' => [
            'id',
            [
                'attribute' => 'created_uts',
                'value' => function (Review $review) {
                    return Yii::$app->formatter->asDatetime($review->getCreatedUts());
                },
            ],
            'name',
            [
                'attribute' => 'Product',
                'format' => 'raw',
                'value' => function (Review $review) {
                    return $review->getCatalogProduct()->getTitle();
                }
            ],
            [
                'attribute' => 'Статус',
                'format' => 'raw',
                'value' => function (Review $review) {
                    return $review->getStateTitle();
                }
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Review $review) {
                    return Html::a('Редактировать', ['edit', 'id' => $review->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
