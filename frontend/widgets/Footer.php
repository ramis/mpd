<?php

namespace frontend\widgets;

use yii\bootstrap\Widget;

class Footer extends Widget
{

    public $category;

    /**
     * Renders the widget.
     */
    public function run()
    {

        return $this->render('footer', []);
    }

}
