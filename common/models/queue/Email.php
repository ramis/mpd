<?php

namespace common\models\queue;

use common\models\behavior\Id;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%queue_email}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property boolean $is_done
 */
class Email extends ActiveRecord
{
    Use Id;
    Use \common\models\behavior\Email;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%queue_email}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'subject', 'body', 'is_done'], 'required'],
            [['body'], 'string'],
            [['is_done'], 'boolean'],
            [['email', 'subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'subject' => Yii::t('app', 'Subjest'),
            'body' => Yii::t('app', 'Body'),
            'is_done' => Yii::t('app', 'Is Done'),
        ];
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return bool
     */
    public function isDone()
    {
        return $this->is_done;
    }

    /**
     * @param bool $is_done
     */
    public function setDone($is_done)
    {
        $this->is_done = $is_done;
    }

}
