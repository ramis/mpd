<?php

namespace backend\models\direct;

use common\models\behavior\Active;
use common\models\behavior\Adgroup;
use common\models\behavior\Direct;
use common\models\behavior\Id;
use common\models\behavior\Title;
use Yii;
use yii\db\ActiveRecord;
use backend\models\direct\ads\Text;

/**
 * This is the model class for table "{{%direct_ads}}".
 *
 * @property integer $id
 * @property integer $direct_id
 * @property integer $adgroup_id
 * @property integer $ads_text_id
 * @property string $title
 * @property boolean $mobile
 * @property boolean $is_active
 * @property boolean $is_updated
 * @property string $href
 * @property integer $vcard_id
 * @property string $adimage_hash
 * @property integer $sitelinkset_id
 *
 * @property Adgroup $adgroup
 * @property Sitelinksset $sitelinkset
 * @property Text $text
 */
class Ads extends ActiveRecord
{
    use Id;
    use Direct;
    use Adgroup;
    use Title;
    use Active;

    /**
     * @var Text
     */
    private $ads_text;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%direct_ads}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direct_id', 'adgroup_id', 'title', 'ads_text_id', 'mobile', 'is_active', 'href', 'vcard_id', 'adimage_hash', 'sitelinkset_id'], 'required'],
            [['direct_id', 'adgroup_id', 'ads_text_id', 'vcard_id', 'sitelinkset_id'], 'integer'],
            [['mobile', 'is_active', 'is_updated'], 'boolean'],
            [['title', 'href', 'adimage_hash'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'direct_id' => Yii::t('app', 'Direct ID'),
            'adgroup_id' => Yii::t('app', 'Adgroup ID'),
            'title' => Yii::t('app', 'Title'),
            'ads_text_id' => Yii::t('app', 'Ads Text ID'),
            'mobile' => Yii::t('app', 'Mobile'),
            'is_active' => Yii::t('app', 'Actice'),
            'is_updated' => Yii::t('app', 'Actice'),
            'href' => Yii::t('app', 'Href'),
            'vcard_id' => Yii::t('app', 'Vcard ID'),
            'adimage_hash' => Yii::t('app', 'Adimage Hash'),
            'sitelinkset_id' => Yii::t('app', 'Sitelinkset ID'),
        ];
    }


    /**
     * @return Sitelinksset
     */
    public function getSitelinksset()
    {
        if ($this->sitelinkset === null && $this->sitelinkset_id !== null) {
            $this->sitelinkset = Sitelinksset::findOne($this->sitelinkset_id);
        }

        return $this->sitelinkset;
    }

    /**
     * @param Sitelinksset $sitelinkset
     */
    public function setSitelinksset(Sitelinksset $sitelinkset)
    {
        $this->sitelinkset = $sitelinkset;
        $this->sitelinkset_id = $sitelinkset->getId();
    }

    /**
     * @param int $sitelinkset_id
     */
    public function setSitelinkssetId($sitelinkset_id)
    {
        $this->sitelinkset_id = $sitelinkset_id;
        $this->sitelinkset = Sitelinksset::findOne($this->sitelinkset_id);
    }

    /**
     * @return Text
     */
    public function getAdsText()
    {
        if ($this->_ads_text === null && $this->ads_text_id !== null) {
            $this->_ads_text = Text::findOne($this->ads_text_id);
        }

        return $this->_ads_text;
    }

    /**
     * @param Text $adsText
     */
    public function setText(Text $adsText)
    {
        $this->_ads_text = $adsText;
        $this->ads_text_id = $adsText->getId();
    }

    /**
     * @param int $ads_text_id
     */
    public function setTextId($ads_text_id)
    {
        $this->ads_text_id = $ads_text_id;
        $this->ads_text = Text::find($this->ads_text_id);
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @param string $href
     */
    public function setHref($href)
    {
        $this->href = $href;
    }

    /**
     * @return int
     */
    public function getVcardId()
    {
        return $this->vcard_id;
    }

    /**
     * @return string
     */
    public function getAdimageHash()
    {
        return $this->adimage_hash;
    }

    /**
     * @param string $adimageHash
     */
    public function setAdimageHash($adimageHash)
    {
        $this->adimage_hash = $adimageHash;
    }



    /**
     * @return bool
     */
    public function isUpdated()
    {
        return $this->is_updated;
    }

    /**
     * @param bool $isUpdated
     */
    public function setUpdated($isUpdated)
    {
        $this->is_updated = $isUpdated;
    }

}
