<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\models\Order;
use common\models\Cart\Product;
use common\models\Geo;
use common\models\Cart;
use frontend\widgets\LeftColumn;
use common\models\shipping\GeoValue;
use frontend\models\AccountSingleton;

/* @var $this yii\web\View */
/* @var Order $order */
/* @var Product[] $products */

if ($order->getId()) {
    $this->title = 'Заказ номер ' . $order->getId() . '-MPD оформлен';

    echo $this->render('/layouts/order', ['order' => $order]);

    $goods = [];
    echo ' <script>';
    echo '
        var gaParams = {
            id:  ' . $order->getId() . ',
            affiliation: "MrPlayDoh",
            revenue: ' . $order->getTotal() . ',
            shipping:' . $order->getOrderShipping()->getCost() . ',
            tax: "0"
        };

        setTimeout(function() {
            ga("require", "ecommerce");
            ga("ecommerce:addTransaction", gaParams);';

    $productsOrder = $order->getOrderProducts();
    foreach ($productsOrder as $product) {
        $categoryTitle = $product->getCatalogProduct()->getMainCategory()->getTitle();
        $goods[] = [
            'id' => $product->getId(),
            'name' => $product->getCatalogProductTitle() . '(' . $product->getId() . ')',
            'model' => $product->getCatalogProductArticle(),
            'category' => $categoryTitle,
            'price' => $product->getPrice(),
            'quantity' => $product->getQuantity(),
        ];
        echo "ga('ecommerce:addItem', {
                        'id': " . $product->getId() . ",
                        'name': '" . $product->getCatalogProductTitle() . "',
                        'sku': '" . $product->getCatalogProductArticle() . "',
                        'category': '" . $categoryTitle . "',
                        'price': " . $product->getPrice() . ",
                        'quantity': " . $product->getQuantity() . "
                    });";
    }
    echo 'ga("ecommerce:send");ga("send", "event", "CART", "ORDER");
            }, 5000);
        var yaParams = {
            order_id: ' . $order->getId() . ',
            order_price: ' . $order->getTotal() . ',
            currency: "RUR",
            exchange_rate: 1,
            goods: ' . json_encode($goods) . '
        };
        setTimeout(function() {
            yaCounter31598273.reachGoal("ORDER", yaParams);
            console.log(yaParams);
        }, 5000);
    </script>
    ';
} else {
    $this->title = 'Оформление заказа';

    $account = AccountSingleton::getInstance()->getAccount(false);
    ?>
    <div id="body">
        <?php
        if ($account && $account->getCountCartProduct() > 0) {
            $account = AccountSingleton::getInstance()->getAccountDate();

            if ($account->isAuto() === false) {
                $order->setPhone(($account->getPhone() ? $account->getPhone() : null));
                $email = $account->getEmail() && $account->getEmail() !== 'feedback@mrplaydoh.ru' ? $account->getEmail() : null;
                $fio = $account->getFio() && $account->getFio() !== 'Покупатель' ? $account->getFio() : null;
                $order->setEmail($email);
                $order->setFio($fio);
            }
            $cart = $account->getCart(false);

            $products = $cart->getCartProducts();

            $isProductNotInStock = false;
            foreach ($products as $product) {
                $catalogProduct = $product->getCatalogProduct();
                if ($product->getQuantity() > $catalogProduct->getQuantity()) {
                    $isProductNotInStock = true;
                    break;
                }
            }

            $geoId = 1;
            if (Yii::$app->request->cookies->getValue('geo_id')) {
                $geoId = (int)Yii::$app->request->cookies->getValue('geo_id');
            }
            $geo = Geo::findOne($geoId);

            $geoValues = GeoValue::find()->where(['geo_id' => $geo->getId(), 'is_active' => true])->orderBy('shipping_id asc, point_id asc')->all();
            $shippingKeys = [];
            foreach ($geoValues as $value) {
                $key = ($value->getShipping()->getId() === 1 ? 'courier' : ($value->getShipping()->getId() === 2 ? 'pickup' : 'post'));
                $shippingKeys[$key] = $value->getPrice();
            }
            echo '<div id="cart_body">';

            if ($isProductNotInStock) {
                echo '<div id="cart_alert" class="alert alert-danger" role="alert">Внимание в корзине присутствуют товары которых нет в наличии!</div>';
            }

            $cartTable = '<table class="table">
                            <tr>
                                <th colspan="2">Товар</th>
                                <th class="right" style="padding-right: 30px;">Цена</th>
                                <th  class="center" style="width: 106px">Кол-во</th>
                                <th  class="right">Стоимость</th>
                                <th></th>
                            </tr>';

            foreach ($products as $product) {
                $catalogProduct = $product->getCatalogProduct();
                $image = $catalogProduct->getMainImage();

                $stQuantityInStock = '';
                if ($product->getQuantity() > $catalogProduct->getQuantity()) {
                    $stQuantityInStock = '<div class="clear"></div>'
                        . '<div class="quantity_in_stock">В наличии ' . $catalogProduct->getQuantity() . ' шт</div>';
                }

                $cartTable .= '<tr id="cart_product_' . $catalogProduct->getId() . '" product_id="' . $catalogProduct->getId() . '">

                                   <td width="120">
                                        ' . Html::a($image->getHtml(['width' => 120, 'height' => 120]), ['product/index', 'id' => $catalogProduct->getId()]) . '
                                   </td>

                                   <td class="cart_product_title">
                                        ' . Html::a($catalogProduct->getTitle(), ['product/index', 'id' => $catalogProduct->getId()]) . '
                                   </td>
                                   <td class="cart_product_price" style="padding-right: 30px;">
                                        ' . $catalogProduct->getPrice() . ' руб
                                   </td>
                                   <td class="td_product_quantity">
                                        <input type="hidden" class="product_quantity" value="' . $catalogProduct->getQuantity() . '"/>
                                        <div class="del"> - </div>
                                        <input type="text" class="cart_product_quantity" disabled="disabled" value="' . $product->getQuantity() . '"/>
                                        <div class="add"> + </div> ' . $stQuantityInStock . '
                                   </td>
                                   <td class="cart_product_total_price">
                                        <span class="total_product_price">' . ($catalogProduct->getPrice() * $product->getQuantity()) . '</span>  руб
                                   </td>
                                   <td>
                                        <div class="remove">&times;</div>
                                   </td>
                               </tr>';
            }
            $cartTable .= '<tr>
                                   <td></td>
                                   <td colspan="2" class="cart_product_total_title">Итого без учета доставки</td>
                                   <td colspan="2" class="cart_product_total">
                                        <span id="total">' . $cart->getTotalPrice() . '</span> руб
                                   </td>
                                   <td>

                                   </td>
                               </tr>';
            $cartTable .= '</table>';
            $cartTable .= '<div class="container_button_order"><div id="go_order" class="btn">Перейти к оформлению</div><div class="btn_help">Это займет не более 2 минут</div></div>';
            $form = ActiveForm::begin(
                [
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-7\">{input}</div>\n<div class=\"error col-md-offset-3 col-md-7\">{error}</div>",
                    ],
                ]);

            //                            <div class="order_account">
            //                                <ul>
            //                                    <li class="active"><span>Я покупаю впервые</span></li>
            //                                    <li><span>Я уже покупал у вас</span></li>
            //                                </ul>
            //                                <div class="clear"></div>
            //                            </div>
            $orderForm = '<div class="order_contact">
                            <div class="order_body">
                                <div class="order_contact">
                                    <h2>Контактная информация</h2>
                                    <div style="position: relative;margin-bottom: 30px !important;">' . $form->field($order, 'phone') . '<div class="order_help_phone">для связи с вами</div></div>
                                    <div style="position: relative;margin-bottom: 30px !important;">' . $form->field($order, 'fio') . '<div class="order_help_fio">для обращения к вам</div></div>
                                    <div style="position: relative;margin-bottom: 30px !important;">' . $form->field($order, 'email') . '<div class="order_help_email">для получения деталий заказа</div></div>
                                    <div class="clear"></div>
                                </div>


                               <div class="order_shipping">
                                    <h2>Доставка в <span id="order_shipping_geo" data-toggle="modal" data-target="#modalGeo">' . $geo->getTitleWhere() . '</span></h2>
                                    <div class="form-group required shipping-radio" id="shipping-courier" ' . (empty($shippingKeys['courier']) ? 'style="display: none"' : '') . '>
                                        <div class="col-md-1">
                                            <input type="radio" name="Order[shipping]" value="courier" class="order-shipping order_shipping_select" id="order-shipping-courier">
                                        </div>
                                        <div class="col-md-7">
                                            <label for="order-shipping-courier" class="control-label">Курьером - <span id="courier_cost">' . (!empty($shippingKeys['courier']) ? $shippingKeys['courier'] : '') . '</span> руб</label>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="form-group required shipping-radio" id="shipping-pickup" ' . (empty($shippingKeys['pickup']) ? 'style="display: none"' : '') . '>
                                        <div class="col-md-1">
                                            <input type="radio" name="Order[shipping]" value="pickup" class="order-shipping order_shipping_select" id="order-shipping-pickup">
                                        </div>
                                        <div class="col-md-7">
                                            <label for="order-shipping-pickup" class="control-label">Самовывоз из пункта выдачи - <span id="pickup_cost">' . (!empty($shippingKeys['pickup']) ? $shippingKeys['pickup'] : '') . '</span> руб</label>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="form-group required shipping-radio" id="shipping-post" ' . (empty($shippingKeys['post']) ? 'style="display: none"' : '') . '>
                                        <div class="col-md-1">
                                            <input type="radio" name="Order[shipping]" value="post" class="order-shipping order_shipping_select" id="order-shipping-post">
                                        </div>
                                        <div class="col-md-7">
                                            <label for="order-shipping-post" class="control-label">Почтой России - от <span id="post_cost">' . (!empty($shippingKeys['post']) ? $shippingKeys['post'] : '') . '</span> руб</label>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>


                                <div class="order_shipping_info" id="info-order-shipping-courier">
                                    <h2>Адрес доставки</h2>
                                    <div class="form-group required">
                                        <div class="col-md-3"><label for="order-address" class="control-label">Адрес доставки</label></div>
                                        <div class="col-md-7"><input type="text" name="Order[courier][address]" class="form-control" id="order-address"></div>
                                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                                        <div class="order_help_phone">улица, дом, корпус, подъезд, квартира, офис, и тд</div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="order_shipping_info" id="info-order-shipping-post">
                                    <h2>Адрес доставки</h2>
                                    <div class="form-group required">
                                        <div class="col-md-3"><label for="order-index" class="control-label">Индекс</label></div>
                                        <div class="col-md-7"><input type="text" name="Order[post][index]" class="form-control" id="order-index"></div>
                                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                                        <div class="order_help_index">индекс вашего почтового отделения</div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group required">
                                        <div class="col-md-3"><label for="order-region" class="control-label">Регион</label></div>
                                        <div class="col-md-7"><input type="text" name="Order[post][region]" class="form-control" id="order-region"></div>
                                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                                        <div class="order_help_region">область, pеспублика, край и тд</div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group required">
                                        <div class="col-md-3"><label for="order-city" class="control-label">Город</label></div>
                                        <div class="col-md-7"><input type="text" name="Order[post][city]" class="form-control" id="order-city"></div>
                                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                                        <div class="order_help_city">название города, населеного пункта, поселка, деревни и тд</div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group required">
                                        <div class="col-md-3"><label for="order-address-post" class="control-label">Адрес доставки</label></div>
                                        <div class="col-md-7"><input type="text" name="Order[post][address]" class="form-control" id="order-address-post"></div>
                                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                                        <div class="order_help_address_post">улица, дом, корпус, подъезд, квартира, офис, и тд</div>
                                    </div>

                                    <div class="clear"></div>
                                    <div class="form-group required">
                                        <div class="col-md-3"><label for="order-fio-post" class="control-label">ФИО Получателя</label></div>
                                        <div class="col-md-7"><input type="text" name="Order[post][fio]" class="form-control" id="order-fio-post"></div>
                                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                                        <div class="order_help_fio_post">фамилия имя отчество получателя заказа</div>
                                    </div>

                                    <div class="clear"></div>
                                </div>

                                <div class="order_shipping order_shipping_info" id="info-order-shipping-pickup">
                                    <h2>Адреса пунктов выдачи</h2>
                                    <div id="container_shipping_pickup">
                                ';

            $geoValues = GeoValue::find()->where(['geo_id' => $geoId, 'shipping_id' => 2, 'is_active' => true])->orderBy('point_id')->all();
            foreach ($geoValues as $value) {
                $orderForm .= '<div class="form-group required">
                                        <div class="col-md-1">
                                            <input type="radio" name="Order[pickup][id]" value="' . $value->getId() . '" id="order-shipping-' . $value->getId() . '" class="order-shipping">
                                        </div>
                                        <div class="col-md-7">
                                            <label for="order-shipping-' . $value->getId() . '" class="control-label">' . $value->getAddress() . '</label>
                                        </div>
                                    </div><div class="clear"></div>';
            }

            $orderForm .= '</div><div class="clear"></div>
                                </div>

                                <div class="order_payment">
                                    <h2>Способ оплаты</h2>
                                    <div class="form-group required">
                                        <div class="col-md-1">
                                            <input type="radio" name="Order[payment]" value="1" id="order-shipping-payment" checked="checked">
                                        </div>
                                        <div class="col-md-7">
                                            <label for="order-shipping-payment" class="control-label">Наличными</label>
                                            <div class="order_help_text">При получение заказа</div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="order_comment">
                                    <h2>Комментарий</h2>
                                    <div class="form-group required">
                                        <div class="col-md-10">
                                            <textarea name="Order[comment]" id="comment" class="form-control" style="width: 600px; height: 100px;"></textarea>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="order_summ">
                                    <div id="total_summ"><span id="total_summ_only">' . $cart->getTotalPrice() . '</span> <span>руб.</span></div>
                                    <h2>Итого с учетом доставки:</h2>
                                </div>
                                <div class="order_button">
                                  ' . Html::submitButton('Подтвердить заказ', ['class' => 'btn']) . '
                                  <div class="clear"></div>
                                </div>
                            </div>
                            <div class="order_dignity">
                                <div class="dignity" id="dignity1">
                                    <span>Доставка в срок<br/>и по всей России</span>
                                </div>
                                <div class="clear"></div>
                                <div class="dignity" id="dignity3">
                                    <span>Гарантия<br/>подлинности</span>
                                </div>
                                <div class="clear"></div>
                                <div class="dignity" id="dignity4">
                                    <span>Оперативная<br/>обработка</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>';

            ?>

            <div class="modal fade" id="modalGeo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document" style="width: 800px">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: 0 solid #ffffff;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="myModalLabel">Ваш город</h3>

                            <div>
                                Выберите ваш город, в котором собираетесь получать товары.<br/>
                                От выбора зависит стоимость товаров и доставки.
                            </div>
                        </div>
                        <div class="modal-body geo-body">
                            <?php
                            $geos = Geo::find()->orderBy(['sort'=> 'asc', 'title' => 'asc'])->all();
                            $word = '';
                            echo '<ul>';
                            $i = 0;
                            foreach ($geos as $geo) {
                                $w = mb_substr($geo->getTitle(), 0, 1, 'UTF-8');
                                $title = $geo->getTitle();
                                if ($word !== $w) {
                                    if ($i >= 7) {
                                        $i = 0;
                                        echo '</ul><ul>';
                                    }

                                    $word = $w;
                                    $title = '<div class="geo_word">' . $word . '</div>' . $title;
                                }
                                echo '<li class="select_geo" geo_id="' . $geo->getId() . '">' . $title . '</li>';

                                $i++;
                            }
                            echo '</ul>';
                            ?>
                        </div>
                        <div class="clear"></div>
                        <br/>
                    </div>
                </div>
            </div>
            <?php
            if ($error) {
                echo '<div class="alert alert-danger" role="alert">' . $error . '</div>';
            }

            echo \yii\bootstrap\Tabs::widget([
                'items' => [
                    [
                        'label' => 'Корзина',
                        'content' => $cartTable,
                        'active' => true
                    ],
                    [
                        'label' => 'Оформление заказа',
                        'content' => $orderForm,
                        'active' => false
                    ],
                ],
            ]);
            echo '</div>';
            ActiveForm::end();

        } else {
            ?>
            <div id="left_col">
                <?= LeftColumn::widget(); ?>
            </div>

            <div id="center_col">
                <div id="cart_null">
                    <h2>В корзине пока пусто</h2>

                    <div class="description">
                        Чтобы добавить товары в корзину<br/>
                        воспользуйтесь каталогом нашего магазина.
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
    <div class="clear"></div>
<?php
}