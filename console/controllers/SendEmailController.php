<?php
namespace console\controllers;

use common\models\queue\Email;
use Yii;
use yii\base\Exception;
use yii\validators\EmailValidator;

class SendEmailController extends AbstractController
{

    /**
     * Отправка писем из очереди
     */
    public function actionSend()
    {
        $validator = new EmailValidator();

        $mailer = Yii::$app->mailer;
        $queueEmails = Email::find()->where(['is_done' => false])->orderBy('id')->all();
        foreach($queueEmails as $email) {
            $transaction = Yii::$app->db->beginTransaction();
            try{

                $subject = '';
                if ($validator->validate($email->getEmail())) {
                   $mailer->compose()
                        ->setTo($email->getEmail())
                        ->setFrom([$mailer->getTransport()->getUsername() => 'Мистер Плей До'])
                        ->setSubject($email->getSubject())
                        ->setHtmlBody($email->getBody())
                        ->send();

                } else {
                    $subject = ' Емайл не валиден';
                }

                $mailer->compose()
                    ->setTo(Yii::$app->params['supportEmail'])
                    ->setFrom([$mailer->getTransport()->getUsername() => 'Мистер Плей До'])
                    ->setSubject($email->getSubject() . $subject)
                    ->setHtmlBody($email->getBody())
                    ->send();

                $email->setDone(true);
                if($email->save() === false) {
                    throw new Exception(json_encode($email->getFirstErrors()));
                }
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
                continue;
            }
        }

        return self::EXIT_CODE_NORMAL;

    }

}
