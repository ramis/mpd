<?php
namespace common\models\behavior;

use common\models\Order as ModelOrder;

trait Order
{
    /**
     * @var ModelOrder
     */
    private $order;

    /**
     * @return ModelOrder
     */
    public function getOrder()
    {
        if ($this->order === null && $this->order_id !== null) {
            $this->order = ModelOrder::find()->where(['id' => $this->order_id])->one();
        }

        if ($this->order === null) {
            $this->order = new ModelOrder();
        }

        return $this->order;
    }

    /**
     * @param ModelOrder $order
     */
    public function setOrder(ModelOrder $order)
    {
        $this->order = $order;
        $this->order_id = $order->getId();
    }

}