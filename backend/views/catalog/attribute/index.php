<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\catalog\Attribute;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">
    <h1>Характеристики товара</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'sort',
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Attribute $attribute) {
                    return Html::a('Редактировать', ['edit', 'id' => $attribute->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
