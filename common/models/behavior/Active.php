<?php

namespace common\models\behavior;

trait Active
{
    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->is_active;
    }

    /**
     * @param boolean $is_active
     */
    public function setActive($is_active)
    {
        $this->is_active = $is_active;
    }

}