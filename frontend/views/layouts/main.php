<?php

use yii\helpers\Html;
use yii\web\View;
use frontend\assets\AppAsset;
use frontend\widgets\Header;
use frontend\widgets\Footer;
use common\tools\Tools;

/* @var View $this */
/* @var $content string */
AppAsset::register($this);

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name='yandex-verification' content='58d3469644ee1294'/>
        <meta name="google-site-verification" content="qWGr9FC2e7BzwxgWyOQCqzvuwvvMIYZE07I-dzNArOc"/>
        <meta name='wmail-verification' content='c14bae0cb64ea8fa8ea1d777638b3f83'/>
        <meta name="msvalidate.01" content="B34C7FACD1DD555766C2A2FA2D48FCC8"/>

        <link rel="icon" type="image/png" href="/favicon.png"/>
        <title><?= Html::encode($this->title) ?></title>

        <?php
        list($url, $query) = Tools::parseUrl($_SERVER['REQUEST_URI']);
        if (!empty($query)) {
            echo '<link rel="canonical" href="' . $url . '"/>';
        }
        ?>

        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <?php
        if(Yii::$app->controller instanceof \frontend\controllers\CartController){
    ?>

            <header  style="height: 100px;">
                <div id="header" style="height: 80px;">
                    <div id="header_body">
                        <?= Html::a('<div id="logo_small"><div>Интернет-магазин пластилина</div></div>', \Yii::$app->homeUrl); ?>
                        <div id="block_phone_search">
                            <div id="phone_mks_spb">
                                <div class="phone_num">8 495 278-08-20</div>
                                <div class="phone_help">для Москвы</div>
                            </div>
                            <div id="phone_all">
                                <div class="phone_num">8 812 670-07-90</div>
                                <div class="phone_help">для Санкт-Петербурга</div>
                            </div>
                        </div>
                        <div id="call_time">
                            <div class="header_title">
                                КРУГЛОСУТОЧНО
                            </div>
                            <div class="call_time_help">и без выходных</div>
                        </div>
                    </div>
                </div>
            </header>
        <?php
    }else {
        if ($this->title !== '404') {
            echo Header::widget();
        }
    }

    ?>

    <div class="clear"></div>

    <section>
        <?= $content ?>
    </section>

    <div class="clear"></div>

    <?php if ($this->title !== '404') {
        echo Footer::widget();
    } ?>

    <?php $this->endBody(); ?>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//w' + 'w' + 'w.goo' + 'gle-ana' + 'lyti' + 'cs.c' + 'om/anal' + 'ytics.js', 'ga');

        ga('create', 'UA-65567218-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter31598273 = new Ya.Metrika({
                        id: 31598273,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "htt" + "ps:" + "//mc.ya" + "nd" + "ex.ru/" + "metrik" + "a/wa" + "tch.j" + "s";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <!-- /Yandex.Metrika counter -->
    <!--LiveInternet counter-->
    <script type="text/javascript"><!--
        document.write("<a h" + "ref='//w" + "w" + "w.liv" + "ein" + "tern" + "et" + ".ru" + "/click' " +
            "target=_blank><img " + "sr" + "c=" + "'//co" + "unter.y" + "ad" + "ro.ru" + "/hit" + "?t26.6;r" +
            escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
            ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
            ";" + Math.random() +
            "' alt='' title='LiveInternet: показано число посетителей за" +
            " сегодня' " +
            "border='0' width='0' height='0' style='position: absolute;'><\/a>")
        //--></script>
    <!--/LiveInternet-->
    <!-- Rating@Mail.ru counter -->
    <script type="text/javascript">
        var _tmr = _tmr || [];
        _tmr.push({id: "2685089", type: "pageView", start: (new Date()).getTime()});
        (function (d, w, id) {
            if (d.getElementById(id)) return;
            var ts = d.createElement("script");
            ts.type = "text/javascript";
            ts.async = true;
            ts.id = id;
            ts.src = (d.location.protocol == "ht" + "tps:" ? "ht" + "tps:" : "h" + "ttp:") + "//t" + "op" + "-fwz1" + ".m" + "a" + "il.ru" + "/js/" + "cod" + "e.j" + "s";
            var f = function () {
                var s = d.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(ts, s);
            };
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "topmailru-code");
    </script>
    <noscript>
        <div style="position:absolute;left:-10000px;">
            <img src="//top-fwz1.mail.ru/counter?id=2685089;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru"/>
        </div>
    </noscript>
    <!-- //Rating@Mail.ru counter -->
    </body>
    </html>
<?php $this->endPage();


/*
 * UTM
 *
 * utm_source (источник перехода: direct, begun);
 * utm_medium (средство маркетинга: cpc, баннер, электронное сообщение);
 * utm_campaign (название проводимой рекламной кампании);
 * utm_content (объявление);
 * utm_term (ключевые слова).
 */
$utm = [];
if (!empty($_GET['utm_source'])) {
    $utm['Utm']['utm_source'] = $_GET['utm_source'];
}
if (!empty($_GET['utm_medium'])) {
    $utm['Utm']['utm_medium'] = $_GET['utm_medium'];
}
if (!empty($_GET['utm_campaign'])) {
    $utm['Utm']['utm_campaign'] = $_GET['utm_campaign'];
}
if (!empty($_GET['utm_content'])) {
    $utm['Utm']['utm_content'] = $_GET['utm_content'];
}
if (!empty($_GET['utm_term'])) {
    $utm['Utm']['utm_term'] = $_GET['utm_term'];
}
if (!empty($utm)) {
    $key = md5(implode('', $utm['Utm']));
    $session = Yii::$app->session;
    if (empty($session['utm'][$key])) {
        $utm['Utm']['created_uts'] = time();
        $sessionUtm = $session['utm'];
        $sessionUtm[$key] = $utm;
        Yii::$app->session->set('utm', $sessionUtm);
    }
}
if (!empty($_SERVER['HTTP_REFERER'])) {
    $referer = $_SERVER['HTTP_REFERER'];
    $arr = parse_url($referer);
    if (!empty($referer['host']) && (stripos($arr['host'], Yii::$app->params['DOMAIN']) === false)) {
        $referers = Yii::$app->session->get('referers');
        $referers[] = urldecode($referer);
        Yii::$app->session->set('referers', $referers);
    }
}