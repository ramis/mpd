<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Yml;

/* @var yii\web\View $this */
/* @var $yml Yml */

?>
<div class="site-index row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($yml->getId() ? Url::to(['edit', 'id' => $yml->getId()]) : Url::to(['create'])),
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<div class="button">' . Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) . '</div>';

        echo '<fieldset><legend>'.($yml->getId() ? $yml->getTitle() : 'Новый Yml').'</legend>';
        echo $form->field($yml, 'title');
        echo $form->field($yml, 'file');
        echo $form->field($yml, 'name');
        echo $form->field($yml, 'company');
        echo $form->field($yml, 'shipping_cost');
        echo $form->field($yml, 'export_product_in_stock')->checkbox();
        echo $form->field($yml, 'is_short')->checkbox();
        echo $form->field($yml, 'url_params');


        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
