<?php
namespace common\models\media;

use common\models\behavior\Id;
use common\models\behavior\Sort;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\models\Media;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%media_files}}".
 *
 * @property integer $id
 * @property integer $media_id
 * @property string $hash
 * @property integer $sort
 * @property integer $created_uts
 * @property integer $type
 *
 * @property Media $media
 */
class Files extends ActiveRecord
{
    use Id;
    use Sort;

    const TYPE_IMAGE = 10;
    const TYPE_VIDEO = 20;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%media_files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'hash', 'sort', 'type'], 'required'],
            [['media_id', 'sort', 'created_uts', 'type'], 'integer'],
            [['hash'], 'string', 'max' => 255]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_uts'],
                ],
                'value' => function () {
                    return time();
                },
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'media_id' => Yii::t('app', 'Media ID'),
            'hash' => Yii::t('app', 'Hash'),
            'sort' => Yii::t('app', 'Sort'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return Media
     */
    public function getMedia()
    {
        if ($this->media === null && $this->media_id !== null) {
            $this->media = Media::find()->where(['id' => $this->media_id])->one();
        }

        return $this->media;
    }

    /**
     * @param Media $media
     */
    public function setMedia(Media $media)
    {
        $this->media_id = $media->getId();
        $this->media = $media;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Remove
     */
    public function remove()
    {
        if (is_file(Yii::$app->params['imagesPath'] . $this->getHash())) {
            unlink(Yii::$app->params['imagesPath'] . $this->getHash());
        }

        $this->delete();
    }

    public function getHref($params = [])
    {
        if ($this->type === self::TYPE_IMAGE) {
            $width = !empty($params['width']) ? (int)$params['width'] : 0;
            $height = !empty($params['height']) ? (int)$params['height'] : 0;
            $id = !empty($params['id']) ? $params['id'] : 'img_' . $this->getId();

            if ($width === 0 || $height === 0) {
                return Yii::$app->params['HTTP_IMAGE'] . $this->getHash();
            } else {
                $httpImage = !empty($params['isHigh']) ? Yii::$app->params['HTTP_IMAGE'] :
                    Yii::$app->params['HTTP_IMAGE_P_' . $width . '_' . $height];
                return $httpImage . $this->getHash();
            }
        }
    }

    /**
     * @param array $params
     * @return string
     */
    public function getHtml($params = [])
    {
        if ($this->type === self::TYPE_IMAGE) {
            $width = !empty($params['width']) ? (int)$params['width'] : 0;
            $height = !empty($params['height']) ? (int)$params['height'] : 0;
            $id = !empty($params['id']) ? $params['id'] : 'img_' . $this->getId();


            $param['width'] = $width;
            $param['height'] = $height;
            $param['id'] = $id;

            if (!empty($params['schema'])) {
                $param['width'] = $width > $height ? $height : $width;
                $param['height'] = $width > $height ? $height : $width;
                $param['itemprop'] = "image";
            }

            if($width === 0) {
                unset($param['width']);
            }
            if($height === 0) {
                unset($param['height']);
            }
            return Html::img($this->getHref($params), $params);
        }
    }
}
