<?php
use yii\helpers\Html;
use \common\models\Order;

/**
 * @var Order $order
 */
if (!($order instanceof Order)) {
    return;
}
?>
<div id="body" style="position: relative;">

    <div id="cart_body" class="order" style="font-size: 14px;padding: 20px;border: 1px solid #e6e6e6;">
        <h1 style="font-size: 25px !important;margin: 10px 0 !important;">Спасибо за заказ!</h1>

        <div class="order_title" style="font-size: 18px;">
            Номер заказа: <span class="order_number" style="color: #df1811;"><?= $order->getId() ?>-MPD</span> <span class="order_date" style="color: #888888;font-weight: normal;">от <?= Yii::$app->formatter->asDate($order->getCreatedUts(), 'php:d.m.Y'); ?></span>
        </div>
        <div>
            В ближейшее время наш менеджер позвонит Вам по телефону <span class="order_phone"><b><?= $order->getPhone() ?></b></span> для подверждения заказа
        </div>

        <div class="order_title" style="font-weight: bold;">
            Детали заказа
        </div>
        <?php
        $products = $order->getOrderProducts();
        echo '<table class="table" style="margin-bottom: 20px;max-width: 100%;width: 100%;background-color: transparent;border-collapse: collapse;border-spacing: 0;">
                                <tr>
                                    <th colspan="2" style="color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;text-align: left;">Товар</th>
                                    <th class="center" style="text-align:center;color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">Количество</th>
                                    <th class="right" style="text-align: right;color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">Стоимость</th>
                                </tr>';
        $total = 0;
        foreach ($products as $product) {
            $catalogProduct = $product->getCatalogProduct();
            $image = $catalogProduct->getMainImage();

            echo '<tr>
                               <td width="120" style="color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">
                                    ' . Html::a($image->getHtml(['width' => 120, 'height' => 120]), ['product/index', 'id' => $catalogProduct->getId()]) . '
                               </td>

                               <td class="cart_product_title" style="color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">
                                    ' . Html::a($catalogProduct->getTitle(), \yii\helpers\Url::to(['product/index', 'id' => $catalogProduct->getId()], true), ['style' => 'color: #55b6e4;']) . '<br/>
                                    <span style="color: #888888;font-weight: normal;">' . $catalogProduct->getArticle() . ' </span>
                               </td>
                               <td class="td_product_quantity" style="text-align: center;color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">
                                    ' . $product->getQuantity() . ' x ' . $catalogProduct->getPrice() . ' руб
                               </td>
                               <td class="cart_product_total_price" style="text-align: right;color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">
                                    <span class="total_product_price">' . ($catalogProduct->getPrice() * $product->getQuantity()) . '</span>  руб
                               </td>
                    </tr>';
            $total += ($catalogProduct->getPrice() * $product->getQuantity());
        }
        echo '<tr>
                           <td style="color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;"></td>
                           <td colspan="2" class="cart_product_total_title" style="color: #888888;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">Итоговая стоимость товаров:</td>
                           <td colspan="2" class="cart_product_total" style="color: #000000;text-align: right;font-weight: normal;border-top: 1px solid #ddd;line-height: 1.42857;padding: 8px;vertical-align: top;">
                                <span id="total" style="font-size: 18px;">' . $total . '</span> руб
                           </td>
                       </tr></table>';
        ?>

        <div class="order_contact" style="background-color: #f7f7f7;border-radius: 2px;margin-bottom: 20px;padding: 10px 40px;">
            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Имя и фамилия</div>
            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;"><?= $order->getFio() ?></div>

            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Телефон</div>
            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;"><?= $order->getPhone() ?></div>

            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;overflow: hidden;white-space: nowrap;width: 35%;margin-bottom: 20px;float:left;">Электронная почта</div>
            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;overflow: hidden;white-space: nowrap;margin-bottom: 20px;"><?= $order->getEmail() ?></div>
            <?php
            $type = $order->getOrderShipping()->getType();
            switch($type){
                case \common\models\order\Shipping::TYPE_COURIER:
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Способ доставки</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">Курьерская доставка</div>';

                    $shippingValue = $order->getOrderShipping()->getOrderShippingValue();
                    $geoTitle = $shippingValue->getShippingGeoValue()->getGeo()->getTitle();
                    echo '<div class="col-md-6" style=" min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Адрес доставки</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">'.$geoTitle . ', ' . $shippingValue->getAddress() .'</div>';

                    break;
                case \common\models\order\Shipping::TYPE_POST:
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Способ доставки</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">Почта России</div>';

                    $shippingValue = $order->getOrderShipping()->getOrderShippingValue();
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Индекс</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">'.$shippingValue->getIndex() . '</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Регион</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">'.$shippingValue->getRegion() . '</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Город</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">'.$shippingValue->getCity() . '</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Адрес</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">'.$shippingValue->getAddress() . '</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Фио получателя</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">'.$shippingValue->getFio() . '</div>';

                    break;
                case \common\models\order\Shipping::TYPE_PICKUP:
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Способ доставки</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">Самовывоз</div>';

                    $shippingValue = $order->getOrderShipping()->getOrderShippingValue()->getShippingGeoValue();
                    $geoTitle = $shippingValue->getGeo()->getTitle();
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Адрес пункта выдачи</div>';
                    echo '<div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">'.$geoTitle . ', ' . $shippingValue->getAddress() . ' №' . $shippingValue->getPointId() . '</div>';

                    break;
            }
            ?>

            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;width: 35%;margin-bottom: 20px;float:left;">Стоимость доставки</div>
            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;overflow: hidden;white-space: nowrap;margin-bottom: 20px;"><?= $order->getOrderShipping()->getCost() ?> руб</div>

            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Способ оплаты</div>
            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;">наличными</div>

            <div class="col-md-6" style="min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative; overflow: hidden;white-space: nowrap;margin-bottom: 10px;width: 35%;float:left;">Итого</div>
            <div class="col-md-6" style="font-weight: bold;min-height: 1px;padding-left: 15px;padding-right: 15px;position: relative;float: left;width: 50%;margin-bottom: 10px;overflow: hidden;white-space: nowrap;"><?= $order->getTotal() ?>  руб</div>
            <div class="clear" style="clear: both;float: none;"></div>
        </div>

        <?php
        if (!empty($signature)) {
            echo $signature;
        }
        ?>
    </div>
</div>