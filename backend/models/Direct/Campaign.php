<?php

namespace backend\models\direct;

use common\models\behavior\Direct;
use common\models\behavior\Id;
use common\models\behavior\Name;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%direct_campaign}}".
 *
 * @property integer $id
 * @property integer $direct_id
 * @property string $name
 *
 * @property Adgroup[] $adgroups
 */
class Campaign extends ActiveRecord
{
    use Id;
    use Direct;
    use Name;

    const BELL_NEED_MANUAL_CHECK = 10;

    const STATE_STOP = 20;
    const STATE_ACTIVE = 30;
    const STATE_STOPPED = 40;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%direct_campaign}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direct_id', 'name'], 'required'],
            [['direct_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'direct_id' => Yii::t('app', 'Direct ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return Adgroup[]
     */
    public function getAdgroup()
    {
        return $this->hasMany(Adgroup::className(), ['campaign_id' => 'id']);
    }

    public function analizeStatDirect(array $responce) {
        if(count($responce) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function mustBeStopped()
    {
        return false;
    }

    public function setBell($bell)
    {
        return false;
    }
    public function setState($stop)
    {
        return false;
    }
}
