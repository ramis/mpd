<?php
use yii\grid\GridView;
use yii\helpers\Html;
use backend\models\direct\ads\Text;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">
    <div class="right"><?=Html::a('Добавить', \yii\helpers\Url::to(['direct/ads/text/create'])); ?></div>

    <h1>Текст для объявлений</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'description',
            'updated_uts:datetime',
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Text $text) {
                    return Html::a('Редактировать', ['edit', 'id' => $text->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
