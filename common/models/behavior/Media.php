<?php
namespace common\models\behavior;

use common\models\Media as ModelMedia;

trait Media
{
    /**
     * @var ModelMedia
     */
    private $media;

    /**
     * @return ModelMedia
     */
    public function getMedia()
    {
        if ($this->media === null && $this->media_id !== null) {
            $this->media = ModelMedia::find()->where(['id' => $this->media_id])->one();
        }

        if ($this->media === null) {
            $this->media = new ModelMedia();
        }

        return $this->media;
    }

    /**
     * @param ModelMedia $media
     */
    public function setMedia(ModelMedia $media)
    {
        $this->media = $media;
        $this->media_id = $media->getId();
    }


    public function getMainImage()
    {
        return $this->getMedia()->getMainImage();
    }

}