<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\behavior\Sort;
use common\models\shipping\GeoValue;
use common\models\behavior\GeoObject;

/**
 * This is the model class for table "{{%geo}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $sort
 * @property string $title_where
 * @property string $geo_object
 *
 * @property GeoValue[] $shippingGeoValues
 */
class Geo extends ActiveRecord
{
    use Id;
    use Title;
    use Sort;
    use GeoObject;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'sort'], 'required'],
            [['sort'], 'integer'],
            [['title', 'title_where'], 'string', 'max' => 255],
            [['geo_object'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'sort' => Yii::t('app', 'Sort'),
            'title_where' => Yii::t('app', 'Title Where'),
            'geo_object' => Yii::t('app', 'Geo Object'),
        ];
    }

    /**
     * @return GeoValue
     */
    public function getShippingGeoValues()
    {
        return $this->hasMany(GeoValue::className(), ['geo_id' => 'id']);
    }

    /**
     *
     */
    public function getTitleWhere()
    {
        return $this->title_where;
    }

}
