<?php

namespace common\models\order\shipping;

use common\models\behavior\Id;
use common\models\behavior\OrderShipping;
use common\models\behavior\ShippingGeoValue;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_shipping_pickup}}".
 *
 * @property integer $id
 * @property integer $order_shipping_id
 * @property integer $shipping_geo_value_id
 */
class Pickup extends ActiveRecord
{
    use Id;
    use OrderShipping;
    use ShippingGeoValue;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_shipping_pickup}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_shipping_id', 'shipping_geo_value_id'], 'required'],
            [['order_shipping_id', 'shipping_geo_value_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_shipping_id' => Yii::t('app', 'Order Shipping ID'),
            'shipping_geo_value_id' => Yii::t('app', 'Shipping Geo Value ID'),
        ];
    }

    /**
     * @return string
     */
    public function getText()
    {
        $geoTitle = $this->getShippingGeoValue()->getGeo()->getTitle();
        $typeTitle = $this->getShippingGeoValue()->getShipping()->getTitle();

        return $geoTitle . ' ' . $typeTitle . ' (' . $this->getShippingGeoValue()->getAddress() . ')';
    }

}
