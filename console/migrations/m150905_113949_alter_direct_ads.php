<?php

use yii\db\Schema;
use yii\db\Migration;

class m150905_113949_alter_direct_ads extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%direct_ads}}', 'is_updated', Schema::TYPE_BOOLEAN . ' NOT NULL');
        $this->addColumn('{{%direct_ads}}', 'ads_text_id', Schema::TYPE_INTEGER . ' NOT NULL');

        $this->dropColumn('{{%direct_ads}}', 'text');

        $this->addForeignKey('FK_direct_ads_text_id', '{{%direct_ads}}', 'ads_text_id', '{{%direct_ads_text}}', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function safeDown()
    {
    }
}
