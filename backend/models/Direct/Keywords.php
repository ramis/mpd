<?php

namespace backend\models\direct;

use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Adgroup;
use common\models\behavior\Direct;
use common\models\behavior\Id;

/**
 * This is the model class for table "{{%direct_keywords}}".
 *
 * @property integer $id
 * @property integer $direct_id
 * @property integer $adgroup_id
 * @property string $keyword
 * @property string $userparam1
 * @property string $userparam2
 *
 * @property Adgroup $adgroup
 */
class Keywords extends ActiveRecord
{
    use Id;
    use Direct;
    use Adgroup;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%direct_keywords}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adgroup_id', 'keyword'], 'required'],
            [['direct_id', 'adgroup_id'], 'integer'],
            [['keyword', 'userparam1', 'userparam2'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'direct_id' => Yii::t('app', 'Direct ID'),
            'adgroup_id' => Yii::t('app', 'Adgroup ID'),
            'keyword' => Yii::t('app', 'Keyword'),
            'userparam1' => Yii::t('app', 'Userparam1'),
            'userparam2' => Yii::t('app', 'Userparam2'),
        ];
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getUserparam1()
    {
        return $this->userparam1;
    }

    /**
     * @param string $userparam1
     */
    public function setUserparam1($userparam1)
    {
        $this->userparam1 = $userparam1;
    }

    /**
     * @return string
     */
    public function getUserparam2()
    {
        return $this->userparam2;
    }

    /**
     * @param string $userparam2
     */
    public function setUserparam2($userparam2)
    {
        $this->userparam2 = $userparam2;
    }
}
