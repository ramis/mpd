<?php

use yii\db\Schema;
use yii\db\Migration;

class m150830_172129_direct_campaign extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%direct_campaign}}', [
            'id' => Schema::TYPE_PK,
            'direct_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
    }

    public function safeDown()
    {
    }

}
