<?php
use yii\helpers\Html;
use common\models\catalog\Product;
use frontend\widgets\LeftColumn;
use common\models\catalog\tag\Group;
use common\tools\Tools;

/* @var $this yii\web\View */
$this->title = 'Быстрый поиск';

?>

<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col">

        <?php //BreadCrumbs::widget(['breadCrumbs' => $category]);
        ?>

        <div id="site_index" class="tags_index">
            <h1>Поиск по параметрам</h1>

            <div class="row tags_title">
                <div class="col-md-4">Название параметра</div>
                <div class="col-md-8">Количество значений</div>
            </div>
            <?php
                $groups = Group::find()->all();
                foreach ($groups as $group) {
                    $tags = $group->getTags();
                    echo '<div class="row">';
                    echo '<div class="col-md-4"><div class="group_title">'.$group->getTitle().'</div>';
                    echo '<div class="group_count">'.
                        Tools::humanPluralForm($group->getCountProduct(), ['товар','товара','товаров'])
                        .'&nbsp;&nbsp;&nbsp;'.
                        Tools::humanPluralForm(count($tags), ['значение','значения','значений']).'</div></div>';
                    echo '<div class="col-md-8">';
                    foreach ($tags as $tag) {
                        echo Html::a($tag->getTitle(), ['tags/tag', 'url' => $tag->getUrl()]);
                    }
                    echo '</div></div>';

                }
            ?>
        </div>
        <div class="clear"></div>

        <?php
            if (false) {
                echo '<div class="description"><p>'.$description.'</p>';
                echo '<a href="https://plus.google.com/u/1/108888585391425984422/posts" rel="author">Mr Play-Doh</a></div>';
            }
        ?>

    </div>
</div>