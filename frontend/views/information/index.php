<?php
use common\models\Information;
use frontend\widgets\LeftColumn;
use frontend\widgets\BreadCrumbs;

/* @var $this yii\web\View */
/* @var Information $information */

$this->title = ($information->getSeoTitle() ? $information->getSeoTitle() : $information->getTitle());
$this->params['page'] = $information;

?>

<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col">

        <?= BreadCrumbs::widget(['breadCrumbs' => $information]); ?>

        <div id="information">
            <?php
                echo '<h1>' . $information->getTitle() . '</h1>';

               if ($information->getDescription()) {
                   echo '<div id="description">' . $information->getDescription() . '</div>';
                }
            ?>
        </div>
    </div>
</div>