<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use common\models\catalog\Category;
use common\models\catalog\Product;

/**
 * Site controller
 */
class CategoryController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionIndex($id)
    {

        /**
         * @var Category $category
         */
        $category = Category::find()->where(['id' => (int)$id, 'is_published' => true, 'is_special' => false])->one();

        if ($category === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['error']));
        }

        $page = (int)Yii::$app->request->get('page', 1);
        $limit = (int)Yii::$app->request->get('limit', 12);
        $sort = Yii::$app->request->get('sort', 'price');
        $order = Yii::$app->request->get('order', 'asc');

        Yii::$app->set('category', $category);

        return $this->render('index', ['category' => $category, 'page' => $page, 'sort' => $sort, 'order' => $order, 'limit' => $limit]);
    }

}
