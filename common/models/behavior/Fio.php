<?php

namespace common\models\behavior;

trait Fio
{
    /**
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     * @return $this
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
        return $this;
    }

}