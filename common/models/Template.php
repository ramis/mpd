<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%template}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $alias
 * @property string $subject
 * @property string $message
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'alias', 'subject', 'message'], 'required'],
            [['id', 'alias'], 'integer'],
            [['message'], 'string'],
            [['title', 'subject'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'subject' => 'Subject',
            'message' => 'Message',
        ];
    }
}
