<?php

namespace common\models;

use common\models\behavior\Id;
use common\models\behavior\Name;
use common\models\behavior\Title;
use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%yml}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $file
 * @property string $name
 * @property string $company
 * @property integer $shipping_cost
 * @property boolean $export_product_in_stock
 * @property boolean $is_short
 * @property string $url_params
 */
class Yml extends ActiveRecord
{
    Use Id;
    Use Title;
    Use Name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%yml}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'file', 'name', 'company', 'shipping_cost', 'export_product_in_stock', 'url_params', 'is_short'], 'required'],
            [['shipping_cost'], 'integer'],
            [['export_product_in_stock', 'is_short'], 'boolean'],
            [['title', 'file', 'name', 'company', 'url_params'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'file' => Yii::t('app', 'File'),
            'name' => Yii::t('app', 'Name'),
            'company' => Yii::t('app', 'Company'),
            'shipping_cost' => Yii::t('app', 'Shipping Cost'),
            'export_product_in_stock' => Yii::t('app', 'Export Product In Stock'),
            'is_short' => Yii::t('app', 'Is short'),
            'url_params' => Yii::t('app', 'Url Params'),
        ];
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getUrlParams()
    {
        return $this->url_params;
    }

    /**
     * @param string $urlParams
     */
    public function setUrlParams($urlParams)
    {
        $this->url_params = $urlParams;
    }

    /**
     * @return int
     */
    public function getShippingCost()
    {
        return $this->shipping_cost;
    }

    /**
     * @param int $shippingCost
     */
    public function setShippingCost($shippingCost)
    {
        $this->shipping_cost = $shippingCost;
    }

    /**
     * @return bool
     */
    public function getExportProductInStock()
    {
        return $this->export_product_in_stock;
    }

    /**
     * @param bool $exportProductInStock
     */
    public function setExportProductInStock($exportProductInStock)
    {
        $this->export_product_in_stock = $exportProductInStock;
    }

    /**
     * @return bool
     */
    public function isShort()
    {
        return $this->is_short;
    }

    /**
     * @param bool $isShort
     */
    public function setShort($isShort)
    {
        $this->is_short = $isShort;
    }

}
