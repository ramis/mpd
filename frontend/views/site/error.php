<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
Yii::$app->response->setStatusCode(404, 'Not Found');
$this->title = '404';
?>
<div id="body" class="error_404">
    <div class="logo">
        <?= Html::a('<div id="logo"></div>', \Yii::$app->homeUrl); ?>
    </div>
    <div class="error_title">
        Ошибка 404
    </div>
    <div class="error_body">
        Неправильно набран адрес, или такой страницы на сайте не существует
    </div>
    <div class="error_link">
        <?= Html::a('Вернуться на главную', \Yii::$app->homeUrl); ?>
    </div>
</div>
<div class="error_delimiter">
</div>
<div class="error_404">
    <div class="product_list_small">
    <h3>Вас могут заинтерисовать</h3>
    <?php
        $category = \common\models\catalog\Category::findOne(10);
        $products = $category->getProductsInStock(6);
        foreach ($products as $prod) {
            echo $this->render('/layouts/productItemSmall', ['product' => $prod]);
        }
        echo '</div><div class="clear"></div>';
    ?>
    </div>
</div>