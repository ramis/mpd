<?php
namespace backend\controllers;

use common\models\Geo;
use common\models\Shipping;
use common\models\shipping\GeoValue;
use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/**
 * Shipping controller
 */
class ShippingController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Geo::find()->orderBy(['sort'=> 'asc', 'title' => 'asc']),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
    }

    /**
     * Geo list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Geo create
     *
     * @return string
     */
    public function actionCreate()
    {
        $geo = new Geo();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            if(self::fillSaveModel($geo)) {
                return Yii::$app->getResponse()->redirect(Url::to(['shipping/index']));
            }
            $errors = $geo->getErrors();
        }
        return $this->render('form', ['geo' => $geo, 'errors' => $errors]);
    }

    /**
     * Geo create
     *
     * @return string
     */
    public function actionEdit($id)
    {
        /** @var Geo $geo */
        $geo = Geo::findOne((int)$id);

        if ($geo === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['shipping']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            if(self::fillSaveModel($geo)) {
                return Yii::$app->getResponse()->redirect(Url::to(['shipping/index']));
            }
            $errors = $geo->getErrors();
        }

        return $this->render('form', ['geo' => $geo, 'errors' => $errors]);
    }

    private static function fillSaveModel(Geo $geo)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $geo->load($_POST);
            if ($geo->save()) {

                if (!empty($_POST['Geo']['courier']['price'])) {
                    $courier = $_POST['Geo']['courier'];
                    $courierValue = null;
                    if (!empty($courier['id'])) {
                        $courierValue = GeoValue::findOne((int)$courier['id']);
                    }
                    $courierValue = $courierValue ? $courierValue : new GeoValue();
                    $courierValue->setPrice($courier['price']);
                    $courierValue->setGeo($geo);
                    $courierValue->setPointId(0);
                    $courierValue->setAddress('');
                    $courierValue->setDescription('');
                    $courierValue->setShipping(Shipping::findOne(Shipping::SHIPPING_COURIER));

                    if($courierValue->save() === false){
                        var_dump($courierValue->getFirstErrors());die;
                    }
                } else {
                    //Удаление
                }
                if (!empty($_POST['Geo']['post']['price'])) {
                    $post = $_POST['Geo']['post'];
                    $postValue = null;
                    if (!empty($post['id'])) {
                        $postValue = GeoValue::findOne((int)$post['id']);
                    }
                    $postValue = $postValue ? $postValue : new GeoValue();
                    $postValue->setPrice($post['price']);
                    $postValue->setGeo($geo);
                    $postValue->setShipping(Shipping::findOne(Shipping::SHIPPING_POST));
                    $postValue->setPointId(0);
                    $postValue->setAddress('');
                    $postValue->setDescription('');
                    if($postValue->save() === false)
                    {
                        var_dump($postValue->getFirstErrors());die;
                    }
                } else {
                    //Удаление
                }

                if (!empty($_POST['Geo']['pickup'])) {
                    $pickups = $_POST['Geo']['pickup'];
                    if (!empty($pickups['address']) && !empty($pickups['point_id']) && !empty($pickups['price'])) {
                        $pickupValue = new GeoValue();
                        $pickupValue->setPrice($pickups['price']);
                        $pickupValue->setAddress($pickups['address']);
                        $pickupValue->setDescription($pickups['description']);
                        $pickupValue->setPointId($pickups['point_id']);
                        $pickupValue->setLatitude($pickups['latitude']);
                        $pickupValue->setLongitude($pickups['longitude']);
                        $pickupValue->setGeo($geo);
                        $pickupValue->setActive(true);
                        $pickupValue->setShipping(Shipping::findOne(Shipping::SHIPPING_PICKUP));
                        if($pickupValue->save() === false)
                        {
                            var_dump($pickupValue->getFirstErrors());die;
                        }
                        unset($pickups['price']);
                        unset($pickups['address']);
                        unset($pickups['description']);
                        unset($pickups['point_id']);
                        unset($pickups['latitude']);
                        unset($pickups['longitude']);
                    }
                    foreach ($pickups as $id => $value) {
                        $pickupValue = GeoValue::findOne((int)$id);
                        if ($pickupValue) {
                            $pickupValue->setPrice($value['price']);
                            $pickupValue->setAddress($value['address']);
                            $pickupValue->setDescription($value['description']);
                            $pickupValue->setPointId($value['point_id']);
                            $pickupValue->setLatitude($value['latitude']);
                            $pickupValue->setLongitude($value['longitude']);
                            $pickupValue->setGeo($geo);
                            $pickupValue->setActive(!empty($value['is_active']));
                            $pickupValue->setShipping(Shipping::findOne(Shipping::SHIPPING_PICKUP));
                            $pickupValue->save();
                        }
                    }
                }
                $transaction->commit();
                return true;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            var_dump($e->getMessage());
        }
        return false;
    }
}
