<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 12],
        ];
    }

    public function save($file)
    {
        $hash = md5($file->baseName . time() . rand(1, 100));
        $fileName = substr($hash, 0, 15) . '.' . $file->extension;
        /**
         * @TODO Проверка что файла с таким именем нет
         */
        $file->saveAs(Yii::$app->params['imagesPath'] . $fileName);

        return $fileName;
    }
}