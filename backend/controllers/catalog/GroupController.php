<?php
namespace backend\controllers\catalog;

use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use backend\controllers\AbstractController;
use common\models\catalog\tag\Group;

/**
 * Catalog Group controller
 */
class GroupController extends AbstractController
{

    /**
     * Group list
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Group::find(),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Group create
     *
     * @return string
     */
    public function actionCreate()
    {
        $group = new Group();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $group->load($_POST);
            if ($group->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/group']) . '#' . $group->getId());
            }
            $errors = $group->getErrors();
        }
        return $this->render('form', ['group' => $group, 'errors' => $errors]);
    }

    /**
     * Group create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Group $group */
        $group = Group::findOne((int)$id);

        if ($group === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['catalog/group']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $group->load($_POST);

            if ($group->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/group']) . '#' . $group->getId());
            }
            $errors = $group->getErrors();
        }

        return $this->render('form', ['group' => $group, 'errors' => $errors]);
    }

}
