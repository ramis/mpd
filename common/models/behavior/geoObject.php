<?php

namespace common\models\behavior;

trait GeoObject
{

    /**
     * @return array
     */
    public function getGeoObject()
    {
        return unserialize($this->geo_object);
    }

    /**
     * @param array $geoObject
     */
    public function setGeoObject(array $geoObject)
    {
        $this->geo_object = serialize($geoObject);
    }

}