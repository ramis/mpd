<?php
namespace common\models\behavior;

use backend\models\direct\Campaign as ModelCampaign;

trait Campaign
{
    /**
     * @var ModelCampaign
     */
    private $campaign;
    private $campaign_id;

    /**
     * @return ModelCampaign
     */
    public function getCampaign()
    {
        if ($this->campaign === null && $this->campaign_id !== null) {
            $this->campaign = ModelCampaign::findOne($this->campaign_id);
        }

        if ($this->campaign === null) {
            $this->campaign = new ModelCampaign();
        }

        return $this->campaign;
    }

    /**
     * @param ModelCampaign $campaign
     */
    public function setCampaign(ModelCampaign $campaign)
    {
        $this->campaign = $campaign;
        $this->campaign_id = $campaign->getId();
    }

    /**
     * @param int $campaign_id
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = $campaign_id;
        $this->campaign = ModelCampaign::findOne($this->campaign_id);
    }

}