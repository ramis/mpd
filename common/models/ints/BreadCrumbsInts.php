<?php
namespace common\models\ints;

interface BreadCrumbsInts
{
    /**
     * @return array
     */
    public function getBreadCrumbs();
}
