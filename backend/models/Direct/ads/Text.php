<?php

namespace backend\models\direct\ads;

use common\models\behavior\Campaign;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\direct\Ads;
use backend\models\direct\Campaign as ModelCampaign;
use common\models\behavior\Description;
use common\models\behavior\Id;

/**
 * This is the model class for table "{{%direct_ads_text}}".
 *
 * @property integer $id
 * @property string $description
 * @property string $updated_uts
 * @property integer $campaign_id
 *
 * @property Ads[] $directAds
 * @property ModelCampaign $campaign
 */
class Text extends ActiveRecord
{
    Use Id;
    Use Description;
    Use Campaign;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%direct_ads_text}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'campaign_id'], 'required'],
            [['updated_uts'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['updated_uts'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_uts'],
                ],
                'value' => function () {
                    return time();
                },
            ],
        ];
    }

    /**
     * @return int
     */
    public function getUpdatedUts()
    {
        return $this->updated_uts;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'Description' => Yii::t('app', 'Description'),
            'Campaign_id' => Yii::t('app', 'Campaign Id'),
            'updated_uts' => Yii::t('app', 'Updated Uts'),
        ];
    }

    /**
     * @return Ads[]
     */
    public function getDirectAds()
    {
        return $this->hasMany(Ads::className(), ['ads_text_id' => 'id']);
    }
}
