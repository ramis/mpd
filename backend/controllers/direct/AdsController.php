<?php
namespace backend\controllers\direct;

use backend\models\direct\ads\Text;
use Yii;
use yii\data\ActiveDataProvider;
use backend\controllers\AbstractController;
use backend\models\direct\Ads;
use yii\helpers\Url;

/**
 * Direct Ads controller
 */
class AdsController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Ads::find(),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);
    }

    /**
     * Ads list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Ads list
     *
     * @return string
     */
    public function actionText()
    {
        $provider = new ActiveDataProvider([
            'query' => Text::find(),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);

        return $this->render('text', ['dataProvider' => $this->getDataProvider()]);
    }


    /**
     * Attribute create
     *
     * @return string
     */
    public function actionCreate()
    {
        $attribute = new Attribute();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $attribute->load($_POST);

            if ($attribute->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/attribute']));
            }
            $errors = $attribute->getErrors();
        }
        return $this->render('form', ['attribute' => $attribute, 'errors' => $errors]);
    }

    /**
     * Attribute create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Attribute $attribute */
        $attribute = Attribute::findOne((int)$id);

        if ($attribute === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['catalog/attribute']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $attribute->load($_POST);

            if ($attribute->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/attribute']));
            }
            $errors = $attribute->getErrors();
        }

        return $this->render('form', ['attribute' => $attribute, 'errors' => $errors]);
    }


}
