<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\FileInput;
use common\models\catalog\Category;

/* @var yii\web\View $this */
/* @var $category Category */

$this->title = 'Catalog Category List';
?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1><?= ($category->getId() ? $category->getTitle() : 'Новая') ?></h1>

        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($category->getId() ? Url::to(['edit', 'id'=>$category->getId()]) : Url::to(['create'])),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($category, 'title');
        echo $form->field($category, 'title2');
        echo $form->field($category, 'url');
        echo $form->field($category, 'sort');
        echo $form->field($category, 'seo_text')->textarea(['col' => 5, 'row' => 10]);
        echo $form->field($category, 'description')->textarea(['col' => 5, 'row' => 10]);
        echo $form->field($category, 'is_published')->checkbox();
        echo $form->field($category, 'is_special')->checkbox();
        echo '</fieldset>';

        echo '<fieldset><legend>Сео</legend>';
        echo $form->field($category, 'seo_title');
        echo $form->field($category, 'seo_keywords');
        echo $form->field($category, 'seo_description')->textarea(['col' => 5, 'row' => 10]);;
        echo '</fieldset>';

        echo '<fieldset><legend>Картинки</legend>';
        $image = $category->getMainImage();
        if($image){
            echo '<div id="img_'.$image->getId().'"><div class="col-md-2"></div><div class="col-md-10">' . $image->getHtml() . '</div></div>';
        }
        echo '<div>';
        echo $form->field($category->getMedia()->getImageForm(), 'imageFiles[]')->widget(FileInput::classname(), ['options' => ['multiple'=>false, 'accept' => 'image/*']]);
        echo '</div><div class="clear"></div>';
        echo '</fieldset>';

        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
