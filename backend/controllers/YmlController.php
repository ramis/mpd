<?php
namespace backend\controllers;

use common\models\Yml;
use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/**
 * Yml controller
 */
class YmlController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Yml::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    /**
     * Yml list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Yml create
     *
     * @return string
     */
    public function actionCreate()
    {
        $yml = new Yml();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $yml->load($_POST);

            if ($yml->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['yml/index']));
            }
            $errors = $yml->getErrors();
        }
        return $this->render('form', ['yml' => $yml, 'errors' => $errors]);
    }

    /**
     * Yml create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Yml $yml */
        $yml = Yml::findOne((int)$id);

        if ($yml === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['yml/index']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $yml->load($_POST);

            if ($yml->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['yml/index']));
            }
            $errors = $yml->getErrors();
        }

        return $this->render('form', ['yml' => $yml, 'errors' => $errors]);
    }

}
