<?php
use yii\helpers\Html;
use frontend\widgets\LeftColumn;
use common\models\Information;

/* @var $this yii\web\View */
/* @var Information $information */
$this->title = $information->getSeoTitle();
/*
?>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&load=package.full" type="text/javascript"></script>.
<style type="text/css">
    #YMapsID {
        width: 650px;
        height: 350px;
    }
</style>
<script>
    ymaps.ready(function () {
        var myMap = new ymaps.Map('YMapsID', {
            center: [55.733835, 37.588227],
            zoom: 15,
            controls: []
        });
        var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            balloonContentBody: [
                '<address>',
                '<h4>Пункт выдачи заказов №4</h4>',
                '<strong>м. Братиславская, Адрес: ул. Братиславская, д. 16к2</strong>',
                '<p>м. Братиславская. Выход в город на ул. Братиславская. Из стеклянных дверей направо до конца, по ступенькам налево. Идти прямо по Братиславской улице. Между домами 14 и 16/1, в котором находится Макдональдс, повернуть направо. Перед вами будет магазин «Пятерочка». На цокольном этаже магазина находится Пункт Самовывоза.</p>',

                '<table><tr><td>Стоимость</td><td>99 руб</td></tr>',
                '<tr><td>Телефон</td><td>+7 (495) 323-32-32</td></tr>',
                '<tr><td>Время работы:</td><td>с 10:00 до 20:00</td></tr></table>',
                '</address>'
            ].join('')
        }, {
            preset: 'islands#redDotIcon'
        });

        myMap.geoObjects.add(myPlacemark);
    });
</script>
 */
?>
<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col" class="information">
        <h1>Доставка</h1>

        <?php
        $shippingCourier = '<div class="text">
        <p>&nbsp;</p>

        <h4>Курьером по Москве и Московской области</h4>
        <p>Скорость доставки заказа зависит от удаленности вашего региона от Москвы, а также от конкретных товаров, которые вы заказали, и времени суток, в которое вы оформили заказ на сайте.</p>
        <p>
            — Москва - 200 руб.<br/>
            — Московская область - 200 руб. плюс 50 руб. за каждые 5 км удаленности от МКАД<br/>
            — Доставка до двери<br/>
            — Заказы весом более 10 кг доставляются до подъезда. Подъем на этаж оговаривается отдельно.<br/>
            — Доставить ваш заказ мы сможем в течение 1-3 дней.<br/>
            — Дневная доставка осуществляется в любой день недели с 10:00 до 18:00<br/>
            — Вечерняя доставка осуществляется с понедельника по пятницу с 18:00 до 21:00<br/>
            — Время доставки можно выбрать в интервале трех часов, во все дни, кроме субботы и воскресенья.<br/>
            — Стоимость и сроки доставки негабаритного груза оговариваются отдельно.<br/>
        </p>
        <br/>

        <h4>Курьером по Санкт-Петербургу и Ленинградской области</h4>
        <p>
            — Санкт-Петербург - 200 руб.<br/>
            — Ленинградская область - 300 руб. плюс 50 руб. за каждые 5 км удаленности от КАДа<br/>
            — Доставка до двери<br/>
            — Заказы весом более 10 кг доставляются до подъезда. Подъем на этаж оговаривается отдельно.<br/>
            — Доставить ваш заказ мы сможем в течение 1-3 дней.<br/>
            — Дневная доставка осуществляется с понедельника по субботу с 10:00 до 18:00<br/>
            — Вечерняя доставка осуществляется с понедельника по пятницу с 18:00 до 21:00<br/>
            — Время доставки можно выбрать в интервале трех часов, во все дни, кроме субботы.<br/>
            — Стоимость и сроки доставки негабаритного груза оговариваются отдельно.<br/>

        </p>
        <p>Курьерская служба осуществляет доставку по Ленинградской области до 35 км от КАД. Для населенных пунктов, находящихся за пределами 35 км, предлагаем воспользоваться почтовой доставкой.</p>
        <br/>
        <h4>Курьером другие регионы</h4>

        <h5>Брянск, Вологда, Владимир, Иваново, Калуга, Кострома, Курск, Нижний Новгород, Нижний Новгород, Орел, Рязань, Тверь, Тула, Ярославль</h5>
        <p>
            — Стоимость - 300 руб.<br/>
            — Доставить ваш заказ мы сможем в течение 1-3 дней.<br/>
            — Доставка курьером осуществляется во все дни, кроме воскресенья и праздничных дней.<br/>
            — Время доставки можно выбрать в интервале 3-х часов, во все дни, кроме субботы.<br/>
        </p>

        <h5>Казань, Ростов-на-Дону</h5>
        <p>
            — Стоимость - 300 руб.<br/>
            — Доставить ваш заказ мы сможем на второй рабочий день после его подтверждения с 12:00 до 18:00.<br/>
        </p>

        <h5>Екатеринбург, Челябинск</h5>
        <p>
            — Стоимость - 300 руб.<br/>
            — Доставить ваш заказ мы сможем на пятый рабочий день после его подтверждения с 12:00 до 18:00.<br/>
        </p>

        <h5>Тюмень</h5>
        <p>
            — Стоимость - 300 руб.<br/>
            — Доставить ваш заказ мы сможем на шестой рабочий день после его подтверждения с 12:00 до 18:00.<br/>
        </p>

        </p>
        <p>Доставка до двери. Заказы весом более 10 кг доставляются до подъезда. Подъем на этаж оговаривается отдельно. Стоимость и сроки доставки негабаритного груза оговариваются отдельно.</p>
        </div>';
        $shippingPickup = '<div class="text">
    <h4>Условия доставки:</h4>
	<p>
		— доставка Вашего заказа до пункта самовывоза осуществляется в течение 1-3 дней (в зависимости от выбранного региона)<br/>
		— время работы пунктов самовывоза можно уточнить в таблице ниже<br/>
		— стоимость доставки до пункта самовывоза&nbsp;можно уточнить в таблице ниже или при оформлении заказа на сайте<br/>
		— адреса всех пунктов самовывоза представлены в таблице ниже<br/>
		— срок хранения заказов на пункте самовывоза <strong>7&nbsp;дней</strong><br/>
	</p>

	<h4 style="padding: 5px; margin: 0;">
		<span id="msk" class="title_geo">Москва и Московская область</span></h4>
		<div id="YMapsID"></div>
	<div id="container_msk" class="containers">
		<table class="table">
			<tbody>
				<tr >
					<td>
						Метро</td>
					<td >
						Адрес</td>
					<td>
						Время работы</td>
					<td>
						Стоимость</td>
				</tr>
				<tr>
					<td>
						Алтуфьево</td>
					<td>
						Алтуфьевское шоссе, 80, ТД "Арфа"</td>
					<td>
						с 10:00 до 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Братиславская</td>
					<td>
						ул. Братиславская, д. 16к2</td>
					<td>
						с 10:00 до 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Бульвар Дмитрия Донского</td>
					<td>
						ул. Старокачаловская, д.5А</td>
					<td>
						с 10:00 до 22:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						ВДНХ</td>
					<td>
						ул. Проспект Мира, д.176</td>
					<td>
						с 10:00 до 22:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Выхино</td>
					<td>
						Рязанский проспект, вл.101Б, рынок "Альтаир"</td>
					<td>
						с 10:00 до 21:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Митино</td>
					<td>
						ул. Митинская, д.42</td>
					<td>
						<div>
							пн-сб 10:00 &ndash; 21:00</div>
						<div>
							вс 10:00 &ndash; 20:00</div>
					</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Новогиреево</td>
					<td>
						Свободный проспект, д.33А</td>
					<td>
						<div>
							пн-сб 10:00 &ndash; 20:30</div>
						<div>
							вс 10:00 &ndash; 19:30</div>
					</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Новые Черемушки</td>
					<td>
						ул. Профсоюзная, д.45</td>
					<td>
						с 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Коломенская</td>
					<td>
						Проспект Андропова д.36</td>
					<td>
						с 10:00 до 22:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Комсомольская</td>
					<td>
						Комсомольская пл., д.6</td>
					<td>
						с 10:00 до 21:30</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Петровско-Разумовская</td>
					<td>
						Линии Октябрьской железной дороги д.1, стр.5</td>
					<td>
						с 10:00 до 20:30</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Преображенская площадь</td>
					<td>
						Преображенская площадь, д.6</td>
					<td>
						с 10:00 до 22:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Пушкинская, Тверская, Чеховская</td>
					<td>
						Малый Палашевский пер., д.6</td>
					<td>
						с 10:00 &ndash; 22:00</td>
					<td>
						129 руб.</td>
				</tr>
				<tr>
					<td>
						Речной вокзал</td>
					<td>
						ул. Смольная 24г</td>
					<td>
						с 10:00 до 21:30</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Сходненская</td>
					<td>
						ул. Героев Панфиловцев, д.1а</td>
					<td>
						<div>
							пн-сб 10:00 &ndash; 21:00</div>
						<div>
							вс 10:00 &ndash; 20:00</div>
					</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Теплый стан</td>
					<td>
						Новоясеневский проспект, д.2</td>
					<td>
						с 10:00 до 22:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Третьяковская</td>
					<td>
						ул. Малая Ордынка, д.25, Офисный центр «Ордынский»</td>
					<td>
						с 10:00 до 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Щелковская</td>
					<td>
						ул. 9-я парковая, д.61А, стр.1</td>
					<td>
						<div>
							пн-сб 10:00 &ndash; 21:00</div>
						<div>
							вс 10:00 &ndash; 19:00</div>
					</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Юго-Западная</td>
					<td>
						Проспект Вернадского, д.86Б, стр.1</td>
					<td>
						с 10:00 &ndash; 20:30</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						г. Подольск</td>
					<td>
						Революционный проспект, д.34/29, пом. 9</td>
					<td>
						с 10:00 до 21:00</td>
					<td>
						100 руб.</td>
				</tr>
			</tbody>
		</table>
	</div>
	<h4 style=" padding: 5px; margin: 0;">
		<span id="spb" class="title_geo">Санкт-Петербург и Ленинградская область</span></h4>
	<div id="container_spb" class="containers">
		<table class="table">
			<tbody>
				<tr >
					<td>
						Метро</td>
					<td >
						Адрес</td>
					<td>
						Время работы</td>
					<td>
						Стоимость</td>
				</tr>
				<tr>
					<td>
						Автово</td>
					<td>
						пр. Стачек, д. 92, корп.3</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Гражданский проспект</td>
					<td>
						Гражданский проспект, д.118, к.1</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Комендантский проспект</td>
					<td>
						ул. Ильюшина, д.5, кор.2, лит.А</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Ладожская</td>
					<td>
						Уткин проспект, д.13, корп.1</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Международная</td>
					<td>
						ул. Белы Куна д.1, корпус 1</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Московский пр., д. 165</td>
					<td>
						Парк Победы</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Парк Победы</td>
					<td>
						Московский пр., д.165</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Площадь Восстания</td>
					<td>
						ул. Пушкинская, д.10</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Проспект Просвещения</td>
					<td>
						пр. Энгельса, д.150, к.1</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Сенная площадь</td>
					<td>
						ул. Гороховая, д.49</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Улица Дыбенко</td>
					<td>
						ул. Дыбенко, д.27, к.1</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
				<tr>
					<td>
						Чернышевская</td>
					<td>
						Ул. Фурштатская, д.33</td>
					<td>
						пн-сб 10:00 &ndash; 20:00</td>
					<td>
						100 руб.</td>
				</tr>
			</tbody>
		</table>
	</div>
	<h4 style="padding: 5px; margin: 0;">
		<span id="brn" class="title_geo">Брянск</span></h4>
	<div  id="container_brn" class="containers">
		<table class="table">
			<tbody>
				<tr >
					<td>
						Адрес</td>
					<td>
						Телефон</td>
					<td>
						Выдача заказов</td>
					<td>
						Стоимость</td>
				</tr>
				<tr>
					<td>
						ул. Ромашина, д.1</td>
					<td>
						&nbsp;</td>
					<td>
						пн-пт 10:00 &ndash; 19:00<br>
						сб 10:00 - 15:00</td>
					<td>
						150 руб.</td>
				</tr>
			</tbody>
		</table>
	</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="vln" class="title_geo">Великий Новгород</span></h4>
<div  id="container_vln" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					Стратилатовская д.16</td>
				<td>
					+7(473)235-55-55</td>
				<td>
					пн-пт 10:00 &ndash; 17:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="vld" class="title_geo">Вологда</span></h4>
<div  id="container_vld" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул.Благовещенская, д.35</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 09:00 &ndash; 18:00<br>
					сб 10:00 - 14:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="vrg" class="title_geo">Воронеж</span></h4>
<div  id="container_vrg" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Плехановская, д.28</td>
				<td>
					+7(473)235-5555</td>
				<td>
					пн-пт 10:00 &ndash; 19:00<br>
					сб 10:00 - 15:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="vbr" class="title_geo">Выборг</span></h4>
<div  id="container_vbr" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					Ленинградский проспект, д.12</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 10:00 &ndash; 18:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="ekb" class="title_geo">Екатеринбург</span></h4>
<div  id="container_ekb" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. 8 Марта, 8Д</td>
				<td>
					+7(967)633-36-48</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
			<tr>
				<td>
					ул. Фронтовых Бригад д.15/28</td>
				<td>
					+7(343)272-0700</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="ivn" class="title_geo">Иваново</span></h4>
<div  id="container_ivn" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Арсения, д.25</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 10:00 &ndash; 18:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="kaz" class="title_geo">Казань</span></h4>
<div  id="container_kaz" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Воровского, д.21</td>
				<td>
					+7(843)204-0857</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="kal" class="title_geo">Калуга</span></h4>
<div  id="container_kal" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Театральная, д.4Б</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 09:00 &ndash; 19:00<br>
					сб 10:00 - 16:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="kir" class="title_geo">Киров</span></h4>
<div  id="container_kir" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Ленина, д.18</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 08:00 &ndash; 19:00<br>
					сб 10:00 - 14:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="krd" class="title_geo view">Краснодар</span></h4>
<div style="" id="container_krd" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул.Суворова, д.74 (Центральный округ)</td>
				<td>
					+7(905)425-0213</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="nig" class="title_geo">Нижний Новгород</span></h4>
<div  id="container_nig" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Короленко, д. 19 б</td>
				<td>
					+7(831)421-6731</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
			<tr>
				<td>
					ул. Баррикад, д. 1 корпус 5</td>
				<td>
					+7(831)421-6732</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="nvr" class="title_geo">Новороссийск</span></h4>
<div  id="container_nvr" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Мира, д.10, оф.22</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 09:00 &ndash; 18:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="nsb" class="title_geo">Новосибирск</span></h4>
<div  id="container_nsb" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Гоголя, д.35</td>
				<td>
					+7(383)284-6998</td>
				<td>
					пн-пт 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="nrk" class="title_geo">Норильск</span></h4>
<div  id="container_nrk" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					Ленинский проспект, д.35</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 10:00 &ndash; 20:00<br>
					сб 10:00 - 16:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="omk" class="title_geo">Омск</span></h4>
<div  id="container_omk" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Красный путь, д.22</td>
				<td>
					+7(3812)66-0103</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="ore" class="title_geo">Орел</span></h4>
<div  id="container_ore" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Полесская д.9 лит.«Б»</td>
				<td>
					+7(4862)44-3070</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="prm" class="title_geo">Пермь</span></h4>
<div  id="container_prm" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Лебедева, д.35</td>
				<td>
					+7(342)259-7894</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="psk" class="title_geo">Псков</span></h4>
<div  id="container_psk" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					Октябрьский проспект, д.54, помещение 1038, офис 104.</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 09:00 &ndash; 18:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="ros" class="title_geo">Ростов-на-Дону</span></h4>
<div  id="container_ros" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					пр. Чехова, д.105/1</td>
				<td>
					+7(905)425-0146</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="ryaz" class="title_geo">Рязань</span></h4>
<div  id="container_ryaz" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Циолковского, д.21, офис "Mail Boxes Etc"</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 09:00 &ndash; 18:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="smol" class="title_geo">Смоленск</span></h4>
<div  id="container_smol" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					бульвар Гагарина, д.4</td>
				<td>
					&nbsp;</td>
				<td>
					пн-пт 09:00 &ndash; 18:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="tve" class="title_geo">Тверь</span></h4>
<div  id="container_tve" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					наб. Афанасия Никитина, д.24</td>
				<td>
					+7(4822)39-0844</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="tul" class="title_geo">Тула</span></h4>
<div  id="container_tul" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Макса Смирнова, д.2</td>
				<td>
					+7(4872)71-00-16</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;">
	<span id="tum" class="title_geo">Тюмень</span></h4>
<div  id="container_tum" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Советская, 55/3</td>
				<td>
					+7(909)190-7766</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style=" padding: 5px; margin: 0;">
	<span id="che" class="title_geo">Челябинск</span></h4>
<div  id="container_che" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					Проспект Ленина, д.35 о.100, здание «Гипромез»</td>
				<td>
					+7(351)225-4072</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
<h4 style="padding: 5px; margin: 0;"><span id="yar" class="title_geo">Ярославль</span></h4>
<div  id="container_yar" class="containers">
	<table class="table">
		<tbody>
			<tr >
				<td>
					Адрес</td>
				<td>
					Телефон</td>
				<td>
					Выдача заказов</td>
				<td>
					Стоимость</td>
			</tr>
			<tr>
				<td>
					ул. Рыбинская, д.9, офис №6</td>
				<td>
					+7(4852)64-3886</td>
				<td>
					пн-сб 10:00 &ndash; 20:00</td>
				<td>
					150 руб.</td>
			</tr>
		</tbody>
	</table>
</div>
</div>';

        $shippingPost = '<div class="text">
        <p>&nbsp;</p>
        <p>
            Стоимость доставки Почтой России зависит от региона. Подробнее с тарифами можно ознакомиться в следующей таблице:
        </p>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
            <tbody>
            <tr>
                <th>
                    Магистральный пояс
                </th>
                <th>
                    Стоимость доставки, для габаритного заказа
                </th>
                <th>
                    Регионы России
                </th>
            </tr>
            <tr>
                <td>
                    Первый
                    <br>
                    до 600 км
                </td>
                <td>
                    300 р.
                </td>
                <td>
                    Москва, Московская область, Брянская область, Владимирская область, Вологодская область, Воронежская
                    область, Ивановская область, Калужская область, Костромская область, Курская область, Липецкая область,
                    Нижегородская область, Орловская область, Рязанская область, Смоленская область, Тамбовская область,
                    Тверская область, Тульская область, Ярославская область
                </td>
            </tr>
            <tr>
                <td>
                    Второй
                    <br>
                    от 601 км
                    <br>
                    до 2000 км
                </td>
                <td>
                    450 р.
                </td>
                <td>
                    Санкт-Петербург, Ленинградская область, республика Адыгея, Архангельская область, Астраханская область,
                    республика Башкортостан, Белгородская область, Волгоградская область, республика Ингушетия,
                    Кабардино-балкарская республика, Калининградская область, республика Калмыкия, Карачаево-черкесская
                    республика, республика Карелия, Кировская область, республика Коми, АР Крым, Коми-Пермяцкий округ, Пермский
                    край, Краснодарский край, республика Марий-Эл, республика Мордовия, Мурманская область, Ненецкий АО(кроме
                    г.Нарьян-Мар), Архангельская область, Новгородская область, Оренбургская область, Пензенская область,
                    Пермский край, Псковская область, Ростовская область, Самарская область, Саратовская область, Свердловская
                    область, республика Северная Осетия-Алания, Ставропольский край, республика Татарстан, Удмуртская
                    республика, Ульяновская область, Челябинская область, Чеченская республика, Чувашская республика
                </td>
            </tr>
            <tr>
                <td>
                    Третий
                    <br>
                    от 2001 км
                    <br>
                    до 5000 км
                </td>
                <td>
                    600 р.
                </td>
                <td>
                    республика Алтай, Алтайский край, республика Дагестан , анклав Байконур, Кемеровская область, Красноярский
                    край(кроме г.Норильск, г.Талнах), Курганская область, Новосибирская область, Омская область, Таймырский
                    (Долгано-Ненецкий) АО(кроме г.Дудинка), Томская область, республика Тыва, Тюменская область, республика
                    Хакасия, Ханты-Мансийский (Югра) АО, Тюменская область, Эвенский АО, Ямало-Ненецкий АО, Тюменская область
                </td>
            </tr>
            <tr>
                <td>
                    Четвертый
                    <br>
                    от 5001 км
                    <br>
                    до 8000 км
                </td>
                <td>
                    750 р.
                </td>
                <td>
                    Агинский Бурятский округ, Забайкальский край, Амурская область, республика Бурятия, Иркутская область,
                    Усть-Ордынский Бурятский округ
                </td>
            </tr>
            <tr>
                <td>
                    Пятый
                    <br>
                    от 8001 км
                </td>
                <td>
                    950р.
                </td>
                <td>
                    Еврейская АО, Корякский АО, Приморский край, Хабаровский край
                </td>
            </tr>
            <tr>
                <td>
                    Только авиадоставка
                </td>
                <td>
                    стоимость рассчитывается индивидуально
                </td>
                <td>
                    <br>
                    Красноярский край (г.Норильск, г.Талнах), Камчатский край, Магаданская область, Ненецкий АО (г.Нарьян-Мар),
                    республика Саха (Якутия), Таймырский (Долгано-Ненецкий) АО (г.Дудинка), Чукотский АО
                </td>
            </tr>
            </tbody>
            </table>
        </div>
        <p>
            Пожалуйста, обратите внимание:
        </p>
        <ul>
            <li>Стоимость доставки Почтой России может существенно меняться для крупногабаритных заказов! <br>
                В этом случае с вами свяжется наш менеджер и уточнит стоимость доставки, исходя из размеров и веса вашей
                посылки.
            </li>
            <li>Стоимость доставки не зависит от суммы заказа.</li>
            <li>При получении заказа на Почте к стоимости заказа добавляется почтовый сбор за наложенный платеж согласно тарифам
                Почты России.
            <br>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <td>
                    Размер суммы заказа
                    </td>
                    <td>
                    Сбор «Почты России»
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                    до 1 000 руб. включительно
                    </td>
                    <td>
                    40 руб. + 5% от суммы заказа
                    </td>
                </tr>
                <tr>
                    <td>
                    свыше 1 000 до 5 000 руб. включительно
                    </td>
                    <td>
                    50 руб. + 4% от суммы заказа
                    </td>
                </tr>
                <tr>
                    <td>
                    свыше 5 000 руб. до 20 000 руб. включительно
                    </td>
                    <td>
                    150 руб. + 2% от суммы заказа
                    </td>
                </tr>
                <tr>
                    <td>
                    свыше 20 000 руб. до 500 000 руб. включительно
                    </td>
                    <td>
                    250 руб. + 1,5% от суммы заказа
                    </td>
                </tr>
                </tbody>
                </table>
            </div>
            </li>
            <li>Указанная стоимость доставки может превышать стоимость выдаваемую почтовым калькулятором, т. к. при отправке
                посылки Почта России добавляет обязательные услуги (маркировка посылки, оклейка коробки фирменным скотчем,
                оформление наложенного платежа, стоимость самой коробки), без которых не принимает почтовое отправление.
            </li>
        </ul></div>';

        echo \yii\bootstrap\Tabs::widget([
            'items' => [
                [
                    'label' => 'Курьером',
                    'content' => $shippingCourier,
                    'active' => true
                ],
                [
                    'label' => 'Самовывоз',
                    'content' => $shippingPickup,
                    'active' => false
                ],
                [
                    'label' => 'Почтой России',
                    'content' => $shippingPost,
                    'active' => false
                ],
            ],
        ]);
        ?>
    </div>
</div>
<div class="clear"></div>