<?php

use yii\db\Schema;
use yii\db\Migration;

class m151129_223550_alter_table_shipping_geo_value extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%shipping_geo_value}}', 'is_active', Schema::TYPE_BOOLEAN);
    }

    public function safeDown()
    {
    }
}
