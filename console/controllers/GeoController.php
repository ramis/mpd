<?php
namespace console\controllers;

use common\tools\Geo;
use Yii;
use yii\console\Controller;
use common\models\geo\Cities;

class GeoController extends Controller
{

    /**
     * Получение geoObject от dadata.ru
     *
     * @throws \Exception
     */
    public function actionGeoObject()
    {
        $cities = Cities::find()->all();

        foreach ($cities as $city) {
            $geoObject = Geo::getGeoObjectByAddress($city->getArea() . ' ' . $city->getCity());
            $city->setGeoObject($geoObject);
            if ($city->save() === false) {
                throw new \Exception(json_encode($city->getFirstErrors()));
            }
        }
    }

}
