<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\uAdminAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
uAdminAsset::register($this);

$route = Yii::$app->controller->getRoute();

$menuItems = [
    [
        'icon' => 'fa fa-tint',
        'title' => 'Панель управления',
        'url' => 'site/index',
        'sub_menu' => [],
    ],
    [
        'icon' => 'fa fa-th-list',
        'title' => 'Каталог',
        'url' => '#',
        'sub_menu' => [
            [
                'icon' => 'fa fa-book',
                'title' => 'Категории',
                'url' => 'catalog/category',
            ],
            [
                'icon' => 'fa fa-folder-o',
                'title' => 'Товары',
                'url' => 'catalog/product',
            ],
            [
                'icon' => 'fa fa-building',
                'title' => 'Цены',
                'url' => 'catalog/product/price',
            ],
            [
                'icon' => 'fa fa-list-alt',
                'title' => 'Отзывы',
                'url' => 'catalog/review',
            ],
            [
                'icon' => 'fa fa-tag',
                'title' => 'Теги',
                'url' => 'catalog/tag',
            ],
            [
                'icon' => 'fa fa-list',
                'title' => 'Атрибуты',
                'url' => 'catalog/attribute',
            ],
        ],
    ],
    [
        'icon' => 'fa fa-info',
        'title' => 'Информация',
        'url' => '#',
        'sub_menu' => [
            [
                'icon' => 'fa fa-file-text-o',
                'title' => 'Статические страницы',
                'url' => 'information',
            ],
        ],
    ],
    [
        'icon' => 'fa fa-rub',
        'title' => 'Продажа',
        'url' => '#',
        'sub_menu' => [
            [
                'icon' => 'fa fa-credit-card',
                'title' => 'Заказы',
                'url' => 'order/index',
            ],
//            [
//                'icon' => 'fa fa-bullhorn',
//                'title' => 'Баннеры',
//                'url' => 'site/index',
//            ],
//            [
//                'icon' => 'fa fa-pencil',
//                'title' => 'Подписки',
//                'url' => 'site/index',
//            ],
        ],
    ],
    [
        'icon' => 'fa fa-car',
        'title' => 'Доставки',
        'url' => '#',
        'sub_menu' => [
            [
                'icon' => 'fa fa-institution',
                'title' => 'Города',
                'url' => 'shipping',
            ],
        ],
    ],
    [
        'icon' => 'fa fa-signal',
        'title' => 'Маркет',
        'url' => '#',
        'sub_menu' => [
            [
                'icon' => 'fa fa-thumbs-up',
                'title' => 'Utm метки',
                'url' => 'market/utm/index',
            ],
            [
                'icon' => 'fa fa-sitemap',
                'title' => 'Yml files',
                'url' => 'yml',
            ],
        ],
    ],
    [
        'icon' => 'fa fa-wrench',
        'title' => 'Админ',
        'url' => '#',
        'sub_menu' => [
            [
                'icon' => 'fa fa-suitcase',
                'title' => 'Скрипты',
                'url' => 'crontab/index',
            ],
        ],
    ],
];

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico"/>

    <?= Html::csrfMetaTags() ?>
    <title>Панель управления</title>
    <?php $this->head() ?>

</head>
<body <?= (Yii::$app->user->isGuest) ? 'class="login"' : '' ?> >
<?php $this->beginBody() ?>
<?php
if (Yii::$app->user->isGuest) {
    echo '<div class="page-container">' . $content . '</div>';
} else {
    ?>

    <div id="page-container">
        <header class="navbar navbar-inverse">
            <ul class="navbar-nav-custom pull-right hidden-md hidden-lg">
                <li class="divider-vertical"></li>
                <li>
                    <a href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>

            <?= Html::a(Html::img('img/template/logo.png'), ['/site/index'], ['class' => 'navbar-brand']); ?>

            <ul id="widgets" class="navbar-nav-custom pull-right">

                <li class="divider-vertical"></li>

                <li class="dropdown pull-right dropdown-user">
                    <?= Html::a(Html::img('img/template/avatar.png') . ' <b class="caret"></b>', '#', ['class' => 'dropdown-toggle', 'data-toggle' => "dropdown"]); ?>
                    <ul class="dropdown-menu">
                        <li>
                            <?= Html::a('<i class="fa fa-lock"></i> Log out', ['/site/logout']); ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </header>

        <div id="inner-container">

            <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                <nav id="primary-nav">
                    <ul>
                        <?php
                        {
                            foreach ($menuItems as $item) {
                                $isActive = false;
                                $subMenu = '';
                                if (!empty($item['sub_menu'])) {
                                    foreach ($item['sub_menu'] as $sub_item) {
                                        if (strpos($route, $sub_item['url']) !== false) {
                                            $isActive = true;
                                        }
                                        $title = '<i class="' . $sub_item['icon'] . '"></i> ' . $sub_item['title'];
                                        $url = [(strpos($sub_item['url'], '/') ? $sub_item['url'] : $sub_item['url'] . '/index')];
                                        $class = (strpos($route, $sub_item['url']) !== false ? 'active' : '');
                                        $subMenu .=
                                            '<li>' .
                                                Html::a($title, $url, ['class' => $class]) .
                                            '</li>';
                                    }
                                } else {
                                    if (strpos($route, $item['url']) !== false) {
                                        $isActive = true;
                                    }
                                }
                                $title = '<i class="' . $item['icon'] . '"></i> ' . $item['title'];
                                $url = ($item['url'] === '#' ? '' : [$item['url']]);
                                $class = ($isActive and $subMenu === '' ? 'active' : '');
                                echo '<li class ="' . ($isActive ? 'active' : '') . '">' .
                                    Html::a($title, $url, ['class' => $class]) .
                                    ($subMenu !== '' ? '<ul>' . $subMenu . '</ul>' : '') .
                                    '</li>';
                            }
                        }
                        ?>
                </nav>
            </aside>

            <!-- Page Content -->
            <div id="page-content">

                <ul id="nav-info" class="clearfix">
                    <li>
                        <?= Html::a('<i class="fa fa-home"></i>', Yii::$app->getHomeUrl()); ?>
                    </li>
                    <?php
                    {
                        $breadCrumbs = Yii::$app->controller->getBreadCrumbs();

                        foreach($breadCrumbs as $item) {
                            echo '<li>' . ($item['url'] !== '' ? Html::a($item['title'], $item['url']) : $item['title']) . '</li>';
                        }
                    }
                    ?>
                </ul>

                <!-- Nav Dash -->
                <!--                <ul class="nav-dash">-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Users" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-user"></i>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Comments" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-comments"></i> <span class="badge badge-success">3</span>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Calendar" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-calendar"></i> <span class="badge badge-inverse">5</span>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Photos" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-camera-retro"></i>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Projects" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-paperclip"></i>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Tasks" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-tasks"></i> <span class="badge badge-warning">1</span>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Reader" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-book"></i>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                    <li>-->
                <!--                        <a href="javascript:void(0)" data-toggle="tooltip" title="Settings" class="animation-fadeIn">-->
                <!--                            <i class="fa fa-cogs"></i>-->
                <!--                        </a>-->
                <!--                    </li>-->
                <!--                </ul>-->
                <!-- END Nav Dash -->

                <?= $content; ?>

            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <footer class="footer">
                <div class="container">
                    <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Inner Container -->
    </div>
    <!-- END Page Container -->

    <!-- Scroll to top link, check main.js - scrollToTop() -->
    <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>

<?php }
$this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>
