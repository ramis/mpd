<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * @var ActiveDataProvider $dataProvider
 * @var $searchModel backend\search\SearchOrder
 */
$priceOrder = 0;
$orders = $dataProvider->getModels();
foreach ($orders as $order) {
    $priceOrder += $order->getTotal();
}
$priceOrder = $dataProvider->getCount() > 0 ? round($priceOrder / $dataProvider->getCount(), 0) : 0;

echo GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'pjax' => true,
    'pjaxSettings' => [
        'neverTimeout' => true,
        'beforeGrid' => '',
        'afterGrid' => '',
    ],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn'
        ],

        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },

            'allowBatchToggle' => true,
            'detail' => function (Order $order) {
                return Yii::$app->controller->renderPartial('order_info', ['order' => $order]);
            },
            'detailOptions' => [
                'class' => 'kv-state-enable',
            ],

        ],


        [
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'id',
            'value' => function (Order $order) {
                return $order->getId();
            },
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'pageSummary' => true,
            'attribute' => 'created_uts',
            'width' => '200px',
            'filterType' => GridView::FILTER_DATE_RANGE,
            'value' => function (Order $order) {
                return Yii::$app->formatter->asDatetime($order->getCreatedUts());
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'fio',
            'value' => function (Order $order) {
                return $order->getFio();
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'State',
            'value' => function (Order $order) {
                return $order->getOrderState()->getTitle();
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'Shipping',
            'value' => function (Order $order) {
                return $order->getOrderShipping()->getText();
            },
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'ref',
            'format' => 'raw',
            'value' => function (Order $order) {
                $referers = $order->getReferers();
                $referers = $referers ? unserialize($referers) : [];
                $stReferer = '';
                foreach ($referers as $referer) {
                    $arr = parse_url($referer);
                    $stReferer .= '<div>' . $arr['host'] . '</div>';
                }
                return $stReferer;
            },
        ],

        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'utm',
            'format' => 'raw',
            'value' => function (Order $order) {
                $utms = \common\models\Utm::find()->where(['order_id' => $order->getId()])->all();
                $stUtm = '';
                foreach ($utms as $utm) {
                    $stUtm .= '<div>' . $utm->getUtmSource() . '|' . $utm->getUtmMedium() . '</div>';
                }
                return $stUtm;
            },
        ],

        'total',
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'profit',
            'value' => function (Order $order) {
                return $order->getProfit();
            },
        ],
    ],
    'panel' => [
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Заказы</h3>',
        'type' => 'success',
        'before' => 'Средний чек - ' . Yii::$app->formatter->asCurrency($priceOrder),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        'footer' => ''
    ],
]);
