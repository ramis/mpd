<?php

namespace frontend\models;

use common\tools\Tools;
use Yii;
use common\models\Account;
use yii\base\Exception;

class AccountSingleton
{
    /**
     * @var AccountSingleton
     */
    private static $_instance;

    private $account;

    private function __construct()
    {

    }

    /**
     * @return AccountSingleton
     */
    public static function getInstance()
    {

        if (self::$_instance === null) {
            if (Yii::$app->session->get(__CLASS__)) {
                self::$_instance = unserialize(Yii::$app->session->get(__CLASS__));
            } else {
                self::$_instance = new self();
                Yii::$app->session->set(__CLASS__, serialize(self::$_instance));
            }
        }

        return self::$_instance;
    }

    /**
     * @param bool|true $isCreated
     * @param bool|true $isCleared
     * @return Account
     */
    public function getAccount($isCreated = true, $isCleared = false)
    {
        if ($isCleared) {
            $this->account = null;
        }

        if ($isCreated && $this->account === null) {
            $this->account = Account::autoCreateAccount();
            Yii::$app->session->set(__CLASS__, serialize(self::$_instance));
        }

        return $this->account;
    }

    /**
     * @TODO Сохранение актуальных данный в сессию
     * @return Account
     */
    public function getAccountDate()
    {
        if ($this->account && $this->account->getId()) {
            $this->account = Account::findOne($this->account->getId());
        }
        return $this->account;
    }

    public function saveAccount(Account $account)
    {
        $this->account = $account;

        if ($this->account->save()) {
            //@TODO Сохранение актуальных данный в сессию
        } else {
            Tools::getError($this->account);
        }

    }

}