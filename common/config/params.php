<?php
return [
    'adminEmail' => 'gizzatullinrsh@gmail.com',
    'supportEmail' => 'help@mrplaydoh.ru',
    'saleEmail' => 'sale@mrplaydoh.ru',
    'user.passwordResetTokenExpire' => 7200,
];
