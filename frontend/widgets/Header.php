<?php
namespace frontend\widgets;

use yii\bootstrap\Widget;

class Header extends Widget
{

    public $category;

    /**
     * Renders the widget.
     */
    public function run()
    {
        return $this->render('header');
    }

}
