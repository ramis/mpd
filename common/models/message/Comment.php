<?php

namespace common\models\message;

use Yii;
use common\models\Message;

/**
 * This is the model class for table "{{%message_comment}}".
 *
 * @property integer $id
 * @property integer $message_id
 * @property integer $state_old
 * @property integer $state_new
 * @property string $comment
 * @property integer $created_uts
 *
 * @property Message $message
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%message_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'message_id', 'state_old', 'state_new', 'comment', 'created_uts'], 'required'],
            [['id', 'message_id', 'state_old', 'state_new', 'created_uts'], 'integer'],
            [['comment'], 'string'],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'message_id' => Yii::t('app', 'Message ID'),
            'state_old' => Yii::t('app', 'State Old'),
            'state_new' => Yii::t('app', 'State New'),
            'comment' => Yii::t('app', 'Comment'),
            'created_uts' => Yii::t('app', 'Created Uts'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return Message::find()->where(['id' => $this->message_id])->one();
    }
}
