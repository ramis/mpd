<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\models\direct\ads\Text;

/* @var yii\web\View $this */
/* @var $text Text */

$this->title = 'Text';
?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1><?= ($text->getId() ? 'Редактирование' : 'Новый') ?></h1>
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($text->getId() ? Url::to(['edit', 'id'=>$text->getId()]) : Url::to(['create'])),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($text, 'description');
        echo '</fieldset>';
        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
