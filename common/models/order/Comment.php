<?php

namespace common\models\order;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\behavior\Id;
use common\models\behavior\Account;
use common\models\behavior\Order;
use common\models\order\State;

/**
 * This is the model class for table "{{%order_comment}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $state_old_id
 * @property integer $state_new_id
 * @property string $comment
 * @property integer $account_id
 * @property integer $created_uts
 *
 * @property Account $account
 * @property Order $order
 */
class Comment extends ActiveRecord
{
    use Id;
    use Order;
    use Account;

    private $state_new;
    private $state_old;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'account_id', 'state_new_id'], 'required'],
            [['id', 'order_id', 'state_old_id', 'state_new_id', 'account_id', 'created_uts'], 'integer'],
            [['comment'], 'string'],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'state_old_id' => Yii::t('app', 'State Old'),
            'state_new_id' => Yii::t('app', 'State New'),
            'comment' => Yii::t('app', 'Comment'),
            'account_id' => Yii::t('app', 'Account ID'),
            'created_uts' => Yii::t('app', 'Created Uts'),
        ];
    }
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_uts'],
                ],
                'value' => function () {
                    return time();
                },
            ],
        ];
    }

    /**
     * @return int
     */
    public function getCreatedUts()
    {
        return $this->created_uts;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }


    /**
     * @return State
     */
    public function getStateNew()
    {
        if ($this->state_new === null && $this->state_new_id !== null) {
            $this->state_new = State::find()->where(['id' => $this->state_new_id])->one();
        }

        if ($this->state_new === null) {
            $this->state_new = new State();
        }

        return $this->state_new;
    }

    /**
     * @param State $stateNew
     */
    public function setStateNew(State $stateNew)
    {
        $this->state_new = $stateNew;
        $this->state_new_id = $stateNew->getId();
    }


    /**
     * @return State
     */
    public function getStateOld()
    {
        if ($this->state_old === null && $this->state_old_id !== null) {
            $this->state_old = State::find()->where(['id' => $this->state_old_id])->one();
        }

        if ($this->state_old === null) {
            $this->state_old = new State();
        }

        return $this->state_old;
    }

    /**
     * @param State $state_old
     */
    public function setStateOld(State $state_old)
    {
        $this->state_old = $state_old;
        $this->state_old_id = $state_old->getId();
    }
}
