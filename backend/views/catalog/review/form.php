<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\catalog\product\Review;

/* @var yii\web\View $this */
/* @var $review Review */

$this->title = 'Review';
?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1>Отзыв</h1>
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($review->getId() ? Url::to(['edit', 'id'=>$review->getId()]) : Url::to(['create'])),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>&nbsp;</legend>';
        echo $form->field($review, 'name');
        echo $form->field($review, 'email');
        echo $form->field($review, 'rating');
        echo $form->field($review, 'review')->textarea(['col' => 5, 'row' => 10]);
        echo '<div class="form-group"><div class="col-md-2"><label class="control-label">Статус</label></div><div class="col-md-10">';
        echo \kartik\select2\Select2::widget([
            'name' => 'Review[state]',
            'data' => Review::getStatus(),
            'value' => $review->getState(),
            'options' => [
                'placeholder' => 'Select ...',
            ],
        ]);
        echo '</fieldset>';
        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
