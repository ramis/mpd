<?php

use yii\db\Schema;
use yii\db\Migration;

class m150912_172932_geo_ip extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%geo_cities}}', [
            'id' => Schema::TYPE_PK,
            'city' => Schema::TYPE_STRING  . ' NOT NULL',
            'region' => Schema::TYPE_STRING  . ' NOT NULL',
            'area' => Schema::TYPE_STRING  . ' NOT NULL',
            'geo_object' => Schema::TYPE_TEXT,
        ]);

        $this->createTable('{{%geo_cidr}}', [
            'id' => Schema::TYPE_PK,
            'ip_start' => Schema::TYPE_BIGINT  . ' NOT NULL',
            'ip_end' => Schema::TYPE_BIGINT  . ' NOT NULL',
            'ip_interval' => Schema::TYPE_STRING  . ' NOT NULL',
            'geo_cities_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_geo_cidr_geo_cities_id', '{{%geo_cidr}}', 'geo_cities_id', '{{%geo_cities}}', 'id', 'NO ACTION', 'NO ACTION');

        $this->createIndex('geo_cidr_index_ip_start', '{{%geo_cidr}}', 'ip_start');
        $this->createIndex('geo_cidr_index_ip_end', '{{%geo_cidr}}', 'ip_end');
    }

    public function safeDown()
    {
    }
}
