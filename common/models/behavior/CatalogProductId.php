<?php

namespace common\models\behavior;

use common\models\catalog\Product as CatalogProduct;

trait CatalogProductId
{

    /**
     * @var CatalogProduct
     */
    private $product;

    /**
     * @return CatalogProduct
     */
    public function getCatalogProduct()
    {
        if ($this->product === null && $this->catalog_product_id !== null) {
            $this->product = CatalogProduct::find()->where(['id' => $this->catalog_product_id])->one();
        }

        return $this->product;
    }

    /**
     * @return int
     */
    public function getCatalogProductId()
    {
        if ($this->product !== null && $this->catalog_product_id !== null) {
            $this->catalog_product_id = $this->product->getId();
        }

        return $this->catalog_product_id;
    }

    /**
     * @param CatalogProduct $product
     */
    public function setCatalogProduct(CatalogProduct $product)
    {
        $this->product = $product;
        $this->catalog_product_id = $product->getId();
    }

}