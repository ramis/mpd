<?php

use yii\db\Schema;
use yii\db\Migration;

class m150905_113909_direct_ads_text extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%direct_ads_text}}', [
            'id' => Schema::TYPE_PK,
            'description' => Schema::TYPE_STRING  . ' NOT NULL',
            'updated_uts' => Schema::TYPE_BIGINT  . ' NOT NULL',
            'campaign_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_direct_ads_text_campaign_id', '{{%direct_ads_text}}', 'campaign_id', '{{%direct_campaign}}', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function safeDown()
    {

    }
}
