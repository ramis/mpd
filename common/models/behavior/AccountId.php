<?php

namespace common\models\behavior;

use common\models\Account;

trait AccountId
{

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * @param int $account_id
     * @return $this
     */
    public function setAccountId($account_id)
    {
        $this->account_id = $account_id;
        return $this;
    }

    /**
     * @param bool $isCreated
     */
    public function getAccount($isCreated = false)
    {
        $this->account = Account::find()->where(['id' => $this->account_id])->one();

        if($isCreated && $this->account === null){
            $this->account = new Account();
            $this->account->setAuto(true);
            $this->account->setName();
        }
    }

}