<?php
namespace console\controllers;

use backend\models\direct\Campaign;
use backend\models\direct\Sitelinksset;
use common\models\catalog\Category;
use common\models\catalog\Product;
use Yii;
use yii\base\Exception;
use yii\console\Controller;

class MarketController extends Controller
{

    const DIRECT_API_URL = 'http://mrplaydoh.ru';

    public static function checkDirectApi()
    {
        $curlInit = curl_init(self::DIRECT_API_URL);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curlInit);
        curl_close($curlInit);

        var_dump($response);die;

        return ($response);
    }

    private static function curl($request)
    {
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'url');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 12);
            curl_setopt($ch, CURLOPT_USERAGENT, 'JSON-RPC PHP Client');
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));

            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($httpcode !== 200) {
                throw new Exception('Bad request ' . $httpcode);
            }
            $response = json_decode($result, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new Exception (json_last_error_msg());
            }

            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Генерация быстрых ссылок
     * 1 - Товар
     * 2 - Категория
     * 3 - Товар
     * 4 - Категория
     */
    public function actionSitelinksset()
    {
        $products = Product::find()
            ->where(['is_published' => true,])
            ->andWhere(['<>', 'title_direct', ''])
            ->andWhere(['>', 'quantity', '3'])->all();

        foreach ($products as $product) {
            print $product->getTitleDirect() . "\n";
        }

        $categories = Category::find()->where(['is_published' => true, 'is_special' => false])->all();
        foreach ($categories as $category) {
            print $category->getTitle() . "\n";
        }

    }

    public function pingDirectApi()
    {
        $handle = fsockopen('example.com', 80);
        if ($handle !== false)
        {
            print('работает');
            fclose($handle);
        } else {
            print('не работает');
        }
    }

    public function getChanges()
    {

    }
    public function getRegions()
    {

    }

    public function actionChecksitelinksset()
    {
        $productIds = [];
        $products = Product::find()
            ->where(['is_published' => false])
            ->orFilterWhere(['<', 'price', 100])
            ->orFilterWhere(['<', 'quantity', 3])
            ->all();
        foreach ($products as $product) {
            $productIds[$product->getId()] = $product->getId();
        }
        if (!empty($productIds)) {
            Sitelinksset::updateAll(['is_active' => false], ['id1' => $productIds, 'class1' => 'Product']);
            Sitelinksset::updateAll(['is_active' => false], ['id3' => $productIds, 'class1' => 'Product']);
        }

        $categoryIds = [];
        $categories = Category::find()
            ->where(['is_published' => false])
            ->all();
        foreach ($categories as $category) {
            $categoryIds[$category->getId()] = $category->getId();
        }
        if (!empty($categoryIds)) {
            Sitelinksset::updateAll(['is_active' => false], ['id2' => $productIds, 'class1' => 'Category']);
            Sitelinksset::updateAll(['is_active' => false], ['id4' => $productIds, 'class1' => 'Category']);
        }
    }

    /**
     * Крон метод получение статистики для всех активных компаний
     */
    public function actionGetSummaryStat()
    {
        $campaings = Campaign::find()->where(['state' => Campaign::STATE_ACTIVE])->all();
        foreach($campaings as $campaing) {
            try {
                /** @var Campaign $campaing */
                $this->getSummaryStatByCampaign($campaing);
            } catch (Exception $e) {
                Yii::error("Direct error | " . $e->getMessage());
                break;
            }
        }
    }

    /**
     * Получение статистики по компании от Директа, если необходимо то остановка компании
     *
     * @param Campaign $campaign
     * @throws Exception
     */
    private function getSummaryStatByCampaign(Campaign $campaign)
    {
        //Проверка доступности апи и наличие баллов для аккаунта
        if(self::checkDirectApi() === false) {
            throw new Exception('Direct Api unavailable');
        }

        $request = [
            'method' => 'GetSummaryStat',
            'param' => [
                'CampaignIDS' => [$campaign->getId()],
                'StartDate' => date('Y-m-d'),
                'EndDate' => date('Y-m-d')
            ]
        ];

        try {
            //Получаем ответ от Директа
            $response = self::curl($request);

            //Ответ пуст - ошибка
            if (empty($response)) {
                throw new Exception('Not stat campaign by period');
            }
            Yii::info("Direct campaing stat info | " . json_encode($response));
            //Анализируем полученную статистику
            $campaign->analizeStatDirect($response);

            //Если нужно остановить компанию
            if ($campaign->mustBeStopped()) {

                //То пытаемся остановить компанию. Всего 3 попытки
                $cnt = 0;
                while ($cnt < 2 && $campaign->mustBeStopped()) {
                    try {
                        //Останавливаем компанию в директе
                        $response = self::curl(['method' => 'StopCampaign', 'param' => ['CampaignID' => $campaign->getId()]]);
                        if(!(!empty($response['data']) && $response['data'] === 1)) {
                            $campaign->setState(Campaign::STATE_STOP);
                            if ($campaign->save() === false) {
                                throw new Exception (json_encode($campaign->getFirstErrors()));
                            }
                        }
                    } catch (Exception $e) {
                        //Не смогли остановить компанию
                        $cnt++;
                        //Ждем 10 секунд
                        sleep(10);
                    }
                }

                //Если не смогли остановить компанию то отмечаем ее как требующую проверки
                if ($campaign->mustBeStopped()) {
                    $campaign->setBell(Campaign::BELL_NEED_MANUAL_CHECK);
                    if ($campaign->save() === false) {
                        throw new Exception (json_encode($campaign->getFirstErrors()));
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}
