<?php

$domain = 'mpd.box';

return [
    'DOMAIN' => $domain,
    'HTTP_SITE' => 'http://' . $domain . '/',
    'HTTP_ADMIN' => 'http://adm.' . $domain . '/',

    'HTTP_IMAGE' => 'http://'.$domain.'/images/',

    'HTTP_IMAGE_C_210_190' => 'http://'.$domain.'/images/',

    'HTTP_IMAGE_P_210_190' => 'http://'.$domain.'/images/',
    'HTTP_IMAGE_P_100_100' => 'http://'.$domain.'/images/',

    'HTTP_IMAGE_P_200_200' => 'http://'.$domain.'/images/',
    'HTTP_IMAGE_P_500_500' => 'http://'.$domain.'/images/',
    'HTTP_IMAGE_P_430_430' => 'http://'.$domain.'/images/',
    'HTTP_IMAGE_P_120_120' => 'http://'.$domain.'/images/',
    'HTTP_IMAGE_P_50_50'   => 'http://'.$domain.'/images/',
    'HTTP_IMAGE_P_150_150' => 'http://'.$domain.'/images/',
    'HTTP_IMAGE_P_420_360' => 'http://'.$domain.'/images/',
];