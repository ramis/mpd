<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * Abstract controller
 */
class AbstractController extends Controller
{
    protected $_bread_crumbs = [];

    /**
     * @return array
     */
    public function getBreadCrumbs()
    {
        return $this->_bread_crumbs;
    }

    /**
     * @param array $item
     */
    public function appendBreadCrumbs(array $item) {
        $this->_bread_crumbs[] = $item;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

}
