<?php
namespace common\tools;

use yii\base\Exception;
use yii\db\ActiveRecord;

class Tools
{
    private static $_urmParams = ['sort', 'order', 'price_from', 'price_to', 'limit', 'page', 'hero', 'gender', '_search'];

    /**
     * Подстановка нужного окончания по количеству
     *
     * @param $number
     * @param array $titles
     * @return string
     */
    public static function humanPluralForm($number, $titles = ['комментарий', 'комментария', 'комментариев'])
    {
        $cases = [2, 0, 1, 1, 1, 2];
        return $number . " " . $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    /**
     * Получение 1 ошибки при сохранение модели
     *
     * @param ActiveRecord $model
     * @throws Exception
     */
    public static function getError(ActiveRecord $model)
    {
        $errors = $model->getFirstErrors();

        if(current($errors)){
            throw new Exception (current($errors));
        }

        throw new Exception ('Ошибка при сохранение модели');
    }

    /**
     * Разбор и валидация урла
     *
     * @param $url
     * @return array|bool
     */
    public static function parseUrl($url)
    {
        if(empty($url)){
            return false;
        }
        $urlParams = parse_url($url);

        $urlPath = !empty($urlParams['path']) ? $urlParams['path'] : '';
        $urlQuery = [];
        if(!empty($urlParams['query'])){

            $query = explode('&', $urlParams['query']);

            foreach($query as $param){
                $params = explode('=', $param);
                if(!empty($params[0]) && !empty($params[1])){
                    if(in_array($params[0],self::$_urmParams)){
                        $urlQuery[$params[0]] = $params[1];
                    }
                }
            }
        }

        return [$urlPath, $urlQuery];
    }
}