<?php

namespace common\models\order;

use common\models\order\shipping\Courier;
use common\models\order\shipping\Pickup;
use common\models\order\shipping\Post;
use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Id;
use common\models\behavior\Order;

/**
 * This is the model class for table "{{%order_shipping}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $order_id
 * @property integer $cost
 */
class Shipping extends ActiveRecord
{
    use Id;
    use Order;

    CONST TYPE_COURIER = 10;
    CONST TYPE_PICKUP = 20;
    CONST TYPE_POST = 30;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_shipping}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'order_id', 'cost'], 'required'],
            [['type', 'order_id', 'cost'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'order_id' => Yii::t('app', 'Order ID'),
            'cost' => Yii::t('app', 'Cost'),
        ];
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return array|bool|null|ActiveRecord
     */
    public function getOrderShippingValue()
    {
        switch ($this->type) {
            case self::TYPE_COURIER:
                return Courier::find()->where(['order_shipping_id' => $this->id])->one();
                break;
            case self::TYPE_POST:
                return Post::find()->where(['order_shipping_id' => $this->id])->one();
                break;
            case self::TYPE_PICKUP:
                return Pickup::find()->where(['order_shipping_id' => $this->id])->one();
                break;
        }

        return false;
    }


    public function getText()
    {
        $shippingGeoValue = $this->getOrderShippingValue();

        return $shippingGeoValue->getText();
    }

}
