<?php

use yii\db\Schema;
use yii\db\Migration;

class m150830_174129_direct_ads extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%direct_ads}}', [
            'id' => Schema::TYPE_PK,

            'direct_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'adgroup_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'title' => Schema::TYPE_STRING  . ' NOT NULL',
            'text' => Schema::TYPE_STRING  . ' NOT NULL',
            'mobile' => Schema::TYPE_BOOLEAN  . ' NOT NULL',
            'href' => Schema::TYPE_STRING  . ' NOT NULL',
            'vcard_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'adimage_hash' => Schema::TYPE_STRING  . ' NOT NULL',
            'sitelinkset_id' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'is_active' => Schema::TYPE_BOOLEAN . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_direct_ads_adgroup_id', '{{%direct_ads}}', 'adgroup_id', '{{%direct_adgroup}}', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_direct_ads_sitelinkset_id', '{{%direct_ads}}', 'sitelinkset_id', '{{%direct_sitelinksset}}', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function safeDown()
    {
    }

}
