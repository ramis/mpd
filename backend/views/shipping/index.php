<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\Geo;
use common\models\Shipping;
use common\models\shipping\GeoValue;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
$this->title = 'Города';

?>

<div class="site-index">
    <div class="right"><?= Html::a('Добавить', \yii\helpers\Url::to(['create'])); ?></div>

    <h1>Города</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'sort',
            [
                'attribute' => 'Shipping',
                'format' => 'raw',
                'value' => function (Geo $geo) {


                    $shippingValues = GeoValue::find()->where(['geo_id' => $geo->getId()])->orderBy(['shipping_id'=>'asc'])->all();
                    $shipping = '';
                    $count = 0;
                    $pickup = 0;
                    foreach ($shippingValues as $value) {
                        switch ($value->getShippingId()) {
                            case Shipping::SHIPPING_COURIER:
                                $des = $value->getDescription() ? $value->getDescription() : '';
                                $shipping .= 'Курьер ' . $des . ' - ' . $value->getPrice() . '<br/>';
                                break;
                            case Shipping::SHIPPING_POST:
                                $shipping .= 'Почта России - ' . $value->getPrice() . '<br/>';
                                break;
                            case Shipping::SHIPPING_PICKUP:
                                $count++;
                                $pickup = $value->getPrice();
                                break;
                        }
                    }
                    if ($count > 0) {
                        $shipping .= 'Самовывоз (' . $count . ') - ' . $pickup . '<br/>';
                    }
                    return $shipping;
                }
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Geo $geo) {
                    return Html::a('Редактировать', ['edit', 'id' => $geo->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
