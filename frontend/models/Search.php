<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ints\BreadCrumbsInts;

class Search extends Model implements BreadCrumbsInts
{

    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function getBreadCrumbs()
    {
        $breadCrumbs[] = 'Поиск';

        return $breadCrumbs;
    }
}
