<?php
namespace common\tools;

use yii\helpers\VarDumper;

class Vd
{
    public static function dump($text) {
        VarDumper::dump($text, 10, true);
    }
}
