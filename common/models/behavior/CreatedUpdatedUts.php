<?php

namespace common\models\behavior;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

trait CreatedUpdatedUts
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_uts', 'updated_uts'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_uts'],
                ],
                'value' => function () {
                    return time();
                },
            ],
        ];
    }

    /**
     * @return int
     */
    public function getCreatedUts()
    {
        return $this->created_uts;
    }

    /**
     * @return int
     */
    public function getUpdatedUts()
    {
        return $this->updated_uts;
    }

}