<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\NullModel;

/**
 * Site controller
 */
class MarketController extends Controller
{
    private static $_social = ['facebook.com', 'vk.com', 'instagram.com', 'ok.ru', 'www.livejournal.com', 'www.liveinternet.ru', 'plus.google.com',
        'youtube.com'];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionUtm()
    {
        $links = [];
        $url = '';
        if (Yii::$app->request->getIsPost()) {
            $url = Yii::$app->request->post('url');
            if (!empty($url)) {
                foreach (self::$_social as $social) {
                    $links[$social] = $url . '?utm_source=' . $social . '&utm_medium=social';
                }
            }
        }
        return $this->render('utm', ['links' => $links, 'url' => $url]);
    }

}
