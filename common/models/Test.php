<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\media\ints\MediaInts;
use common\models\behavior\Media;

/**
 * This is the model class for table "{{%test}}".
 *
 * @property integer $id
 * @property integer $media_id
 * @property string $title
 *
 * @property Media $media
 */
class Test extends ActiveRecord implements MediaInts
{
    use Id;
    use Title;
    use Media;

    /**
     * @var Media
     */
    private $media;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%test}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id'], 'integer'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'media_id' => Yii::t('app', 'Media ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

}
