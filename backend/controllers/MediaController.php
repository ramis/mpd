<?php
namespace backend\controllers;

use common\models\media\Files;

class MediaController extends AbstractController
{

    public function actionRemove($id)
    {
        $files = Files::findOne((int)$id);
        if($files){
            $files->remove();
        }

    }

}
