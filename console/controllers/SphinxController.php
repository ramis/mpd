<?php
namespace console\controllers;

use Yii;
use yii\base\ErrorException;

class SphinxController extends AbstractController
{

    /**
     * Обновление индекса sphinx
     */
    public function actionIndex()
    {
        $pathConfig = "/opt/mrplaydoh.ru/sphinx/sphinx.conf";

        passthru("/usr/bin/searchd --stop --config " . $pathConfig);

        passthru("/usr/bin/indexer --config " . $pathConfig . " --all --rotate");

        passthru("/usr/bin/searchd --config " . $pathConfig);

        return self::EXIT_CODE_NORMAL;

    }


    /**
     * Тест
     */
    public function actionTest()
    {


            $where = '*додошка*';
            $ids = [];
            $cl = new \SphinxClient();

            $ranker = SPH_RANK_PROXIMITY_BM25;
            $ranker = SPH_RANK_BM25;
            $mode = SPH_MATCH_ALL;
            $host = "localhost";
            $port = 9312;
            $index = "*";

            $cl->SetServer($host, $port);
            $cl->SetConnectTimeout(1);
            $cl->SetArrayResult(true);
            $cl->SetFieldWeights(['barcode' => 10, 'article' => 5, 'title' => 3, 'title2' => 2, 'description' => 1]);
            $cl->SetMatchMode($mode);
            $cl->SetLimits(0, 100);

            $cl->SetRankingMode($ranker);
            $res = $cl->Query($where, 'catalogProductIndex');

        if ($res !== false && !$cl->GetLastWarning()) {
            $ids = [];
            if (!empty($res['matches'])) {
            foreach ($res['matches'] as $matches) {
                $ids[] = (int)$matches['id'];
            }
            }
        }

        var_dump($ids);

        return self::EXIT_CODE_NORMAL;
    }
}
