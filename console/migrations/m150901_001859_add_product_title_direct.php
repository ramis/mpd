<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_001859_add_product_title_direct extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%catalog_product}}', 'title_direct', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
    }
}
