<?php

namespace frontend\widgets;

use yii\bootstrap\Widget;
use common\models\catalog\Category;

class LeftColumn extends Widget
{

    public $categories;

    /**
     * Renders the widget.
     */
    public function run()
    {
        if ($this->categories === null) {
            $this->categories = Category::find()->where(['is_special' => false, 'is_published' => true])->orderBy('sort asc')->all();
        }

        return $this->render('leftColumn', ['categories' => $this->categories]);
    }

}
