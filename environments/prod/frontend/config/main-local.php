<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'hG9f9fdR26rJmj5trFjvEeEbsi9GjKgz',
        ],
    ],
];

return $config;