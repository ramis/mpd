<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\FileInput;
use common\models\catalog\Product;
use common\models\catalog\Category;
use common\models\catalog\Attribute;
use common\models\catalog\Tag;
/* @var yii\web\View $this */
/* @var $product Product */

//All Category
$categories = Category::find()->orderBy('sort asc')->all();
$dataCategories = [];
foreach ($categories as $category) {
    $dataCategories[$category->getId()] = $category->getTitle();
}

//Product Category
$productCategories = $product->getCategories();
$productCategoriesIds = [];
foreach ($productCategories as $category) {
    $productCategoriesIds[] = $category->getId();
}

//All Tag
$tags = Tag::find()->orderBy('group_id asc')->all();
$dataTags = [];
foreach ($tags as $tag) {
    $dataTags[$tag->getGroup()->getTitle()][$tag->getId()] = $tag->getTitle();
}

//Product Category
$productTags = $product->getTags();
$productTagsIds = [];
foreach ($productTags as $tag) {
    $productTagsIds[] = $tag->getId();
}

//All Attribute
$attributes = Attribute::find()->orderBy('sort asc')->all();
$values = $product->getAttributeValues();
$productValuesVal = [];
foreach ($values as $value) {
    $productValuesVal[$value->getCatalogAttribute()->getId()] = $value->getValue();
}
?>
<div class="site-index row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($product->getId() ? Url::to(['edit', 'id' => $product->getId()]) : Url::to(['create'])),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<div class="button">' . Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) . '</div>';

        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($product, 'title');
        echo $form->field($product, 'title2');
        echo $form->field($product, 'title_direct');
        echo $form->field($product, 'article');
        echo $form->field($product, 'barcode');
        echo $form->field($product, 'code');
        echo $form->field($product, 'base_keywords')->textarea(['col' => 5, 'row' => 10]);
        echo $form->field($product, 'description')->textarea(['col' => 5, 'row' => 10]);
        echo $form->field($product, 'is_published')->checkbox();
        echo '</fieldset>';

        echo '<fieldset><legend>Цены и количество</legend>';
        echo $form->field($product, 'quantity');
        echo $form->field($product, 'price');
        echo '</fieldset>';

        echo '<fieldset><legend>Категории</legend>';

        echo $form->field($product, 'catalog_category_id')->dropDownList($dataCategories, ['class' => 'select-chosen col-md-10']);

        echo '<div class="form-group"><div class="col-md-2"><label class="control-label">Выберите категории</label></div><div class="col-md-10">';

        echo Html::dropDownList('product_categories_id', $productCategoriesIds, $dataCategories, ['multiple' => 'multiple', 'class' => 'select-chosen col-md-10']);

        echo '</div></div>';
        echo '</fieldset>';

        echo '<fieldset><legend>Теги</legend>';
        echo '<div class="form-group"><div class="col-md-2"><label class="control-label">Выберите тег</label></div><div class="col-md-10">';
        echo Html::dropDownList('product_tags_id', $productTagsIds, $dataTags, ['multiple' => 'multiple', 'class' => 'select-chosen col-md-10']);

        echo '</div></div>';
        echo '</fieldset>';

        echo '<fieldset><legend>Характеристики</legend>';
        foreach ($attributes as $attribute) {
            echo '<div class="form-group"><div class="col-md-2"><label class="control-label">' . $attribute->getTitle() . '</label></div><div class="col-md-10">';
            echo '<textarea name="values[' . $attribute->getId() . ']" class="form-control" id="product-seo_description">';
            echo (!empty($productValuesVal[$attribute->getId()]) ? $productValuesVal[$attribute->getId()] : '');
            echo '</textarea>';
            echo '</div></div>';
        }
        echo '</fieldset>';

        echo '<fieldset><legend>Сео</legend>';
        echo $form->field($product, 'seo_title');
        echo $form->field($product, 'seo_keywords');
        echo $form->field($product, 'seo_description')->textarea(['col' => 5, 'row' => 10]);
        echo '</fieldset>';

        echo '<fieldset><legend>Картинки</legend>';
        $media = $product->getMedia();
        if ($media) {
            $images = $media->getImages();
            echo '<div><div class="col-md-2"></div><div class="col-md-10 images">';
            foreach ($images as $image) {
                echo '<div>' . $image->getHtml(['width' => 200, 'height' => 200]) . '</div>';
            }
            echo '</div></div>';
        }
        echo $form->field($product->getMedia()->getImageForm(), 'imageFiles[]')->widget(FileInput::classname(),
            ['options' => ['multiple' => true, 'accept' => 'image/*']]);
        echo '</fieldset>';

        $video = '';
        if($media) {
            $videos = $media->getVideos();
            foreach($videos as $item) {
                $video = $item->getHash();
            }
        }
        echo '<fieldset><legend>Видео</legend>';
        echo '<div class="form-group field-text">
                <div class="col-md-2"><label for="video-text1" class="control-label">Видео</label></div>
                <div class="col-md-10"><input type="text" name="video[]" class="form-control" id="video-text1" value="'.Html::encode($video).'"></div>
                <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
             </div>';
        echo '</fieldset>';

        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
