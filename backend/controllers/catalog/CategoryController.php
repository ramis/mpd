<?php
namespace backend\controllers\catalog;

use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use backend\controllers\AbstractController;
use common\models\catalog\Category;
use common\models\media\ints\MediaInts;

/**
 * Catalog Category controller
 */
class CategoryController extends AbstractController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->appendBreadCrumbs([
            'title' => 'Каталог',
            'url' => '',
        ]);
        $this->appendBreadCrumbs([
            'title' => 'Категория',
            'url' => ['catalog/category'],
        ]);

        return parent::beforeAction($action);
    }

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Category::find()->orderBy('sort asc'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    /**
     * Category list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Category create
     *
     * @return string
     */
    public function actionCreate()
    {
        $category = new Category();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $category->load($_POST);

            if ($category->save()) {

                if ($category instanceof MediaInts) {
                    $media = $category->getMedia();
                    $media->saveContainer($category);
                }
                return Yii::$app->getResponse()->redirect(Url::to(['catalog/category']));
            }
            $errors = $category->getErrors();
        }

        $this->appendBreadCrumbs([
            'title' => 'Добавление',
            'url' => '',
        ]);

        return $this->render('form', ['category' => $category, 'errors' => $errors]);
    }

    /**
     * Category create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Category $category */
        $category = Category::findOne((int)$id);

        if ($category === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['catalog/category']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $category->load($_POST);

            if ($category->save()) {
                if ($category instanceof MediaInts) {
                    $media = $category->getMedia();
                    $media->saveContainer($category);
                }

                return Yii::$app->getResponse()->redirect(Url::to(['catalog/category']));
            }
            $errors = $category->getErrors();
        }

        $this->appendBreadCrumbs([
            'title' => $category->getTitle(),
            'url' => '',
        ]);

        return $this->render('form', ['category' => $category, 'errors' => $errors]);
    }

}
