<?php

use yii\db\Schema;
use yii\db\Migration;

class m150830_181759_direct_bids extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%direct_bids}}', [
            'id' => Schema::TYPE_PK,
            'adgroup_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'bid' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'context_bid' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'strategy_priority' => Schema::TYPE_INTEGER  . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_direct_bids_adgroup_id', '{{%direct_bids}}', 'adgroup_id', '{{%direct_adgroup}}', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function safeDown()
    {
    }

}
