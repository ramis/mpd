<?php

namespace common\models\catalog\product;

use Yii;
use yii\db\ActiveRecord;
use common\models\catalog\Product as CatalogProduct;
use common\models\catalog\Category as CatalogCategory;

/**
 * This is the model class for table "{{%catalog_product_category}}".
 *
 * @property integer $catalog_product_id
 * @property integer $catalog_category_id
 *
 * @property CatalogCategory $catalogCategory
 * @property CatalogProduct $catalogProduct
 */
class Category extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_product_id', 'catalog_category_id'], 'required'],
            [['catalog_product_id', 'catalog_category_id'], 'integer'],
            [['catalog_product_id', 'catalog_category_id'], 'unique',
                'targetAttribute' => ['catalog_product_id', 'catalog_category_id'],
                'message' => 'The combination of Catalog Product ID and Catalog Category ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'catalog_product_id' => 'Catalog Product ID',
            'catalog_category_id' => 'Catalog Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogCategory()
    {
        return $this->hasOne(CatalogCategory::className(), ['id' => 'catalog_category_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProduct()
    {
        return $this->hasOne(CatalogProduct::className(), ['id' => 'catalog_product_id'])->one();
    }

    public function setCatalogProduct(CatalogProduct $product)
    {
        $this->catalog_product_id = $product->getId();
    }

    public function setCatalogCategory(CatalogCategory $category)
    {
        $this->catalog_category_id = $category->getId();
    }

}
