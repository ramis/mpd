<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\catalog\Product;
use common\models\catalog\Tag;
use common\models\catalog\tag\Group;

/* @var yii\web\View $this */
/* @var $tag Tag */


//All Product
$products = Product::find()->all();
$dataProducts = [];
foreach ($products as $product) {
    $dataProducts[$product->getId()] = $product->getTitle();
}

//Product Category
$tagProducts = $tag->getCatalogProducts();
$tagProductsIds = [];
foreach ($tagProducts as $product) {
    $tagProductsIds[] = $product->getId();
}

//All Group
$groups = Group::find()->all();
$dataGroups = [];
foreach ($groups as $group) {
    $dataGroups[$group->getId()] = $group->getTitle();
}

$groupId = ($tag->getId() ? $tag->getGroup()->getId() : null);
?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1><?= ($tag->getId() ? $tag->getTitle() : 'Новый') ?></h1>
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($tag->getId() ? Url::to(['edit', 'id'=>$tag->getId()]) : Url::to(['create'])),
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($tag, 'title');
        echo $form->field($tag, 'tag');
        echo $form->field($tag, 'url');

        echo '</fieldset>';
        echo '<fieldset><legend>Группа</legend>';
        echo '<div class="form-group"><div class="col-md-2"><label class="control-label">Выберите категории</label></div><div class="col-md-10">';
        echo \kartik\select2\Select2::widget([
            'name' => 'Tag[group_id]',
            'data' => $dataGroups,
            'value' => $groupId,
            'options' => [
                'placeholder' => 'Select group ...',
            ],
        ]);
        echo '</div></div>';
        echo '</fieldset>';

        echo '<fieldset><legend>Товары</legend>';
        echo '<div class="form-group"><div class="col-md-2"><label class="control-label">Выберите товары</label></div><div class="col-md-10">';
        echo \kartik\select2\Select2::widget([
            'name' => 'products_ids',
            'data' => $dataProducts,
            'value' => $tagProductsIds,
            'size'=>\kartik\select2\Select2::SMALL,
            'options' => [
                'placeholder' => 'Select tag ...',
                'multiple' => true
            ],
        ]);
        echo '</div></div>';
        echo '</fieldset>';

        echo '<fieldset><legend>Сео</legend>';
        echo $form->field($tag, 'seo_title');
        echo $form->field($tag, 'seo_keywords');
        echo $form->field($tag, 'seo_text')->textarea(['col' => 30, 'row' => 10]);;
        echo $form->field($tag, 'seo_description')->textarea(['col' => 30, 'row' => 10]);;
        echo '</fieldset>';

        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
