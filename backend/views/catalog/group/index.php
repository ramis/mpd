<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\catalog\tag\Group;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">
    <div class="right"><?= Html::a('Добавить', \yii\helpers\Url::to(['create'])); ?></div>

    <h1>Группы тегов</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Id',
                'format' => 'raw',
                'value' => function (Group $group) {
                    return '<a name="' . $group->getId() . '" >' . $group->getId() . '</a>';
                }
            ],
            [
                'attribute' => 'Title',
                'value' => function (Group $group) {
                    return $group->getTitle();
                }
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Group $group) {
                    return Html::a('Редактировать', ['edit', 'id' => $group->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
