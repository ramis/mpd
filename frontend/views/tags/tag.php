<?php
use common\models\catalog\Tag;
use common\models\catalog\Product;
use frontend\widgets\LeftColumn;
use frontend\widgets\BreadCrumbs;
use frontend\widgets\Paginator;
use frontend\widgets\Sort;

/* @var $this yii\web\View */
/* @var Tag $tag */
/* @var Product[] $products */

$this->title = $tag->getSeoTitle() . ' | товары Play Doh ' . $tag->getTitle() . ' купить в магазине mrplaydoh.ru ';


if ($tag->getSeoDescription()) {
    Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $tag->getSeoDescription()]);
} else {
    Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => 'Купить '.$tag->getSeoTitle().' от Hasbro по самым выгодным ценам вы можете в интернет-магазине MrPlayDoh.ru! Доставка по Москве и во все регионы России!']);
}
if ($tag->getSeoKeywords()) {
    Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $tag->getSeoKeywords()]);
} else {
    Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $tag->getTag() . ' плей до play doh хасбро hasbro купить цена интернет магазин']);
}

$countPage = ceil($tag->getProductsCount() / $limit);
$products = $tag->getProducts(['sort' => $sort, 'limit' => $limit, 'offset' => ($page - 1) * $limit]);
?>

<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col">

        <?= BreadCrumbs::widget(['breadCrumbs' => $tag]); ?>

        <div id="site_index" class="tag_index">
            <?php
            echo '<h1>' . $tag->getSeoTitle() . '</h1>';
            /*       ?>

                   <div id="block_filters">
                       <div id="clear_all">x Сбросить все</div>
                       <ul>
                           <li>
                               Фильтр
                           </li>
                           <li class="filter">
                               Цена
                           </li>
                           <li class="filter active" style="width: 110px">
                               Категория
                               <div class="selected_filter">2 категории</div>
                           </li>
                           <li class="filter">
                               Возраст
                               <div class="select_filters">
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">до 1 года</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 1 года до 2 лет</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 2 лет до 4 лет</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 4 лет</label>
                                   </div>
                                   <div class="filter_btn"><div class="btn">Применить</div></div>
                               </div>
                           </li>
                           <li class="filter">
                               Пол
                           </li>
                       </ul>
                   </div>
            */
            if ($tag->getProductsCount() > 0) {
                Sort::widget(['url' => $_SERVER['REQUEST_URI'], 'limit' => $limit, 'sort' => $sort, 'order' => $order]);
            }
            ?>

            <div class="clear"></div>

            <div class="product_item_list">
                <?php
                foreach ($products as $product) {
                    echo $this->render('/layouts/productItem', ['product' => $product]);
                }
                ?>
            </div>
            <div class="clear"></div>
            <?= Paginator::widget(['url' => $_SERVER['REQUEST_URI'], 'page' => $page, 'countPage' => $countPage]); ?>

            <?php
            if ($tag->getSeoText()) {
                $description = preg_replace("/\n/", "</p><p>", $tag->getSeoText());
                echo '<div class="description"><p>' . $description . '</p>';
                echo '<a href="https://plus.google.com/u/1/108888585391425984422/posts" rel="author">Mr Play-Doh</a></div>';
            }
            ?>

        </div>
    </div>
</div>