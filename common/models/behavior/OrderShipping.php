<?php

namespace common\models\behavior;

use common\models\order\Shipping as ModelShipping;

trait OrderShipping
{
    /**
     * @var ModelShipping
     */
    private $order_shipping;

    /**
     * @return ModelShipping
     */
    public function getOrderShipping()
    {
        if ($this->order_shipping === null && $this->order_shipping_id !== null) {
            $this->order_shipping = ModelShipping::find()->where(['id' => $this->order_shipping_id])->one();
        }

        if ($this->order_shipping === null) {
            $this->order_shipping = new ModelShipping();
        }

        return $this->order_shipping;
    }

    /**
     * @param ModelShipping $orderShipping
     */
    public function setOrderShipping(ModelShipping $orderShipping)
    {
        $this->order_shipping = $orderShipping;
        $this->order_shipping_id = $orderShipping->getId();
    }

}