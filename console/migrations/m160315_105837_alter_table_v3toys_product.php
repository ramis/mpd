<?php

use yii\db\Migration;
use yii\db\Schema;

class m160315_105837_alter_table_v3toys_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%catalog_product_v3toys_product}}', [
            'id' => Schema::TYPE_PK,
            'v3toys_product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'catalog_product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_catalog_product_v3toys_product_catalog_product_id', '{{%catalog_product_v3toys_product}}', 'catalog_product_id', '{{%catalog_product}}', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_catalog_product_v3toys_product_v3toys_product_id', '{{%catalog_product_v3toys_product}}', 'v3toys_product_id', '{{%v3toys_product}}', 'v3toys_product_id', 'NO ACTION', 'NO ACTION');

        $this->addColumn('{{%catalog_product}}', 'v3toys_product_id', Schema::TYPE_INTEGER);
        $this->addForeignKey('FK_catalog_product_v3toys_product_id', '{{%catalog_product}}', 'v3toys_product_id', '{{%v3toys_product}}', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
    }
}
