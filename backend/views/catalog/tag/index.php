<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\catalog\Tag;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
Tag::getTagsToCntProduct();
?>

<div class="site-index">
    <div class="right"><?= Html::a('Добавить', \yii\helpers\Url::to(['create'])); ?></div>

    <h1>Тег</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,

        'rowOptions' => function (Tag $tag) {
            if ($tag->getCntProduct() > 0) {
                return ['class' => 'description'];
            }
        },
        'columns' => [
            [
                'attribute' => 'Id',
                'format' => 'raw',
                'value' => function (Tag $tag) {
                    return '<a name="' . $tag->getId() . '" >' . $tag->getId() . '</a>';
                }
            ],
            [
                'attribute' => 'Title',
                'value' => function (Tag $tag) {
                    return $tag->getTitle();
                }
            ],
            [
                'attribute' => 'Tag',
                'value' => function (Tag $tag) {
                    return $tag->getTag();
                }
            ],
            [
                'attribute' => 'Url',
                'value' => function (Tag $tag) {
                    return $tag->getUrl();
                }
            ],
            [
                'attribute' => 'Group',
                'value' => function (Tag $tag) {
                    return $tag->getGroup()->getTitle();
                }
            ],
            [
                'attribute' => 'Cnt Product',
                'value' => function (Tag $tag) {
                    return $tag->getCntProduct();
                }
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Tag $tag) {
                    return Html::a('Редактировать', ['edit', 'id' => $tag->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
