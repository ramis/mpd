<?php

namespace frontend\widgets;

use common\models\ints\BreadCrumbsInts;
use yii\bootstrap\Widget;

class BreadCrumbs extends Widget
{

    public $breadCrumbs;

    /**
     * Renders the widget.
     */
    public function run()
    {
        if (!($this->breadCrumbs instanceof BreadCrumbsInts)) {
            return '';
        }

        return $this->render('breadCrumbs', ['breadCrumbs' => $this->breadCrumbs]);
    }

}
