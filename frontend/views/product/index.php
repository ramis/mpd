<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Dropdown;
use frontend\widgets\BreadCrumbs;
use frontend\models\AccountSingleton;
use common\models\catalog\Category;
use common\models\catalog\Product;
use common\tools\Tools;

/* @var $this yii\web\View */
/* @var Product $product */
/* @var Product[] $product2similar */
$this->title = $product->getPageTitle();
$product2similar = $product->getSimilarProducts();
$mainImage = $product->getMainImage();
$metaDescription = Html::encode(mb_substr($product->getDescription(), 0, mb_stripos($product->getDescription(), '.', 0, 'UTF-8'), 'UTF-8'));

Yii::$app->view->registerMetaTag(['name' => 'vk:url', 'content' => Url::to(['product/index', 'id' => $product->getId()], true)]);
Yii::$app->view->registerMetaTag(['name' => 'vk:title', 'content' => $product->getTitle()]);
Yii::$app->view->registerMetaTag(['name' => 'vk:image', 'content' => Yii::$app->params['HTTP_IMAGE_P_500_500'] . $mainImage->getHash()]);
Yii::$app->view->registerMetaTag(['name' => 'vk:description', 'content' => $metaDescription]);
Yii::$app->view->registerMetaTag(['name' => 'og:url', 'content' => Url::to(['product/index', 'id' => $product->getId()], true)]);
Yii::$app->view->registerMetaTag(['name' => 'og:title', 'content' => $product->getTitle()]);
Yii::$app->view->registerMetaTag(['name' => 'og:image', 'content' => Yii::$app->params['HTTP_IMAGE_P_500_500'] . $mainImage->getHash()]);
Yii::$app->view->registerMetaTag(['name' => 'og:description', 'content' => $metaDescription]);
Yii::$app->view->registerMetaTag(['name' => 'og:site_name', 'content' => Yii::$app->params['DOMAIN']]);
Yii::$app->view->registerMetaTag(['name' => 'og:type', 'content' => 'website']);

if ($product->getSeoDescription()) {
    Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $product->getSeoDescription()]);
} else {
    Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => 'Купить ' . $product->getTitle() . ' по низким ценам вы можете в интернет-магазине MrPlayDoh с доставкой по всей России!']);
}
if ($product->getSeoKeywords()) {
    Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $product->getSeoKeywords()]);
} else {
    Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $product->getTitleDirect() . ' плей до play doh хасбро hasbro купить цена интернет магазин']);
}

/**
 * @TODO Для просмотренных товаров нужно создавать аккаунт
 */
$account = AccountSingleton::getInstance()->getAccount(false);
$cartProducts = [];
$cart = ($account) ? $account->getCart(false) : null;
if ($cart) {
    $cartProducts = $cart->getCartProducts();
}

$categories = Category::find()->where(['is_special' => false, 'is_published' => true])->orderBy('sort asc')->all();
$category = $product->getMainCategory();
$dropDownListCategory = [];
foreach ($categories as $item) {
    $image = $item->getMainImage();
    $label = '';
    if ($image) {
        $label .= Html::img(Yii::$app->params['HTTP_IMAGE'] . $image->getHash(), ['width' => '45']);
    }
    $label .= mb_strtoupper($item->getTitle(), 'UTF-8');
    $dropDownListCategory[] = [
        'label' => $label,
        'url' => Url::to(['route/index', 'url' => $item->getUrl()]),
    ];
    $dropDownListCategory[] = '<li role="presentation" class="divider"></li>';
}
?>

<div id="body">
    <div id="center_col" class="product_body" itemscope itemtype="http://schema.org/Product">

        <div class="product_navigation">

            <div class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <div class="categories_title">Каталог товаров <b class="caret"></b></div>
                </a>
                <?= Dropdown::widget(['items' => $dropDownListCategory, 'encodeLabels' => false]); ?>
            </div>
            <?= BreadCrumbs::widget(['breadCrumbs' => $product]); ?>

        </div>

        <h1 itemprop="name"> <?= $product->getTitle() ?></h1>

        <?php
        if ($product->getReviewCount() > 0) {
            ?>
            <div class="info_user">
                <div id="product_reviews">
                    <div class="stars">
                        <?php
                        {
                            $reviewHtml = '';
                            $selected = $product->getReviewRating() === 5 ? 'checked' : '';
                            $reviewHtml .= '<label for="star5" class="' . $selected . '"></label>';
                            $selected = $product->getReviewRating() === 4 ? 'checked' : $selected;
                            $reviewHtml .= '<label for="star4" class="' . $selected . '"></label>';
                            $selected = $product->getReviewRating() === 3 ? 'checked' : $selected;
                            $reviewHtml .= '<label for="star3" class="' . $selected . '"></label>';
                            $selected = $product->getReviewRating() === 2 ? 'checked' : $selected;
                            $reviewHtml .= '<label for="star2" class="' . $selected . '"></label>';
                            $selected = $product->getReviewRating() === 1 ? 'checked' : $selected;
                            $reviewHtml .= '<label for="star1" class="' . $selected . '"></label>';
                            echo $reviewHtml;
                        }
                        ?>
                    </div>
                    <span class="pseudo_link"><?= Tools::humanPluralForm($product->getReviewCount(), ['отзыв', 'отзыва', 'отзывов']) ?></span>
                </div>
                <!--                    <div id="product_favorite">-->
                <!--                        В избранное-->
                <!--                    </div>-->
                <!--                    <div id="product_tags">-->
                <!--                        Тэги: машинки синий мальчикам-->
                <!--                    </div>-->
            </div>

            <div class="clear"></div>
            <?php
        }
        ?>
        <div class="info_product">
            <div class="info_product_main">
                <div class="info_product_main">
                    <div class="info_product_photo">
                        <?php
                        $media = $product->getMedia();
                        if ($media) {
                            $images = $media->getImages();
                            ?>
                            <ul class="product_images">
                                <?php
                                foreach ($images as $k => $image) {
                                    echo '<li ' . ($k === 0 ? 'class="active"' : '') . '>' . $image->getHtml(['width' => 50, 'height' => 50]) . '</li>';
                                }

                                $videos = $media->getVideos();
                                if (!empty($videos)) {
                                    foreach ($videos as $k => $video) {
                                        echo '<li id="video_' . $video->getId() . '" class="prev_video">&nbsp;</li>';
                                    }
                                }
                                ?>
                            </ul>
                            <?php
                            $mainImage = $product->getMainImage();
                            if ($mainImage) {

                                $videoContainer = '';
                                if (!empty($videos)) {
                                    foreach ($videos as $k => $video) {
                                        $videoContainer .= '<div id="container_video_' . $video->getId() . '" class="container_video">' . $video->getHash() . '</div>';
                                    }
                                }
                                echo '<div id="container_media">' . $mainImage->getHtml(['width' => 500, 'height' => 500, 'id' => 'product_big_img', 'isHigh' => true, 'schema' => true]) . $videoContainer . '</div>';
                            }
                        }
                        ?>
                    </div>
                    <div class="info_product_box">
                        <div class="<?php echo($product->isSale() ? 'sale' : '') ?> price" itemprop="offers" itemscope
                             itemtype="http://schema.org/Offer">
                            <?php if ($product->getOldPrice()) {
                                echo '<div class="old_price">' . $product->getOldPrice() . '</div><div class="clear"></div>';
                            } ?>
                            <?= $product->getPrice(); ?> <span>руб.</span>
                            <meta itemprop="price" content="<?= $product->getPrice(); ?>"/>
                            <meta itemprop="priceCurrency" content="RUB">
                        </div>

                        <!--                        <div class="balls">-->
                        <!--                            250 бонусных баллов-->
                        <!--                        </div>-->
                        <?php
                        if ($product->getQuantity() > 0) {
                            ?>
                            <div class="quantity">
                                В наличии: <?= ($product->getQuantity() > 10 ? 'более 10' : $product->getQuantity()) . ' шт.' ?>
                            </div>
                            <?php
                            $isProductToCart = false;
                            foreach ($cartProducts as $cartProduct) {
                                if ($product->getId() === $cartProduct->getCatalogProductId()) {
                                    $isProductToCart = true;
                                    break;
                                }
                            }
                            if ($isProductToCart) {
                                echo '<div class="btn product_to_cart">В корзине</div>';
                            } else {
                                echo '<div class="btn add_product_cart" id="add_product_cart" product_id="' . $product->getId()
                                    . '">Добавить в корзину</div>';
                            }
                            ?>

                            <div class="buy_1_click">
                                <div class="buy_1_click_form">
                                    <?php
                                    $form = ActiveForm::begin(
                                        [
                                            'options' => ['class' => 'form-horizontal']
                                        ]);
                                    echo '<input type="text" name="buy_1_click" value="" id="buy_1_click" />';
                                    echo '<input type="hidden" name="product_id" value="' . $product->getId() . '" id="product_id" />';
                                    echo Html::submitButton('Купить в 1 клик', ['id' => 'button_1_click']);
                                    ActiveForm::end();
                                    ?>
                                </div>
                                <div class="buy_1_click_text">
                                    Наш менеджер свяжется с вами через несколько минут и уточнит детали заказа
                                </div>
                            </div>
                            <?php
                        } else {
                            echo '<div class="not_in_stock">Нет в наличии</div>';
                        }
                        ?>
                        <div class="main_info row">
                            <div class="col-md-6">Артикул</div>
                            <div class="col-md-6"><?= $product->getArticle(); ?></div>

                            <div class="col-md-6">Код</div>
                            <div class="col-md-6"><?= $product->getCode(); ?></div>

                            <div class="col-md-6">Бренд</div>
                            <div class="col-md-6">Play Doh</div>

                            <?php if ($product->getBarcode() > 0) { ?>
                                <div class="col-md-6">Штрихкод</div>
                                <div class="col-md-6"><?= $product->getBarcode(); ?></div>
                            <?php } ?>

                            <div class="col-md-6">Производитель</div>
                            <div class="col-md-6">Hasbro</div>
                        </div>
                    </div>
                </div>
                <div class="info_product_sale">

                </div>
            </div>
            <!--            <div class="info_product_shipping">-->
            <!---->
            <!--            </div>-->
        </div>

        <div class="clear"></div>

        <div class="tabs_product">
            <?php
            $description = preg_replace("/\n/", "</p><p>", $product->getDescription());
            $description = preg_replace('/[\,\.\!\?]/', Html::a('$0', Url::to(['product/index', 'id' => $product->getId()], true)), $description, 1);

            $description .= '<p>Купить "'.$product->getTitle().'" вы можете по низким ценам в нашем интернет-магазине MrPlayDoh.ru с доставкой по Москве и во все регионы России!</p>';

            $attributes = '';
            $values = $product->getAttributeValues();
            foreach ($values as $value) {
                if ($value->getCatalogAttribute()->getId() === 1) {
                    $val = ($value->getValue() == 1) ? 'от ' . $value->getValue() . ' года' : 'от ' . $value->getValue() . ' лет';
                } else {
                    $val = $value->getValue();
                }
                $attributes .= '<div class="col-md-3">' . $value->getCatalogAttribute()->getTitle() . '</div><div class="col-md-9">' . $val . '</div>';
            }

            $reviews = '';
            $productReviews = $product->getReviews();
            if (!empty($productReviews)) {
                $reviewLabel = 'Отзывы (' . count($productReviews) . ')';
                foreach ($productReviews as $review) {
                    $reviews .= $this->render('/layouts/productReviewItem', ['review' => $review]);
                }
            } else {
                $reviewLabel = 'Отзывы (0)';
                $reviews = '<h3>Отзывов нет, вы можете стать первым!</h3>
                            <h4>Расскажите о своём опыте использования товара.</h4>
                            <h4>Обратите внимание на качество, удобство и соответствие заявленным характеристикам.</h4>';
            }
            echo \yii\bootstrap\Tabs::widget([
                'items' => [
                    [
                        'label' => 'Описание',
                        'content' => '<h2>' . $product->getTitle2() . '</h2><div class="description" itemprop="description"><p>' . $description . '</p>
                                        <a href="https://plus.google.com/u/1/108888585391425984422/posts" rel="author">Mr Play-Doh</a></div>',
                        'active' => true
                    ],
                    [
                        'label' => 'Характеристики',
                        'content' => '<br/><div class="attribute_info row">' . $attributes . '</div>',
                    ],
                    [
                        'label' => $reviewLabel,
                        'content' => '<br/><div class="reviews_info">' . $reviews . '
                                           <div class="add_review">
                                                <div class="btn add_review_bnt" data-toggle="modal" data-target="#modalReview">
                                                    Оставить отзыв
                                                </div>
                                           </div></div>',
                    ],
//                        [
//                            'label' => 'Ответы',
//                            'content' => 'Anim pariatur cliche...',
//                        ],
//                        [
//                            'label' => 'Обзоры',
//                            'content' => 'Anim pariatur cliche...',
//                        ],
                    [
                        'label' => 'Доставка',
                        'content' => '<br/><div class="text">
                                            <h4>Курьерская доставка</h4>
                                            <p>' . Html::a('Правилами курьерской доставки!', ['route/index', 'url' => 'shipping']) . '</p>
                                            <h5>Курьером по Москве и Московской области</h5>
                                            <p>
                                                Стоимость:<br/>
                                                    — Москва - 200 руб.<br/>
                                                    — Московская область - 200 руб. плюс 50 руб. за каждые 5 км удаленности от МКАД<br/>
                                            </p>
                                            <h5>Курьером по Санкт-Петербургу и Ленинградской области</h5>
                                            <p>
                                                Стоимость:<br/>
                                                — Санкт-Петербург - 200 руб.<br/>
                                                — Ленинградская область - 300 руб. плюс 50 руб. за каждые 5 км удаленности от КАДа<br/>
                                            </p>
                                            <h5>Курьером другие регионы</h5>
                                            <h5>Брянск, Вологда, Владимир, Екатеринбург, Иваново, Казань, Калуга, Кострома, Курск, Нижний Новгород, Орел,
                                            Ростов-на-Дону, Рязань, Тверь, Тула, Тюмень, Челябинск, Ярославль</h5>
                                            <p>
                                                Стоимость:<br/>
                                                — Стоимость - 300 руб.<br/>
                                            </p>
                                            <br/>
                                            <h4>Самовывоз</h4>
                                            <p>' . Html::a('Правилами работы пунктов выдачи заказов!', ['route/index', 'url' => 'shipping']) . '</p>
                                            <p>Стоимость:</p>
                                            <h5>Москва - 100 руб.</h5>
                                            <h5>Санкт-Петербургу - 100 руб.</h5>
                                            <h5>Брянск, Великий Новгород, Вологда, Екатеринбург, Иваново, Казань, Калуга, Киров, Краснодар
                                             Нижний Новгород, Новороссийск, Новосибирск, Норильск, Омск, Орел, Пермь, Псков,
                                            Ростов-на-Дону, Рязань, Смоленск, Тверь, Тула, Тюмень, Челябинск, Ярославль - 150 руб.</h5>
                                            <br/>
                                            <h4>Почта России</h4>
                                            <p>Стоимость доставки Почтой России зависит от региона. Подробнее с тарифами можно ознакомиться в '
                            . Html::a('на данной странице', ['route/index', 'url' => 'shipping']) . '.</p>
                            </div>',
                    ],
                    [
                        'label' => 'Оплата',
                        'content' => '<br/><div class="payment_description">
                                            <div class="payment_block">
                                            <div class="payment_title" id="pay_n">
                                            Наличный расчет
                                            </div>
                                            <div class="payment_description">
                                            — курьеру при доставке,<br/>
                                            — при самовывозе из пунктов выдачи,<br/>
                                            — при самовывозе с нашего склада.
                                            </div>
                                            </div>
                                            <div class="payment_block">
                                            <div class="payment_title" id="pay_b">
                                            Безналичный расчет
                                            </div>
                                            <div class="payment_description">
                                            — перевод на карту Сбербанка,<br/>
                                            — перевод на расчетный счет компании.<br/>
                                            — наложенным платежом (для Почты России).
                                            </div>
                                            </div>
                                            <div class="payment_block">
                                            <div class="payment_title" id="pay_e">
                                            Электронные деньги
                                            </div>
                                            <div class="payment_description">
                                            — через систему Яндекс.Деньги,<br/>
                                            — через систему Webmoney,<br/>
                                            — через систему Qiwi.
                                            </div>
                                            </div>
                                            <div class="payment_alarm">
                                            Обратите внимание, что варианты способа оплаты могут изменяться, в зависимости от выбранного вами способа доставки товара.<br/>
                                            Пожалуйста, уточняйте это при оформлении заказа!
                                            </div>',
                    ],
                ],
            ]);
            ?>
        </div>

        <div class="clear"></div>

        <div class="product_list_small">
            <h3>Похожие товары</h3>
            <?php
            foreach ($product2similar as $prod) {
                echo $this->render('/layouts/productItemSmall', ['product' => $prod]);
            }
            ?>
        </div>

        <div class="clear"></div>

        <?php /**
         * @TODO  Вынести в виджет
         * <div class="viewed_product">
         * <h3>Вы уже смотрели</h3>
         * <?php
         * foreach ($product2viewed as $prod) {
         * echo '<div class="viewed_item">';
         * $image = $prod->getMainImage();
         * if($image){
         * echo Html::img(Yii::$app->params['HTTP_IMAGE'] . $image->getHash(), ['width' => '50', 'height' => '50']);
         * }
         * echo '
         * <div class="viewed_title">' .
         * Html::a($prod->getTitle(), ['product/index', 'id' => $prod->getId()]) .
         * '<div class="product_price">
         * <div class="price"><?= $prod->getPrice(); ?> <span class="rub">руб.</span></div>
         * </div>
         * </div>
         * </div>';
         * }
         * ?>
         * </div>
         */ ?>
    </div>
</div>


<div class="modal fade" id="modalReview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" id="modalReviewBody" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 solid #ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabelReview">Оставьте отзыв о товаре</h3>
            </div>
            <div role="alert" id="review_error" class="alert alert-danger col-md-12"></div>
            <div class="clear"></div>
            <div class="modal-body" id="modal_review_body">
                <div class="form-group field-review_name">
                    <div class="col-md-2"><label for="review_name" class="control-label">Ваше имя</label></div>
                    <div class="col-md-10"><input type="text" value="" name="review_name" class="form-control" id="review_name"></div>
                    <div class="col-md-offset-2 col-md-10">
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="form-group field-review_email">
                    <div class="col-md-2"><label for="review_email" class="control-label">Эл почта</label></div>
                    <div class="col-md-10"><input type="text" value="" name="review_email" class="form-control" id="review_email"></div>
                    <div class="col-md-offset-2 col-md-10">
                        <div class="help-block">адрес не будет опубликован</div>
                    </div>
                </div>
                <div class="form-group field-review_rating">
                    <div class="col-md-2"><label for="review_rating" class="control-label">Оценка</label></div>
                    <div class="col-md-10">

                        <div class="rating">
                            <input type="radio" name="rating" id="rating5" class="radio_rating" value="5">
                            <label title="5 stars" for="rating5">5 stars</label>
                            <input type="radio" name="rating" id="rating4" class="radio_rating" value="4">
                            <label title="4 stars" for="rating4">4 stars</label>
                            <input type="radio" name="rating" id="rating3" class="radio_rating" value="3">
                            <label title="3 stars" for="rating3">3 stars</label>
                            <input type="radio" name="rating" id="rating2" class="radio_rating" value="2">
                            <label title="2 stars" for="rating2">2 stars</label>
                            <input type="radio" name="rating" id="rating1" class="radio_rating" value="1">
                            <label title="1 stars" for="rating1">1 stars</label>
                        </div>
                    </div>
                </div>
                <div class="form-group field-review_text">
                    <div class="col-md-2"><label for="review_text" class="control-label">Отзыв</label></div>
                    <div class="col-md-10"><textarea rows="10" cols="5" name="review_text" class="form-control" id="review_text"></textarea></div>
                    <div class="col-md-offset-2 col-md-10">
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2">&nbsp;</div>
                    <div class="col-md-10">
                        <div class="btn send_product_review" id="<?= $product->getId() ?>">Отправить</div>
                    </div>
                    <div class="col-md-offset-2 col-md-10">
                        <div class="help-block"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <br/>
        </div>
    </div>
</div>