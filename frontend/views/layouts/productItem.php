<?php
use yii\helpers\Html;
use common\models\catalog\Product;
use frontend\models\AccountSingleton;

/**
 * @var Product $product
 */
if (!($product instanceof Product)) {
    return;
}
$account = AccountSingleton::getInstance()->getAccount(false);
$cart = ($account) ? $account->getCart(false) : null;
$isProductToCart = false;
if($cart){
    $cartProducts = $cart->getCartProducts();
    foreach($cartProducts as $cartProduct){
        if($product->getId() === $cartProduct->getCatalogProductId()){
            $isProductToCart = true;
            break;
        }
    }
}
?>

<div class="product_item">
    <?php
        $image = $product->getMainImage();
        if($image){
            echo Html::a($image->getHtml(['width' => 200, 'height' => 200, 'title' => $product->getTitle(), 'alt' => $product->getTitle()]),
                ['product/index', 'id' => $product->getId()]);
        }

        if ($product->getQuantity() > 0) {
            echo '<div class="product_in_stock">В наличии</div>';
        }else{
            echo '<div class="product_not_in_stock">Нет в наличии</div>';
        }
    ?>
    <div class="product_title">
        <?= Html::a($product->getTitle(), ['product/index', 'id' => $product->getId()]) ?>
    </div>
    <div class="product_price">
        <?php
            $oldPrice = $product->getOldPrice() ? '<div class="old_price">'.$product->getOldPrice().' <span class="rub">руб.</span></div>': '';
            echo '<div class="container_price '. ($product->getOldPrice() ? 'has_old_price' : '').'">'.$oldPrice.'<div class="'. (!$product->getOldPrice() ? 'not_old' : '').' '. ($product->isSale() ? 'sale' : '').' price">' . $product->getPrice() . ' <span class="rub">руб.</span></div></div>';
            if ($product->getQuantity() > 0) {
                echo '<div class="container_bnt" id="container_bnt"><div class="arrow_box" id="arrow_box_' . $product->getId() . '"></div>';
                if($isProductToCart) {
                    echo '<div class="btn product_to_cart">В корзине</div>';
                }else {
                    echo '<div class="btn add_product_cart" id="add_product_cart_'.$product->getId().'" product_id="'.$product->getId().'">В корзину</div>';
                }
                echo '</div>';
            } else {
                echo '<div class="subscribe">Уведомить меня</div>';
            }
        ?>
    </div>
</div>