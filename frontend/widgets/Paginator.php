<?php

namespace frontend\widgets;

use yii\bootstrap\Widget;

class Paginator extends Widget
{

    /**
     * Текущая страница
     * @var int
     */
    public $page;

    /**
     * Кол страниц
     * @var int
     */
    public $countPage;

    /**
     * Url
     * @var string
     */
    public $url;

    /**
     * Renders the widget.
     */
    public function run()
    {
        if ($this->countPage < 2) {
            return '';
        }

        return $this->render('paginator', ['page' => $this->page, 'countPage' => $this->countPage, 'url' => $this->url]);
    }

}
