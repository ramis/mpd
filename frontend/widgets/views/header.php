<?php
use yii\helpers\Html;
use common\models\Account;
use common\models\Information;
use common\tools\Tools;
use frontend\models\AccountSingleton;
use common\models\catalog\Product;
use yii\widgets\ActiveForm;

/**
 * @var $account Account
 */
$informations = Information::find()->all();
$dataInformation = [];
foreach ($informations as $information) {
    $dataInformation[$information->getUrl()] = $information->getTitle();
}

$account = AccountSingleton::getInstance()->getAccount(false);

$ref = null;

?>

<header>
    <div id="header_top">
        <div id="header_top_body">
            <!--            <div id="header_geo">
                            <span>Ваш город:</span> <span id="user_geo">Ханты-Мансийск</span>
                        </div>
            -->
            <div id="menu_top" style="left: 20px !important;">
                <ul>
                    <li>
                        <?= Html::a($dataInformation['company'], ['route/index', 'url' => 'company'], $ref); ?>
                    </li>
                    <li>
                        <?= Html::a($dataInformation['shipping'], ['route/index', 'url' => 'shipping'], $ref); ?>
                    </li>
                    <li>
                        <?= Html::a($dataInformation['payment'], ['route/index', 'url' => 'payment'], $ref); ?>
                    </li>
                    <!--                    <li>-->
                    <!--                        <a href="">Отзывы</a>-->
                    <!--                    </li>-->
                    <li>
                        <?= Html::a($dataInformation['contact'], ['route/index', 'url' => 'contact'], $ref); ?>
                    </li>
                </ul>
            </div>
            <!--            <div id="favorite">
                            <a href="">Избранное</a>
                        </div>
                        <div id="account">
                            <a href="">Вход в личный кабинет</a>
                        </div>
            -->
        </div>
    </div>
    <div id="header">
        <div id="header_body">
            <?= Html::a('<div id="logo"><span>Интернет-магазин пластилина</span></div>', \Yii::$app->homeUrl); ?>
            <div id="block_phone_search">
                <div id="phone_mks_spb">
<!--                    <div class="phone_num"><span class="phone_code">8 495</span> 278-08-20</div>-->
                    <div class="phone_num">8 495 278-08-20</div>
                    <div class="phone_help">для Москвы</div>
                </div>
                <div id="phone_all">
                    <div class="phone_num">8 812 670-07-90</div>
                    <div class="phone_help">для Санкт-Петербурга</div>
                </div>
                <div class="clear"></div>
                <div id="search">
                    <?php
                    $search = Html::encode(Yii::$app->request->get('_search', ''));
                    $countProduct = Product::find()->where(['is_published' => true])->count();
                    $stCountProduct = Tools::humanPluralForm($countProduct, ['товару', 'товарам', 'товарам']);
                    $form = ActiveForm::begin(['action' => '/search/', 'method' => 'get', 'id' => 'search']);
                    echo '<input type="text" id="text_search" name="_search" autocomplete="off" placeholder="поиск по ' . $stCountProduct . '" value="' . $search . '"/>';
                    echo '<input type="submit" id="submit_search" value="" name=""/>';
                    ActiveForm::end();
                    ?>

                    <div id="search_wrapper_container">
                    </div>
                </div>
            </div>
            <div id="call_time">
                <div class="header_title">
                    КРУГЛОСУТОЧНО
                </div>
                <div class="call_time_help">и без выходных</div>
<!--                <div id="call_button">Заказать звонок</div>-->
            </div>
            <div id="cart_small">
                <div class="header_title">
                    <?= Html::a('КОРЗИНА', ['cart/index'], ['onclick' => 'ga("send", "event", "CART", "OPEN");']); ?>
                </div>
                <?php
                $quantity = 0;
                if ($account && $account->getCountCartProduct() > 0) {
                    $quantity = $account->getCountCartProduct();
                    $cart = $account->getCart(false);
                    echo '<div class="cart_product" id="cart_product">' .
                        Tools::humanPluralForm($quantity, ['товар', 'товара', 'товаров']) . ' на ' . $cart->getTotalPrice() .
                        ' <span class="rub">pуб.</span></div>';
                } else {
                    echo '<div class="cart_product" id="cart_product">нет товаров</div>';
                }
                echo '<div class="btn ' . ($quantity > 0 ? "" : "hidden") . '" id="cart_button" onclick="location.href=\'/cart/\';">' .
                    Html::a('Оформить заказ', ['cart/index'], ['onclick' => 'ga("send", "event", "CART", "OPEN");']) . '</div>';
                ?>
            </div>
        </div>
    </div>
    <div id="header_bottom">
        <div id="header_bottom_body">
            <div id="dignity1" class="dignity" onclick="location.href='/shipping/';">
                <span>Доставка в срок и по всей России</span>
            </div>
            <div id="dignity2" class="dignity">
                <span>Минимальные цены</span>
            </div>
            <div id="dignity3" class="dignity">
                <span>Гарантия подлинности</span>
            </div>
            <div id="dignity4" class="dignity">
                <span>Оперативная обработка</span>
            </div>
        </div>
    </div>
</header>