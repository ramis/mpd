<?php
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\catalog\Product;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
$this->title = 'Цены';
?>

<div class="site-index">
    <h1>Список товаров</h1>

    <?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Id',
                'format' => 'raw',
                'value' => function (Product $product) {
                    $href = Yii::$app->params['HTTP_SITE'] . 'product/' . $product->getId() . '/';
                    return '<a name="' . $product->getId() . '" href="' . $href . '" target="_blank">' . $product->getId() . '</a>';
                }
            ],
            [
                'attribute' => 'Foto',
                'format' => 'raw',
                'value' => function (Product $product) {
                    $image = $product->getMainImage();
                    $img = '';
                    if ($image) {
                        $img = $image->getHtml(['width' => 100, 'height' => 100]);
                    }

                    return $img;
                }
            ],
            [
                'attribute' => 'Title',
                'value' => function (Product $product) {
                    return ($product->getTitle() ? $product->getTitle() : $product->getTitle2());
                }
            ],
            [
                'attribute' => 'Quantity',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" value="'.$product->getQuantity().'" disabled="disabled" />';
                }
            ],
            [
                'attribute' => 'Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" name="price" id="price_'.$product->getId().'" value="'.$product->getPrice().'" disabled="disabled" />';
                }
            ],
            [
                'attribute' => 'Sale Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" name="sale_price" class="input_price"  product="'.$product->getId().'" id="sale_price_'.$product->getId().'" value="'.$product->getSalePrice().'" />';
                }
            ],
            [
                'attribute' => 'Old Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" name="old_price" class="input_price" product="'.$product->getId().'" id="old_price_'.$product->getId().'" value="'.$product->getOldPrice().'" />';
                }
            ],
            [
                'attribute' => 'Persent Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" name="persent_price" class="input_price"  product="'.$product->getId().'" id="persent_price_'.$product->getId().'" value="'.$product->getPersentPrice().'" />';
                }
            ],
            [
                'attribute' => 'Manual Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" name="manual_price" class="input_price" product="'.$product->getId().'" id="manual_price_'.$product->getId().'" value="'.$product->getManualPrice().'" />';
                }
            ],
            [
                'attribute' => 'Buy Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" value="'.$product->getBuyPrice().'" disabled="disabled" />';
                }
            ],
            [
                'attribute' => 'Base Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" value="'.$product->getBasePrice().'" disabled="disabled" />';
                }
            ],
            [
                'attribute' => 'V3t Price',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return '<input type="text" value="'.$product->getV3tPrice().'" disabled="disabled" />';
                }
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return Html::a('Save', ['save'], ['id' => $product->getId(), 'class' => 'price_save']);
                }
            ],
        ],
    ]);
    ?>
</div>
<style>
    td{
        text-align: center;
    }
    input{
        width: 60px;
    }
</style>