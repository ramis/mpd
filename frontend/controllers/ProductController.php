<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use common\models\catalog\Product;

/**
 * Site controller
 */
class ProductController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @param $id
     * @return string
     */
    public function actionIndex($id)
    {

        $product = Product::findOne((int)$id);

        if (!($product instanceof Product && $product->isPublished())) {
            return Yii::$app->getResponse()->redirect(Url::to(['error']));
        }

        return $this->render('index', ['product' => $product]);
    }

}
