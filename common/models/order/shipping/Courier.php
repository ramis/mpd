<?php

namespace common\models\order\shipping;

use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Address;
use common\models\behavior\Id;
use common\models\behavior\OrderShipping;
use common\models\behavior\ShippingGeoValue;

/**
 * This is the model class for table "{{%order_shipping_courier}}".
 *
 * @property integer $id
 * @property integer $order_shipping_id
 * @property integer $shipping_geo_value_id
 * @property string $address
 */
class Courier extends ActiveRecord
{
    use Id;
    use ShippingGeoValue;
    use OrderShipping;
    use Address;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_shipping_courier}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_shipping_id', 'shipping_geo_value_id', 'address'], 'required'],
            [['order_shipping_id', 'shipping_geo_value_id'], 'integer'],
            [['address'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_shipping_id' => Yii::t('app', 'Order Shipping ID'),
            'shipping_geo_value_id' => Yii::t('app', 'Shipping Geo Value ID'),
            'address' => Yii::t('app', 'Address'),
        ];
    }

    /**
     * @return string
     */
    public function getText()
    {
        $geoTitle = $this->getShippingGeoValue()->getGeo()->getTitle();
        $typeTitle = $this->getShippingGeoValue()->getShipping()->getTitle();

        return $geoTitle . ' ' . $typeTitle . ' (' . $this->getAddress() . ')';
    }

}
