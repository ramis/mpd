<?php

namespace common\models\v3toys;

use common\models\behavior\Id;
use common\models\behavior\Price;
use common\models\behavior\Quantity;
use common\models\behavior\Title;
use Yii;
use common\models\catalog\Product as CatalogProduct;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%v3toys_product}}".
 *
 * @property integer $id
 * @property integer $v3toys_product_id
 * @property string $title_unit
 * @property string $sku
 * @property string $barcode
 * @property string $brand
 * @property string $price
 * @property integer $buy_price
 * @property integer $base_price
 * @property integer $mr_price
 * @property integer $quantity
 * @property boolean $is_assortiment
 * @property boolean $is_deleted
 * @property integer $catalog_product_id
 * @property string $title
 * @property string $category
 * @property string $img
 *
 * @property CatalogProduct $catalogProduct
 */
class Product extends ActiveRecord
{
    use Id;
    use Title;
    use Price;
    use Quantity;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%v3toys_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['v3toys_product_id', 'title_unit', 'price', 'buy_price', 'base_price', 'mr_price', 'quantity', 'is_assortiment', 'is_deleted'], 'required'],
            [['v3toys_product_id', 'price', 'buy_price', 'base_price', 'mr_price', 'quantity', 'catalog_product_id'], 'integer'],
            [['is_assortiment', 'is_deleted'], 'boolean'],
            [['title_unit', 'sku', 'barcode', 'brand', 'title', 'category', 'img'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'v3toys_product_id' => Yii::t('app', 'V3toys Product ID'),
            'title_unit' => Yii::t('app', 'Title Unit'),
            'sku' => Yii::t('app', 'Sku'),
            'barcode' => Yii::t('app', 'Barcode'),
            'brand' => Yii::t('app', 'Brand'),
            'price' => Yii::t('app', 'Price'),
            'buy_price' => Yii::t('app', 'Buy Price'),
            'base_price' => Yii::t('app', 'Base Price'),
            'mr_price' => Yii::t('app', 'Mr Price'),
            'quantity' => Yii::t('app', 'Quantity'),
            'is_assortiment' => Yii::t('app', 'Is Assortiment'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'catalog_product_id' => Yii::t('app', 'Catalog Product ID'),
            'title' => Yii::t('app', 'Title'),
            'category' => Yii::t('app', 'Category'),
            'img' => Yii::t('app', 'Img'),
        ];
    }

    /**
     * @param int $v3toysProductId
     * @return Product
     */
    public function setV3toysProductId($v3toysProductId) {
        $this->v3toys_product_id = $v3toysProductId;
        return $this;
    }

    /**
     * @return int
     */
    public function getV3toysProductId() {
        return $this->v3toys_product_id;
    }

    /**
     * @param string $titleUnit
     * @return Product
     */
    public function setTitleUnit($titleUnit) {
        $this->title_unit = $titleUnit;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitleUnit() {
        return $this->title_unit;
    }

    /**
     * @return boolean
     */
    public function isAssortiment()
    {
        return $this->is_assortiment;
    }

    /**
     * @param boolean $isAssortiment
     * @return $this
     */
    public function setAssortiment($isAssortiment)
    {
        $this->is_assortiment = $isAssortiment;
        return $this;
    }

    public function getCatalogProduct()
    {
        return $this->catalog_product_id === null ? null : CatalogProduct::findOne($this->catalog_product_id);
    }
    /**
     * @param int $catalogProductId
     * @return $this
     */
    public function setCatalogProductId($catalogProductId)
    {
        $this->catalog_product_id = $catalogProductId;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $isDeleted
     * @return $this
     */
    public function setDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * @return int
     */
    public function getBuyPrice()
    {
        return $this->buy_price;
    }

    /**
     * @param int $buyPrice
     * @return $this
     */
    public function setBuyPrice($buyPrice)
    {
        $this->buy_price = $buyPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return int
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * @param int $basePrice
     * @return $this
     */
    public function setBasePrice($basePrice)
    {
        $this->base_price = $basePrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getMrPrice()
    {
        return $this->mr_price;
    }

    /**
     * @param int $mrPrice
     * @return $this
     */
    public function setMrPrice($mrPrice)
    {
        $this->mr_price = $mrPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param string $img
     * @return $this
     */
    public function setImg($img)
    {
        $this->img = $img;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @return $this
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return $this
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

}
