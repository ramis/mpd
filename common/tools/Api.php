<?php
namespace common\tools;

class Api
{
    CONST V3TOYS_API_URL = 'http://www.v3toys.ru/pear-www/Kiwi_Shop/api.php';
    CONST V3TOYS_API_VERSION = '0.4';

    /**
     * @param array $request
     * @return array|mixed
     * @throws \Exception
     */
    private static function curl(array $request)
    {

        try {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, self::V3TOYS_API_URL);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 12);
            curl_setopt($ch, CURLOPT_USERAGENT, 'JSON-RPC PHP Client');
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));

            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($httpcode === 200) {
                $response = json_decode($result, true);
            } else {
                throw new \Exception($result);
            }
            curl_close($ch);
        } catch (\Exception $e) {
            throw new \Exception ($e);
        }

        return $response;
    }

    /**
     * @param $method
     * @param array $params
     * @return array|mixed
     * @throws \Exception
     */
    public static function send($method, array $params)
    {

        if(empty($method)) {
            throw new \Exception ('Method can`t null');
        }
        if(empty($params)) {
            throw new \Exception ('Params can`t null');
        }

        $request = [
            'v' => self::V3TOYS_API_VERSION,
            'method' => $method,
            'params' => $params
        ];

        return self::curl($request);

    }
}
