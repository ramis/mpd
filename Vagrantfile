VAGRANT_API_VERSION = "2"
VAGRANT_DEFAULT_PROVIDER = "virtualbox"

cpus   = `sysctl -n hw.ncpu`.to_i
mem_mb = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4

@ui = Vagrant::UI::Colored.new
Vagrant.require_version ">= 1.7.2"

plugins = [
    'vagrant-hostsupdater',
    'vagrant-env',
    'vagrant-vbguest',
    'vagrant-cachier',
]

plugins.each do |plugin|
  if !Vagrant.has_plugin?(plugin)
      system("vagrant plugin install #{plugin}")
  end
end

Vagrant.configure(VAGRANT_API_VERSION) do |config|

  config.package.name = 'rl-ubuntu.box'
  config.vm.box_url   = 'https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box'
  config.vm.box       = 'ubuntu/trusty64'

  config.vm.define 'v3-ubuntu'
  config.ssh.forward_agent = true
  config.env.enable

  config.cache.scope = :box
  config.cache.auto_detect = false
  config.cache.enable :apt
  config.cache.enable :apt_lists

  #network
  config.vm.network :private_network, ip: "10.0.0.10"
  config.vm.network "forwarded_port", guest: 80,    host: 8080    # ginx
  config.vm.network "forwarded_port", guest: 5432,  host: 5432    # postgres

  config.vm.hostname = "mpd.box"
  config.hostsupdater.aliases = ['a.mpd.box']

  #shared
  config.vm.synced_folder "./", "/home/vagrant/mpd", type: 'nfs'

  #virtualbox
  config.vbguest.auto_update = true

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory",             mem_mb]
    vb.customize ["modifyvm", :id, "--cpuexecutioncap",      "95"]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1",  "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1",         "on"]
    vb.customize ["modifyvm", :id, "--ioapic", "on"]
    vb.customize ["modifyvm", :id, "--cpus", cpus]
    vb.gui = false
  end

  # Pass vagrant_root variable to the VM and cd into the directory upon login.
  config.vm.provision :shell, path: "./provision/setup.sh"

  # Pass vagrant_root variable to the VM and cd into the directory upon login.
  config.vm.provision "shell", run: "always" do |s|
    s.inline = <<-SCRIPT
      sudo service nginx restart
      sudo service php5-fpm restart
      sudo service postgresql restart
    SCRIPT
  end

  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  config.hostsupdater.remove_on_suspend = true

end
