<?php

use yii\db\Schema;
use yii\db\Migration;

class m151224_033835_alter_geo_active extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%geo}}', 'is_active', Schema::TYPE_BOOLEAN);
    }

    public function safeDown()
    {
    }
}
