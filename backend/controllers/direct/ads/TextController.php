<?php
namespace backend\controllers\direct\ads;

use backend\models\direct\ads\Text;
use Yii;
use yii\data\ActiveDataProvider;
use backend\controllers\AbstractController;
use yii\helpers\Url;

/**
 * Direct Text controller
 */
class TextController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Text::find(),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);
    }

    /**
     * Text list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Text create
     *
     * @return string
     */
    public function actionCreate()
    {
        $text = new Text();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $text->load($_POST);

            if ($text->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['direct/ads/text/index']));
            }
            $errors = $text->getErrors();
        }
        return $this->render('form', ['text' => $text, 'errors' => $errors]);
    }

    /**
     * Text create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Text $text */
        $text = Text::findOne((int)$id);

        if ($text === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['direct/ads/text/index']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $text->load($_POST);

            if ($text->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['direct/ads/text/index']));
            }
            $errors = $text->getErrors();
        }

        return $this->render('form', ['text' => $text, 'errors' => $errors]);
    }


}
