<?php
namespace common\models\catalog;


use Yii;
use common\models\behavior\Media;
use common\models\media\ints\MediaInts;
use common\models\behavior\Id;
use common\models\AbstractModel;
use common\models\behavior\Title;
use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\Description;
use common\models\behavior\Published;
use common\models\behavior\Sort;
use common\models\behavior\Title2;
use common\models\behavior\Url;
use common\models\behavior\seo\Title as SeoTitle;
use common\models\behavior\seo\Description as SeoDescription;
use common\models\behavior\seo\Keywords as SeoKeywords;
use common\models\behavior\seo\Text as SeoText;
use common\models\catalog\product\Category as ProductCategory;
use common\models\ints\BreadCrumbsInts;

/**
 * This is the model class for table "{{%catalog_category}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $title2
 * @property string $url
 * @property string $description
 * @property integer $sort
 * @property boolean $is_published
 * @property integer $created_uts
 * @property integer $updated_uts
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_text
 * @property boolean $is_special
 *
 * @property Media $media
 * @property ProductCategory[] $catalogProductCategories
 * @property Product[] $catalogProducts
 */
class Category extends AbstractModel implements MediaInts, BreadCrumbsInts
{
    use Id;
    use Title;
    use Title2;
    use Description;
    use Url;
    use Sort;
    use CreatedUpdatedUts;
    use Published;
    use SeoTitle;
    use SeoDescription;
    use SeoKeywords;
    use SeoText;
    use Media;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url', 'sort', 'is_published', 'is_special'], 'required'],
            [['id', 'sort', 'created_uts', 'updated_uts'], 'integer'],
            [['description', 'seo_text'], 'string'],
            [['is_published', 'is_special'], 'boolean'],
            [['title', 'title2', 'seo_title', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['url'], 'match', 'pattern' => '/[A-Za-z0-9_]+/'],
            [['url'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'title2' => 'Сео Название',
            'url' => 'Урл',
            'description' => 'Описание',
            'sort' => 'Сортировка',
            'is_published' => 'Опубликовано',
            'created_uts' => 'Время создания',
            'updated_uts' => 'Время добавления',
            'seo_title' => 'Сео Тайтл',
            'seo_description' => 'Мета Description',
            'seo_keywords' => 'Мета Keywords',
            'seo_text' => 'Сео текст',
            'is_special' => 'Спец категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['catalog_category_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getProductsCount()
    {
        return $this->hasMany(Product::className(),
            ['id' => 'catalog_product_id'])->viaTable('{{%catalog_product_category}}',
            ['catalog_category_id' => 'id'])->where(['is_published' => true])->count();
    }

    /**
     * @param array $param
     * @return Product[]
     */
    public function getProducts($param = [])
    {
        $sort = !empty($param['sort']) ? $param['sort'] : 'price';
        $query = $this->hasMany(Product::className(),
            ['id' => 'catalog_product_id'])->viaTable('{{%catalog_product_category}}',
            ['catalog_category_id' => 'id'])->where(['is_published' => true])->orderBy($sort);

        if(!empty($param['limit'])){
            $query->limit($param['limit']);
        }

        if(!empty($param['offset'])){
           $query->offset($param['offset']);
        }

        return $query->all();
    }

    /**
     * @param array $limit
     * @return Product[]
     */
    public function getProductsInStock($limit)
    {
        return $this->hasMany(Product::className(),
            ['id' => 'catalog_product_id'])->viaTable('{{%catalog_product_category}}',
            ['catalog_category_id' => 'id'])->where(['is_published' => true])->andWhere(['>', 'quantity', 0])->limit($limit)->all();
    }

    /**
     * @return boolean
     */
    public function isSpecial()
    {
        return $this->is_special;
    }

    /**
     * @param boolean $is_special
     */
    public function setSpecial($is_special)
    {
        $this->is_special = $is_special;
    }

    /**
     * @return array
     */
    public function getBreadCrumbs()
    {
        return [$this->getTitle()];
    }

    public function getPageTitle()
    {
        if ($this->getSeoTitle()) {
            $title = $this->getSeoTitle();
        } else {
            $title = $this->getTitle() . ' от Хасбро/' .  $this->getTitle2() . ' Hasbro купить в магазине ' . Yii::$app->params['DOMAIN'];
        }

        return $title;
    }
}
