<?php

namespace backend\models\fin;

use Yii;

/**
 * This is the model class for table "{{%fin_income}}".
 *
 * @property integer $id
 * @property integer $pd_fin_income_type_id
 * @property integer $created_uts
 * @property integer $summ
 * @property string $text
 */
class Income extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%fin_income}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pd_fin_income_type_id', 'created_uts', 'summ', 'text'], 'required'],
            [['id', 'pd_fin_income_type_id', 'created_uts', 'summ'], 'integer'],
            [['text'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pd_fin_income_type_id' => Yii::t('app', 'Pd Fin Income Type ID'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'summ' => Yii::t('app', 'Summ'),
            'text' => Yii::t('app', 'Text'),
        ];
    }
}
