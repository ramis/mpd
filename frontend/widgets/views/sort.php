<?php
use yii\helpers\Html;
use common\tools\Tools;

list($url, $query) = Tools::parseUrl($url);

$delimiterSort = $delimiterLimit = '?';
$urlSort = $urlLimit = $url;
unset($query['page']);
foreach ($query as $key => $value) {
    if($key !== 'sort' && $key !== 'order'){
        $urlSort .= $delimiterSort . $key . '=' . $value;
        $delimiterSort = '&';
    }
    if($key !== 'limit'){
        $urlLimit .= $delimiterLimit . $key . '=' . $value;
        $delimiterLimit = '&';
    }
}

$allSort = [
    'title' => 'названию',
//    'view' => 'популярности',
    'price' => 'цене'
];
$allLimitProductPage = [12 => 12, 18 => 18, 24 => 24, 1000 => 'все'];
?>
<div id="block_sort_count" style="padding: 10px 0 20px;">
    <div id="block_count">
        <ul>
            <li>
                На странице:
            </li>
            <?php
                $echo = '';
                foreach ($allLimitProductPage as $key => $val) {
                    $echo .= '<li class="filter '.($key === $limit ? ' active ' : '').'">';
                    $echo .= ($key === $limit ? $val : Html::a($val, $urlLimit . $delimiterLimit . 'limit=' . $key));
                    $echo .='</li>';
                }
                echo $echo;
            ?>
        </ul>
    </div>
    <div id="block_sort">
        <ul>
            <li>
                Сортировка по:
            </li>
            <?php
                $echo = '';
                foreach ($allSort as $key => $val) {
                    $echo .= '<li class="filter '.($key === $sort ? ' active ' : '').'">';
                    $echo .= ($key === $sort ? $val : Html::a($val, $urlSort . $delimiterSort . 'sort=' . $key));
                    $echo .='</li>';
                }
                echo $echo;
            ?>
        </ul>

    </div>
</div>