$(function () {

    $(".input_price").keypress(function(event) {
        if (event.keyCode == 13) {
            productId = $(this).attr('product');
            savePrice(productId);
        }
    });
    $(".price_save").click(function (e) {
        productId = $(this).attr('id');
        savePrice(productId);

        e.stopPropagation();
        return false;
    });
});

savePrice = function(productId){
    salePrice = $('#sale_price_'+productId).val();
    oldPrice = $('#old_price_'+productId).val();
    persentPrice = $('#persent_price_'+productId).val();
    manualPrice = $('#manual_price_'+productId).val();


    params = 'r=/catalog/product/save&id=' + productId + '&sale_price=' +
            salePrice+ '&old_price=' + oldPrice+ '&persent_price=' + persentPrice+ '&manual_price=' + manualPrice;
    $.ajax({
        url: '/index.php?' + params,
        type: 'get',
        data: '',
        dataType: 'json',
        success: function (data) {
            if (data['price']) {
                $('#sale_price_'+productId).val(data['sale_price']);
                $('#price_'+productId).val(data['price']);
            }
        }
    });
};
