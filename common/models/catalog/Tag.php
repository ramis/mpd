<?php

namespace common\models\catalog;

use common\models\ints\BreadCrumbsInts;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\models\catalog\tag\Group;
use common\models\catalog\Product;
use common\models\catalog\product\Tag as ProductTag;
use common\models\behavior\seo\Description as SeoDescription;
use common\models\behavior\seo\Title as SeoTitle;
use common\models\behavior\seo\Keywords as SeoKeywords;
use common\models\behavior\seo\Text as SeoText;
use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\behavior\Url;
use yii\helpers\Html;
use yii\db\Query;

/**
 * This is the model class for table "{{%catalog_tag}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $tag
 * @property string $url
 * @property integer $group_id
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_text
 *
 * @property Product[] $catalogProducts
 * @property Group $group
 */
class Tag extends ActiveRecord implements BreadCrumbsInts
{
    use Id;
    use Title;
    use Url;
    use SeoDescription;
    use SeoTitle;
    use SeoText;
    use SeoKeywords;

    protected static $_tagsToCountProduct;

    private $_products;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'tag', 'url', 'group_id'], 'required'],
            [['group_id'], 'integer'],
            [['seo_text'], 'string'],
            [['title', 'tag', 'url', 'seo_title', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['url'], 'match', 'pattern' => '/[A-Za-z0-9_]+/'],
            [['url'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'tag' => Yii::t('app', 'Tag'),
            'url' => Yii::t('app', 'Url'),
            'group_id' => Yii::t('app', 'Group ID'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'seo_text' => Yii::t('app', 'Seo Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProductTags()
    {
        return $this->hasMany(ProductTag::className(), ['catalog_tag_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getProductsCount()
    {
        return $this->hasMany(Product::className(),
            ['id' => 'catalog_product_id'])->viaTable('{{%catalog_product_tag}}',
            ['catalog_tag_id' => 'id'])->where(['is_published' => true])->count();
    }

    /**
     * @param array $param
     * @return Product[]
     */
    public function getProducts($param = [])
    {
        $sort = !empty($param['sort']) ? $param['sort'] : 'price';
        $query = $this->hasMany(Product::className(),
            ['id' => 'catalog_product_id'])->viaTable('{{%catalog_product_tag}}',
            ['catalog_tag_id' => 'id'])->where(['is_published' => true])->orderBy($sort . ' asc');

        if(!empty($param['limit'])){
            $query->limit($param['limit']);
        }

        if(!empty($param['offset'])){
            $query->offset($param['offset']);
        }

        return $query->all();
    }

    /**
     * @return Product[]
     */
    public function getCatalogProducts()
    {
        if ($this->_products === null) {
            $this->_products = $this->hasMany(Product::className(), ['id' => 'catalog_product_id'])->viaTable('{{%catalog_product_tag}}', ['catalog_tag_id' => 'id'])->all();
        }
        return $this->_products;
    }

    /**
     * @param array $_products
     */
    public function setCatalogProducts(array $_products)
    {
        $this->_products = $_products;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return Group::findOne($this->group_id);
    }
    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * @params $group_id
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }


    /**
     * @param bool|true $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function modelSave($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {

            if (parent::save() === false) {
                throw new Exception(json_encode($this->getFirstErrors()));
            }

            ProductTag::deleteAll('catalog_tag_id = :catalog_tag_id', [':catalog_tag_id' => $this->getId()]);
            $values = [];
            foreach ($this->getCatalogProducts() as $product) {
                $values[] = [$product->getId(), $this->getId()];
            }

            if(!empty($values)){
                self::getDb()->createCommand()->batchInsert(ProductTag::tableName(), ['catalog_product_id', 'catalog_tag_id'], $values)->execute();
            }

            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
        }

        return false;
    }

    /**
     * @return array
     */
    public function getBreadCrumbs()
    {
        $breadCrumbs = [];
        $breadCrumbs[] = Html::a('Поиск по параметрам', ['tags/index']);
        $breadCrumbs[] = $this->getSeoTitle();

        return $breadCrumbs;
    }

    public static function getTagsToCntProduct()
    {
        if(self::$_tagsToCountProduct === null){
            $rows = (new Query())
                ->select(['catalog_tag_id', 'count(catalog_tag_id) as cnt'])
                ->from(ProductTag::tableName())
                ->groupBy('catalog_tag_id')
                ->all();

            self::$_tagsToCountProduct = [];
            foreach($rows as $row){
                self::$_tagsToCountProduct[$row['catalog_tag_id']] = $row['cnt'];
            }
        }
    }

    public function getCntProduct()
    {
        $id = $this->getId();
        self::getCatalogProductTags();
        return !empty(self::$_tagsToCountProduct[$id]) ? self::$_tagsToCountProduct[$id] : 0;
    }

}
