<?php

namespace common\models\behavior;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

trait CreatedUpdatedTs
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_ts', 'updated_ts'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_ts'],
                ],
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return int
     */
    public function getCreatedTs()
    {
        return $this->created_ts;
    }

    /**
     * @return int
     */
    public function getUpdatedTs()
    {
        return $this->updated_ts;
    }

}