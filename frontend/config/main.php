<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'charset' => 'utf-8',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => ['_GET', '_POST', '_FILES'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' => [
                '' => 'site/index',
                'patterns' => [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'json',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST add' => 'add',
                        'POST del' => 'del',
                        'POST geo' => 'geo',
                        'POST api' => 'api',
                        'POST buy' => 'buy',
                        'POST search' => 'search',
                        'POST feedback' => 'feedback',
                        'POST review' => 'review',
                    ],
                ],
                'cart' => 'cart/index',
                'map' => 'site/map',
                'search' => 'search/index',
                'tags' => 'tags/index',
                'tags/<url:.+>' => 'tags/tag',
                'product/<id:\d+>' => 'product/index',
                '<url:.+>' => 'route/index',
            ]
        ],
        'session' => [
            'name' => 'PHPFRONTSESSID',
            'savePath' => __DIR__ . '/../session',
            'useCookies' => true,
            'class' => 'yii\web\Session',
            'cookieParams' => [
                'lifetime' => 30 * 24 * 60 * 60,
            ],
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'pgsql:host=127.0.0.1;port=5432;',
            'username' => 'postgres',
            'password' => 'CS1Oa4Dt5bYf',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'app' => 'app.php'
                    ]
                ],
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
        ],
//        'cache' => [
//            'class' => 'yii\caching\MemCache',
//            'useMemcached' => true,
//            'servers' => [
//                [
//                    'host' => 'server',
//                    'port' => 11211,
//                    'weight' => 100,
//                ],
//            ],
//        ],
    ],
    'params' => $params,
];
