<?php

use yii\db\Migration;

class m160227_075427_alter_crontab_stat extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crontab_stat}}', 'controller', \yii\db\Schema::TYPE_STRING);
    }

    public function safeDown()
    {
    }
}
