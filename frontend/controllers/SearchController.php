<?php
namespace frontend\controllers;

use common\models\catalog\Product;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Site controller
 */
class SearchController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $ids = [];
        $search = '';

        if ($search = trim(Yii::$app->request->get('_search', ''))) {
            list($words, $ids, $product) = Product::searchSphinx($search);

            if ($product) {
                return Yii::$app->getResponse()->redirect(Url::to(['product/index', 'id' => $product->getId()]));
            }

            $search = implode(' ', $words);
        }

        $page = Yii::$app->request->get('page', 1);
        $limit = (int)Yii::$app->request->get('limit', 12);
        $sort = Yii::$app->request->get('sort', null);
        $order = Yii::$app->request->get('order', 'asc');

        return $this->render('index', ['ids' => $ids, 'search' => $search, 'page' => $page, 'sort' => $sort, 'order' => $order, 'limit' => $limit]);
    }

}
