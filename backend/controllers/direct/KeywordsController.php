<?php
namespace backend\controllers\direct;

use backend\models\direct\Keywords;
use Yii;
use backend\controllers\AbstractController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class KeywordsController extends AbstractController
{


    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Keywords::find(),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);
    }

    /**
     * Keywords list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Keywords create
     *
     * @return string
     */
    public function actionCreate()
    {
        $keywords = new Keywords();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $keywords->load($_POST);

            if ($keywords->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['direct/keywords/index']));
            }
            $errors = $keywords->getErrors();
        }
        return $this->render('form', ['keywords' => $keywords, 'errors' => $errors]);
    }

    /**
     * Keywords create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Keywords $keywords */
        $keywords = Keywords::findOne((int)$id);

        if ($keywords === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['direct/keywords/index']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $keywords->load($_POST);

            if ($keywords->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['direct/keywords/index']));
            }
            $errors = $keywords->getErrors();
        }

        return $this->render('form', ['keywords' => $keywords, 'errors' => $errors]);
    }

}
