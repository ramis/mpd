<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Exception;
use common\models\order\Product;
use common\models\behavior\Email;
use common\models\behavior\Fio;
use common\models\behavior\Id;
use common\models\behavior\Phone;
use common\models\behavior\Account;
use common\models\behavior\CreatedUpdatedUts;
use common\models\order\Shipping;
use common\models\order\Product as OrderProduct;
use common\models\order\shipping\Courier;
use common\models\order\shipping\Pickup;
use common\models\order\shipping\Post;
use common\models\order\State;
use common\models\shipping\GeoValue;
use common\models\Account as ModelAccount;
use common\models\catalog\Product as catalogProduct;
use common\models\queue\Email as ModelEmail;
use common\models\order\Comment;
use common\tools\Tools;
use frontend\models\AccountSingleton;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $fio
 * @property integer $phone
 * @property string $email
 * @property integer $total
 * @property integer $created_uts
 * @property integer $updated_uts
 * @property integer $payment_id
 * @property integer $state_id
 * @property string $referers
 *
 * @property Account $account
 * @property Product[] $orderProducts
 */
class Order extends ActiveRecord
{
    use Id;
    use Fio;
    use Phone;
    use Email;
    use Account;
    use CreatedUpdatedUts;

    /**
     * @var State
     */
    private $state;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'fio', 'email', 'total'], 'required'],
            [['id', 'account_id', 'total', 'created_uts', 'updated_uts'], 'integer'],
            [['referers'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'fio' => Yii::t('app', 'Fio'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'total' => 'Total',
            'created_uts' => 'Created Uts',
            'updated_uts' => 'Updated Uts',
            'referers' => 'Referers',
        ];
    }

    /**
     * @return Product[]
     */
    public function getOrderProducts()
    {
        return Product::find()->where(['order_id' => $this->id])->all();
    }

    /**
     * @return State
     */
    public function getOrderState()
    {
        if ($this->state === null && $this->state_id !== null) {
            $this->state = State::find()->where(['id' => $this->state_id])->one();
        }
        return $this->state;
    }

    /**
     * @param State $state
     */
    public function setOrderState(State $state)
    {
        $this->state = $state;
        $this->state_id = $state->getId();
    }

    /**
     * @return Shipping
     */
    public function getOrderShipping()
    {
        return Shipping::find()->where(['order_id' => $this->id])->one();
    }

    /**
     *
     */
    public function setStateId()
    {
        $this->state_id = 1;
    }

    public function getProfit()
    {
        $productPrice = 0;
        $orderProducts = $this->getOrderProducts();
        foreach ($orderProducts as $product) {
            $productPrice += $product->getQuantity() * $product->getBuyPrice();
        }
        return $this->getTotal() - $this->getOrderShipping()->getCost() - $productPrice;
    }

    /**
     *
     */
    public function setReferers($referers)
    {
        $this->referers = $referers;
    }

    /**
     *
     */
    public function getReferers()
    {
        return $this->referers;
    }

    /**
     *
     */
    public function setPayment()
    {
        $this->payment_id = 1;
    }

    /**
     *
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     *
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Сохранения заказа в 1 клик
     *
     * @param $product_id
     * @param $phone
     * @param array $params
     * @return array|bool
     */
    public function saveOrderBuyOneClick($product_id, $phone, array $params)
    {
        try {
            $product = CatalogProduct::findOne($product_id);
            if ($product === null) {
                throw new Exception('Нет такого товара');
            }
            $account = AccountSingleton::getInstance()->getAccount();
            //@TODO Сохранение актуальных данный в сессию
            $account = ModelAccount::findOne($account->getId());
            $value['Order'] = [
                'phone' => $phone,
                'fio' => ($account->isAuto() ? 'Покупатель' : $account->getFio()),
                'email' => ($account->isAuto() ? 'feedback@mrplaydoh.ru' : $account->getEmail()),
                'shipping' => 'courier',
                'courier' => ['address' => 'Заказ в 1 клик. Уточнить способ и адрес доставки'],
                'comment' => 'Заказ в 1 клик! Уточнить контактные данные клиента, способ и адрес доставки.',
                'products' => [
                    ['id' => $product->getId(), 'quantity' => 1, 'price' => $product->getPrice()],
                ]
            ];
            if ($this->saveOrder($value, $params) === false) {
                Tools::getError($this);
            }
            return true;
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        return ['error' => $error];
    }

    /**
     * @param array $value
     * @param array $params
     * @return array
     * @throws \yii\db\Exception
     */
    public function saveOrder(array $value, array $params)
    {
        $error = '';
        $this->load($value);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $geo = null;
            if (!empty($params['geo_id'])) {
                $geo = Geo::findOne((int)$params['geo_id']);
            }
            if ($geo === null) {
                $geo = Geo::findOne(1);
            }

            if (count($value['Order']['products']) < 1) {
                throw new Exception('В корзине нет ни одного товара');
            }

            if (empty($value['Order']['phone'])) {
                throw new Exception('Не указан номер телефона');
            }
            $phone = (int)preg_replace('/\D+/', '', $value['Order']['phone']);
            $this->setPhone($phone);

            $this->setPayment(1);

            if (empty($value['Order']['shipping'])) {
                throw new Exception('Не указан способ доставки');
            }

            $shipping = $value['Order']['shipping'];

            if (empty($value['Order'][$shipping])) {
                throw new Exception('Не указаны данные доставки');
            }

            $orderShipping = new Shipping();
            switch ($shipping) {
                case 'courier':
                    if (empty($value['Order'][$shipping]['address'])) {
                        throw new Exception('Не указан адрес доставки');
                    }
                    $geoValue = GeoValue::find()
                        ->where(['geo_id' => $geo->getId(), 'shipping_id' => 1])
                        ->one();

                    if ($geoValue === null) {
                        throw new Exception('Не указан способ доставки');
                    }
                    $shippingValue = new Courier();
                    $shippingValue->setAddress($value['Order'][$shipping]['address']);
                    $shippingValue->setShippingGeoValue($geoValue);
                    $shippingValue->setOrderShipping($orderShipping);

                    $orderShipping->setType(Shipping::TYPE_COURIER);
                    $orderShipping->setCost($geoValue->getPrice());
                    break;
                case 'pickup':
                    if (empty($value['Order'][$shipping]['id'])) {
                        throw new Exception('Не указан пункт выдачи');
                    }
                    $geoValue = GeoValue::find()
                        ->where([
                            'geo_id' => $geo->getId(),
                            'shipping_id' => 2,
                            'id' => (int)$value['Order'][$shipping]['id']
                        ])
                        ->one();

                    if ($geoValue === null) {
                        throw new Exception('Не указан пункт выдачи');
                    }
                    $shippingValue = new Pickup();
                    $shippingValue->setShippingGeoValue($geoValue);
                    $shippingValue->setOrderShipping($orderShipping);

                    $orderShipping->setType(Shipping::TYPE_PICKUP);
                    $orderShipping->setCost($geoValue->getPrice());
                    break;
                case 'post':
                    $geoValue = GeoValue::find()
                        ->where(['geo_id' => $geo->getId(), 'shipping_id' => 3])
                        ->one();

                    if ($geoValue === null) {
                        throw new Exception('Не указан способ доставки');
                    }

                    if (empty($value['Order'][$shipping]['index'])) {
                        throw new Exception('Не указан индекс города');
                    }

                    if (empty($value['Order'][$shipping]['region'])) {
                        throw new Exception('Не указан регион доставки');
                    }

                    if (empty($value['Order'][$shipping]['city'])) {
                        throw new Exception('Не указан город доставки');
                    }

                    if (empty($value['Order'][$shipping]['address'])) {
                        throw new Exception('Не указан адрес доставки');
                    }

                    if (empty($value['Order'][$shipping]['fio'])) {
                        throw new Exception('Не указаны фио получателя');
                    }

                    $shippingValue = new Post();

                    $shippingValue->setIndex($value['Order'][$shipping]['index']);
                    $shippingValue->setRegion($value['Order'][$shipping]['region']);
                    $shippingValue->setCity($value['Order'][$shipping]['city']);
                    $shippingValue->setAddress($value['Order'][$shipping]['address']);
                    $shippingValue->setFio($value['Order'][$shipping]['fio']);

                    $shippingValue->setShippingGeoValue($geoValue);
                    $orderShipping->setType(Shipping::TYPE_POST);
                    $orderShipping->setCost($geoValue->getPrice());
                    break;
                default:
                    throw new Exception('Не указан способ доставки');
            }
            $account = AccountSingleton::getInstance()->getAccount();

            $totalPrice = 0;
            foreach ($value['Order']['products'] as $product) {
                $totalPrice += (int)($product['price'] * $product['quantity']);
            }
            $this->setAccount($account);
            $this->setTotal($totalPrice + $orderShipping->getCost());
            $this->setStateId();

            $session = Yii::$app->session;
            if (!empty($session['referers'])) {
                $this->setReferers(serialize($session['referers']));
                Yii::$app->session->set('referers', []);
            }

            if ($this->save()) {

                if (!empty($value['Order']['comment'])) {
                    $comment = new Comment();
                    $comment->setOrder($this);
                    $comment->setAccount($this->getAccount());
                    $comment->setComment($value['Order']['comment']);
                    $comment->setStateNew($this->getOrderState());
                    if ($comment->save() === false) {
                        Tools::getError($comment);
                    }
                }

                $orderShipping->setOrder($this);
                if ($orderShipping->save()) {

                    $shippingValue->setOrderShipping($orderShipping);

                    if ($shippingValue->save()) {
                        foreach ($value['Order']['products'] as $product) {

                            $catalogProduct = CatalogProduct::findOne($product['id']);

                            if ($product['quantity'] > $catalogProduct->getQuantity()) {
                                throw new Exception('Не достаточно товара');
                            }

                            $orderProduct = new OrderProduct();
                            $orderProduct->setOrder($this);
                            $orderProduct->setPrice($catalogProduct->getPrice());
                            $buyPrice = $catalogProduct->getBuyPrice();
                            $buyPrice = $buyPrice === 0 ? $catalogProduct->getBasePrice() : $buyPrice;
                            $orderProduct->setBuyPrice($buyPrice);
                            $orderProduct->setCatalogProduct($catalogProduct);
                            $orderProduct->setCatalogProductArticle($catalogProduct->getArticle());
                            $orderProduct->setCatalogProductTitle($catalogProduct->getTitle());
                            $orderProduct->setQuantity($product['quantity']);
                            $orderProduct->setTotal($catalogProduct->getPrice() * $product['quantity']);
                            $orderProduct->setDeleted(false);

                            if ($orderProduct->save() === false) {
                                Tools::getError($orderProduct);
                            }
                        }

                        $account->setFio($this->getFio());
                        $account->setPhone($this->getPhone());
                        $account->setEmail($this->getEmail());
                        $account->setAuto(false);
                        AccountSingleton::getInstance()->saveAccount($account);

                        //Отправление письма пользователю
                        $email = new ModelEmail();
                        $email->setDone(false);
                        $email->setEmail($this->getEmail());

                        $email->setSubject('Ваш заказ №' . $this->getId() . '-MPD принят');
                        $signature = '<div style="margin-top: 10px;">С уважением, MrPlayDoh.ru<br/>Телефоны: 8(495)278-08-20, 8(812)670-07-90</div>';

                        $body = Yii::$app->controller->getView()->renderFile(Yii::getAlias('@webroot') . '/../views/layouts/order.php', ['order' => $this, 'signature' => $signature]);
                        $email->setBody('<section>' . $body . '</section>');

                        if ($email->save() === false) {
                            Tools::getError($email);
                        }

                        //Сохраняем Utm метки пользователя
                        if (!empty($session['utm'])) {
                            foreach ($session['utm'] as $key => $value) {
                                $utm = new Utm();
                                $utm->load($value);
                                $utm->setOrder($this);
                                if ($utm->save() === false) {
                                    Tools::getError($utm);
                                }
                                unset($utm);
                            }
                            $utmOrderId['Utm'] = ['utm_order_id' => $this->getId(), 'created_uts' => time()];
                            $key = md5(implode('', $utmOrderId['Utm']));
                            $sessionUtm = $session['utm'];
                            $sessionUtm[$key] = $utmOrderId;
                            Yii::$app->session->set('utm', $sessionUtm);
                        }
                        $transaction->commit();

                        return true;
                    } else {
                        Tools::getError($shippingValue);
                    }
                } else {
                    Tools::getError($orderShipping);
                }
            } else {
                Tools::getError($this);
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            $error = $e->getMessage();
        }
        return ['error' => $error];
    }

    /**
     * Сохранение статуса + сохранение комментария
     *
     * @param State $state
     * @return bool
     * @throws Exception
     */
    public function saveState(State $state)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $oldState = $this->getOrderState();

            if ($oldState->getId() !== $state->getId()) {
                $this->setOrderState($state);
                if ($this->save() === false) {

                    throw new Exception (json_encode($this->getFirstErrors()));
                }
                $comment = new Comment();
                $comment->setOrder($this);
                $comment->setStateOld($oldState);
                $comment->setStateNew($state);
                $comment->setComment('');
                $comment->setAccount(ModelAccount::findOne(1123));
                if ($comment->save() === false) {
                    throw new Exception (json_encode($this->getFirstErrors()));
                }
            } else {
                throw new Exception ('New State === Old State');
            }

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new Exception($e->getMessage());
        }
    }
}
