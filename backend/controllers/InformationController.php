<?php
namespace backend\controllers;

use common\models\Information;
use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/**
 * Information  controller
 */
class InformationController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Information::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    /**
     * Information list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

    /**
     * Information create
     *
     * @return string
     */
    public function actionCreate()
    {
        $information = new Information();
        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $information->load($_POST);

            if ($information->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['information/index']));
            }
            $errors = $information->getErrors();
        }
        return $this->render('form', ['information' => $information, 'errors' => $errors]);
    }

    /**
     * Information create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Information $information */
        $information = Information::findOne((int)$id);

        if ($information === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['information/index']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $information->load($_POST);

            if ($information->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['information/index']));
            }
            $errors = $information->getErrors();
        }

        return $this->render('form', ['information' => $information, 'errors' => $errors]);
    }

}
