<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Information;

/* @var yii\web\View $this */
/* @var $information Information */

?>
<div class="site-index row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($information->getId() ? Url::to(['edit', 'id' => $information->getId()]) : Url::to(['create'])),
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<div class="button">' . Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) . '</div>';

        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($information, 'title');
        echo $form->field($information, 'url');
        echo $form->field($information, 'description')->textarea(['col' => 5, 'row' => 10]);
        echo '</fieldset>';


        echo '<fieldset><legend>Сео</legend>';
        echo $form->field($information, 'seo_title');
        echo $form->field($information, 'seo_keywords');
        echo $form->field($information, 'seo_description')->textarea(['col' => 5, 'row' => 10]);;
        echo '</fieldset>';

        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
