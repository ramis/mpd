<?php
namespace frontend\controllers;

use common\models\catalog\product\Review;
use common\models\order\Comment;
use common\tools\Tools;
use Yii;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\rest\Controller;
use yii\web\Cookie;
use yii\web\Response;
use common\models\Order;
use common\models\Cart;
use common\models\Cart\Product as CartProduct;
use common\models\Geo;
use common\models\catalog\Product;
use common\models\order\Shipping;
use common\models\shipping\GeoValue;
use frontend\models\AccountSingleton;

/**
 * Json controller
 */
class JsonController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    private function formatJson()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    /**
     * Оформление заказа в 1 клик
     *
     * @return string
     */
    public function actionBuy()
    {
        $this->formatJson();
        $response = [];

        if (Yii::$app->request->getIsPost()) {
            $order = new Order();
            $phone = Yii::$app->request->post('phone');
            $product_id = Yii::$app->request->post('product_id');
            $params['geo_id'] = (int)Yii::$app->request->cookies->getValue('geo_id');
            $result = $order->saveOrderBuyOneClick($product_id, $phone, $params);
            if ($result === true) {
                $products = [];
                foreach ($order->getOrderProducts() as $product) {
                    $products[] = [
                        'id' => $product->getCatalogProduct()->getId(),
                        'title' => $product->getCatalogProductTitle(),
                        'article' => $product->getCatalogProductArticle(),
                        'price' => $product->getPrice(),
                        'quantity' => $product->getQuantity(),
                        'category' => $product->getCatalogProduct()->getMainCategory()->getTitle(),
                    ];
                }
                $response = [
                    'order_id' => $order->getId(),
                    'total' => $order->getTotal(),
                    'shipping_cost' => $order->getOrderShipping()->getCost(),
                    'products' => $products
                ];
            } else {
                $response['error'] = !empty($result['error']) ? $result['error'] : '';
            }
        } else {
            $response['error'] = 'Ошибка сохранения заявки';
        }

        return $response;
    }

    /**
     * Добавление товара в корзину (добавление количества к товару в корзине)
     *
     * @return string
     */
    public function actionAdd()
    {
        $this->formatJson();
        $response = [];

        if (Yii::$app->request->getIsPost()) {

            $quantity = (int)Yii::$app->request->post('quantity');
            /** @var Product $product */
            $product = Product::findOne((int)Yii::$app->request->post('product_id'));
            if ($product && $product->getQuantity() >= $quantity) {
                $account = AccountSingleton::getInstance()->getAccount();
                $cart = $account->getCart();
                $result = $cart->addCartProduct($product, $quantity);
                if ($result === true) {
                    $productQuantity = $cart->getQuantityToProduct($product);
                    $response = [
                        'total' => $cart->getTotalPrice(),
                        'product_quantity' => $productQuantity,
                        'total_product_price' => $product->getPrice() * $productQuantity,
                        'cart_text' => Tools::humanPluralForm($cart->getCartProductsQuantity(),
                                ['товар', 'товара', 'товаров']) . ' на ' . $cart->getTotalPrice() . ' <span class="rub">pуб.</span>',
                    ];
                } else {
                    $response['error'] = $result;
                }
            } else {
                $response['error'] = 'Ошибка недостаточно товара';
            }
        } else {
            $response['error'] = 'Ошибка добавления товара';
        }

        return $response;
    }

    /**
     * Удаление товара их корзины (уменьшение количества)
     *
     * @return string
     */
    public function actionDel()
    {
        $this->formatJson();
        $response = [];

        $quantity = (int)Yii::$app->request->post('quantity');
        /** @var Product $product */
        $product = Product::findOne((int)Yii::$app->request->post('product_id'));

        $account = AccountSingleton::getInstance()->getAccount();
        $cart = $account->getCart();
        $result = $cart->delCartProduct($product, $quantity);
        if ($result === true) {

            $productQuantity = $cart->getQuantityToProduct($product);
            $response = [
                'total' => $cart->getTotalPrice(),
                'product_not_in_stock' => 0,
                'product_quantity' => $productQuantity,
                'total_product_price' => $product->getPrice() * $productQuantity,
            ];

            /** @var CartProduct $product */
            $products = $cart->getCartProducts();
            foreach ($products as $product) {
                $catalogProduct = $product->getCatalogProduct();
                if ($product->getQuantity() > $catalogProduct->getQuantity()) {
                    $response['product_not_in_stock'] = 1;
                    break;
                }
            }

            $response['cart_text'] = Tools::humanPluralForm($cart->getCartProductsQuantity(), ['товар', 'товара', 'товаров'])
                . ' на ' . $cart->getTotalPrice() . ' <span class="rub">pуб.</span>';

        } else {
            $response['error'] = $result;
        }

        return $response;
    }

    /**
     * Отправка сообщения от пользователя
     *
     * @return string
     */
    public function actionFeedback()
    {
        $this->formatJson();
        $response = [];

        if (Yii::$app->request->isPost) {
            $mailer = Yii::$app->mailer;
            try {
                if (Yii::$app->request->post('feedback_email', null) === null ||
                    Yii::$app->request->post('feedback_title', null) === null ||
                    Yii::$app->request->post('feedback_description', null) === null
                ) {
                    throw new Exception('Заполните все поля!');
                }

                $mailer->compose()
                    ->setTo(Yii::$app->params['saleEmail'])
                    ->setFrom(Yii::$app->params['saleEmail'])
                    ->setSubject('Сообщение от пользователя - ' . Yii::$app->request->post('feedback_title') . ' ' . Yii::$app->request->post('feedback_email', null))
                    ->setHtmlBody(Yii::$app->request->post('feedback_description', null))
                    ->send();
            } catch (Exception $e) {
                $response['error'] = $e->getMessage();
            }
        }
        return $response;
    }

    /**
     * Сохранение отзыва от пользователя
     *
     * @return string
     */
    public function actionReview()
    {
        $this->formatJson();
        $response = [];

        if (Yii::$app->request->isPost) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (Yii::$app->request->post('review', null) === null ||
                    Yii::$app->request->post('review', '') === '' ||
                    Yii::$app->request->post('rating', null) === null
                ) {
                    throw new Exception('Заполните все поля!');
                }
                $product = Product::findOne((int)Yii::$app->request->post('product_id', 0));
                if($product === null) {
                    throw new Exception('Ошибка 500, обновите пожалуйста страницу!');
                }
                $account = AccountSingleton::getInstance()->getAccount();

                $review = new Review();
                $review->setRating((int)Yii::$app->request->post('rating', 1))
                    ->setReview(trim(Yii::$app->request->post('review', '')))
                    ->setName(trim(Yii::$app->request->post('name', '')))
                    ->setEmail(trim(Yii::$app->request->post('email', '')))
                    ->setState(Review::STATE_NEW)
                    ->setAccountId($account->getId())
                    ->setCatalogProduct($product);

                if($review->save() === false) {
                    throw new Exception('Ошибка 500, обновите пожалуйста страницу!');
                }
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
                $response['error'] = 'Ошибка, обновите пожалуйста страницу!';
//                $response['error'] = $e->getMessage();
            }
        }
        return $response;
    }

    /**
     * Подсказки к поиску
     *
     * @return string
     */
    public function actionSearch()
    {
        $this->formatJson();
        $response = [];

        $search = Yii::$app->request->post('query');

        list($words, $ids, $product) = Product::searchSphinx($search);

        if ($product) {
            $img = $product->getMainImage() ? $product->getMainImage()->getHtml(['width' => 50, 'height' => 50]) : '';

            $price = $product->getPrice() > 0 ? '<span class="wrapper_price"> ' . $product->getPrice() . ' <span class="rub">руб.</span></a></span>' : '';
            $response[] = [
                'img' => ($product->getMainImage() ? $product->getMainImage()->getHtml(['width' => 50, 'height' => 50]) : ''),
                'title' => $product->getTitle() . ' ' . $price,
                'url' => Url::to(['product/index', 'id' => $product->getId()]),
            ];
        } elseif (count($ids) > 0) {
            foreach ($ids as $id) {
                $product = Product::findOne($id);
                $img = $product->getMainImage() ? $product->getMainImage()->getHtml(['width' => 50, 'height' => 50]) : '';

                $price = $product->getPrice() > 0 ? '<span class="wrapper_price"> ' . $product->getPrice() . ' <span class="rub">руб.</span></a></span>' : '';
                $response[] = [
                    'img' => ($product->getMainImage() ? $product->getMainImage()->getHtml(['width' => 50, 'height' => 50]) : ''),
                    'title' => $product->getTitle() . ' ' . $price,
                    'url' => Url::to(['product/index', 'id' => $product->getId()]),
                ];
            }
        }

        return $response;
    }

    /**
     * Выбор гео пользователем и сохраниние его в куки
     *
     * @return string
     */
    public function actionGeo()
    {
        $this->formatJson();

        $geoId = (int)Yii::$app->request->post('geo_id');
        /* @var Geo $geo */
        $geo = Geo::findOne($geoId);

        if ($geo === null) {
            if ($geoId = (int)Yii::$app->request->cookies->getValue('geo_id')) {
                $geo = Geo::findOne($geoId);
            }
            if ($geo === null) {
                $geo = Geo::findOne(1);
            }
        }

        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'geo_id',
            'value' => $geo->getId(),
            'path' => '/',
            'expire' => time() + (30 * 24 * 60 * 60),
        ]));

        $geoValues = GeoValue::find()->where(['geo_id' => $geo->getId(), 'is_active' => true])->orderBy('shipping_id asc, point_id asc')->all();
        $shippingValue = [];
        $shippingKeys = [];
        foreach ($geoValues as $value) {
            $shippingKeys[$value->getShipping()->getId()] = [
                'geo' => $geo->getTitleWhere(),
                'id' => $value->getShipping()->getId(),
                'key' => ($value->getShipping()->getId() === 1 ? 'courier' : ($value->getShipping()->getId() === 2 ? 'pickup' : 'post')),
                'price' => $value->getPrice(),
            ];

            $shippingValue[$value->getShipping()->getId()][] = [
                'id' => $value->getId(),
                'price' => $value->getPrice(),
                'address' => $value->getAddress(),
                'description' => $value->getDescription(),
            ];
        }
        $shippingKeys = array_values($shippingKeys);

        return ['shippingKeys' => $shippingKeys, 'shippingValue' => $shippingValue];
    }

    /**
     *
     */
    public function actionApi()
    {
        $this->formatJson();

        $status = 200;
        try {
            if (!Yii::$app->request->getIsPost()) {
                throw new \Exception('Do not post request', 1000);
            }

            $raw_request = file_get_contents('php://input');
            $request = json_decode($raw_request, true);

            //Проверяем версию
            if (empty($request['v'])) {
                throw new \Exception('Version can not be null', 1001);
            }

            if ($request['v'] !== '0.4') {
                throw new \Exception('This version is not supported', 1002);
            }

            //Проверяем method
            if (empty($request['method'])) {
                throw new \Exception('Method can not be null', 1010);
            }

            //Проверяем парамерты
            if (!(isset($request['params']))) {
                throw new \Exception('Params can not be null', 1020);
            }

            $v = $request['v'];
            $method = $request['method'];

            switch ($method) {
                case 'getOrdersIdsByPeriod':

                    //Проверяем если все входные данные
                    if (empty($request['params']['start'])) {
                        throw new \Exception('"start" from params cannot be null', 1021);
                    }

                    $where = ['state_id' => 1];
                    $start = new \DateTime($request['params']['start']);
                    $query = Order::find()->where($where)->andWhere(['>', 'created_uts', $start->getTimestamp()]);
                    if (!empty($request['params']['end'])) {
                        $end = new \DateTime($request['params']['end']);
                        $query->andWhere(['<', 'created_uts', $end->getTimestamp()]);
                    }

                    $orderIds = [];
                    $orders = $query->all();
                    foreach ($orders as $o) {
                        $orderIds[] = $o->getId();
                    }

                    $response = [
                        'v' => $v,
                        'method' => $method,
                        'data' => [
                            'orders_ids' => $orderIds,
                        ],
                    ];
                    break;
                case 'getOrderDataById':

                    if (empty($request['params']['order_id'])) {
                        throw new \Exception('"order_id" from params cannot be null', 1022);
                    }

                    $orderId = (int)$request['params']['order_id'];
                    /**
                     * @var Order $order
                     */
                    $order = Order::findOne($orderId);

                    if ($order) {
                        $orderProducts = $order->getOrderProducts();
                        $dataOrderProducts = [];
                        $comments = [];
                        $orderComments = Comment::find()->where(['order_id' => $order->getId()])->all();
                        foreach ($orderComments as $comment) {
                            if($comment->getComment() !== '') {
                                $comments[] = $comment->getComment();
                            }
                        }

                        foreach ($orderProducts as $product) {

                            $v3ToysProduct  = \common\models\v3toys\Product::find()->where(['v3toys_product_id' => $product->getCatalogProduct()->getCode()])->one();

                            if($v3ToysProduct && $v3ToysProduct->isAssortiment()) {
                                $comments[] = 'Нужен товар - "' . $product->getCatalogProductTitle() . '"';
                            }
                            $dataOrderProducts[] = [
                                'product_id' => $product->getCatalogProduct()->getCode(),
                                'price' => $product->getPrice(),
                                'quantity' => $product->getQuantity(),
                            ];
                        }
                        /**
                         * @var Shipping $shipping
                         */
                        $shipping = $order->getOrderShipping();

                        switch ($shipping->getType()) {
                            case Shipping::TYPE_COURIER:
                                $shippingValue = $shipping->getOrderShippingValue();
                                $geoTitle = $shippingValue->getShippingGeoValue()->getGeo()->getTitle();

                                $city = $geoTitle === 'Москва' ? 'Москва до МКАД' : $geoTitle;
                                $city = $city === 'Санкт-Петербург' ? 'Санкт-Петербург до КАД' : $city;

                                $shippingMethod = 'COURIER';
                                $shippingCost = $shipping->getCost();
                                $shippingData = [
                                    'city' => $city,
                                    'address' => $shippingValue->getAddress(),
                                ];
                                break;
                            case Shipping::TYPE_PICKUP:
                                $shippingValue = $shipping->getOrderShippingValue();
                                $geoTitle = $shippingValue->getShippingGeoValue()->getGeo()->getTitle();

                                $city = $geoTitle === 'Московская область' ? 'Москва' : $geoTitle;
                                $city = $city === 'Ленинградская область' ? 'Санкт-Петербург' : $city;

                                $shippingMethod = 'PICKUP';
                                $shippingCost = $shipping->getCost();
                                $shippingData = [
                                    'city' => $city,
                                    'point_id' => $shippingValue->getShippingGeoValue()->getPointId(),
                                ];
                                break;
                            case Shipping::TYPE_POST:
                                $shippingValue = $shipping->getOrderShippingValue();

                                $shippingMethod = 'POST';
                                $shippingCost = $shipping->getCost();
                                $shippingData = array(
                                    'index' => $shippingValue->getIndex(),
                                    'area' => $shippingValue->getRegion(),
                                    'region' => $shippingValue->getRegion(),
                                    'city' => $shippingValue->getCity(),
                                    'address' => $shippingValue->getAddress(),
                                    'recipient' => $shippingValue->getFio(),
                                );
                                break;
                            default:
                                throw new \Exception('Invalid shipping method', 1000);
                                break;
                        }

                        $dataOrder = [
                            'order_id' => $order->getId(),
                            'fake' => 0,
                            'created_at' => Yii::$app->formatter->asDatetime($order->getCreatedUts(), 'php:Y-m-d H:i:s'),
                            'full_name' => $order->getFio(),
                            'phone' => $order->getPhone(),
                            'email' => $order->getEmail(),

                            'products' => $dataOrderProducts,

                            'shipping_method' => $shippingMethod,
                            'shipping_cost' => $shippingCost,
                            'shipping_data' => $shippingData,

                            'comment' => implode('<br>', $comments),
                            'discount' => 0,
                        ];
                    } else {
                        throw new \Exception('"order_id" ' . $orderId . ' not found');
                    }
                    $response = [
                        'v' => $v,
                        'method' => $method,
                        'data' => $dataOrder,
                    ];
                    break;
                case 'getProductDataById':

                    if (empty($request['params']['product_id'])) {
                        throw new \Exception('"product_id" from params cannot be null', 1023);
                    }

                    $productId = (int)$request['params']['product_id'];

                    $product = Product::find()->where(['code' => $productId])->one();

                    if (!($product instanceof Product)) {
                        throw new \Exception('Product with this "product_id" is no', 400);
                    }

                    $images = [];

                    $media = $product->getMedia();
                    if ($media) {
                        $images = $media->getImages();
                        foreach ($images as $image) {
                            $images [] = Yii::$app->params['HTTP_IMAGE_P_500_500'] . $image->getHash();
                        }
                    }
                    $response = [
                        'v' => $v,
                        'method' => $method,
                        'data' => [
                            'product_id' => $product->getCode(),
                            'title' => $product->getTitle(),
                            'url' => Url::toRoute(['product/index', 'id' => $product->getId()], true),
                            'price' => $product->getPrice(),
                            'images' => $images,
                        ],
                    ];
                    break;
                case 'getProductsDataByIds':

                    if (empty($request['params']['products_ids'])) {
                        throw new \Exception("Invalid params products_ids", 400);
                    }
                    $response = [
                        'v' => $v,
                        'method' => $method,
                        'data' => []
                    ];

                    $where = [];

                    $productIds = (array)$request['params']['products_ids'];
                    if (empty($productIds)) {
                        throw new \Exception("Invalid params products_ids", 400);
                    } else {
                        $ids = [];
                        foreach ($productIds as $id) {
                            $id = (int)$id;
                            if ($id > 0) {
                                $ids[] = $id;
                            }
                        }
                        if (!empty($ids)) {
                            $where['code'] = $ids;
                        } else {
                            throw new \Exception("Can't null products_ids", 400);
                        }
                    }
                    $products = Product::find()->where($where)->limit(50)->all();

                    $productsData = [];
                    foreach ($products as $product) {

                        /**
                         * @var Product $product
                         */
                        if ($product->getQuantity() > 0) {
                            $imagesProduct = [];

                            $media = $product->getMedia();
                            if ($media) {
                                $images = $media->getImages();
                                foreach ($images as $image) {
                                    if ($image->getHash()) {
                                        $imagesProduct[] = Yii::$app->params['HTTP_IMAGE_P_500_500'] . $image->getHash();
                                    }

                                }
                            }

                            $productsData[] = [
                                'product_id' => $product->getCode(),
                                'title' => $product->getTitle(),
                                'url' => Url::toRoute(['product/index', 'id' => $product->getId()], true),
                                'price' => $product->getPrice(),
                                'images' => $imagesProduct,
                            ];
                        }
                    }

                    $response['data'] = $productsData;
                    break;
                case 'getMessageIdsByPeriod':
                    $response = [];
                    break;
                case 'getMessageDataById':
                    $response = [];
                    break;
                default:
                    throw new \Exception('This method is not supported', 1011);
            }

        } catch (\Exception $e) {
            $status = 400;
            $error['error_code'] = $e->getCode();
            $error['error_message'] = $e->getMessage();
        }

        if ($status === 400) {
            header("HTTP/1.0 400 Bad Request", true, 400);
        }

        print $status === 200 ? json_encode($response) : json_encode($error);
    }

}
