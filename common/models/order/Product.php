<?php

namespace common\models\order;

use Yii;
use common\models\catalog\Product as CatalogProduct;
use \common\models\behavior\Order;
use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\Id;
use common\models\behavior\Price;
use common\models\behavior\Quantity;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_product}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $catalog_product_id
 * @property string $catalog_product_title
 * @property string $catalog_product_article
 * @property integer $quantity
 * @property integer $price
 * @property integer $buy_price
 * @property integer $total
 * @property integer $created_uts
 * @property integer $updated_uts
 * @property boolean $is_deleted
 *
 * @property CatalogProduct $product
 * @property Order $order
 */
class Product extends ActiveRecord
{
    use Id;
    use Order;
    use Quantity;
    use Price;
    use CreatedUpdatedUts;

    private $product;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'catalog_product_id', 'catalog_product_title', 'catalog_product_article', 'quantity', 'price', 'total'], 'required'],
            [['order_id', 'catalog_product_id', 'quantity', 'price', 'buy_price', 'total', 'created_uts', 'updated_uts'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['catalog_product_title', 'catalog_product_article'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'catalog_product_id' => Yii::t('app', 'Catalog Product ID'),
            'catalog_product_title' => Yii::t('app', 'Catalog Product Title'),
            'catalog_product_article' => Yii::t('app', 'Catalog Product Article'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'total' => Yii::t('app', 'Total'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'updated_uts' => Yii::t('app', 'Updated Uts'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
        ];
    }

    /**
     * @return CatalogProduct
     */
    public function getCatalogProduct()
    {
        if ($this->product === null && $this->catalog_product_id !== null) {
            $this->product = CatalogProduct::find()->where(['id' => $this->catalog_product_id])->one();
        }

        return $this->product;
    }

    /**
     * @param CatalogProduct $product
     */
    public function setCatalogProduct(CatalogProduct $product)
    {
        $this->product = $product;
        $this->catalog_product_id = $product->getId();
    }

    /**
     * @param string $catalogProductTitle
     */
    public function setCatalogProductTitle($catalogProductTitle)
    {
        $this->catalog_product_title = $catalogProductTitle;
    }

    /**
     * @return string
     */
    public function getCatalogProductTitle()
    {
        return $this->catalog_product_title;
    }

    /**
     * @param string $catalogProductArticle
     */
    public function setCatalogProductArticle($catalogProductArticle)
    {
        $this->catalog_product_article = $catalogProductArticle;
    }

    /**
     * @return string
     */
    public function getCatalogProductArticle()
    {
        return $this->catalog_product_article;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param bool $is_deleted
     */
    public function setDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @return int
     */
    public function getBuyPrice()
    {
        return (int)$this->buy_price;
    }

    /**
     * @param int $buy_price
     */
    public function setBuyPrice($buy_price)
    {
        $this->buy_price = $buy_price;
    }
}
