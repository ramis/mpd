<?php

use yii\db\Schema;
use yii\db\Migration;

class m150928_200505_alter_catalog_product_subscription extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%catalog_product_subscription}}', 'updated_uts', Schema::TYPE_BIGINT);
    }

    public function safeDown()
    {
    }

}
