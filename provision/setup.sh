#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive

sudo locale-gen en_US en_US.UTF-8 ru_RU ru_RU.UTF-8

sudo apt-get update
sudo apt-get upgrade

# Install mysql, nginx, php5-fpm
sudo aptitude install -q -y -f nginx php5-fpm git postgresql postgresql-contrib
#postgres-xc-client postgres-xc


# ----------------------
# --- PHP packages -----
# ----------------------

# Install commonly used php packages
sudo aptitude install -q -y -f php5-dev pkg-config
sudo aptitude install -q -y -f php5-mysql php5-curl php5-gd php5-intl php-pear php5-imagick
sudo aptitude install -q -y -f php5-imap php5-mcrypt php5-memcached php5-ming php5-ps php5-pspell
sudo aptitude install -q -y -f php5-recode php5-tidy php5-xmlrpc php5-xsl php5-xcache php5-pgsql


#sudo mkdir /data/postgres
#sudo chown -r postgres:postgres /data/postgres
#sudo su - postgres -c "pg_ctl -D /data/postgres -o '--nodename foo' initdb"

# composer
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /bin/composer

composer global require "fxp/composer-asset-plugin:~1.0"

sudo ln -sf /home/vagrant/mpd/provision/files/nginx.conf /etc/nginx/sites-enabled/host
sudo ln -sf /home/vagrant/mpd/provision/files/pg_hba.conf /etc/postgresql/9.3/main/pg_hba.conf
#sudo ln -sf /home/vagrant/v3/provision/files/postgresql.conf /etc/postgresql/9.3/main/postgresql.conf
sudo ln -sf /home/vagrant/mpd/provision/files/php5-fpm.conf /etc/php5/fpm/pool.d/www.conf

sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'postgres';"
sudo -u postgres psql -c "create database mpd;"