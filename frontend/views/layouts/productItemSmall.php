<?php
use yii\helpers\Html;
use common\models\catalog\Product;
use frontend\models\AccountSingleton;

/**
 * @var Product $product
 */
if (!($product instanceof Product)) {
    return;
}
$account = AccountSingleton::getInstance()->getAccount(false);
$cart = ($account) ? $account->getCart(false) : null;
$isProductToCart = false;
if ($cart) {
    $cartProducts = $cart->getCartProducts();
    foreach ($cartProducts as $cartProduct) {
        if ($product->getId() === $cartProduct->getCatalogProductId()) {
            $isProductToCart = true;
            break;
        }
    }
} ?>
<div class="product_item_small">
    <?php
    $image = $product->getMainImage();
    if ($image) {
        echo Html::a($image->getHtml(['width' => 150, 'height' => 150, 'title' => $product->getTitle(), 'alt' => $product->getTitle()]),
            ['product/index', 'id' => $product->getId()]);
    }
    ?>
    <div class="product_item_small_title">
        <?= Html::a($product->getTitle(), ['product/index', 'id' => $product->getId()]); ?>
    </div>
    <div class="product_price">
        <div class="<?php echo ($product->isSale() ? 'sale' : '')?> price"><?= $product->getPrice(); ?><span class="rub">руб.</span></div>
        <?php
        if ($product->getQuantity() > 0) {
            if($isProductToCart) {
                echo '<div class="btn product_to_cart">В корзине</div>';
            }else {
                echo '<div class="btn add_product_cart" id="add_product_cart_'.$product->getId().'" product_id="'.$product->getId().'">В
                корзину</div>';
            }

        } else {
            echo '<div class="subscribe">Уведомить меня</div>';
        }
        ?>

    </div>
</div>
