<?php
namespace common\models\catalog;

use Yii;
use yii\db\ActiveRecord;
use common\models\catalog\product\AttributeValue;
use common\models\behavior\Id;
use common\models\behavior\Sort;
use common\models\behavior\Title;

/**
 * This is the model class for table "{{%catalog_attribute}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $sort
 *
 * @property AttributeValue[] $values
 */
class Attribute extends ActiveRecord
{
    use Id;
    use Title;
    use Sort;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'sort'], 'required'],
            [['id', 'sort'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }

}
