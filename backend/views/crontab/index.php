<?php
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">

    <h1>Скрипты</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'controller',
            'command',
            'title',
            'updated_ts'
        ],
    ]);
    ?>
</div>
