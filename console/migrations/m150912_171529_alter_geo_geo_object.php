<?php

use yii\db\Schema;
use yii\db\Migration;

class m150912_171529_alter_geo_geo_object extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%geo}}', 'geo_object', Schema::TYPE_TEXT);
    }

    public function safeDown()
    {

    }
}
