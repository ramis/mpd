<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\catalog\product\Review;
use common\models\catalog\product\Subscription;
use common\models\order\Comment;
use common\models\behavior\Email;
use common\models\behavior\Fio;
use common\models\behavior\Phone;
use frontend\models\AccountSingleton;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer $id
 * @property string $fio
 * @property integer $phone
 * @property string $email
 * @property boolean $is_auto
 * @property integer $user_id
 *
 * @property User $user
 * @property Cart $cart
 * @property Review[] $catalogProductReviews
 * @property Subscription[] $catalogProductSubscriptions
 * @property Message[] $messages
 * @property Order[] $orders
 * @property Comment[] $orderComments
 */
class Account extends ActiveRecord
{
    use Fio;
    use Phone;
    use Email;

    private $cart;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'is_auto'], 'required'],
            [['id', 'phone', 'user_id'], 'integer'],
            [['is_auto'], 'boolean'],
            [['fio', 'email'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Fio',
            'phone' => 'Phone',
            'email' => 'Email',
            'is_auto' => 'Is Auto',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return User::find()->where(['id' => $this->user_id])->one();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getCountCartProduct()
    {
        $cart = $this->getCart(false);
        return ($cart) ? $cart->getCartProductsQuantity() : 0;
    }

    /**
     * @param bool|true $isAutoCreated
     * @return Cart|null
     */
    public function getCart($isAutoCreated = true)
    {

        $this->cart = Cart::find()->where(['account_id' => $this->id])->one();

        if ($isAutoCreated && $this->cart === null) {
            $this->cart = new Cart();

            $checkAccount = Account::findOne($this->getId());
            if($checkAccount === null){
                $checkAccount = AccountSingleton::getInstance()->getAccount(true, true);
            }
            $this->cart->setAccount($checkAccount);
            $this->cart->save();
        }

        return $this->cart;
    }

    /**
     * @return bool
     */
    public function isAuto()
    {
        return $this->is_auto;
    }

    /**
     * @param bool $is_auto
     */
    public function setAuto($is_auto)
    {
        $this->is_auto = $is_auto;
    }

    /**
     * @return Account
     */
    public static function autoCreateAccount()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try{
            $account = new Account();
            $account->setFio('autoAccount' . time() . rand(0, 100));
            $account->setEmail('');
            $account->setAuto(true);
            $account->save();
            $transaction->commit();
            return $account;
        } catch (\Exception $e) {
            $transaction->rollBack();
        }
    }

}
