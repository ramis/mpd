<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use common\models\media\Files;
use backend\models\ImageForm;
use common\models\behavior\CreatedUpdatedUts;
use common\models\behavior\Id;
use common\models\behavior\ParentCn;
use common\models\media\ints\MediaInts;

/**
 * This is the model class for table "{{%media}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $parent_cn
 * @property integer $created_uts
 * @property integer $updated_uts
 *
 * @property Files[] $mediaFiles
 */
class Media extends ActiveRecord
{
    use Id;
    use ParentCn;
    use CreatedUpdatedUts;

    private $videoFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%media}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'parent_cn'], 'required'],
            [['parent_id', 'created_uts', 'updated_uts'], 'integer'],
            [['parent_cn'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'parent_cn' => Yii::t('app', 'Parent Cn'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'updated_uts' => Yii::t('app', 'Updated Uts'),
        ];
    }

    /**
     * @return Files[]
     */
    public function getMediaFiles()
    {
        return $this->hasMany(Files::className(), ['media_id' => 'id']);
    }

    public function getImageForm()
    {
        return new ImageForm();
    }

    public function getFilesSort($type)
    {
        $file = Files::find()
            ->select('f.*')
            ->from(Files::tableName() . ' f')
            ->where(['type' => $type, 'media_id' => $this->id])
            ->orderBy('sort desc')
            ->one();
        return $file ? $file->getSort() : 0;
    }

    public function saveContainer(MediaInts $parent)
    {

        $this->setParentId($parent->getId());
        $this->setParentCn(get_class($parent));

        $transaction = Yii::$app->db->beginTransaction();

        try {

            if ($this->save() === false) {
                throw new Exception(json_encode($this->getFirstErrors()));
            }

            $parent->setMedia($this);
            if ($parent->save() === false) {
                throw new Exception(json_encode($parent->getFirstErrors()));
            }

            if (!empty($this->videoFiles)) {
                foreach ($this->videoFiles as $item) {
                    if ($item->save() === false) {
                        throw new Exception(json_encode($item->getFirstErrors()));
                    }
                }
            }

            $imageForm = $this->getImageForm();
            $imageForm->imageFiles = UploadedFile::getInstances($imageForm, 'imageFiles');

            if ($imageForm->validate() === false) {
                throw new Exception('Error validate image');
            }
            $sort = $this->getFilesSort(Files::TYPE_IMAGE);
            foreach ($imageForm->imageFiles as $item) {
                $sort++;
                $hash = $imageForm->save($item);

                $mediaFile = new Files();
                $mediaFile->setMedia($this);
                $mediaFile->setHash($hash);
                $mediaFile->setSort($sort);

                $mediaFile->setType(Files::TYPE_IMAGE);

                if ($mediaFile->save() === false) {
                    throw new Exception(json_encode($mediaFile->getFirstErrors()));
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }
    }


    /**
     * Первая (Главная картинка) Медиа
     *
     * @return null|Files
     */
    public function getMainImage()
    {
        return Files::find()->where(['media_id' => $this->getId(), 'type' => Files::TYPE_IMAGE])->orderBy('sort asc')->one();
    }

    /**
     * Все картинки Медиа
     *
     * @return array Files
     */
    public function getImages()
    {
        return Files::find()->where(['media_id' => $this->getId(), 'type' => Files::TYPE_IMAGE])->orderBy('sort asc')->all();
    }


    /**
     * Все видео Медиа
     *
     * @return array Files
     */
    public function getVideos()
    {
        if($this->videoFiles === null) {
            $this->videoFiles = Files::find()->where(['media_id' => $this->getId(), 'type' => Files::TYPE_VIDEO])->orderBy('sort asc')->all();
        }
        return $this->videoFiles;
    }

    /**
     * @param array $values
     */
    public function setVideos(array $values)
    {
        if (!empty($values)) {

            $videos = $this->getVideos();
            foreach ($values as $item) {
                if ($item === '') {
                    continue;
                }
                /** @TODO Переделать на n видео */
                $sort = 0;
                if(!empty($videos[$sort])) {
                    $videoFile = $videos[$sort];
                 } else {
                    $videoFile = new Files();
                }

                $videoFile->setHash($item);
                $videoFile->setSort(0);
                $videoFile->setType(Files::TYPE_VIDEO);
                $videoFile->setMedia($this);
                $this->videoFiles[] = $videoFile;
            }
        }
    }
}
