<?php
use yii\grid\GridView;
use yii\helpers\Html;
use backend\models\direct\Keywords;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="site-index">
    <div class="right"><?=Html::a('Добавить', \yii\helpers\Url::to(['direct/keywords/create'])); ?></div>

    <h1>Ключевые запросы</h1>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'keyword',
            'direct_id',
            [
                'attribute' => 'Adgroup',
                'value' => function (Keywords $keywords) {
                    return $keywords->getAdgroup()->getName();
                }
            ],
            'userparam1',
            'userparam2',
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Keywords $keywords) {
                    return Html::a('Редактировать', ['direct/keywords/edit', 'id' => $keywords->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
