<?php

use yii\helpers\Html;
use frontend\widgets\LeftColumn;
use common\models\catalog\Category;
use common\models\catalog\Product;
use common\models\catalog\tag\Group;
use common\models\catalog\Tag;

/* @var $this yii\web\View */
$this->title = 'Карта сайта Плей До (Play-Doh) интернет магазин MrPlayDoh.ru';

$categories = Category::find()->where(['is_published' => true, 'is_special' => false])->orderBy('sort asc')->all();
$groups = Group::find()->all();
?>

<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col">
        <div class="site_index sitemap">
            <h1>Карта сайта</h1>
            <div class="row">
                <?php
                echo '<div class="col-md-6"><ul>';
                foreach ($categories as $category) {
                    echo '<li>' . Html::a($category->getTitle(), ['route/index', 'url' => $category->getUrl()]) . '</li>';
                    echo '<li><ul>';
                    $products = Product::find()->where(['catalog_category_id' => $category->getId(), 'is_published' => true])->all();
                    if (count($products) > 0) {
                        foreach ($products as $product) {
                            echo '<li>' . Html::a($product->getTitle(), ['product/index', 'id' => $product->getId()]) . '</li>';
                        }
                    }
                    echo '</li></ul>';
                }
                echo '</div></ul>';
                echo '<div class="col-md-6"><ul>';
                foreach ($groups as $group) {
                    echo '<li>' . $group->getTitle() . '</li>';
                    echo '<li><ul>';
                    $tags = $group->getTags();
                    foreach ($tags as $tag) {
                        echo '<li>' . Html::a($tag->getTag(), ['tags/tag', 'url' => $tag->getUrl()]) . '</li>';
                    }
                    echo '</li></ul>';
                }
                echo '</div></ul>';
                ?>
            </div>
        </div>
    </div>
</div>