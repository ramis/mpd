<?php

namespace common\models\catalog\product;

use Yii;
use yii\db\ActiveRecord;
use common\models\catalog\Tag as CatalogTag;
use common\models\catalog\Product as CatalogProduct;

/**
 * This is the model class for table "{{%catalog_tag_id}}".
 *
 * @property integer $catalog_product_id
 * @property integer $catalog_tag_id
 *
 * @property CatalogProduct $catalogProduct
 * @property CatalogTag $catalogProductTag
 */
class Tag extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_product_id', 'catalog_tag_id'], 'required'],
            [['catalog_product_id', 'catalog_tag_id'], 'integer'],
            [['catalog_tag_id', 'catalog_product_id'], 'unique', 'targetAttribute' => ['catalog_tag_id', 'catalog_product_id'], 'message' => 'The combination of Catalog Product ID and Catalog Product Tag has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'catalog_product_id' => Yii::t('app', 'Catalog Product ID'),
            'catalog_tag_id' => Yii::t('app', 'Catalog Product Tag'),
        ];
    }

    /**
     * @return CatalogProduct[]
     */
    public function getCatalogProduct()
    {
        return $this->hasOne(CatalogProduct::className(), ['id' => 'catalog_product_id']);
    }

    /**
     * @return CatalogTag[]
     */
    public function getCatalogProductTag()
    {
        return $this->hasOne(CatalogTag::className(), ['id' => 'catalog_tag_id']);
    }
}
