<?php

namespace backend\models\direct;

use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Id;
use common\models\behavior\Adgroup;

/**
 * This is the model class for table "{{%direct_bids}}".
 *
 * @property integer $id
 * @property integer $adgroup_id
 * @property integer $bid
 * @property integer $context_bid
 * @property integer $strategy_priority
 *
 * @property Adgroup $adgroup
 */
class Bids extends ActiveRecord
{
    use Id;
    use Adgroup;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%direct_bids}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adgroup_id', 'bid', 'context_bid', 'strategy_priority'], 'required'],
            [['adgroup_id', 'bid', 'context_bid', 'strategy_priority'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'adgroup_id' => Yii::t('app', 'Adgroup ID'),
            'bid' => Yii::t('app', 'Bid'),
            'context_bid' => Yii::t('app', 'Context Bid'),
            'strategy_priority' => Yii::t('app', 'Strategy Priority'),
        ];
    }

    /**
     * @return int
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * @param int $bid
     */
    public function setBid($bid)
    {
        $this->bid = $bid;
    }

    /**
     * @return int
     */
    public function getContextBid()
    {
        return $this->context_bid;
    }

    /**
     * @param int $context_bid
     */
    public function setContextBid($context_bid)
    {
        $this->context_bid = $context_bid;
    }

    /**
     * @return int
     */
    public function getStrategyPriority()
    {
        return $this->strategy_priority;
    }

    /**
     * @param int $strategy_priority
     */
    public function setStrategyPriority($strategy_priority)
    {
        $this->strategy_priority = $strategy_priority;
    }
}
