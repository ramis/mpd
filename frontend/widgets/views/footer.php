<?php
use yii\helpers\Html;
use common\models\Information;

$informations = Information::find()->all();
$dataInformation = [];
foreach ($informations as $information) {
    $dataInformation[$information->getUrl()] = $information->getTitle();
}
$ref = null;

?>
<footer>
    <div id="footer">
        <div id="footer_top">
            <div id="social">
                <ul>
                    <li>
                        <a id="link_ok" target="_blank">
                            <div class="ok"></div>
                        </a>
                    </li>
                    <li>
                        <a id="link_fb" target="_blank">
                            <div class="fb"></div>
                        </a>
                    </li>
                    <li>
                        <a id="link_vk" target="_blank">
                            <div class="vk"></div>
                        </a>
                    </li>
<!--                    <li>-->
<!--                        <a href="">-->
<!--                            <div class="in"></div>-->
<!--                        </a>-->
<!--                    </li>-->
                    <li>
                        <a id="link_tw" target="_blank">
                            <div class="tw"></div>
                        </a>
                    </li>
<!--                    <li>-->
<!--                        <a href="">-->
<!--                            <div class="yt"></div>-->
<!--                        </a>-->
<!--                    </li>-->
                </ul>
            </div>
            <div id="footer_top_links">
                <ul>
                    <li>
                        <?= Html::a($dataInformation['shipping'], ['route/index', 'url' => 'shipping'], $ref); ?>
                    </li>
                    <li>
                        <?= Html::a($dataInformation['payment'], ['route/index', 'url' => 'payment'], $ref); ?>
                    </li>
                    <!--                    <li>-->
                    <!--                        <a href="">Отзывы</a>-->
                    <!--                    </li>-->
                    <li>
                        <?= Html::a($dataInformation['company'], ['route/index', 'url' => 'company'], $ref); ?>
                    </li>
                    <!--                    <li>-->
                    <!--                        <a href="">Новости</a>-->
                    <!--                    </li>-->
                    <!--                    <li>-->
                    <!--                        <a href="">Статьи</a>-->
                    <!--                    </li>-->
                    <li>
                        <?= Html::a($dataInformation['contact'], ['route/index', 'url' => 'contact'], $ref); ?>
                    </li>
                    <li>
                        <?= Html::a('Быстрый поиск', ['tags/index'], $ref); ?>
                    </li>
                    <li id="map_site">
                        <?= Html::a('Карта сайта', ['route/index', 'url' => 'map'], $ref); ?>
                    </li>
                </ul>
            </div>
        </div>
        <div id="footer_border">
        </div>
        <div id="footer_bottom">
            <div id="footer_bottom_title">
                &copy; Магазин пластилина Play Doh, <?= date('Y'); ?>
            </div>
            <div id="footer_bottom_phone">
                <span class="phone_code">8 495</span> 278-08-20<br/>
                <span class="phone_code">8 812</span> 670-07-90
            </div>
            <div id="footer_bottom_email" data-toggle="modal" data-target="#modalFeedback">
                Напишите нам
            </div>

        </div>
    </div>
</footer>
<div class="modal fade" id="modalFeedback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 solid #ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Напишите нам</h3>
            </div>
            <div role="alert" id="feedback_error" class="alert alert-danger col-md-12"></div><div class="clear"></div>
            <div class="modal-body" id="modal_body">
                <div class="form-group field-feedback_title">
                    <div class="col-md-2"><label for="field-feedback_title" class="control-label">Ваше имя</label></div>
                    <div class="col-md-10"><input type="text" value="" name="feedback_title" class="form-control" id="feedback_title"></div>
                    <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                </div>
                <div class="form-group field-feedback_email">
                        <div class="col-md-2"><label for="field-feedback_email" class="control-label">Эл почта</label></div>
                        <div class="col-md-10"><input type="text" value="" name="feedback_email" class="form-control" id="feedback_email"></div>
                        <div class="col-md-offset-2 col-md-10"><div class="help-block">адрес не будет опубликован</div></div>
                </div>
                <div class="form-group field-feedback_description">
                        <div class="col-md-2"><label for="product-feedback_description" class="control-label">Сообщение</label></div>
                        <div class="col-md-10"><textarea row="10" col="5" name="feedback_description" class="form-control" id="feedback_description"></textarea></div>
                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                </div>
                <div class="form-group">
                        <div class="col-md-2">&nbsp;</div>
                        <div class="col-md-10">
                            <div class="btn send_email_account" id="">Отправить</div>
                        </div>
                        <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
                </div>
            </div>
            <div class="clear"></div>
            <br/>
        </div>
    </div>
</div>