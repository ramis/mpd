<?php
namespace backend\controllers\direct;

use backend\models\direct\Bids;
use Yii;
use backend\controllers\AbstractController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;


/**
 * Catalog Product controller
 */
class BidsController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Bids::find(),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);
    }

    /**
     * Bids list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

}
