<?php

namespace common\models\behavior;

trait Id
{
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}