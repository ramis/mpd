<?php
use yii\helpers\Html;
use common\models\Order;
use common\models\Cart;
use backend\models\CrontabStat;
use common\models\Account;
use common\models\catalog\Product;
use common\models\catalog\product\Review;
/* @var $this yii\web\View */
?>

<div class="dash-tiles row">

    <!-- Column 3 of Row 1 -->
    <div class="col-sm-3">
        <!-- Popularity Tile -->
        <div class="dash-tile dash-tile-oil clearfix animation-pullDown">
            <div class="dash-tile-header">
                Корзины
            </div>
            <div class="dash-tile-icon"><i class="fa fa-shopping-cart"></i></div>
            <div class="dash-tile-text"><?= Cart::find()->andFilterWhere(['>', 'created_uts', strtotime("today")])->count(); ?></div>
        </div>
        <!-- END Popularity Tile -->

        <!-- Server Downtime Tile -->
        <div class="dash-tile dash-tile-dark clearfix animation-pullDown">
            <div class="dash-tile-header">
                Пользователи
            </div>
            <div class="dash-tile-icon"><i class="fa fa-users"></i></div>
            <div class="dash-tile-text"><?= Account::find()->count(); ?></div>
        </div>
        <!-- END Server Downtime Tile -->
    </div>
    <!-- END Column 3 of Row 1 -->

    <!-- Column 1 of Row 1 -->
    <div class="col-sm-3">
        <!-- Total Users Tile -->
        <div class="dash-tile dash-tile-ocean clearfix animation-pullDown">
            <div class="dash-tile-header">
                Заказов сегодня
            </div>
            <div class="dash-tile-icon"><i class="fa fa-credit-card"></i></div>
            <div class="dash-tile-text"><?= Order::find()->andFilterWhere(['>', 'created_uts', strtotime("today")])->count(); ?></div>
        </div>
        <!-- END Total Users Tile -->

        <!-- Total Profit Tile -->
        <div class="dash-tile dash-tile-leaf clearfix animation-pullDown">
            <div class="dash-tile-header">
                На сумму
            </div>
            <div class="dash-tile-icon"><i class="fa fa-money"></i></div>
            <div class="dash-tile-text"><?= Yii::$app->formatter->asCurrency((int)Order::find()->andFilterWhere(['>', 'created_uts', strtotime("today")])->sum('total')); ?></div>
        </div>
        <!-- END Total Profit Tile -->
    </div>
    <!-- END Column 1 of Row 1 -->

    <!-- Column 2 of Row 1 -->
    <div class="col-sm-3">
        <!-- Total Sales Tile -->
        <div class="dash-tile dash-tile-flower clearfix animation-pullDown">
            <div class="dash-tile-header">
                Отзывов сегодня
            </div>
            <div class="dash-tile-icon"><i class="fa fa-list-alt"></i></div>
            <div class="dash-tile-text"><?= Review::find()->andFilterWhere(['>', 'created_uts', strtotime("today")])->count(); ?></div>
        </div>
        <!-- END Total Sales Tile -->

        <!-- Total Downloads Tile -->
        <div class="dash-tile dash-tile-fruit clearfix animation-pullDown">
            <div class="dash-tile-header">
                Отзывов не опубликованных
            </div>
            <div class="dash-tile-icon"><i class="fa fa-info"></i></div>
            <div class="dash-tile-text"><?= Review::find()->where(['state' => Review::STATE_NEW])->count(); ?></div>
        </div>
        <!-- END Total Downloads Tile -->
    </div>
    <!-- END Column 2 of Row 1 -->

    <!-- Column 4 of Row 1 -->
    <div class="col-sm-3">
        <!-- RSS Subscribers Tile -->
        <div class="dash-tile dash-tile-balloon clearfix animation-pullDown">
            <div class="dash-tile-header">
                Товаров в наличии
            </div>
            <div class="dash-tile-icon"><i class="fa fa-folder-o"></i></div>
            <div class="dash-tile-text"><?= Product::find()->andFilterWhere(['>', 'quantity', 0])->count(); ?></div>
        </div>
        <!-- END RSS Subscribers Tile -->

        <!-- Total Tickets Tile -->
        <div class="dash-tile dash-tile-doll clearfix animation-pullDown">
            <div class="dash-tile-header">
                Total Tickets
            </div>
            <div class="dash-tile-icon"><i class="fa fa-wrench"></i></div>
            <div class="dash-tile-text">1.5k</div>
        </div>
        <!-- END Total Tickets Tile -->
    </div>
    <!-- END Column 4 of Row 1 -->
</div>


<div class="row">
    <div class="col-sm-6">
        <div class="dash-tile dash-tile-2x">
            <div class="dash-tile-header">
                <i class="fa fa-shopping-cart"></i> Заказы
            </div>
            <div class="dash-tile-content">
                <div class="dash-tile-content-inner-fluid">
                    <table id="dash-example-orders" class="table table-striped table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>Дата</th>
                                <th>Фио</th>
                                <th>Статус</th>
                                <th>Доставка</th>
                                <th>Итого</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        {
                            /** @var Order[] $orders */
                            $orders = Order::find()->limit(10)->orderBy(['id' => SORT_DESC])->all();
                            foreach($orders as $order) {
                                echo '
                                    <tr>
                                        <td>'.$order->getId().'</td>
                                        <td>'.Yii::$app->formatter->asDatetime($order->getCreatedUts(), 'dd.MM.YY').'</td>
                                        <td>'.$order->getFio().'</td>
                                        <td>'.$order->getOrderState()->getTitle().'</td>
                                        <td>'.$order->getOrderShipping()->getText().'</td>
                                        <td>'.$order->getTotal().'</td>
                                    </tr>
                                ';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Datatables Tile -->
    </div>
    <!-- END Column 1 of Row 3 -->

    <!-- Column 2 of Row 3 -->
    <div class="col-sm-6">
        <!-- Users Tile -->
        <div class="dash-tile remove-margin">
            <div class="dash-tile-header">
                <i class="fa fa-user"></i> Отзывы
            </div>
            <div class="dash-tile-content">
                <div class="dash-tile-content-inner-fluid">
                    <table id="dash-example-orders" class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Дата</th>
                            <th>Фио</th>
                            <th>Товар</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        {
                            /** @var Review[] $reviews */
                            $reviews = Review::find()->limit(5)->orderBy(['id' => SORT_DESC])->all();
                            foreach($reviews as $review) {
                                echo '
                                    <tr>
                                        <td>'.$review->getId().'</td>
                                        <td>'.Yii::$app->formatter->asDatetime($review->getCreatedUts(), 'dd.MM.YY').'</td>
                                        <td>'.$review->getName().'</td>
                                        <td>'.$review->getCatalogProduct()->getTitle().'</td>
                                        <td><span class="label label-'.$review->getStateLabel().'">'.$review->getStateTitle().'</span></td>
                                        <td>'.Html::a('Редактировать', ['edit', 'id' => $review->getId()]).'</td>
                                    </tr>
                                ';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="">&nbsp;</div>
        <div class="dash-tile">
            <div class="dash-tile-header">
                <i class="fa fa-user"></i> Кроны
            </div>
            <div class="dash-tile-content">
                <div class="dash-tile-content-inner-fluid">
                    <table id="dash-example-orders" class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>controller</th>
                            <th>command</th>
                            <th>title</th>
                            <th>updated_ts</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        {
                            /** @var CrontabStat[] $crontabstat */
                            $crontabstat = CrontabStat::find()->limit(5)->orderBy(['id' => SORT_DESC])->all();
                            foreach($crontabstat as $item) {
                                echo '
                                    <tr>
                                        <td>'.$item->getController().'</td>
                                        <td>'.$item->getCommand().'</td>
                                        <td>'.$item->getTitle().'</td>
                                        <td>'.Yii::$app->formatter->asDatetime($item->getUpdatedTs()).'</td>
                                    </tr>
                                ';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
