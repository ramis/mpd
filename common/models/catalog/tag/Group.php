<?php
namespace common\models\catalog\tag;

use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\catalog\Product;
use common\models\catalog\product\Tag as ProductTag;
use Yii;
use yii\db\ActiveRecord;
use common\models\catalog\Tag;

/**
 * This is the model class for table "{{%catalog_tag_group}}".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Tag[] $tags
 */
class Group extends ActiveRecord
{
    use Id;
    use Title;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_tag_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return Tag[]
     */
    public function getTags()
    {
        return Tag::find()->where(['group_id' => $this->getId()])->all();
    }

    public function getCountProduct()
    {
        return Product::find()
            ->select('p.id')
            ->from(Product::tableName().' p')
            ->innerJoin(ProductTag::tableName().' pt', "pt.catalog_product_id = p.id")
            ->innerJoin(Tag::tableName().' t', "pt.catalog_tag_id = t.id and t.group_id = " . $this->getId())
            ->where(['p.is_published' => true])
            ->groupBy('p.id')
            ->count();
    }

}
