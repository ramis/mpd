<?php

namespace common\models\geo;

use Yii;
use yii\db\ActiveRecord;
use common\models\behavior\Area;
use common\models\behavior\City;
use common\models\behavior\GeoObject;
use common\models\behavior\Id;
use common\models\behavior\Region;

/**
 * This is the model class for table "{{%geo_cities}}".
 *
 * @property integer $id
 * @property string $city
 * @property string $region
 * @property string $area
 * @property string $geo_object
 *
 * @property Cidr[] $geoCidrs
 */
class Cities extends ActiveRecord
{
    use Id;
    use City;
    use Region;
    use Area;
    use GeoObject;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_cities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'region', 'area'], 'required'],
            [['geo_object'], 'string'],
            [['city', 'region', 'area'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city' => Yii::t('app', 'City'),
            'region' => Yii::t('app', 'Region'),
            'area' => Yii::t('app', 'Area'),
            'geo_object' => Yii::t('app', 'Geo Object'),
        ];
    }

    /**
     * @return Cidr
     */
    public function getGeoCidrs()
    {
        return $this->hasMany(Cidr::className(), ['geo_cities_id' => 'id']);
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
