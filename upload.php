#!/usr/bin/env php
<?php
define('DS', DIRECTORY_SEPARATOR);
$time = time();

$spliter1 = '-----------------------------------------------------------------------------------' . "\n";
$spliter2 = '***********************************************************************************' . "\n";

$logname = getenv('LOGNAME');
$prefix = '/Users/Ramis/mpd';

if (!empty($prefix)) {

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        '--exclude runtime ' .
        '--exclude web/assets ' .
        '--exclude config ' .
        '--exclude web/index.php ' .
        $prefix . '/backend ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        '--exclude config ' .
        '--exclude mail ' .
        $prefix . '/common ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        '--exclude runtime ' .
        '--exclude config ' .
        $prefix . '/console ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        $prefix . '/environments ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        '--exclude runtime ' .
        '--exclude config/main-local.php ' .
        '--exclude config/params-local.php ' .
        '--exclude web/sitemap.xml ' .
        '--exclude web/markt.yml ' .
        '--exclude web/index.php ' .
        '--exclude web/search.yml ' .
        '--exclude migrations ' .
        '--exclude session ' .
        '--exclude web/assets ' .
        $prefix . '/frontend ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        '--exclude index ' .
        $prefix . '/sphinx ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        $prefix . '/crontab ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command =
        'rsync -av --del ' .
        '--exclude .DS_Store ' .
        '--exclude runtime ' .
        '--exclude config/main-local.php ' .
        '--exclude config/params-local.php ' .
        '--exclude web/sitemap.xml ' .
        '--exclude migrations ' .
        '--exclude session ' .
        '--exclude web/assets ' .
        $prefix . '/composer.json ramis@mpd:/opt/mrplaydoh.ru/';
    echo $spliter1 . $command . "\n";
    passthru($command);

    $command = 'ssh ramis@mpd "/opt/mrplaydoh.ru/init --env=prod --overwrite=All";';
    echo $spliter1 . $command . "\n";
    passthru($command);

}
