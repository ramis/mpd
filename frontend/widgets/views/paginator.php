<?php
use yii\helpers\Html;
use common\tools\Tools;

list($url, $query) = Tools::parseUrl($url);
unset($query['page']);
$urlPage = $url;
$delimiter = '?';
foreach ($query as $key => $value) {
    $urlPage .= $delimiter . $key . '=' . $value;
    $delimiter = '&';
}
?>

<div id="block_pages">
    <!--                <div id="show_more">-->
    <!--                    <ul>-->
    <!--                        <li>-->
    <!--                            <a hre="">Показать еще</a>-->
    <!--                        </li>-->
    <!--                    </ul>-->
    <!--                </div>-->
    <div id="pages_container">
        <ul>
            <?php
            if ($page > 1) {
                echo '<li>' . Html::a('В начало', $urlPage, ['class' => "beginning"]) . '</li>';
            }
            if ($page > 2) {
                echo '<li>' . Html::a('&larr;', $urlPage . $delimiter . 'page=' . ($page - 1)) . '</li>';
            }

            $cnt = 5;
            if ($countPage > $cnt) {
                if ($page < 4) {
                    $start = 1;
                    $end = $countPage > $cnt ? ($cnt + 1) : $countPage + 1;
                } elseif ($countPage > $cnt && ($countPage - $page) < ($cnt - 1)) {
                    $start = $countPage - ($cnt - 1);
                    $end = $countPage + 1;
                } else {
                    $start = $page - 2;
                    $end = $countPage > ($page + 3) ? $page + 3 : $countPage + 1;
                }
            } else {
                $start = 1;
                $end = $countPage > $cnt ? ($cnt + 1) : $countPage + 1;
            }

            for ($i = $start; $i < $end; $i++) {
                echo '<li>' . Html::a($i, (($i > 1) ? $urlPage . $delimiter . 'page=' . ($i) : $urlPage)) . '</li>';
            }
            if (($page + 1) < $countPage) {
                echo '<li>' . Html::a('&rarr;', $urlPage . $delimiter . 'page=' . ($page + 1)) . '</li>';
            }
            ?>
        </ul>
    </div>
</div>