<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_222925_yml extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%yml}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING  . ' NOT NULL',
            'file' => Schema::TYPE_STRING  . ' NOT NULL',
            'name' => Schema::TYPE_STRING  . ' NOT NULL',
            'company' => Schema::TYPE_STRING  . ' NOT NULL',
            'shipping_cost' => Schema::TYPE_INTEGER  . ' NOT NULL',
            'export_product_in_stock' => Schema::TYPE_BOOLEAN  . ' NOT NULL',
            'is_short' => Schema::TYPE_BOOLEAN  . ' NOT NULL',
            'url_params' => Schema::TYPE_STRING  . ' NOT NULL',
        ]);
    }

    public function safeDown()
    {
    }
}
