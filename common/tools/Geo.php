<?php
namespace common\tools;

use Yii;
use yii\base\Exception;
use common\models\Geo as ModelGeo;
use common\models\geo\Cidr;

class Geo
{
    CONST DADATA_URL = 'https://dadata.ru/api/v2';
    CONST DADATA_TOKEN = '8924947f3b6d177fdcdafa07cd0eedd46c65168f';

    /**
     * Текущие гео(Название) пользователя
     *
     * @return string
     */
    public static function getCityByGeoObject()
    {
        $geoObject = self::getGeoByIp();
        return self::getTitleCityByGeoObject($geoObject);
    }

    /**
     * Текущие гео данные пользователя для заказа по почте
     *
     * @return array [postcode, region, area, city]
     */
    public static function getPostDataGeoObject()
    {

        $geoObject = self::getGeoByIp();

        return [$geoObject['postal_code'], $geoObject['region_with_type'], $geoObject['area_with_type'], self::getTitleCityByGeoObject($geoObject)];
    }

    /**
     * Определение гео объекта пользователя по ip
     *
     * @return array $geoObject
     */
    private static function getGeoByIp()
    {

        $ip = self::getMyIp();

        $userIp = null;
        $geoObject = null;

        $session = Yii::$app->session->get(__CLASS__);

        //Проверяем мб у нас уже есть данные этого пользователя
        if (!empty($session)) {
            //Сохраненный у нас ip
            $userIp = !empty($session['ip']) ? $session['ip'] : null;
            $geoObject = !empty($session['geo_object']) ? json_decode($session['geo_object'], true) : null;
        }

        if (!($userIp && $userIp === $ip && $geoObject)) {

            $geoCidr = Cidr::find()->andFilterWhere(['<=', 'ip_start', $ip])->andFilterWhere(['<=', $ip, 'ip_end'])->one();

            if ($geoCidr && $geoObject->getGeoCities()) {
                $geoObject = $geoObject->getGeoCities();

                if (self::getTitleCityByGeoObject($geoObject) === null) {
                    $geoObject = null;
                }
            }

            if ($geoObject === null) {
                $geoObject = self::defaultGeoObject();
            }

            self::saveResultSession($userIp, $geoObject);
        }

        return $geoObject;
    }

    /**
     * Дефолтный гео объект Москва
     *
     * @return array
     */
    public static function defaultGeoObject()
    {
        return [
            'value' => 'г Москва',
            'unrestricted_value' => 'г Москва',
            'data' => [
                'qc_complete' => null,
                'qc_house' => null,
                'qc_geo' => 4,
                'postal_code' => 125000,
                'postal_box' => null,
                'country' => 'Россия',
                'region_with_type' => 'г Москва',
                'region_type' => 'г',
                'region_type_full' => 'город',
                'region' => 'Москва',
                'area_with_type' => null,
                'area_type' => null,
                'area_type_full' => null,
                'area' => null,
                'city_with_type' => 'г Москва',
                'city_type' => 'г',
                'city_type_full' => 'город',
                'city' => 'Москва',
                'city_district' => null,
                'settlement_with_type' => null,
                'settlement_type' => null,
                'settlement_type_full' => null,
                'settlement' => null,
                'street_with_type' => null,
                'street_type' => null,
                'street_type_full' => null,
                'street' => null,
                'house_type' => null,
                'house_type_full' => null,
                'house' => null,
                'block_type' => null,
                'block' => null,
                'flat_area' => null,
                'flat_type' => null,
                'flat' => null,
                'fias_id' => '0c5b2444-70a0-4932-980c-b4dc0d3f02b5',
                'fias_level' => null,
                'kladr_id' => 7700000000000,
                'tax_office' => 7700,
                'tax_office_legal' => null,
                'capital_marker' => 0,
                'okato' => 45000000000,
                'oktmo' => 45000000,
                'geo_lat' => 55.753689,
                'geo_lon' => 37.620099,
                'unparsed_parts' => null,
                'qc' => null
            ]
        ];

    }

    /**
     * Название города или населенного пункта из гео объекта
     *
     * @param $geoObject
     * @return string | null
     */
    private static function getTitleCityByGeoObject($geoObject)
    {
        return !empty($geoObject['value']) ? $geoObject['value'] : null;
    }

    /**
     * Ip пользователя
     *
     * @return int
     */
    private static function getMyIp()
    {
        $ip = ip2long($_SERVER['REMOTE_ADDR']);

        $ip = ip2long('91.199.153.4');

        return $ip;
    }

    /**
     * Сохранение geoObject для выбранного пользователем города или населенного пункта
     *
     * @param array $geoObject
     * @return boolean
     */
    public static function saveGeoObject($geoObject)
    {

        if (!empty($geoObject['data'])) {
            $ip = self::getMyIp();
            self::saveResultSession($ip, $geoObject['data']);
        }

        return true;
    }

    /**
     * Сохранение выбранного пользователем города
     *
     * @param $query
     * @param $is_static
     * @return string
     */
    public static function saveSelectCity($query, $is_static)
    {

        if ($is_static == 1) {
            $geoObject = self::getStaticGeoObject($query);
        } else {
            $geoObject = self::getGeoByIp();
        }

        $result = [
            'city' => self::getTitleCityByGeoObject($geoObject),
            'html' => Kiwi_Shop_Order_Admin::getShippingBlockToGeo(),
            'suggestCourierAddress' => self::getJQuerySuggestAddress('checkout[sm_Kiwi_ImLogistics_ShippingMethodCourier][address]'),
            'suggestPostAddress' => self::getJQuerySuggestAddress('checkout[sm_Kiwi_ImLogistics_ShippingMethodPost][address]'),
            'suggestPostName' => self::getJQuerySuggestName('checkout[sm_Kiwi_ImLogistics_ShippingMethodPost][recipient]'),
        ];

        return json_encode($result);
    }

    /**
     * Привязка geoObject с городом из БД и сохранение данных пользователя в сессии
     *
     * @param int $userIp
     * @param array $geoObject
     */
    private static function saveResultSession($userIp, $geoObject)
    {
        Yii::$app->session->set(__CLASS__, ['ip' => $userIp, 'geo_object' => json_encode($geoObject)]);
    }

    public static function getShippingMethodByGeo($type)
    {
        switch ($type) {
            case "Kiwi_ImLogistics_ShippingMethodCourier":
                return self::getShippingMethodCourierByGeo();
                break;
            case "Kiwi_ImLogistics_ShippingMethodPickup":
                return self::getShippingMethodPickupByGeo();
                break;
            case "Kiwi_ImLogistics_ShippingMethodPost":
                return true;
                break;
        }

        return false;
    }

    public static function getShippingMethodCourierByGeo()
    {

        $geoObject = self::getGeoByIp();

        $city = !empty($geoObject['city']) ? $geoObject['city'] : $geoObject['settlement'];
        $region = $geoObject['region'];

        //Курьер
        $shippingCity = false;
        $shippingOption = false;

        if (mb_strpos('Москва', $city, 0, 'UTF-8') !== false) {
            $shippingCity = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodCourier1msk');
            $shippingOption = $shippingCity->getChild('Kiwi_ImLogistics_ShippingOption');
        } elseif (mb_strpos('Санкт-Петербург', $city, 0, 'UTF-8') !== false) {
            $shippingCity = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodCourier1spb');
            $shippingOption = $shippingCity->getChild('Kiwi_ImLogistics_ShippingOption');
        } elseif ((mb_strpos('Москва', $region, 0, 'UTF-8') !== false) || (mb_strpos('Московская', $region, 0, 'UTF-8') !== false)) {
            $shippingCity = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodCourier1pdm');
            $shippingMOChilds = $shippingCity->getChilds('Kiwi_ImLogistics_ShippingOption');
            foreach ($shippingMOChilds as $child) {
                if (mb_strpos($child->getTitle(), $city, 0, 'UTF-8') !== false) {
                    $shippingOption = $child;
                    break;
                }
            }
            if ($shippingOption === false) {
                $shippingOption = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodCourier1pdm_other');
            }
        } elseif ((mb_strpos('Санкт-Петербург', $region, 0, 'UTF-8') !== false) || (mb_strpos('Ленинградская', $region, 0, 'UTF-8') !== false)) {
            $shippingCity = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodCourier1lenob');
            $shippingMOChilds = $shippingCity->getChilds('Kiwi_ImLogistics_ShippingOption');
            foreach ($shippingMOChilds as $child) {
                if (mb_strpos($child->getTitle(), $city, 0, 'UTF-8') !== false) {
                    $shippingOption = $child;
                    break;
                }
            }
            if ($shippingOption === false) {
                $shippingOption = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodCourier1lenob_other');
            }
        } else {
            $parent = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodCourier1reg');
            $childs = $parent->getChilds('Kiwi_ImLogistics_ShippingOption');
            foreach ($childs as $child) {
                if (mb_strpos($child->getTitle(), $city, 0, 'UTF-8') !== false) {
                    $shippingCity = $parent;
                    $shippingOption = $child;
                    break;
                }
            }
        }

        return ($shippingCity ? [$shippingCity, $shippingOption] : $shippingCity);
    }

    public static function getShippingMethodPickupByGeo()
    {

        $geoObject = self::getGeoByIp();

        $city = !empty($geoObject['city']) ? $geoObject['city'] : $geoObject['settlement'];
        $region = $geoObject['region'];

        //Самовывоз
        $shippingCity = false;
        $shippingOption = false;
        if (mb_strpos('Москва', $city, 0, 'UTF-8') !== false) {
            $shippingCity = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodPickup1msk');
            $shippingOption = $shippingCity->getChilds('Kiwi_ImLogistics_ShippingOption');
        } elseif ((mb_strpos('Санкт-Петербург', $city, 0, 'UTF-8') !== false) || (mb_strpos('Санкт-Петербург', $region, 0, 'UTF-8') !== false)) {
            $shippingCity = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodPickup1spb');
            $shippingOption = $shippingCity->getChilds('Kiwi_ImLogistics_ShippingOption');
        } elseif ((mb_strpos('Москва', $region, 0, 'UTF-8') !== false) || (mb_strpos('Московская', $region, 0, 'UTF-8') !== false)) {
            $shippingCity = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodPickup1msk');
            $shippingOption = $shippingCity->getChilds('Kiwi_ImLogistics_ShippingOption');
        } else {
            $parent = Kiwi_Node::get('Kiwi_ImLogistics_ShippingMethodPickup1reg');
            $childs = $parent->getChilds('Kiwi_ImLogistics_ShippingOption');
            foreach ($childs as $child) {
                if (mb_strpos($child->getTitle(), $city, 0, 'UTF-8') !== false) {
                    $shippingCity = $parent;
                    $shippingOption[] = $child;
                }
            }
        }

        return ($shippingCity ? [$shippingCity, $shippingOption] : $shippingCity);
    }

    public static function getJQuerySuggestAddress($id)
    {
        $geoObject = self::getGeoByIp();

        return 'jQuery("#"+Kiwi_Base.jQuote("' . $id . '")).suggestions({
                serviceUrl: "' . self::DADATA_URL . '",
                token: "' . self::DADATA_TOKEN . '",
                type: "ADDRESS",
                constraints: {
                    locations: {
                        kladr_id: "' . $geoObject['kladr_id'] . '"
                    }
                },
                restrict_value: true,
                onSelect: function(suggestion) {
                    args = new Array();
                    args.push(suggestion);
                    $.post(Kiwi_Base.getPearWWWVDir("Kiwi_Ajax") + "/ajax.php",
                        {
                            cn: "Kiwi_V3Toys_Geo",
                            func: "saveGeoObject",
                            args: args
                        },
                        function (data) {
                            return;
                        },
                        "json"
                    );
                }
            });';
    }

    public static function getJQuerySuggestName($id)
    {
        return 'jQuery("#"+Kiwi_Base.jQuote("' . $id . '")).suggestions({
                serviceUrl: "' . self::DADATA_URL . '",
                token: "' . self::DADATA_TOKEN . '",
                type: "NAME",
                count: "5",
                onSelect: function(suggestion) {
                }
            });';
    }

    public static function getJQuerySuggestCity($id)
    {
        return 'jQuery("#"+Kiwi_Base.jQuote("' . $id . '")).suggestions({
                serviceUrl: "' . self::DADATA_URL . '",
                token: "' . self::DADATA_TOKEN . '",
                type: "ADDRESS",
                geoLocation: true,
                bounds: "city-settlement",
                onSelect: function(suggestion) {
                    args = new Array();
                    args.push(suggestion);
                    $.post(Kiwi_Base.getPearWWWVDir("Kiwi_Ajax") + "/ajax.php",
                        {
                            cn: "Kiwi_V3Toys_Geo",
                            func: "saveGeoObject",
                            args: args
                        },
                        function (data) {
                            return;
                        },
                        "json"
                    );
                }
            });';
    }

    /**
     * Получение Гео объекта из таблицы geo, если там пусто то отдаем дефолтный
     *
     * @param $city
     * @return array|null
     */
    public static function getStaticGeoObject($city)
    {
        $geoObject = null;

        $geo = ModelGeo::find()->where(['title' => $city])->one();
        if ($geo && $geo->getGeoObject()) {
            $geoObject = self::getTitleCityByGeoObject($geo->getGeoObject()) ? $geo->getGeoObject() : null;
        }

        return $geoObject ? $geoObject : self::defaultGeoObject();
    }

    /**
     * Получение Гео объекта от dadata, если там пусто то отдаем дефолтный
     *
     * @param $city
     * @return array
     */
    public static function getGeoObjectByAddress($city)
    {

        $request = ['query' => $city, 'count' => 1];
        $geoObject = self::defaultGeoObject();

        try {
            $ch = curl_init();
            $headers = [
                'Content-Type: application/json',
                'Accept:  application/json',
                'Authorization: Token ' . self::DADATA_TOKEN
            ];

            curl_setopt($ch, CURLOPT_URL, self::DADATA_URL . '/suggest/address');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 12);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));

            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ($httpcode === 200) {
                $response = json_decode($result, true);
                if (!empty($response['suggestions'][0])) {
                    $geoObject = $response['suggestions'][0];
                }
            }
        } catch (Exception $e) {

        }

        return $geoObject;

    }
}