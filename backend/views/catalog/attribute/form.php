<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\catalog\Attribute;

/* @var yii\web\View $this */
/* @var $attribute Attribute */

$this->title = 'Catalog Category List';
?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1><?= ($attribute->getId() ? $attribute->getTitle() : 'Новый') ?></h1>
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($attribute->getId() ? Url::to(['edit', 'id'=>$attribute->getId()]) : Url::to(['create'])),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($attribute, 'title');
        echo $form->field($attribute, 'sort');
        echo '</fieldset>';
        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
