<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var yii\web\View $this */


?>
<div class="site-index row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin();
        echo '<div class="button">' . Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) . '</div>';

        echo '<fieldset><legend>Ссылка</legend>';
        echo '<div class="form-group field-nullmodel-text">
              <div class="col-md-2"><label for="nullmodel-text" class="control-label">Text</label></div>
              <div class="col-md-10"><input type="text" name="url" value="'.$url.'" class="form-control" id="nullmodel-text"></div>
              <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div>
              </div>';
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
    <h2>&nbsp;</h2>
    <table class="table">
        <?php
            foreach($links as $key => $link) {
                echo '<tr><td>' . $key . '</td>';
                echo '<td>' . $link . '</td></tr>';
            }
        ?>
    </table>
</div>
