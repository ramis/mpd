<?php

namespace common\models\behavior;

trait State
{
    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

}