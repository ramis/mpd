<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class AbstractModel extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_uts', 'updated_uts'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_uts'],
                ],
                'value' => function () {
                    return time();
                },
            ],
        ];
    }
}
