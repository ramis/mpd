<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\FileInput;
use common\models\catalog\Category;

/* @var yii\web\View $this */
/* @var $category Category */

$this->title = 'Test';
?>
<div class="site-index row">
    <div class="col-sm-10">
        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($test->getId() ? Url::to(['edit', 'id'=>$test->getId()]) : Url::to(['create'])),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>Основные параметры</legend>';
        echo $form->field($test, 'title');
        echo '</fieldset>';

        echo $form->field($test->getMedia()->getImageForm(), 'imageFiles[]')->widget(FileInput::classname(), ['options' => ['multiple'=>true, 'accept' => 'image/*']]);

        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
