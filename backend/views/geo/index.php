<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var yii\web\View $this */

$this->title = 'Catalog Geo List';
?>
<?= Html::csrfMetaTags() ?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1>Обновление геолокации</h1>

        <?php
            if($message === 'ok') {
                echo '<div class="alert alert-success col-md-12" role="alert">Обновили данные по Городам и Ip</div>';
            } else {
                if($message !== '') {
                    echo '<div class="alert alert-danger col-md-12" role="alert" style="float: none;">'.$message.'</div>';
                }
        ?>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>

        <fieldset><legend>Гео Данные</legend>
            <div class="form-group field-courier-cost required">
                <div class="col-md-2"><label for="cities" class="control-label">Города</label></div>
                <div class="col-md-10"><input type="file" name="cities" id="cities"></div>
            </div>
            <div class="form-group field-courier-cost required">
                <div class="col-md-2"><label for="cidr" class="control-label">Ip</label></div>
                <div class="col-md-10"><input type="file" name="cidr" id="cidr"></div>
            </div>
        </fieldset>

        <fieldset><legend>&nbsp;</legend>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </fieldset>

        <?php ActiveForm::end(); } ?>
    </div>
</div>
