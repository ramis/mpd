$(function () {
    //$('.form-control').change(function(){
    //    u = $('.form-control option:selected').val();
    //    $.pjax({url: u, container: '#w0', options: {"push":true,"replace":false,"timeout":1000,"scrollTo":false}});
    //});

    $('#link_ok').attr('href', 'htt' + 'p:' + '/' + '/' + 'o' + 'k' + '.' + 'r' + 'u' + '/' + 'g' + 'r' + 'o' + 'u' + 'p' + '/' + '5' + '4' + '620148989956');
    $('#link_fb').attr('href', 'http' + 's:/' + '/' + 'w' + 'w' + 'w' + '.fa' + 'ceb' + 'ook.' + 'com' + '/groups/' + '139647466376324/');
    $('#link_tw').attr('href', 'h' + 'tt' + 'ps:/' + '/tw' + 'itt' + 'er.co' + 'm/Mi' + 'sterPl' + 'ayDoh');
    $('#link_vk').attr('href', 'htt' + 'ps:' + '/' + '/v' + 'k.' + 'com/' + 'mrp' + 'la' + 'ydohru');

    var shippingSumm = 0;
    $.mask.definitions["h"] = "[3,4,8,9]";
    $('#order-phone').mask("+7 (h99) 999-99-99");
    $('#buy_1_click').mask("+7 (h99) 999-99-99");

    $(".add_product_cart").click(function () {
        product_id = parseInt($(this).attr('product_id'));
        if (product_id == 0) {
            return;
        }
        id = $(this).attr('id');
        $.ajax({
            url: '/json/add/',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=1',
            dataType: 'json',
            success: function (json) {
                if (json['total']) {
                    $('#cart_product').html(json['cart_text']);
                    $('#cart_button').removeClass('hidden');

                    $('#arrow_box_' + product_id).html('Товар добавлен в <a href="/cart/">корзину</a>');
                    $('#arrow_box_' + product_id).fadeIn(100);
                    setTimeout(function () {
                        $('#arrow_box_' + product_id).fadeOut(500);
                    }, 5000);
                    $('#' + id).removeClass('add_product_cart');
                    $('#' + id).html('В корзине');
                    $('#' + id).addClass('product_to_cart');
                    $('#' + id).attr('product_id', 0);
                    $(".product_to_cart").click(function (e) {
                        window.location.href = '/cart/';
                        e.stopPropagation();
                        return false;
                    });

                    ga('send', 'event', 'CART', 'ADD');
                }
            }
        });
    });

    $(".send_email_account").click(function () {
        id = $(this).attr('id');
        $('#feedback_error').css('display', 'none');
        $.ajax({
            url: '/json/feedback/',
            type: 'post',
            data: {
                feedback_title: $('#feedback_title').val(),
                feedback_email: $('#feedback_email').val(),
                description: $("textarea[name='feedback_description']").val()
            },
            dataType: 'json',
            success: function (json) {
                if (json['error']) {
                    $('#feedback_error').html(json['error']).fadeIn(100);
                } else {
                    $('.modal-header').css('padding-bottom', '0px');
                    $('#myModalLabel').html('Вопрос отправлен');
                    $('#modal_body').css('padding', '0px 15px').html('<h4>Спасибо за Ваш вопрос.<br/>Наш менеджер ответить на него в ближайщее время. Ответ поступит на указанную Вами почту.</h4>');
                }
            }
        });
    });

    $(".send_product_review").click(function () {
        $('#review_error').css('display', 'none');
        $.ajax({
            url: '/json/review/',
            type: 'post',
            dataType: 'json',
            data: {
                name: $('#review_name').val(),
                email: $('#review_email').val(),
                review: $("textarea[name='review_text']").val(),
                product_id: $(this).attr('id'),
                rating: $( "input:checked" ).val()
            },
            success: function (json) {
                if (json['error']) {
                    $('#review_error').html(json['error']).fadeIn(100);
                } else {
                    $('.modal-header').css('padding-bottom', '0px');
                    $('#modalReviewBody').css('width', '400px');
                    $('#modal_review_body').css('padding', '0px 15px').html('<h4>Спасибо за Ваш отзыв.<br/><small>Он будет опубликован после проверки модератором.</small></h4>');
                    $('#myModalLabelReview').html('Отзыв отправлен!');
                }
            }
        });
    });

    $(".product_to_cart").click(function (e) {
        window.location.href = '/cart/';
        e.stopPropagation();
        return false;
    });

    $(".product_images li").click(function () {

        $('#product_big_img').css('display', 'none');
        $('.container_video').css('display', 'none');
        $(".product_images li").each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        if ($(this).hasClass('prev_video')) {
            id = $(this).attr('id');
            $('#container_' + id).css('display', 'block');
        } else {
            img = $(this).children('img').attr('src');


            re = /p\/50x50/;
            newimg = img.replace(re, "images");
            //newimg = img.replace(re, "p\/420x360");
            re = /i\./;
            newimg = newimg.replace(re, "");

            $('#product_big_img').attr('src', newimg).css('display', 'block');
        }
    });

    $("#cart_body .del").click(function () {
        product_id = $(this).parent().parent().attr('product_id');
        cartAddDelProduct(product_id, 'del', 1);
    });

    $("#cart_body .remove").click(function () {
        product_id = $(this).parent().parent().attr('product_id');
        cartAddDelProduct(product_id, 'del', 1000);
    });

    $("#cart_body .add").click(function () {
        product_id = $(this).parent().parent().attr('product_id');
        productQuantity = $('#cart_product_' + product_id + ' .product_quantity').val();
        productQuantityCart = $('#cart_product_' + product_id + ' .cart_product_quantity').val();
        productQuantityCart = parseInt(productQuantityCart) + 1;
        if (productQuantityCart > productQuantity) {
            alert('Данного товара в наличии только ' + productQuantity + " шт.");
        } else {
            cartAddDelProduct(product_id, 'add', 1);
        }
    });

    $('.order_shipping_select').click(function () {
        $('.order_shipping_info').each(function () {
            $(this).css('display', 'none');
        });
        id = $(this).attr('id');
        val = $(this).val();
        $('#info-' + id).css('display', 'block');
        //Обновляем цену
        totalSumm = parseInt($('#total_summ_only').html()) - shippingSumm;
        shippingSumm = parseInt($('#' + val + '_cost').html());
        $('#total_summ_only').html(totalSumm + shippingSumm);
    });

    $('#go_order').click(function () {
        $('#w1 li').each(function () {
            $(this).toggleClass('active');
        });
        $('#w1-tab0').removeClass('active');
        $('#w1-tab1').addClass('active');
        ga('send', 'event', 'CART', 'PRE');
    });

    $(".select_geo").click(function () {
        geo_id = $(this).attr('geo_id');
        $.ajax({
            url: '/json/geo/',
            type: 'post',
            data: 'geo_id=' + geo_id,
            dataType: 'json',
            success: function (data) {
                //Скрываем все доставки
                $('.order_shipping_info').each(function () {
                    $(this).css('display', 'none');
                });
                $('.shipping-radio').each(function () {
                    $(this).css('display', 'none').prop("checked", false);
                });
                $('.order-shipping').each(function () {
                    $(this).prop("checked", false);

                });
                $('#container_shipping_pickup').html('');

                //Показываем только нужные
                for (i = 0; i < data['shippingKeys'].length; i++) {
                    shippingId = data['shippingKeys'][i]['id'];
                    shippingKey = data['shippingKeys'][i]['key'];

                    $('#shipping-' + shippingKey).css('display', 'block');
                    $('#order_shipping_geo').html(data['shippingKeys'][i]['geo']);

                    if (shippingId == 2) {
                        for (j = 0; j < data['shippingValue'][shippingId].length; j++) {
                            value = data['shippingValue'][shippingId][j];
                            $('#container_shipping_pickup').append('<div class="form-group required">' +
                                '<div class="col-md-1">' +
                                '<input type="radio" id="order-shipping-' + j + '" value="' + value["id"] +
                                '" name="Order[pickup][id]"></div><div class="col-md-7">' +
                                '<label class="control-label" for="order-shipping-' + j + '">' +
                                value["address"] + '</label></div></div><div class="clear"></div>');
                        }
                        $('#' + shippingKey + '_cost').html(data['shippingKeys'][i]['price']);
                    } else {
                        $('#' + shippingKey + '_cost').html(data['shippingKeys'][i]['price']);
                    }
                }
                $('.close').trigger('click');
                //Обновляем цену
                //reloadTotalSumm();
            }
        });
    });
    $('#button_1_click').click(function (e) {
        phone = $('#buy_1_click').val();
        product_id = $('#product_id').val();
        $.ajax({
            url: '/json/buy/',
            type: 'post',
            data: 'quantity=1&product_id=' + product_id + '&phone=' + phone.replace(/\D+/g, ''),
            dataType: 'json',
            success: function (data) {
                if (data['order_id']) {
                    $('.buy_1_click_form').html('');
                    $('.buy_1_click_form').html('<h4>Спасибо за заказ!</h4>Ваша заявка №' + data['order_id'] + ' принята.');


                    var gaParams = {
                        id: data['order_id'],
                        affiliation: "MrPlayDoh",
                        revenue: data['total'],
                        shipping: data['shipping_cost'],
                        tax: "0"
                    };
                    ga("require", "ecommerce");
                    ga("ecommerce:addTransaction", gaParams);

                    var goods = [];
                    for (i = 0; i < data['products'].length; i++) {
                        product = data['products'][i];
                        ga('ecommerce:addItem', {
                            id: product['id'],
                            name: product['title'],
                            sku: product['article'],
                            category: product['category'],
                            price: product['price'],
                            quantity: product['quantity']
                        });

                        goods.push({
                            id: product['id'],
                            name: product['title'],
                            model: product['article'],
                            category: product['category'],
                            price: product['price'],
                            quantity: product['quantity']
                        });
                    }
                    ga("ecommerce:send");
                    ga("send", "event", "CART", "ORDER");
                    var yaParams = {
                        order_id: data['order_id'],
                        order_price: data['total'],
                        currency: "RUR",
                        exchange_rate: 1,
                        goods: goods
                    };
                    console.log(yaParams);
                    yaCounter31598273.reachGoal("ORDER", yaParams);
                }
                return false;
            }
        });
        e.stopPropagation();
        return false;
    });


    $("#text_search").keyup(function (I) {
        switch (I.keyCode) {
            case 13:  // enter
                href = $('#search_wrapper_container .active a').attr('href');
                console.log(href);
                if (href != undefined) {
                    window.location.href = href;
                    return false;
                } else {
                    window.location.href = '/search/?_search=' + $('#text_search').val();
                }
                break;
            case 27:  // escape
            case 38:  // стрелка вверх
            case 40:  // стрелка вниз
                break;
            default:
                clearTimeout(timerId);

                if ($(this).val().length > 2) {
                    timerId = setTimeout(function (search) {
                        inputInitialValue = search;

                        $.ajax({
                            url: '/json/search/',
                            type: 'post',
                            data: 'query=' + search,
                            dataType: 'json',
                            success: function (data) {
                                //var list = eval("(" + data + ")");
                                suggestCount = data.length;
                                if (suggestCount > 0) {
                                    $("#search_wrapper_container").html("").show();
                                    suggestCount = suggestCount > 5 ? 5 : suggestCount;
                                    for (i = 0; i < suggestCount; i++) {
                                        item = data[i];
                                        div = '<div class="search_wrapper_product">' + item.img +
                                            '<span class="search_wrapper_title"><a href="' + item.url + '">' + item.title + '</a></span></div>';
                                        $('#search_wrapper_container').append(div);
                                    }
                                }
                            }
                        });
                    }, 200, $(this).val());
                }
                break;
        }
    });

    $("#text_search").keydown(function (I) {
        switch (I.keyCode) {
            case 13:
                href = $('#search_wrapper_container .active a').attr('href');
                if (href != undefined) {
                    window.location.href = href;
                    return false;
                } else {
                    console.log('qqqqq');
                    $('#search').on("submit");
                }
                break;
            case 27: // escape
                $('#search_wrapper_container').hide();
                return false;
                break;
            case 38: // стрелка вверх
            case 40: // стрелка вниз
                I.preventDefault();
                if (suggestCount) {
                    keyActivate(I.keyCode - 39);
                }
                break;
        }
    });

    $(document).on("click", ".search_wrapper_product", function () {
        $('#search').val($(this).text());
        $('#search_wrapper_container').fadeOut(100).html('');
    });

    $('html').click(function () {
        $('#search_wrapper_container').hide();
    });

    $('#search').click(function (event) {
        if (suggestCount) {
            $('#search_wrapper_container').show();
        }
        event.stopPropagation();
    });

});

cartAddDelProduct = function (product_id, type, quantity) {
    $.ajax({
        url: '/json/' + type + '/',
        type: 'post',
        data: 'product_id=' + product_id + '&quantity=' + quantity,
        dataType: 'json',
        success: function (data) {

            if (data['total']) {
                total = data['total'];
                if (total == 0) {
                    window.location.href = window.location.href;
                } else {
                    $('#cart_product').html(data['cart_text']);

                    if (data['product_not_in_stock'] == 0) {
                        $('#cart_alert').remove();
                        $('.quantity_in_stock').remove();
                    }
                    $('#total_summ_only').html(total);
                    $('#total').html(total);


                    if (data['product_quantity'] == 0) {
                        $('#cart_product_' + product_id).remove();
                    } else {

                        $('#cart_product_' + product_id + ' .total_product_price').html(data['total_product_price']);
                        $('#cart_product_' + product_id + ' .cart_product_quantity').val(data['product_quantity']);
                    }
                }
            } else {
                window.location.href = window.location.href;
            }
        }
    });
};

var suggestSelected = 0;
var suggestCount = 0;
var inputInitialValue = '';
var timerId = null;

keyActivate = function (n) {

    $('#search_wrapper_container div').removeClass('active');

    if (n == 1 && suggestSelected < suggestCount) {
        suggestSelected++;
    } else if (n == -1 && suggestSelected > 0) {
        suggestSelected--;
    }

    if (suggestSelected > 0) {
        $('#search_wrapper_container div').eq(suggestSelected - 1).addClass('active');
    } else {
        $("#text_search").val(inputInitialValue);
    }
};


(function () {

    var stSv = function() {

        var hash = window.location.hash || false;

        if (!'MutationObserver' in window) {
            return false;
        }

        var stSvObserver = new MutationObserver(function(allmutations) {
            allmutations.map(function(mr) {
                var m = mr.addedNodes[0];
                var found = false;
                var check = {
                    'z-index': '2147483647',
                    //'background-color': 'rgb(250, 223, 117)',
                    'display': 'table',
                    'position': 'fixed'
                };

                if (m && m.tagName === 'DIV') {
                    for (var c in check) {
                        if (window.getComputedStyle(m).getPropertyValue(c) === check[c]) {
                            found = true;
                        } else {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        m.style.cssText = 'display: none !important; opacity: 0 !important;';
                    document.documentElement.style.marginTop='0';
                        stSvObserver.disconnect();

                    }
                }
            });
        });

        var marginAnimationObserver = new MutationObserver(function() {
            var mt = document.documentElement.style.marginTop;
            if (mt && parseInt(mt) > 0) {
                document.documentElement.style.marginTop = '';
            }
        });

        var runObserver = function () {
            if (!document.body) {
                setTimeout(runObserver, 100);
                return;
            }

            if (stSvObserver) {
                stSvObserver.observe(document.body, {
                    'childList': true,
                    'attributes': true,
                    'attributeFilter': ['style']
                });

                marginAnimationObserver.observe(document.documentElement, {
                    'attributes': true,
                    'attributeFilter': ['style']
                });
            }
        }
        if (!(hash && hash === '#ssoff')) {
            runObserver();
        }
    };
    stSv();
})();