<?php
use yii\grid\GridView;
use common\models\catalog\Product;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
$this->title = 'Товары';
?>

<div class="site-index">
    <div class="right"><?= Html::a('Добавить', \yii\helpers\Url::to(['create'])); ?></div>
    <h1>Список товаров</h1>

    <?php

    $i = 0;
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function (Product $product) {
            if ($product->getDescription()) {
                return ['class' => 'description'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Id',
                'format' => 'raw',
                'value' => function (Product $product) {
                    $id = $product->getQuantity() > 0 ? $product->getId() : '<span style="color: #FF0000">' . $product->getId() . '</span>';

                    $href = Yii::$app->params['HTTP_SITE'] . 'product/' . $product->getId() . '/';

                    return '<a name="' . $product->getId() . '" href="' . $href . '" target="_blank">' . $id . '</a>';
                }
            ],
            [
                'attribute' => 'Foto',
                'format' => 'raw',
                'value' => function (Product $product) {
                    $image = $product->getMainImage();
                    $img = '';
                    if ($image) {
                        $img = $image->getHtml(['width' => 100, 'height' => 100]);
                    }

                    return $img;
                }
            ],
            [
                'attribute' => 'Title',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return ($product->getTitle() ? $product->getTitle() : $product->getTitle2());
                }
            ],
            [
                'attribute' => 'Article',
                'format' => 'raw',
                'value' => function (Product $product) {
                    $article = $product->isPublished() ? $product->getArticle() : '<span style="color: #FF0000">' . $product->getArticle() . '</span>';

                    return $article;
                }
            ],
            [
                'attribute' => 'V3Toys Id',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return Html::a($product->getCode(), 'http://v3toys.ru/index.php?nid=' . $product->getCode(), ['target' => "_blank"]);
                }
            ],
            [
                'attribute' => 'Price',
                'value' => function (Product $product) {
                    return (int)$product->getPrice();
                }
            ],
            [
                'attribute' => 'Quantity',
                'value' => function (Product $product) {
                    return $product->getQuantity();
                }
            ],
            [
                'attribute' => 'Cnt',
                'value' => function (Product $product) {
                    return mb_strlen($product->getDescription(), 'UTF-8');
                }
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'value' => function (Product $product) {
                    return Html::a('Редактировать', ['edit', 'id' => $product->getId()]);
                }
            ],
        ],
    ]);
    ?>
</div>
