<?php

use yii\db\Schema;
use yii\db\Migration;

class m160227_053154_crontab_stat extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%crontab_stat}}', [
            'id' => Schema::TYPE_PK,
            'command' => Schema::TYPE_STRING . ' NOT NULL UNIQUE CHECK (command != \'\')',
            'title' => Schema::TYPE_STRING . ' NOT NULL CHECK (title != \'\')',
            'created_ts' => Schema::TYPE_TIMESTAMP . ' WITH TIME ZONE NOT NULL',
            'updated_ts' => Schema::TYPE_TIMESTAMP . ' WITH TIME ZONE NOT NULL',
        ]);
    }

    public function safeDown()
    {

    }
}
