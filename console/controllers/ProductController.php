<?php
namespace console\controllers;

use common\models\catalog\Category;
use common\models\catalog\product\AttributeValue;
use common\models\catalog\product\V3toysProduct as ProductV3ToysProduct;
use common\models\catalog\product\Tag as ProductTag;
use common\models\catalog\Tag;
use common\tools\Api;
use Yii;
use yii\base\Exception;
use yii\console\Controller;
use common\models\catalog\Product;
use yii\db\Query;
use yii\helpers\Html;
use common\models\v3toys\Product as V3ToysProduct;


class ProductController extends Controller
{

    private static $_urlXml = "http://www.v3toys.ru/prods.yml";
    private static $_urlXmlMarket = "http://www.v3toys.ru/markt.yml";

    /**
     * Обновление цен с V3Toys
     */
    public function actionUpdatePrice()
    {
        $products = Product::find()->filterWhere(['>', 'code', '0'])->all();
        $productRows = [];
        foreach ($products as $product) {
            $productRows[$product->getCode()] = $product;
        }

        $parser = new SimpleDMOZParser($productRows, self::$_urlXml);
        $parser->parse();

        foreach ($parser->getProducts() as $product) {
            $product->save();
        }
    }

    /**
     * Обновление цен из таблицы v3toys_product в каталог
     */
    public function actionUpdatePriceV3ToysProduct()
    {
        $products = Product::find()->all();
        foreach($products as $product) {
            try {
                /** @var Product $product */
                $v3toysProducts = V3ToysProduct::find()
                    ->from ([V3ToysProduct::tableName().' v'])
                    ->innerJoin(ProductV3ToysProduct::tableName().' pv','pv.v3toys_product_id = v.v3toys_product_id')
                    ->where(['pv.catalog_product_id' => $product->getId()])
                    ->orderBy(['v.is_assortiment' => 'desc'])->all();

                /** @var V3ToysProduct $tmpV3ToysProduct */
                $tmpV3ToysProduct = null;
                foreach ($v3toysProducts as $item) {
                    if ($item->getQuantity() > 0) {
                        $tmpV3ToysProduct = $item;
                        break;
                    }
                }

                $product->setQuantity(0);
                $product->setStock(false);
                if ($tmpV3ToysProduct) {
                    $product->setQuantity($tmpV3ToysProduct->getQuantity());
                    $product->setStock(true);
                    $product->setBuyPrice($tmpV3ToysProduct->getBuyPrice());
                    $product->setBasePrice($tmpV3ToysProduct->getBasePrice());
                    $product->setV3tPrice($tmpV3ToysProduct->getPrice());
                    $product->setMrcPrice($tmpV3ToysProduct->getMrPrice());
                    $product->setCode($tmpV3ToysProduct->getV3ToysProductId());
                }
                if ($product->save()) {
                    $product->calcPrice();
                } else {
                    throw new \Exception(json_encode($product->getFirstErrors()));
                }
            }catch(\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }


    /**
     * Обновление цен и количества по Api в таблицу v3toys_product
     */
    public function actionUpdateV3ToysProductAllApi()
    {
        $limit = 1000;
        $offset = 100000;
        $spliter1 = '-----------------------------------------------------------------------------------' . "\n";
        do {
            $cnt = 0;

            try {
                $response = Api::send('getProductsDataByIds', ['in_stock' => 0, 'full' => 1, 'limit' => $limit, 'offset' => $offset, 'products_ids' => 'all']);
                if (!empty($response['data'])) {
                    foreach ($response['data'] as $item) {
                        $cnt++;
                        try {
                            /**
                             * @var V3ToysProduct $v3toysProduct
                             */
                            $v3toysProduct = V3ToysProduct::find()->where(['v3toys_product_id' => (int)$item['id']])->one();
                            if ($v3toysProduct === null) {
                                $v3toysProduct = new V3ToysProduct();
                                $v3toysProduct->setAssortiment(false);
                            }

                            $quantity = (int)$item['quantity'];
                            $v3toysProduct->setQuantity(($quantity < 0 ? 0 : $quantity));
                            $v3toysProduct->setTitleUnit($item['title']);

                            $v3toysProduct->setV3toysProductId((int)$item['id']);
                            $v3toysProduct->setBuyPrice((int)$item['buy_price']);
                            $v3toysProduct->setBasePrice((int)$item['base_price']);
                            $v3toysProduct->setPrice((int)$item['price']);
                            $v3toysProduct->setMrPrice((int)$item['mr_price']);

                            if (!empty($item['barcode'])) {
                                $v3toysProduct->setBarcode($item['barcode']);
                            }
                            if (!empty($item['sku'])) {
                                $v3toysProduct->setSku($item['sku']);
                            }
                            if (!empty($item['brand'])) {
                                $v3toysProduct->setBrand($item['brand']);
                            }
                            $v3toysProduct->setDeleted((bool)$item['deleted']);

                            if ($v3toysProduct->save() === false) {
                                throw new Exception(json_encode($v3toysProduct->getFirstErrors()));
                            };
                        } catch (\Exception $e) {
                            continue;
                        }
                    }
                }
            } catch (\Exception $e) {
                $cnt = 0;
            }

            $offset = $offset === 0 ? $limit : ($offset + $limit - 1);
//            print '$offset - ' . $offset . "\n";
//            print '$limit - ' . $limit . "\n";
//            print '$cnt - ' . $cnt . "\n";
//            print $spliter1;
        } while ($cnt > 0);

    }

    /**
     * Обновление данных с V3Toys для v3toys_product
     */
    public function actionUpdateV3ToysProductXml()
    {
        $parser = new simpleXmlParserV3Toys(self::$_urlXmlMarket);
        $parser->parse();
    }

    /**
     * Обновление цен и количества по Api в таблицу v3toys_product только тех которые есть в каталоге
     */
    public function actionUpdateV3ToysProductCatalogApi()
    {
        $v3toysProducts = V3ToysProduct::find()->filterWhere(['>', 'catalog_product_id', '0'])->all();

        $productsIds = [];
        foreach ($v3toysProducts as $product) {
            $productsIds[] = $product->getV3toysProductId();
        }

        try {
            $response = Api::send('getProductsDataByIds', ['in_stock' => 0, 'full' => 1, 'products_ids' => implode(',', $productsIds)]);
            if (!empty($response['data'])) {
                foreach ($response['data'] as $item) {
                    try {
                        /**
                         * @var V3ToysProduct $v3toysProduct
                         */
                        $v3toysProduct = V3ToysProduct::find()->where(['v3toys_product_id' => (int)$item['id']])->one();
                        if ($v3toysProduct === null) {
                            continue;
                        }

                        $quantity = (int)$item['quantity'];
                        $v3toysProduct->setQuantity(($quantity < 0 ? 0 : $quantity));
                        $v3toysProduct->setTitleUnit($item['title']);

                        $v3toysProduct->setV3toysProductId((int)$item['id']);
                        $v3toysProduct->setBuyPrice((int)$item['buy_price']);
                        $v3toysProduct->setBasePrice((int)$item['base_price']);
                        $v3toysProduct->setPrice((int)$item['price']);
                        $v3toysProduct->setMrPrice((int)$item['mr_price']);

                        if (!empty($item['barcode'])) {
                            $v3toysProduct->setBarcode($item['barcode']);
                        }
                        if (!empty($item['sku'])) {
                            $v3toysProduct->setSku($item['sku']);
                        }
                        if (!empty($item['brand'])) {
                            $v3toysProduct->setBrand($item['brand']);
                        }
                        $v3toysProduct->setDeleted((bool)$item['deleted']);

                        if ($v3toysProduct->save() === false) {
                            throw new Exception(json_encode($v3toysProduct->getFirstErrors()));
                        };
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        } catch (\Exception $e) {

        }
    }

    /**
     * Обновление цен и количества по Api
     */
    public function actionUpdatePriceApi()
    {
        $products = Product::find()->filterWhere(['>', 'code', '0'])->all();

        $productsIds = [];
        $products2Id = [];
        foreach ($products as $product) {
            $productsIds[] = $product->getCode();
            $products2Id[$product->getCode()] = $product;
        }

        try {
            $response = Api::send('getProductsDataByIds', ['in_stock' => 0, 'full' => 1, 'products_ids' => implode(',', $productsIds)]);
            if (!empty($response['data'])) {
                foreach ($response['data'] as $item) {
                    try {
                        $product = $products2Id[$item['id']];
                        /**
                         * @var Product $product
                         */
                        $quantity = (int)$item['quantity'];
                        $product->setQuantity(($quantity < 0 ? 0 : $quantity));
                        $product->setStock(($quantity > 0));
                        $product->setBuyPrice((int)$item['buy_price']);
                        $product->setBasePrice((int)$item['base_price']);
                        if (!empty($item['barcode'])) {
                            $product->setBarcode((int)$item['barcode']);
                        }
                        if ($product->save() === false) {
                            throw new Exception($product->getId() . ' ' . json_encode($product->getFirstErrors()));
                        };
                        $product->calcPrice();
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        } catch (\Exception $e) {
//            var_dump($e->getMessage());
        }
    }

    public function actionTestApi()
    {
        $response = Api::send('getProductsDataByIds', ['full' => 1, 'products_ids' => '32504']);
        var_dump($response);
    }

    /**
     * Создания связок Товар - Тег
     */
    public function actionTags()
    {
        $tags = Tag::find()->all();

        try {

//            ProductTag::deleteAll();

            foreach ($tags as $tag) {

                $values = [];

                $products = Product::find()->where(['is_published' => true])->andFilterWhere(['ilike', 'title', $tag->getTag()])->all();
                foreach ($products as $product) {
                    $values[$product->getId()] = [$product->getId(), $tag->getId()];
                }

                $products = Product::find()->where(['is_published' => true])->andFilterWhere(['ilike', 'title2', $tag->getTag()])->all();
                foreach ($products as $product) {
                    $values[$product->getId()] = [$product->getId(), $tag->getId()];
                }

                $products = Product::find()->where(['is_published' => true])->andFilterWhere(['ilike', 'description', $tag->getTag()])->all();

                foreach ($products as $product) {
                    $values[$product->getId()] = [$product->getId(), $tag->getId()];
                }

                $attributeValues = AttributeValue::find()->andFilterWhere(['ilike', 'value', $tag->getTag()])->all();
                foreach ($attributeValues as $value) {
                    if ($value->getCatalogProduct()->isPublished()) {
                        $values[$value->getCatalogProductId()] = [$value->getCatalogProductId(), $tag->getId()];
                    }
                }

                $values = array_values($values);

                if (!empty($values)) {
                    ProductTag::getDb()->createCommand()->batchInsert(ProductTag::tableName(), ['catalog_product_id', 'catalog_tag_id'], $values)->execute();
                }

                var_dump($tag->getTag() . ' - ' . count($values));
            }

        } catch (\Exception $e) {

        }
    }

    public function actionText()
    {
        $template = '<p>(На текущей странице|На данной странице|В рамках данной страницы|На этой странице) (составлен|сформирован|подобран|представлен|собран) (набор|список|ассортимент) (похожих|схожих|сходных) (товаров|игрушке) из (раздела|категории) «title». (Эти|Данные) (товары|игрушки) (подобраны|объединены|выбраны) по (критериям|параметрам|формату) пластилин Плей До и "tag", (в которых|где) (доступно|находятся|можно найти|собрано|добавлены) все (нужное|необходимое|обязательное) для (занимательного|интересного|привлекательного) и (захватывающего|любопытного|увлекательного) (креативного|творческого) (процесса|занятия).</p>
        <p>Основу всех (товаров|игрушек|наборов) (составляет|представляет) (популярный|знаменитый|известный|широко известный|легендарный) пластилин Play-Doh от Hasbro, который (практически|фактически|на самом деле|в действительности) не имеет аналогов в мире. (Эластичный|Мягкий), приятный в (ходе|процессе|течение) (лепки|вылепливания), (созданный|сделанный|изготовленный|произведенный|приготовленный) из (естественных|натуральных) и (безопасных|безвредных|невредных) (ингредиентов|компонентов) и рассчитанный на (многоразовое использование|многоразовое применение|не однократное использование|не однократное применение|значительное количество применений|значительное количество употреблений|значительное количество использований|большое количество применений|большое количество употреблений|большое количество использований).</p>
        <p>Он (великолепно|прекрасно|блестяще|ярко) (проявил|зарекомендовал|продемонстрировал) себя (во всех отношениях|в лучших качествах|в высококачественных свойствах), (точно|буквально|прямо) переводя (занятия лепкой|игру с пластилином|лепку фигурок и игрушек) на (целиком и полностью|совершенно|кардинально) новый (высококачественный|качественный|первоклассный) уровень. </p>
        <p>(Напоминаем|Хочется подчеркнуть|Важно подчеркнуть), что купить (товары|игрушки) из (раздела|категории) «title» (можно|вы можете) как на данной странице, так и (перейдя|переходя) на (страницу|карточку|витрину) (товара|игрушки), (на которой|где) вы (можете получить больше информации|можете подробнее узнать о продукте|можете составить представление о продукте) прочитав (обзор|описания) товара, (пролистать|просмотреть) большие и красивые (фотографии|картинки), а также (описаны|добавлены) (подробные|полные|детальные) технические характеристики.</p>
        <p>(Если вам необходима дополнительная информация|Если вам необходима дополнительная консультация|Если у вас возникают вопросы|Если вам требуется дополнительная консультация|В случае возникновения вопросов), <a href="mailto:info@mrplaydoh.ru">пишите нам</a> или звоните по телефону: +7 (495) 278-08-20</p>
        <p>* Обращаем ваше внимание, что цены на (игрушки|товары|наборы) (на текущей странице|на данной странице|в рамках данной страницы|на этой странице) «title» не включают доставку в ваш регион. Полная стоимость заказа будет автоматически пересчитана на странице оформления заказа.</p>
        <p>Рекомендуем обратить внимание на другие (игрушки|товары) из (категории|раздела) «rand».</p>';

        $categories = Category::find()->where(['is_special' => false, 'is_published' => true])->andFilterWhere(['<>', 'url', 'all'])->orderBy('sort asc')->all();
        $cnt = count($categories) - 1;
        $tags = Tag::find()->where(['seo_text' => ''])->all();
        foreach ($tags as $tag) {
            $rnd = rand(0, $cnt);
            $category = $categories[$rnd];

            $string = preg_replace_callback("#\((.+?)\)+?#", function ($matches) {
                $tmp = explode('|', $matches[1]);
                shuffle($tmp);
                return isset($tmp[0]) ? $tmp[0] : '';
            }, $template);
            $string = preg_replace("#title#", $tag->getSeoTitle(), $string);
            $string = preg_replace("#tag#", $tag->getTitle(), $string);
            $string = preg_replace("#rand#", Html::a($category->getTitle(), '/' . $category->getUrl() . '/'), $string);

            $tag->setSeoText($string);
            $tag->save();
        }

    }
}

class SimpleDMOZParser
{
    private $_stack = [];
    private $_file = "";
    private $_parser = null;

    private $_tag_name = '';
    private $_is_offer = false;
    private $products = [];

    public function __construct(array $products, $file)
    {
        $this->_file = $file;
        $this->products = $products;

        $this->_parser = xml_parser_create("UTF-8");
        xml_set_object($this->_parser, $this);
        xml_set_element_handler($this->_parser, "startTag", "endTag");
        xml_set_character_data_handler($this->_parser, "tagData");
    }

    public function startTag($parser, $name, $attribs)
    {
        if ($name === 'OFFER' && isset($attribs['ID']) && $id = (int)$attribs['ID']) {
            if (isset($this->products[$id])) {
                $this->_is_offer = true;
                $this->_stack = [];
            }
        }

        if ($this->_is_offer) {
            $this->_tag_name = $name;
            foreach ($attribs as $key => $val) {
                if (isset($this->_stack[$this->_tag_name])) {
                    $this->_tag_name = $val;
                }
                $this->_stack[$this->_tag_name][$key] = $val;
            }
        }
    }

    public function endTag($parser, $name)
    {
        if ($this->_is_offer && $name === 'OFFER') {
            $this->_is_offer = false;
            $product = $this->products[$this->_stack['OFFER']['ID']];
            $product->setV3tPrice((isset($this->_stack['PRICE']['tagData']) ? $this->_stack['PRICE']['tagData'] : 0));
        }
    }

    function tagData($parser, $tagData)
    {
        if (trim($tagData)) {
            if ($this->_is_offer) {
                $this->_stack[$this->_tag_name]['tagData'] = $tagData;
            }
        }
    }

    public function parse()
    {
        $fh = fopen($this->_file, "r");
        if (!$fh) {
            die("Epic fail!\n");
        }

        while (!feof($fh)) {
            $data = fread($fh, 4096);
            xml_parse($this->_parser, $data, feof($fh));
        }
    }

    public function getProducts()
    {
        return $this->products;
    }
}

class simpleXmlParserV3Toys
{
    private $_stack = [];
    private $_file = "";
    private $_parser = null;

    private $_tag_name = '';
    private $_is_offer = false;
    private $_is_category = false;
    private $categories = [];

    public function __construct($file)
    {
        $this->_file = $file;

        $this->_parser = xml_parser_create("UTF-8");
        xml_set_object($this->_parser, $this);
        xml_set_element_handler($this->_parser, "startTag", "endTag");
        xml_set_character_data_handler($this->_parser, "tagData");
    }

    public function startTag($parser, $name, $attribs)
    {
        if ($name === 'OFFER' && isset($attribs['ID'])) {
            $this->_is_offer = true;
            $this->_stack = [];
        }

        if ($name === 'CATEGORY' && isset($attribs['ID'])) {
            $this->_is_category = true;
            $this->_stack = [];
        }

        if ($this->_is_category) {
            $this->_tag_name = $name;
            foreach ($attribs as $key => $val) {
                $this->_stack[$this->_tag_name][$key] = $val;
            }
        }

        if ($this->_is_offer) {
            $this->_tag_name = $name;
            foreach ($attribs as $key => $val) {
                $this->_stack[$this->_tag_name][$key] = $val;
            }
        }
    }

    public function endTag($parser, $name)
    {
        if ($this->_is_offer && $name === 'OFFER') {
            $this->_is_offer = false;
            $categoryId = (isset($this->_stack['CATEGORYID']) ? (int)$this->_stack['CATEGORYID']['tagData'] : 0);

            $v3toysProduct = V3ToysProduct::find()->where(['v3toys_product_id' => (int)$this->_stack['OFFER']['ID']])->one();
            if($v3toysProduct) {
                $v3toysProduct->setTitle((string)$this->_stack['NAME']['tagData']);
                $v3toysProduct->setImg((string)$this->_stack['PICTURE']['tagData']);
                $v3toysProduct->setCategory((!empty($this->categories[$categoryId]) ? $this->categories[$categoryId] : ''));
                $v3toysProduct->save();
            }
        }

        if ($this->_is_category && $name === 'CATEGORY') {
            $this->_is_category = false;
            $id = (int)$this->_stack['CATEGORY']['ID'];
            $this->categories[$id] = (isset($this->_stack['CATEGORY']) ? $this->_stack['CATEGORY']['tagData'] : '');
        }
    }

    function tagData($parser, $tagData)
    {
        if (trim($tagData)) {
            if ($this->_is_offer) {
                $this->_stack[$this->_tag_name]['tagData'] = $tagData;
            }
            if ($this->_is_category) {
                $this->_stack[$this->_tag_name]['tagData'] = $tagData;
            }
        }
    }

    public function parse()
    {
        $fh = fopen($this->_file, "r");
        if (!$fh) {
            die("Epic fail!\n");
        }

        while (!feof($fh)) {
            $data = fread($fh, 4096);
            xml_parse($this->_parser, $data, feof($fh));
        }
    }

}
