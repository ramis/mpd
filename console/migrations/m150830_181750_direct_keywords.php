<?php

use yii\db\Schema;
use yii\db\Migration;

class m150830_181750_direct_keywords extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%direct_keywords}}', [
            'id' => Schema::TYPE_PK,

            'direct_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'adgroup_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'keyword' => Schema::TYPE_STRING  . ' NOT NULL',
            'userparam1' => Schema::TYPE_STRING  . ' NOT NULL',
            'userparam2' => Schema::TYPE_STRING  . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_direct_keywords_adgroup_id', '{{%direct_keywords}}', 'adgroup_id', '{{%direct_adgroup}}', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function safeDown()
    {
    }

}
