<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\catalog\Category;
use common\models\catalog\Product;
use frontend\widgets\LeftColumn;
use frontend\widgets\BreadCrumbs;
use frontend\widgets\Paginator;
use frontend\widgets\Sort;
use common\models\catalog\product\Review;

/* @var $this yii\web\View */
/* @var Category $category */
/* @var Product[] $products */
$mainImage = $category->getMainImage();
$metaDescription = Html::encode(mb_substr($category->getDescription(), 0, mb_stripos($category->getDescription(), '.', 0, 'UTF-8'), 'UTF-8'));
Yii::$app->view->registerMetaTag(['name' => 'vk:url', 'content' => Url::to(['route/index', 'url' => $category->getId()], true),]);
Yii::$app->view->registerMetaTag(['name' => 'vk:title', 'content' => $category->getTitle()]);
Yii::$app->view->registerMetaTag(['name' => 'vk:image', 'content' => Yii::$app->params['HTTP_IMAGE'] . $mainImage->getHash()]);
Yii::$app->view->registerMetaTag(['name' => 'vk:description', 'content' => $metaDescription]);
Yii::$app->view->registerMetaTag(['name' => 'og:url', 'content' => Url::to(['route/index', 'url' => $category->getId()], true),]);
Yii::$app->view->registerMetaTag(['name' => 'og:title', 'content' => $category->getTitle()]);
Yii::$app->view->registerMetaTag(['name' => 'og:image', 'content' => Yii::$app->params['HTTP_IMAGE'] . $mainImage->getHash()]);
Yii::$app->view->registerMetaTag(['name' => 'og:description', 'content' => $metaDescription]);
Yii::$app->view->registerMetaTag(['name' => 'og:site_name', 'content' => Yii::$app->params['DOMAIN']]);
Yii::$app->view->registerMetaTag(['name' => 'og:type', 'content' => 'website']);

Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $category->getSeoDescription()]);
Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $category->getSeoKeywords()]);

$this->title = $category->getPageTitle();
$this->params['page'] = $category;

if ($category->getUrl() === 'all') {
    $countPage = ceil(Product::find()->where(['is_published' => true])->count() / $limit);
    $products = Product::find()->where(['is_published' => true])->limit($limit)->offset(($page - 1) * $limit)->orderBy(['d_is_stock' => SORT_DESC, $sort => SORT_ASC])->all();

} else {
    $countPage = ceil($category->getProductsCount() / $limit);
    $products = $category->getProducts(['sort' => ['d_is_stock' => SORT_DESC, $sort => SORT_ASC], 'limit' => $limit, 'offset' => ($page - 1) * $limit]);
}

?>

<div id="body">
    <div id="left_col">
        <?= LeftColumn::widget(); ?>
    </div>

    <div id="center_col">

        <?= BreadCrumbs::widget(['breadCrumbs' => $category]); ?>

        <div id="site_index" class="category_index">
            <?php
            if ($mainImage) {
                echo Html::img(Yii::$app->params['HTTP_IMAGE'] . $mainImage->getHash(), ['class' => 'category_img']);
            }

            echo '<h1>' . $category->getTitle() . '</h1>';
            /*       ?>

                   <div id="block_filters">
                       <div id="clear_all">x Сбросить все</div>
                       <ul>
                           <li>
                               Фильтр
                           </li>
                           <li class="filter">
                               Цена
                           </li>
                           <li class="filter active" style="width: 110px">
                               Категория
                               <div class="selected_filter">2 категории</div>
                           </li>
                           <li class="filter">
                               Возраст
                               <div class="select_filters">
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">до 1 года</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 1 года до 2 лет</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 2 лет до 4 лет</label>
                                   </div>
                                   <div class="filter_checked">
                                       <input type="checkbox" value="1" name="filter" id="filter1"/><label for="filter1">от 4 лет</label>
                                   </div>
                                   <div class="filter_btn"><div class="btn">Применить</div></div>
                               </div>
                           </li>
                           <li class="filter">
                               Пол
                           </li>
                       </ul>
                   </div>
           */
            ?>
            <?= Sort::widget(['url' => $_SERVER['REQUEST_URI'], 'limit' => $limit, 'sort' => $sort, 'order' => $order]); ?>

            <div class="clear"></div>

            <div class="product_item_list">
                <?php
                foreach ($products as $product) {
                    echo $this->render('/layouts/productItem', ['product' => $product]);
                }
                ?>
            </div>
            <div class="clear"></div>

            <?= Paginator::widget(['url' => $_SERVER['REQUEST_URI'], 'page' => $page, 'countPage' => $countPage]); ?>
            <?php
            if($page === 1) {
                echo '<h2>' . $category->getTitle2() . '</h2>';
                if ($category->getDescription()) {
                    $description = preg_replace("/\n/", "</p><p>", $category->getDescription());
                    $description = preg_replace('/[\,\.\!\?]/', Html::a('$0', ['route/index', 'url' => $category->getUrl()]), $description, 1);

                    echo '<div class="description"><p>' . $description . '</p>';
                    echo '<a href="https://plus.google.com/u/1/108888585391425984422/posts" rel="author">Mr Play-Doh</a></div>';
                }

                $productReviews = Review::find()
                    ->innerJoin(Product::tableName(), Product::tableName() . '.id=' . Review::tableName() . '.catalog_product_id')
                    ->where(['state' => Review::STATE_PUBLISHED, 'catalog_category_id' => $category->getId(), 'is_published' => true])
                    ->orderBy([Review::tableName() . '.created_uts' => SORT_DESC])
                    ->limit(2)
                    ->all();
                if (!empty($productReviews)) {
                    echo '<h3>Отзывы</h3><div class="reviews_info">';
                    foreach ($productReviews as $review) {
                        echo $this->render('/layouts/productReviewItem', ['review' => $review]);
                    }
                    echo '</div>';
                }
            }
            ?>

        </div>
    </div>
</div>