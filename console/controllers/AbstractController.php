<?php
namespace console\controllers;

use backend\models\CrontabStat;
use Yii;
use yii\console\Controller;
use common\tools\Method;
use yii\db\Exception;


/**
 * Abstract controller
 */
class AbstractController extends Controller
{

    public function afterAction($action, $result)
    {

        $result = parent::afterAction($action, $result);

        $method = Method::getDescription($this->className(), $action->actionMethod);
        $description = empty($method['description']) ? '' : $method['description'];
        $command = $action->controller->module->requestedRoute;

        $crontabStat = CrontabStat::find()->where(['command' => $command])->one();

        if($crontabStat === null) {
            $crontabStat = new CrontabStat();
        }
        $crontabStat
            ->setController($this->className())
            ->setCommand($command)
            ->setTitle($description);

        if($crontabStat->save() === false) {
            throw new Exception (json_encode($crontabStat->getFirstErrors()));
        }

        return $result;
    }
}
