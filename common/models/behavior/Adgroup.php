<?php
namespace common\models\behavior;

use backend\models\direct\Adgroup as ModelAdgroup;

trait Adgroup
{
    /**
     * @var ModelAdgroup
     */
    private $adgroup;
    private $adgroup_id;

    /**
     * @return ModelAdgroup
     */
    public function getAdgroup()
    {
        if ($this->adgroup === null && $this->adgroup_id !== null) {
            $this->adgroup = ModelAdgroup::find()->where(['id' => $this->adgroup_id])->one();
        }

        if ($this->adgroup === null) {
            $this->adgroup = new ModelAdgroup();
        }

        return $this->adgroup;
    }

    /**
     * @param ModelAdgroup $adgroup
     */
    public function setAdgroup(ModelAdgroup $adgroup)
    {
        $this->adgroup = $adgroup;
        $this->adgroup_id = $adgroup->getId();
    }

    /**
     * @param int $adgroup_id
     */
    public function setAdgroupId($adgroup_id)
    {
        $this->adgroup_id = $adgroup_id;
        $this->adgroup = ModelAdgroup::find()->where(['id' => $this->adgroup_id])->one();
    }

}