<?php
namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use common\models\Order;
use backend\search\SearchOrder;

/**
 * Order  controller
 */
class OrderController extends AbstractController
{

    /**
     * Order list
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchOrder();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    /**
     * Order create
     *
     * @return string
     */
    public function actionEdit($id)
    {

        /** @var Order $order */
        $order = Order::findOne((int)$id);

        if ($order === null) {
            return Yii::$app->getResponse()->redirect(Url::to(['order/index']));
        }

        $errors = [];
        if (Yii::$app->request->getIsPost()) {
            $order->load($_POST);

            if ($order->save()) {
                return Yii::$app->getResponse()->redirect(Url::to(['order/index']));
            }
            $errors = $order->getErrors();
        }

        return $this->render('form', ['order' => $order, 'errors' => $errors]);
    }

}
