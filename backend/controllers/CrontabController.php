<?php
namespace backend\controllers;

use backend\models\CrontabStat;
use Yii;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/**
 * Information  controller
 */
class CrontabController extends AbstractController
{

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    private function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => CrontabStat::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    /**
     * Information list
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['dataProvider' => $this->getDataProvider()]);
    }

}
