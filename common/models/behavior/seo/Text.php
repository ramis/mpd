<?php

namespace common\models\behavior\seo;

trait Text
{
    /**
     * @return string
     */
    public function getSeoText()
    {
        return $this->seo_text;
    }

    /**
     * @param string $seo_text
     */
    public function setSeoText($seo_text)
    {
        $this->seo_text = $seo_text;
    }

}