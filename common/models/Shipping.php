<?php

namespace common\models;

use common\models\behavior\Id;
use common\models\behavior\Title;
use common\models\behavior\Sort;
use common\models\shipping\GeoValue;
use Yii;

/**
 * This is the model class for table "{{%shipping}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $sort
 * @property boolean $is_published
 *
 * @property GeoValue[] $shippingGeoValues
 */
class Shipping extends \yii\db\ActiveRecord
{
    use Id;
    use Title;
    use Sort;

    const SHIPPING_COURIER = 1;
    const SHIPPING_PICKUP = 2;
    const SHIPPING_POST = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shipping}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'sort', 'is_published'], 'required'],
            [['id', 'sort'], 'integer'],
            [['is_published'], 'boolean'],
            [['title'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sort' => 'Sort',
            'is_published' => 'Is Published',
        ];
    }

    /**
     * @return GeoValue
     */
    public function getShippingGeoValues()
    {
        return $this->hasMany(GeoValue::className(), ['shipping_id' => 'id']);
    }
}
