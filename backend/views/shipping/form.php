<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Geo;
use common\models\shipping\GeoValue;
use common\models\Shipping;
/* @var yii\web\View $this */
/* @var $geo Geo */

$this->title = 'Catalog Geo List';

$shippingValues = GeoValue::find()->where(['geo_id' => $geo->getId()])->orderBy('point_id')->all();
$geoValues = [
    Shipping::SHIPPING_COURIER => ['id'=> '', 'price' =>''],
    Shipping::SHIPPING_POST => ['id'=> '', 'price' =>''],
    Shipping::SHIPPING_PICKUP => [],
];
foreach($shippingValues as $value){
    switch($value->getShippingId()){
        case Shipping::SHIPPING_COURIER:
        case Shipping::SHIPPING_POST:
            $geoValues[$value->getShippingId()] = [
                'id' => $value->getId(),
                'price' => $value->getPrice(),
            ];
            break;
        case Shipping::SHIPPING_PICKUP:
            $geoValues[$value->getShippingId()][] = $value;
            break;
    }
}
?>
<div class="site-index row">
    <div class="col-sm-10">
        <h1><?= ($geo->getId() ? $geo->getTitle() : 'Новый') ?></h1>

        <?php
        $form = ActiveForm::begin(
            [
                'action' => ($geo->getId() ? Url::to(['edit', 'id'=>$geo->getId()]) : Url::to(['create'])),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);
        echo '<fieldset><legend>Город</legend>';
        echo $form->field($geo, 'title');
        echo $form->field($geo, 'title_where');
        echo $form->field($geo, 'sort');
        echo '</fieldset>';

        echo '<fieldset><legend>Курьер</legend>';
        echo '<div class="form-group field-courier-cost required">
                <div class="col-md-2"><label for="courier-cost" class="control-label">Стоимость</label></div>
                <div class="col-md-10"><input type="hidden" value="'.$geoValues[Shipping::SHIPPING_COURIER]['id'].'" name="Geo[courier][id]" class="form-control" id="courier-cost">
                <input type="text" value="'.$geoValues[Shipping::SHIPPING_COURIER]['price'].'" name="Geo[courier][price]" class="form-control" id="courier-cost"></div>
                <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div></div>';
        echo '</fieldset>';
        echo '<fieldset><legend>Почта России</legend>';
        echo '<div class="form-group field-post-cost required">
                <div class="col-md-2"><label for="post-cost" class="control-label">Стоимость</label></div>
                <div class="col-md-10"><input type="hidden" value="'.$geoValues[Shipping::SHIPPING_POST]['id'].'" name="Geo[post][id]" class="form-control" id="courier-cost">
                <input type="text" value="'.$geoValues[Shipping::SHIPPING_POST]['price'].'" name="Geo[post][price]" class="form-control" id="courier-cost"></div>
                <div class="col-md-offset-2 col-md-10"><div class="help-block"></div></div></div>';
        echo '</fieldset>';

        echo '<fieldset><legend>Самовывоз</legend>';
        echo '<table class="table">';
        echo '<tr><th>Адрес</th><th>№ Пункта</th><th>Стоимость</th><th>Lat</th><th>Log</th><th>Description</th><th>Включено</th></tr>';
        foreach($geoValues[Shipping::SHIPPING_PICKUP] as $value) {
            echo '<tr>';
            echo '<td><input type="text" value="'.$value->getAddress().'" name="Geo[pickup]['.$value->getId().'][address]" class="form-control"></td>';
            echo '<td><input type="text" value="'.$value->getPointId().'" name="Geo[pickup]['.$value->getId().'][point_id]" class="form-control"></td>';
            echo '<td><input type="text" value="'.$value->getPrice().'" name="Geo[pickup]['.$value->getId().'][price]" class="form-control"></td>';
            echo '<td><input type="text" value="'.$value->getLatitude().'" name="Geo[pickup]['.$value->getId().'][latitude]" class="form-control"></td>';
            echo '<td><input type="text" value="'.$value->getLongitude().'" name="Geo[pickup]['.$value->getId().'][longitude]" class="form-control"></td>';
            echo '<td><textarea name="Geo[pickup]['.$value->getId().'][description]" class="form-control">'.$value->getDescription().'</textarea></td>';
            echo '<td><input type="checkbox" value="1" name="Geo[pickup]['.$value->getId().'][is_active]" class="form-control" '.($value->isActive() ? 'checked' : '' ).'></td>';
            echo '</tr>';
        }
        echo '<tr>';
        echo '<td><input type="text" value="" name="Geo[pickup][address]" class="form-control"></td>';
        echo '<td><input type="text" value="" name="Geo[pickup][point_id]" class="form-control"></td>';
        echo '<td><input type="text" value="" name="Geo[pickup][price]" class="form-control"></td>';
        echo '<td><input type="text" value="" name="Geo[pickup][latitude]" class="form-control"></td>';
        echo '<td><input type="text" value="" name="Geo[pickup][longitude]" class="form-control"></td>';
        echo '<td><textarea name="Geo[pickup][description]" class="form-control"></textarea></td>';

        echo '</tr>';
        echo '</table>';
        echo '</fieldset>';

        echo '<fieldset><legend>&nbsp;</legend>';
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        echo '</fieldset>';
        ActiveForm::end();
        ?>
    </div>
</div>
