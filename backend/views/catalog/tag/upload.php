<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var yii\web\View $this */
?>
<div class="site-index row">
    <div class="col-sm-12">
        <?php
        if($countUpload > -1){
            echo '<div class="alert alert-success" role="alert">Загружено '.$countUpload.' тегов</div>';
        }
        $form = ActiveForm::begin(
            [
                'action' => Url::to(['upload']),
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-2\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
                ],
            ]);

        echo '<input type="file" name="upload" style="float: left; height: 30px;margin-top: 5px;margin-right: 10px;" />';

        echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        ActiveForm::end();
        ?>
    </div>
</div>
