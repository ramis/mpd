<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Login Container -->
<div id="login-container">
    <div id="login-logo">
        <a href="">
            <img src="/img/template/uadmin_logo.png" alt="logo">
        </a>
    </div>
    <!-- Login Form -->
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    {input}
                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                </div>
            </div>
        </div>',
        ]
    ]); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <div class="clearfix">
            <div class="btn-group btn-group-sm pull-right">
                <button type="button" id="login-button-pass" class="btn btn-warning" data-toggle="tooltip" title="Forgot pass?"><i class="fa fa-lock"></i></button>
                <?= Html::submitButton('<i class="fa fa-arrow-right"></i> Login', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
            </div>
            <label id="topt-fixed-header-top" class="switch switch-success pull-left" data-toggle="tooltip" title="Remember me"><input type="checkbox"><span></span></label>
        </div>
    <?php ActiveForm::end(); ?>
    <!-- END Login Form -->
</div>
<!-- END Login Container -->
