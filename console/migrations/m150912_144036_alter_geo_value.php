<?php

use yii\db\Schema;
use yii\db\Migration;

class m150912_144036_alter_geo_value extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%shipping_geo_value}}', 'description', 'address');
        $this->addColumn('{{%shipping_geo_value}}', 'description', Schema::TYPE_TEXT);
    }

    public function safeDown()
    {
    }
}
