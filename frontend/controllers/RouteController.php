<?php
namespace frontend\controllers;

use common\models\catalog\Category;
use common\models\catalog\Tag;
use common\models\Information;
use common\models\News;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Site controller
 */
class RouteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @param $url
     * @return string
     */
    public function actionIndex($url)
    {
        $category = Category::find()->where(['url' => $url])->one();
        if ($category) {
            $controller = Yii::$app->createController('category')[0];
            return $controller->actionIndex($category->getId());
        }

        $information = Information::find()->where(['url' => $url])->one();
        if ($information) {
            $controller = Yii::$app->createController('information')[0];
            return $controller->actionIndex($information->getId());
        }

        $news = News::find()->where(['url' => $url])->one();
        if ($news) {
            $controller = Yii::$app->createController('news')[0];
            return $controller->actionIndex($news->getId());
        }
        $tag = Tag::find()->where(['url' => $url])->one();
        if ($tag) {
            return Yii::$app->getResponse()->redirect(Url::to(['tags/tag', 'url' => $tag->getUrl()]), 301);
        }

        return $this->render('/site/error');
    }

}
