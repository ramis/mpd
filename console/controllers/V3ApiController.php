<?php
namespace console\controllers;

use common\models\catalog\Product;
use common\models\Order;
use common\models\order\State;
use common\tools\Api;
use Yii;

class V3ApiController extends AbstractController
{
    CONST V3TOYS_API_URL = 'http://www.v3toys.ru/pear-www/Kiwi_Shop/api.php';

    /**
     * Заказ - инфо по статусу
     */
    public function actionOrderState()
    {
        $orders = Order::find()->andFilterWhere(['>', 'updated_uts', strtotime("-1month")])->all();
        foreach ($orders as $order) {

            try {
                $response = Api::send('getOrderStatus', ['order_id' => $order->getId()]);

                if (!empty($response['data'])) {
                    $toStatusId = (int)$response['data']['status_id'];
                    $state = State::findOne($toStatusId);
                    if ($state) {
                        $order->saveState($state);
                    }
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        return self::EXIT_CODE_NORMAL;
    }

    /**
     * Товар - цены и количество
     */
    public function actionUpdateProductPriceApi()
    {
        $products = Product::find()->filterWhere(['>', 'code', '0'])->all();

        $productsIds = [];
        $products2Id = [];
        foreach ($products as $product) {
            $productsIds[] = $product->getCode();
            $products2Id[$product->getCode()] = $product;
        }

        try {
            $response = Api::send('getProductsDataByIds', ['in_stock' => 0, 'full' => 1, 'products_ids' => implode(',', $productsIds)]);
            if (!empty($response['data'])) {
                foreach ($response['data'] as $item) {
                    try {
                        $product = $products2Id[$item['id']];
                        /**
                         * @var Product $product
                         */
                        $quantity = (int)$item['quantity'];
                        $product->setQuantity(($quantity < 0 ? 0 : $quantity));
                        $product->setStock(($quantity > 0));
                        $product->setBuyPrice((int)$item['buy_price']);
                        $product->setBasePrice((int)$item['base_price']);
                        $product->setMrcPrice((int)$item['mr_price']);
                        $product->setV3tPrice((int)$item['price']);

                        if (!empty($item['barcode'])) {
                            $product->setBarcode((int)$item['barcode']);
                        }
                        if ($product->save() === false) {
                            throw new \Exception($product->getId() . ' ' . json_encode($product->getFirstErrors()));
                        };
                        $product->calcPrice();
                    } catch (\Exception $e) {
                        print $e->getMessage() . "\n";
                        continue;
                    }
                }
            }
        } catch (\Exception $e) {
            print $e->getMessage() . "\n";
        }

        return self::EXIT_CODE_NORMAL;
    }


}
