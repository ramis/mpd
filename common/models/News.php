<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $description
 * @property string $annotation
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $created_uts
 * @property integer $updated_uts
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'url', 'description', 'annotation', 'seo_title', 'seo_description', 'seo_keywords', 'created_uts', 'updated_uts'], 'required'],
            [['id', 'created_uts', 'updated_uts'], 'integer'],
            [['description'], 'string'],
            [['title', 'url', 'annotation', 'seo_title', 'seo_description', 'seo_keywords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Url'),
            'description' => Yii::t('app', 'Description'),
            'annotation' => Yii::t('app', 'Annotation'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'created_uts' => Yii::t('app', 'Created Uts'),
            'updated_uts' => Yii::t('app', 'Updated Uts'),
        ];
    }
}
