<?php
namespace backend\models;

use Yii;
use yii\base\Model;

class NullModel extends Model
{
    /**
     * @var string
     */
    public $text;

    /**
     * @var string
     */
    public $content;

    public function setText($text)
    {
        $this->text = $text;
    }
}