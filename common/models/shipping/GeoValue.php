<?php

namespace common\models\shipping;

use common\models\behavior\Address;
use common\models\behavior\Description;
use common\models\behavior\Id;
use common\models\behavior\Price;
use common\models\Geo;
use common\models\Shipping;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%shipping_geo_value}}".
 *
 * @property integer $id
 * @property integer $shipping_id
 * @property integer $geo_id
 * @property integer $price
 * @property string $address
 * @property string $description
 * @property integer $point_id
 * @property string $latitude
 * @property string $longitude
 * @property boolean $is_active
 *
 * @property Geo $geo
 * @property Shipping $shipping
 */
class GeoValue extends ActiveRecord
{
    use Id;
    use Description;
    use Address;
    use Price;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shipping_geo_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipping_id', 'geo_id', 'price'], 'required'],
            [['shipping_id', 'geo_id', 'price', 'point_id'], 'integer'],
            [['latitude', 'longitude'], 'string'],
            [['address'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['is_active'], 'boolean'],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shipping_id' => 'Shipping ID',
            'geo_id' => 'Geo ID',
            'price' => 'Price',
            'address' => 'Address',
            'description' => 'Description',
            'point_id' => 'Point ID',
        ];
    }

    /**
     * @return Geo
     */
    public function getGeo()
    {
        return Geo::find()->where(['id' => $this->geo_id])->one();
    }

    /**
     * @return Shipping
     */
    public function getShipping()
    {
        return Shipping::find()->where(['id' => $this->shipping_id])->one();
    }

    /**
     * @param Geo $geo
     */
    public function setGeo(Geo $geo)
    {
        $this->geo_id = $geo->getId();
    }

    /**
     * @param Shipping $shipping
     */
    public function setShipping(Shipping $shipping)
    {
        $this->shipping_id = $shipping->getId();
    }

    /**
     * @return int
     */
    public function getShippingId()
    {
        return $this->shipping_id;
    }

    /**
     * @return int
     */
    public function getPointId()
    {
        return $this->point_id;
    }
    /**
     * @param int $point_id
     */
    public function setPointId($point_id)
    {
        $this->point_id = $point_id;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @param bool $isActive
     * @return $this
     */
    public function setActive($isActive)
    {
        $this->is_active = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }
}
